function reloadProducts(page, sort) {
	var formId = '#filter-form';
	var productsFieldId = '#products-list-and-paginator';
	
	var loadingImage = $('#filter-loading').clone();
	var ss;
	
	if (page == undefined) 
		page = (defaultPage == undefined) ? 1 : defaultPage;
	if (sort == undefined) {
		sort = (defaultSort == undefined) ? 'none' : defaultSort;
	}
	$(formId).ajaxSubmit({
		data : {"page" : page, "sort" : sort},
		beforeSend : function () {
			$(productsFieldId).css('opacity', .3);
			//show loading
			loadingImage.appendTo('body');
			loadingImage.css({
				'display' : 'block',
				'position' : 'absolute',
				'left' : $(productsFieldId).position().left + 20,
				'top' : $(productsFieldId).position().top
			});
			ss = $('body, html').mousemove( function (e) {
				loadingImage.css({
					'left' : (e.pageX + 10) + 'px',
					'top' : (e.pageY + 20) + 'px'
				});
			});
		},
		success : function ( data, statusText, xhr, element) {
				$(productsFieldId).empty().append($('<ul>').addClass('catalogue')).find('.catalogue').append(data);
				
				var paginDiv = $(productsFieldId).find('.navigation').clone();
				$(productsFieldId).find('.navigation').remove();
				
				$('#paginator-container').html(paginDiv);
				
				$(productsFieldId).css('opacity', 1);
				
				//Hide loading
				ss.unbind('mousemove');
				loadingImage.remove();
				
				ajaxMode = true;
				
				//������������ ������� �� ����������
				$('#sort-filter').unbind('change').bind('change', function() {
					var sort = $(this, ':checked').val();
					reloadProducts(page, sort);
					defaultSort = sort;
					return false;
				});
		}
	});
	return false;
}