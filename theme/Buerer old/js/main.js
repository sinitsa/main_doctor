var price = { 'min':0, 'max':0 };

function add_to_bskt(){

	if(!$(this).is('.active')){
			$(this).addClass('active');
			$(this).text('� �������');
			
			addProductToBasket($(this).data('product_id'), $(this).data('product_price'));
			return false;
	};

}
function isValidEmail(emailAddress) {
    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    return pattern.test(emailAddress);
}

function openMap(){
	$.ajax({
		url: '/ajax/getmap.php', 
		type: 'POST',
		dataType: 'html',
		success: function(data){
			$(".Map .all").html(data);
			$('.Map').css({width:'842px', position: 'fixed', left: '50%', margin: '0', top: '50px', "margin-left": "-421px" });
   			$('.overlay, .Map').show();
    		return false;
		} 
	})
}

$(document).ready(function(){
	$(".text-initem-title").each(function(id, elem){
		if($(elem).text() == "������:"){
			$(elem).attr({id: "mo"});
		}else if($(elem).text() == "�����-���������:"){
			$(elem).attr({id:"sp"});
		}
	});
	// Tips
	$('ul.tips img').hover(function(){
		var text = $(this).attr('alt');
		$(this).after('<div class="tipsPopup">' + text + '</div>');
		$(this).siblings('.tipsPopup').animate({
			'opacity': 1,
			'bottom': '-=12'
		},200);
	},function(){
		$(this).siblings('.tipsPopup').remove();
	});
	
	// Buy link
	$('a.buyLink').click(function(){
		if(!$(this).is('.active')){
			$(this).addClass('active');
			$(this).text('� �������');
			 yaCounter21819568.reachGoal('BUY');
			addProductToBasket($(this).data('product_id'), $(this).data('product_price'));
			return false;
		};
	});
	
	// Intro
	$div = null;
	$('.thumbs').children().each(function(i){
		if (i % 8 == 0) {
			$div = $('<div />');
			$div.appendTo('.thumbs');
		};
		$(this).appendTo($div);
		$(this).addClass('itm'+i);
		$(this).click(function() {
			$('ul.productsSlider').trigger('slideTo', [i, 0, true]);
		});
	});
	$('.thumbs span.itm0').addClass('selected');
	
	$('ul.productsSlider').after('<div class="arrow prev" /><div class="arrow next" />').carouFredSel({
		circular: false,
		infinite: false,
		width: 760,
		height: 443,
		items: 1,
		auto: false,
		prev: '.intro .prev',
		next: '.intro .next',
		scroll: {
			fx: 'directscroll',
			onBefore: function() {
				var pos = $(this).triggerHandler('currentPosition');
				$('.thumbs span').removeClass('selected');
				$('.thumbs span.itm'+pos).addClass('selected');
				var page = Math.floor(pos / 8);
				$('.thumbs').trigger('slideToPage', page);
			}
		}
	});
	
	$('.thumbs').after('<div class="pager" />').carouFredSel({
		circular: false,
		infinite: false,
		width: 182,
		height: 364,
		items: 1,
		align: false,
		auto: false,
		pagination: '.intro .pager'
	});
	
	// Custom select
	$('.sort select').selectbox({
		speed: 0,
		onOpen: function(){
			$(this).next().addClass('focus');
			console.log(1);
		},
		onChange: function(value){
			$('.sort select').val(value);
			$('.sort select').trigger("change");
		},
		onClose: function(){
			$(this).next().removeClass('focus');
		}
	});

	$('.deliveryMethod select').selectbox({
		speed: 0,
		onOpen: function(){
			$(this).next().addClass('focus');
			console.log(1);
		},
		onChange: function(value){
			$("select[name=delivery_type]").val(value);
			$("select[name=delivery_type]").trigger("change");
		},
		onClose: function(){
			$(this).next().removeClass('focus');
		}
	});
	
	$('#sort-filter').bind('change', function() {
		var uri = $(this).find('option:checked').data('uri');
		window.location = uri;
	});
	

	// Sliders
	// Price slider
	$('#price-slider').slider({
		range: true,
		min: price.min,
		max: price.max,
		//step: 500,
		values: [price.min, price.max],
		slide : function(event, ui){
			$(".price-min").val(ui.values[0]);
			$(".price-max").val(ui.values[1]);
		},
		change : function() {
			defaultSort = 'price_asc';
			reloadProducts();
		}
	});
	$('#price-slider').find('.ui-slider-range').append('<input type="text" class="price-min" disabled /><input type="text" class="price-max" disabled />');
	$('.price-min').val($('#price-slider').slider('values', 0));
	$('.price-max').val($('#price-slider').slider('values', 1));
	
	//Range slider
	$('.range-slider').each(function(index, element) {
		var _this = $(element);
		var sliderBody = _this.find('.slider-body');
		
		var dataField = _this.find('.slider-field');
		var minValue = dataField.data('min');
		var maxValue = dataField.data('max');
		var paramId = dataField.data('param_id');
		
		sliderBody.slider({
			range: true,
			min: minValue,
			max: maxValue,
			step: 1,
			values: [minValue, maxValue],
			slide : function(event, ui){
				sliderBody.find(".weight-min").val(ui.values[0]);
				sliderBody.find(".weight-max").val(ui.values[1]);

				_this.find(".weight-min").val(ui.values[0]);
				_this.find(".weight-max").val(ui.values[1]);
			},
			change : function(event, ui) {
				dataField.empty();
				dataField.append(
					$('<input>').attr({
						"type" : "hidden",
						"name" : "filter_range[" + paramId + "][min]",
						"value" : ui.values[0]
					})
				);
				dataField.append(
					$('<input>').attr({
						"type" : "hidden",
						"name" : "filter_range[" + paramId + "][max]",
						"value" : ui.values[1]
					})
				);
				reloadProducts();
			}
		});
		sliderBody.find('.ui-slider-range').append('<input type="text" class="weight-min" disabled /><input type="text" class="weight-max" disabled />');
		sliderBody.find('.weight-min').val(sliderBody.slider('values', 0));
		sliderBody.find('.weight-max').val(sliderBody.slider('values', 1));
	});
	
	$('.filters-field .checkbox-param').bind('click', function(){
		var _this = $(this);
		
		if (_this.hasClass('active')) {
			_this.removeClass('active');
			_this.find('input[type="checkbox"]').removeAttr('checked');
		} else {
			_this.addClass('active');
			_this.find('input[type="checkbox"]').attr('checked', 'checked');
		}
		reloadProducts();
	});
	
	// On change inputs
	$('.range-min').change(function(){
		var wrapper = $(this).closest('.range-slider');
		var sliderBody = wrapper.find('.slider-body');
		
		var newValue = $(this).val();
		var min = sliderBody.slider('option', 'min');
		var max = sliderBody.slider('option', 'max');
		var values = sliderBody.slider('option', 'values');
		if(newValue <= values[1]){
			if(newValue >= min){
				sliderBody.slider('option', 'values', [ newValue, values[1] ]);
				wrapper.find('.weight-min').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ min, values[1] ]);
				wrapper.find('.weight-min').val(min);
			}
		}else{
			if(newValue <= max){
				sliderBody.slider('option', 'values', [ newValue, newValue ]);
				wrapper.find('.weight-min').val(newValue);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ max, max ]);
				wrapper.find('.weight-min').val(max);
				wrapper.find('.weight-max').val(max);
			}
		}
	});
	$('.range-max').change(function(){
		var wrapper = $(this).closest('.range-slider');
		var sliderBody = wrapper.find('.slider-body');
		
		var newValue = $(this).val();
		var min = sliderBody.slider('option', 'min');
		var max = sliderBody.slider('option', 'max');
		var values = sliderBody.slider('option', 'values');
		if(newValue >= values[0]){
			if(newValue <= max){
				sliderBody.slider('option', 'values', [ values[0], newValue ]);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ values[0], max ]);
				wrapper.find('.weight-max').val(max);
			}
		}else{
			if(newValue >= min){
				sliderBody.slider('option', 'values', [ newValue, newValue ]);
				wrapper.find('.weight-min').val(newValue);
				wrapper.find('.weight-max').val(newValue);
			}else{
				sliderBody.slider('option', 'values', [ min, min ]);
				wrapper.find('.weight-min').val(min);
				wrapper.find('.weight-max').val(min);
			}
		}
	});
	
	// Filter
	function filterToggle () {
		if($('.filter').is('.open')){
			$('.filterToggle').find('span').text('������ ����������');
			$('.filter .hidden').slideUp();
			$('.filter').removeClass('open');
		}else{
			$('.filterToggle').find('span').text('������ ���������');
			$('.filter .hidden').slideDown();
			$('.filter').addClass('open');
		}
	}
	$('.filterToggle').click(function(){
		filterToggle();
	});
	
	if ($('.filter').is('.open')) {
		$('.filter').removeClass('open');
		filterToggle();
	}
	
	// Catalogue description text
	var timer = null;
	$('ul.catalogue>li').live({
        mouseenter: function() {
			var _this = $(this);
			if(_this.position().left >= 816){
				$(this).addClass('leftDescription');
			};
			timer = setTimeout(function(){
				_this.addClass('active');
			},500);
        },
        mouseleave: function() {
			clearTimeout(timer);
			$(this).removeClass('active');
           }
       }
    );
	/*
	$('ul.catalogue>li').hover(function(){
		var _this = $(this);
		if(_this.position().left >= 816){
			$(this).addClass('leftDescription');
		};
		timer = setTimeout(function(){
			_this.addClass('active');
		},500);
	},function(){
		clearTimeout(timer);
		$(this).removeClass('active');
	});
	*/
	// Product photo thumbs carousel
	$('.mPhotoThumbs ul').after('<div class="mPhotoThumbsNav"><div class="arrow prev" /><div class="pager" /><div class="arrow next" /></div>').carouFredSel({
		items: 4,
		circular: false,
		infinite: false,
		auto: false,
		prev: '.mPhotoThumbs .prev',
		next: '.mPhotoThumbs .next',
		pagination: '.mPhotoThumbs .pager',
		scroll:{
			items: 4,
			duration: 1000
		}
	});
	$('.mPhotoThumbs ul li:first-child').addClass('current').append('<span class="mask" />');
	$('.mPhotoThumbs ul a').click(function(){
		//if(!$(this).parent('li').is('.current')){
			var curImg = $('<img src="' + $(this).attr("href") + '" alt="' + $(this).attr("alt") + '" style="display:none;">');
			$(this).parents('.mPhotoThumbs ul').find('li.current').removeClass('current').find('.mask').remove();
			$(this).parent('li').addClass('current').append('<span class="mask" />');
			var links = new Array();
			$(this).closest('li').siblings().each(function (index, el) {
				links.push( $(el).find('a').data('original') );
			});
			var a = $('<a>').attr('href', $(this).data('original')).attr('rel', 'fancygroup').addClass('fgallery');
			a.append(curImg);
			
			var container = $(this).parents('.photos').find('.mPhoto');
			
			container.empty();
			container.append(a);
			container.append('<div class="loader" />');
			
			curImg.load(function(){
				$(this).fadeIn(200);
				//$(this).siblings('img').remove();
				//$(this).siblings('.loader').remove();
				$(this).closest('a').siblings().remove();
				
				for (var i in links) {
					container.append(
						$('<a>').attr('href', links[i]).attr('rel', 'fancygroup').addClass('fgallery').css('display','none')
					);
				}
				$("a.fgallery").fancybox();
                                var itemTitle = $('.info h1').html();
                                $("a.fgallery img").attr("alt", itemTitle);
			});
		//};
		return false;
	});
	$("a.fgallery").fancybox();

        
	$('.mPhotoThumbs ul a:first').trigger('click');
	// Tabs
	$('#tabs').tabs();
	
	$('.overlay, .popup .close').click(function(){
		if ( $('.confirmation .close').hasClass('redirectToCatalogue') ){
			$('.confirmation .close').removeClass('redirectToCatalogue');
			$('.confirmation .catalogueLink')[0].click();
		}else
			$('.overlay, .popup').hide();
	});
	
	// Order
	$('.order a.doneBtn').click(function(){
		if(!$(this).is('.disabled')){
			$(this).addClass('disabled');
			$('.order .orderForm').slideDown();
		}
		return false;
	});
	
	//Order delete button
	$('#cart-items .basket-item .delete a').click(function(){
		$('.confirmation-delete').css({
			'top': $(document).scrollTop() + $(window).height()/2 - $('.confirmation').outerHeight()/2
		});
		var basketItem = $(this).closest('.basket-item');
		
		var win = $('.confirmation-delete');
		
		win.find('.name')
			.attr('href', basketItem.find('.name a').attr('href') )
			.text(basketItem.find('.name a').text());
			
		win.find('.price span').text( moneyFormat( basketItem.find('input[name="price"]').val() ) );
		
		//������ ���������� �� ������� �������
		
		win.find('.catalogueLink').unbind('click').bind('click', function(){
			basketItem.remove();
			win.find('.close').trigger('click');
			recalculateBasket();
			if ($('#cart-items .basket-item').length > 0) {
				yaCounter21819568.reachGoal('BASKET_NOT_EMPTY');
			}else{
				yaCounter21819568.reachGoal('BASKET_EMPTY');
			}
			});
		
		$('.overlay, .confirmation-delete').show();
		return false;
	});
	
	//add feedback
	$('.add-feed-button a').click(function(){
		
		$('#feed-back-form input[name="product_id"]').val( $('.add-feed-button').data('product_id') );
		
		$('.feed-back-popup').css({
			'top': $(document).scrollTop() + $(window).height()/2 - $('.quickBuy').outerHeight()/2
			});
		$('.overlay, .feed-back-popup').show();
		
		var win = $('.feed-back-popup');
		var form = $('#feed-back-form');
		var loading = win.find('.loading');
		
		form.show();
		win.find('.success').hide();
		win.find('.send-button').show();
		
		win.find('.send-button').unbind('click').bind('click', function(){
			loading.show();
			
			
			//var formData = form.serialize();
			
			$.ajax({ 
				url : '/ajax/system.php',
				data : form.serialize(),
				type : "post",
				dataType : "json",
				beforeSend : function () {
					form.find('input, textarea').attr('disabled', 'disabled');
					win.find('.error').hide();
				},
				success: function(data) {
					if (!data.error) {
						//win.find('.close').trigger('click');
					
						form.find('input, textarea').each(function(index, el){
							$(el).val(el.defaultValue);
						});
						form.hide();
						win.find('.success').show();
						win.find('.send-button').hide();
					} else {
						win.find('.error-code-' + data.error_code).show();
					}
				},
				complete : function() {
					loading.hide();
					form.find('input, textarea').removeAttr('disabled');
				}
			});
		});
		
		return false;
	});
	
	
	// Placeholder
	if(!Modernizr.input.placeholder){
		if($('input, textarea').attr('placeholder')){
			$('input, textarea').each(function(){
				$(this).focus(function(){
					if($(this).val() == $(this).attr('placeholder')){
						$(this).val("");
					}
				});
				$(this).blur(function(){
					if($(this).val() == ""){
						$(this).val($(this).attr('placeholder'));
					}
				});
				$(this).blur();
			});
		}
	};

	// Product img on click redirect to product page
	$(".cc_4").click(function(){
		//var a = $(this).parent().parent();
		//var h = a.data("href");
		//a.attr("href",h);
		console.log($(this).data("href"));
		//a.trigger("click");
	});
	
});