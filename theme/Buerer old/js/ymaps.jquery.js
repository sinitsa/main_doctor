ymaps.ready(function(){
	$(function(){
		if( $('#delZoom').length!=0 && $('#delCenter').length!=0 && $('#delAdr').length!=0 ){
			init( $('#delCenter').val(), $('#delZoom').val(), $('#delAdr').val() );
		}else{
			init();
		}
	});
	
	var myMap;var coords;var myCollection;var adressArr=[];

	function init(center, zoom, adresses){
		if ( !myMap ){
			if(center!=undefined){
				var centerArr = center.split(',');
			}else{
				var centerArr = $('.mapBtn').eq(0).data('map_center').split(',');
			}
			adressArr = [];
			if(adresses!=undefined){
				var adr = adresses;
			}else{
				var adr = $('.mapBtn').eq(0).data('map_adress');
			}
			while(adr.indexOf('&nbsp;')!=-1){adr = adr.replace('&nbsp','');}
			adressArr = adr.split('&');
			
			if(zoom!=undefined){
				var mapZoom = zoom;
			}else{
				var mapZoom = $('.mapBtn').eq(0).data('map_zoom');
			}
			
			myMap = new ymaps.Map ("myMap", {
				center: [0,0],
				zoom: 7
			});
			myMap.controls.add(
				new ymaps.control.ZoomControl()
			);
			myCollection = new ymaps.GeoObjectCollection();
			getMap(adressArr);
			myMap.setCenter(centerArr);
			myMap.setZoom(mapZoom);
		}
		if(location.pathname=='/page/punkty-samovyvoza.html'){
			$(".ymaps-map.ymaps-i-ua_js_yes").css({"width": "590px", "height": "380px", "position": "relative", "top": "-2px","left": "6px","border-radius": "15px"});
		}
	}

	function getMap(a){
		myCollection.removeAll();
		myCollection = new ymaps.GeoObjectCollection();
		$(adressArr).each(function(i,e){
			var color;
			if(e.indexOf('#')==-1){
				color = 'blueIcon';
			}else{
				color = e.substr(e.indexOf('#')+1)+'Icon';
				e = e.substr(0,e.indexOf('#'));
			}
			var myGeocoder = ymaps.geocode(adressArr[i]);
			color = 'islands#'+color;
			console.log(color);
			myGeocoder.then(
				function (res) {
					coords = res.geoObjects.get(0).geometry.getCoordinates();
					var myGeocoder = ymaps.geocode(coords, {kind: 'house'});
					myCollection.add(new ymaps.Placemark(coords,{
							balloonContent:e
						},{
							hideIconOnBalloonOpen: true,
							preset: color
							// iconColor: 'red'
						}
					));
			},function(err){alert(arr)});
		});
		myMap.geoObjects.add(myCollection);
	}
});

	// function getMap(a){
		// var arr = a;
		// adressArr = a;
		// myCollection.removeAll();
		// myCollection = new ymaps.GeoObjectCollection();
		// $(arr).each(function(i,e){
			// myCollection.add(new ymaps.Placemark([e[1],e[0]],{
					// balloonContent:'dsadsa'
				// },{
					// hideIconOnBalloonOpen: true,
					// // iconColor: e.col
				// }
			// ));
		// });
		// myMap.geoObjects.add(myCollection);
	// }
	
	$('.mapBtn').on('click',function(){
		var centerArr = $(this).data('map_center').split(',');
		myMap.setCenter(centerArr);
		myMap.setZoom($(this).data('map_zoom'));
		adressArr = $(this).data('map_adress').split('&');
		getMap( $(this).data('map_adress'));
	 });