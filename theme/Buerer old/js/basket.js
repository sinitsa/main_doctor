function setCookie(name, value, expires, path, domain, secure) {
	var exdate = new Date();
	exdate.setDate(exdate.getDate() + 30);

	document.cookie = name + "=" + escape(value) +
		((expires) ? "; expires=" + exdate.toUTCString() : "; expires=" + exdate.toUTCString()) +
		((path) ? "; path=" + path : "") +
		((domain) ? "; domain=" + domain : "") +
		((secure) ? "; secure" : "");
}
function getCookie(c_name) {
	if (document.cookie.length > 0 ) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length+1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function parseProductsString( string ) {
	var returnArray = new Array();
	
	if (string.length > 0 ) {
		var products = string.split('|');
		for (var i in products) {
			var temp = products[i].split(':');
			
			returnArray.push({
				'id' : parseInt(temp[0]),
				'amount' : parseInt(temp[1])
			});
		}
	}
	return returnArray;
}
function generateProductsString( products ) {
	var tempArray = new Array();
	for (var i in products) {
		tempArray.push(products[i].id + ':' +  products[i].amount);
	}
	return tempArray.join('|');
}
function addProductToBasket(productId, price) {
	//������� ������ �� ���
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount += price;
	} else {
		totalAmount = price;
	}
	
	//���� ����� ��� ���� � �����, ��������� ��� ����������
	//������ ���������� ���������� ���� �������
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount++;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		products.push({
				"id" : productId,
				"amount" : 1
			});
		productsCount += 1;
	}
	
 
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//�������� ����������
	refreshBasketInfo();
}
function deleteProductFromBasket(productId, price) {
	//������� ������ �� ���
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount -= price;
	} else {
		totalAmount = 0;
	}
	
	//���� ����� ��� ���� � �����, �������� ��� ����������
	//������ ���������� ���������� ���� �������
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount--;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		//do nothing
	}
	
	
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//�������� ����������
	refreshBasketInfo();
}

function setBasketInfo(amount , summ) {
	//�������� �������������
	refreshBasketInfo();
}
function refreshBasketInfo() {
	//������� ������ �� ���
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (isNaN(totalAmount)) {
		totalAmount = 0;
	}
	var productsCount = 0;
	for (var i in products) {
		productsCount += products[i].amount;
	}
	
	$('#basket-summ-products').text(productsCount);
	$('#basket-summ-price').text(moneyFormat(totalAmount));
	$('#basket-summ-products-text').text(declOfNum(productsCount, ['�����', '������', '�������']));
}

function recalculateBasket() {
	var productsCount = 0;
	var priceSumm = 0;
	var products = new Array();
	
	var _break = false;
	//��������� ��������� ������� �� ���������� �����
	$('.amount-input').attr('disabled', 'disabled');
	
	$('#cart-items .basket-item').each(function (index, element) {
		if (_break == false) {
			var amount = $(element).find('input[name="amount"]').val();
			var price = $(element).find('input[name="price"]').val();
			var pid = $(element).find('input[name="id"]').val();
			
			if (amount == '0' || amount == '') {
				$(element).find('input[name="amount"]').val(1);
				amount = '1';
			}
			
			if (amount.match(/^[0-9]{1,3}$/)) {
				amount = parseInt(amount);
				productsCount += amount;
				priceSumm += parseInt(price) * amount;
				//
				$(element).find('.sum span').text(moneyFormat(parseInt(price) * amount));
				//
				products.push({
					"id" : pid,
					"amount" : amount
				});
			} else {
				productsCount = '-';
				priceSumm = '-';
				_break = true;
			}
		}
	});

	//���� ������ �� ����
	if (_break == false) {
		//���������� ����
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', priceSumm, '', '/');
		
		//�������� ������ � ����� �������
		setBasketInfo(productsCount, priceSumm);
		
		var coverField = $('#cart-items');
		var cover = $('<div></div>').css({
			'position' : 'absolute',
			'left' : coverField.offset().left,
			'top' : coverField.offset().top,
			'width' : coverField.width(),
			'height' : coverField.height(),
			'zIndex' : 100,
			'background' : '#fff',
			'opacity' : .6
		  }).html('<div style="text-align:center;">����������� ������...</div>').appendTo('body');
		
		//������ �� ��������� ��������� � ������ ������
		var qiwiTerm = $('#qiwiTerminal').val();
		$.ajax({
			url: '/ajax/basket.php?method=get_delivery_and_discount',
			type : 'POST',
			dataType : 'json',
			data : {"products" : products,'qiwiTerminal':qiwiTerm},
			beforeSend : function () {
				$('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
			},
			success : function (data, textStatus, jqXHR) {
				
				orderPrice.delivery = intVal(data.deliveryPrice);
				orderPrice.discount = intVal(data.discount);
				orderPrice.summ = intVal(priceSumm);
				showPriceInfo();
				
				$('#qiwiSize').val(data.qiwiSize);
				
				if(data.qiwiSize=='none' && $('select[name="delivery_type"]').val()==7){
					$('#overall .qiwi').empty().html('<th>��������:</th><td class="num">��������� � ����������</td><td></td>');
				}else{
					$('#overall .qiwi').empty().html('<th>��������:</th><td class="num">'+data.qiwiDelivery+'</td><td> ���.</td>');
					// var b = $("#totalItemPrice").show();
					// var a = del_spaces(b.text())*1 + data.qiwiDelivery*1;
					// $("#total-summ").show().find(".num").text(moneyFormat(a));
				}
				
				//�������� ������ �� �������� �������
				$('#totalItemPrice').html(moneyFormat(priceSumm));
				var orderInfo = $('#overall');
				var deliveryDiv = orderInfo.find('.qiwi');
				if(deliveryDiv.find('.num').text()!='' && !isNaN(parseInt(deliveryDiv.find('.num').text()))){
					var b = $("#totalItemPrice").show();
					var a = parseInt(b.text().replace(' ','')) +parseInt(deliveryDiv.find('.num').text());
					$("#total-summ").show().find(".num").text(moneyFormat(a));
				}
				$('#overall .qiwi').show();
				$('#cartDiscountPersent').html(orderPrice.discount);
				if (orderPrice.discount > 0) {
					$('#discount-field').show();
				} else {
					if( $('select[name="delivery_type"]').val() !=7 ){
						$('#discount-field').hide();
					}
				}
				
				$('#basket-products-count').html(productsCount);
	
			},
			complete : function () {
				cover.remove();
				//������������ ������ ��� �����
				$('.amount-input').removeAttr('disabled');
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
		
	}
	if (_break == false && productsCount > 0) {
		$('#continue-order').removeAttr('disabled');
	} else {
		$('#continue-order').attr('disabled','disabled');
		$('.amount-input').removeAttr('disabled');
	}
}

function showPriceInfo() {
	var orderInfo = $('#overall');
	var discount = $('#discount-field');
	var summ = $('#total-summ');
	var delivery = $('.delivery');
	deliveryType = $('select[name="delivery_type"]').val();

	var total = $('#total-summ');

	
	var price = orderPrice.summ;
	//��������� �� ���������
	delivery.hide();
	
	//���� � ������
	if (orderPrice.discount > 0) {
		discount.find('.num').text(orderPrice.discount);
		discount.show();
		price = Math.round(price * ((100 - orderPrice.discount) / 100));
	} else {
		discount.hide();
	}
	
	//���� ����� �����
	summ.find('.num').text(moneyFormat(price));

	switch (deliveryType) {
		case 1 :
		case '1':
		case 2:
		case '2':
			var msk = false;
			if (deliveryType == 1 || deliveryType == '1') {
				//�� ������
				var deliveryDiv = orderInfo.find('.msk');
				msk = true;
			} else {
				//�� ����
				var deliveryDiv = orderInfo.find('.msk-region');
			}

			//������� �������� �������� �� ������
			if (orderPrice.delivery === false) {
				orderInfo.find('.individual').show();
			} else if (orderPrice.delivery === 0) {
				if (msk) {
					deliveryDiv.find('.num').text('���������');
					deliveryDiv.find('.unit').hide();
				} else {
					deliveryDiv.find('.value').text('');
					deliveryDiv.find('.section').hide();
				}
				total.find('.num').text(moneyFormat(price));
			} else {
				if (msk) {
					deliveryDiv.find('.num').text(orderPrice.delivery);
					deliveryDiv.find('.unit').show();
					total.find('.num').text(moneyFormat(parseInt(price) + parseInt(orderPrice.delivery))).show();
				} else {
					deliveryDiv.find('.value').html(orderPrice.delivery);
					deliveryDiv.find('.section').show();
					total.hide();
				}
				
				
			}
			deliveryDiv.show();
			//delivery.show();
		break;
		case 5:
		case '5':
			//���������
			// delivery.hide();
			if (parseFloat(price)>1000){
				var pickup = 0;
				var pickupText = '���������';
				var pickupText2 = '';
			}else{
				var pickup = 250;
				var pickupText = '250 ';
				var pickupText2 = '���.';
			}
			
			total.find('.num').text(moneyFormat(parseInt(price) + pickup)).show();
			orderInfo.find('.pickup .num').text(pickupText);
			orderInfo.find('.pickup .unit').text(pickupText2);
			orderInfo.find('.pickup').show();
			total.show();
		break;
                case 7:
                case '7':
                   //qiwi post
                    // discount.hide();
                    // // $(".colRight input[name='post_index']").val('');
                    // // var address = $("#qiwi .sbHolder .sbSelector").html();
                    // // var temp = address.split(',');
                    // // $(".colLeft input[name='adress']").val(address);
                    // $.ajax({
                            // url: '/ajax/basket.php?method=get_delivery_price_by_terminal',
                            // type : 'POST',
                            // dataType : 'json',
                            // data : {"terminal_id" :temp[0]},
                            // beforeSend : function () {
                                    // $('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
                            // },
                            // success : function (data, textStatus, jqXHR) {
							
                                    // var orderInfo = $('#overall');
                                    // var deliveryDiv = orderInfo.find('.qiwi');
									// if(deliveryDiv.find('.num').text()!='' && deliveryDiv.find('.num').text()!='none'){
										// var b = $("#totalItemPrice").show();
										// var a = parseInt(b.text().replace(' ','')) +parseInt(deliveryDiv.find('.num').text());
										// $("#total-summ").show().find(".num").text(moneyFormat(a));
									// }
									
                                    // var totalDiv = $('#total-summ');
                                    // var price = $('#totalItemPrice').html();
                                    // //������� ������� �� ����
                                    // var new_price = '';
                                    // for (var i = 0; i < price.length; ++i) {
                                        // if (price.charAt(i) !== ' ') 
                                            // new_price += price.charAt(i);
                                    // }
                                    // //����� ������ � ���������
                                    // var total_sum = Number(new_price) + Number(data.delivery);
                                    // //����������� ������ � ����
                                    // deliveryDiv.find('.num').html(data.delivery);
                                    // totalDiv.find('.num').html(total_sum);
                                    // totalDiv.show();
									
                                    // deliveryDiv.show();
                            // },
                            // complete : function () {
                            // },
                            // error : function (jqXHR, textStatus, errorThrown) {}
                    // });                    
                break;
		default : 
			var deliveryDiv = orderInfo.find('.individual');
			//delivery.show();
			deliveryDiv.show();
			total.hide();
	}
}



//���������� ��� ��������� ��������� �������� qiwi
function changeSelector(value) {

    var temp = value.split(';');
    $("#qiwi .sbHolder .sbSelector").html(temp[0] + ', ' + 
                                          temp[1] + ' ' + 
                                          temp[2] + ' ' +
                                          temp[3]);
    var address = $("#qiwi .sbHolder .sbSelector").html();
    $(".colLeft input[name='adress']").val(address);
    //�������� ������
    $("#discount-field").hide();
    $.ajax({
            url: '/ajax/basket.php?method=get_delivery_price_by_terminal',
            type : 'POST',
            dataType : 'json',
            data : {"terminal_id" : temp[0]},
            beforeSend : function () {
                    $('#delivery-1 .value, #delivery-2 .value').html('<img src="/img/ajax_loading.gif" />');
            },
            success : function (data, textStatus, jqXHR) {
                    var orderInfo = $('#overall');
                    var deliveryDiv = orderInfo.find('.qiwi');
                    var totalDiv = $('#total-summ');
                    var price = $('#totalItemPrice').html();
                    //������� ������� �� ����
                    var new_price = '';
                    for (var i = 0; i < price.length; ++i) {
                        if (price.charAt(i) !== ' ') 
                            new_price += price.charAt(i);
                    }
                    //����� ������ � ���������
                    var total_sum = Number(new_price) + Number(data.delivery);
                    //����������� ������ � ����
                    deliveryDiv.find('.num').html(data.delivery);
                    totalDiv.find('.num').html(total_sum);
                    totalDiv.show();
                    deliveryDiv.show();
            },
            complete : function () {
            },
            error : function (jqXHR, textStatus, errorThrown) {}
    });
}

var deliveryType;
$(function(){
        //����� ������
        if (deliveryType == 3){
            $(".colRight input[name='post_index']").show();
        }
        else $(".colRight input[name='post_index']").hide();

        //QIWI Post
        if (deliveryType == 7) { 
            $("#discount-field").hide();
            $(".colLeft input[name='adress']").hide();
            $("#qiwi").show();
        }     
        else {
            $(".colLeft input[name='adress']").show();
            $(".colLeft input[name='adress']").val('');
            $("#qiwi").hide();
        }        
	//�������� ��������� ���������� ������� � �������
	var recalcTimer;
	$('.amount-input').bind('keyup', function (e) {
		var code = (e.keyCode ? e.keyCode : e.which);

		/*
		LEFT=   37
		UP=     38
		RIGHT=  39
		DOWN=   40
		SHIFT=  16
		CONTROL=    17
		ALT=    18
		*/
		var keys = [16,17,18,37,38,39,40];
		//�� ����������� �� ������� ������, ����������� ����
		if (recalcTimer != undefined) clearInterval(recalcTimer);
		if (!(keys.indexOf(code) >= 0)) { 
			recalcTimer = setTimeout('recalculateBasket();', 1000);
		}
	});
	deliveryType = $('#order-form select[name="delivery_type"]').val();
	
	$('select[name="delivery_type"]').on('click change', function(){
		console.log(1);
		deliveryType = $(this,':checked').val();
        if (deliveryType == 3) {
			$(".colRight input[name='post_index']").show();
		}
		else $(".colRight input[name='post_index']").hide();

        if (deliveryType == 7) { 
            $("#discount-field").hide();
            $(".colLeft input[name='adress']").hide();
            $("#qiwi").show();
        }     
        else {
            $(".colLeft input[name='adress']").show();
            $(".colLeft input[name='adress']").val('');
            $("#qiwi").hide();
        } 
		showPriceInfo();
	});
	
	//������� �����
	$('#order-form').bind('submit', function(){
			var form = $('#order-form');
			var name = form.find('input[name="name"]');
			var phone = form.find('input[name="phone"]');
			var email = form.find('input[name="email"]');
			var loading = form.find('.loading');
			var inProcess = $(this).hasClass('in-process');
			var sendButton = $(this);
			
			if (inProcess)
				return false;
			
			var error_msg = "";
			
			if (name.val().length <= 3) {	
				error_msg += "������� ���� ��� \n\n";
			}
			
			if (phone.val().length < 5) {
				error_msg += "����������, ������� ����� ������ ��������\n\n";
			}
			
			if ((needEmail)&&( !isValidEmail(email.val()) )){
				error_msg += "����������, ������� email\n\n";
			}
			
			if ( $('select[name="delivery_type"]').val() == 7 && $('#i13').val() == '' ){
				error_msg += "����������, �������� �������� Qiwi Post\n\n";
			}
			
			if (error_msg) {
				alert(error_msg);
				return false;
			}
			
			$.ajax({
					url: '/ajax/basket.php?method=make_order',
					type : 'POST',
					dataType : 'json',
					data : form.serialize(),
					beforeSend : function () {
						loading.show();
						form.find('input').attr('disabled','disabled');
						form.find('textarea').attr('disabled','disabled');
						sendButton.addClass('in-process');
					},
					success : function (data, textStatus, jqXHR) {
						// Order accepted popup
						$('.confirmation .name').text(name.val());
						$('.confirmation .order-code').text(data.order_code);
						//$('.confirmation .close').remove();
						$('.confirmation .close').addClass('redirectToCatalogue');
						
						$('.confirmation').css({
							'top': $(document).scrollTop() + $(window).height()/2 - $('.confirmation').outerHeight()/2
						});
						$('.overlay, .confirmation').show();
						
						
						if (data.payment == 'vkredit') {
						  var exit_btn = $('.confirmation .catalogueLink');
						  exit_btn.html('');
						  exit_btn.html('������� � ���������� �������');
						  exit_btn.attr('href', '/cart/credit.html');
						  exit_btn.css('margin-left','-150px');
						  exit_btn.css('width','-300px');
						}
						
						var goodsArray = new Array();
						for(var i in data.products){
							goodsArray.push ({
							'id':data.products[i].id,
							'price':data.products[i].price,
							'name':data.products[i].converted_title,
							'quantity':data.products[i].amount,
							});
						}
						var yaParams = {
											  order_id: data.order_id,
											  order_price: data.price, 
											  currency: "RUR",
											  exchange_rate: 1,
											  goods: goodsArray
											};
						yaCounter21819568.reachGoal('ORDER', yaParams);
					},
					complete : function () {
						loading.hide();
						form.find('input').removeAttr('disabled');
						form.find('textarea').removeAttr('disabled');
						sendButton.removeClass('in-process');
					},
					error : function (jqXHR, textStatus, errorThrown) {}
				});
			return false;
	   });
	   
	// ������������ �����
	// Quick buy
	
					
	$('#fast-order-send').bind('click', function(){
			var wrap = $(this).parent();
			var name =	wrap.find('input[name="name"]');
			var phone =	wrap.find('input[name="phone"]');
			var product_id = wrap.find('input[name="product_id"]');
			var request = wrap.find('.request');
			var respose = wrap.find('.respose');
			var button = $(this);
			
			if ( name.val().length <= 1 || phone.val().length < 5 || name.val() == '���:' || phone.val() == '�������:') {
				alert('������� ���� ��� � �������');
				return false;
			}
			if ( wrap.hasClass('in-process') ) {
				return false;
			}
			$.ajax({
				url: '/ajax/basket.php?method=make_order',
				type : 'POST',
				dataType : 'json',
				data : {'name' : name.val(), "phone" : phone.val(), "product_id" : product_id.val(), "fast_order" : 1},
				beforeSend : function () {
					name.attr('disabled', 'disabled');
					phone.attr('disabled', 'disabled');
					button.attr('disabled', 'disabled');
					wrap.addClass('in-process');
				},
				success : function (data, textStatus, jqXHR) {
					request.hide();
					respose.show();
					
					
					// Order accepted popup
					$('#order-num').text(data.order_code);
					
					var goodsArray = new Array();
						for(var i in data.products){
							goodsArray.push ({
							'id':data.products[i].id,
							'price':data.products[i].price,
							'name':data.products[i].converted_title,
							'quantity':data.products[i].amount,
							});
						}
						var yaParams = {
											  order_id: data.order_id,
											  order_price: data.price, 
											  currency: "RUR",
											  exchange_rate: 1,
											  goods: goodsArray
											};
						yaCounter21819568.reachGoal('ORDER', yaParams);
				},
				complete : function () {
					name.removeAttr('disabled');
					phone.removeAttr('disabled');
					button.removeAttr('disabled');
					wrap.removeClass('in-process');
				}
			});
			return false;
	   });
});
function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
	if (destWidth) {var iScaleW = destWidth / oldWidth;} else {var iScaleW = 1;}
	if (destHeight) {var iScaleH = destHeight / oldHeight;} else {var iScaleH = 1;}

	var iSizeRelation = ( (iScaleW < iScaleH) ? iScaleW : iScaleH);
	var iWidthNew = Math.round(oldWidth * iSizeRelation);
	var iHeightNew = Math.round(oldHeight * iSizeRelation);

	var iSizeW = iWidthNew;
	var iSizeH = iHeightNew;
	iDestX =0;
	iDestY =0;
	
	return {"width" : iSizeW, "height" : iSizeH};
}
function moneyFormat(nStr) {
	nStr += '';
	x = nStr.split('.');
	x1 = x[0];
	x2 = x.length > 1 ? '.' + x[1] : '';
	var rgx = /(\d+)(\d{3})/;
	while (rgx.test(x1)) {
		x1 = x1.replace(rgx, '$1' + ' ' + '$2');
	}
	return x1 + x2;
}
function declOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}
function intVal( mixed_var, base ) {
	    var tmp;
	 
	    if( typeof( mixed_var ) == 'string' ){
	        tmp = parseInt(mixed_var);
			if(isNaN(tmp)){
	            return 0;
	        } else{
	            return tmp.toString(base || 10);
	        }
	    } else if( typeof( mixed_var ) == 'number' ){
	        return Math.floor(mixed_var);
	    } else{
	        return 0;
	    }
}