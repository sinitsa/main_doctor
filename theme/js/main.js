// JavaScript Document

$(function () {
    //Slider
    $('.cc_0').slick({
        dots: true,
        arrows: false,
        autoplay: false,
        autoplaySpeed: 3000,
        speed: 800,
        infinite: true,
        draggable: false,
        slidesToShow: 1,
        adaptiveHeight: false,
        slide: 'div',
        fade: true
    });
    $('.cc_1').slick({
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000,
        speed: 800,
        infinite: true,
        draggable: false,
        slidesToShow: 4,
        adaptiveHeight: true
    });
    $('.cc_2').slick({
        dots: false,
        lazyLoad: 'ondemand',
        autoplay: false,
        autoplaySpeed: 5000,
        speed: 800,
        infinite: true,
        draggable: false,
        slidesToShow: 4,
        adaptiveHeight: true
    });
    $('.cc_3').slick({
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000,
        speed: 800,
        infinite: true,
        draggable: false,
        slidesToShow: 1,
        adaptiveHeight: false
    });
    cc_4Slick();
    function cc_4Slick(){
        $('.cc_4').slick({
            dots: false,
            lazyLoad: 'ondemand',
            autoplay: false,
            autoplaySpeed: 5000,
            speed: 800,
            infinite: true,
            draggable: false,
            slidesToShow: 1,
            adaptiveHeight: false,
            slide: 'div'
        });
    }
    $('.cc_5').slick({
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000,
        speed: 800,
        infinite: true,
        draggable: false,
        slidesToShow: 4,
        adaptiveHeight: false,
        slide: 'a'
    });
    $('.cc_6').slick({
        dots: false,
        autoplay: false,
        autoplaySpeed: 5000,
        speed: 800,
        infinite: false,
        draggable: false,
        slidesToShow: 3,
        adaptiveHeight: false,
        slide: 'div'
    }); 
    $('.po-main-slider').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.po-slider-for-main'
    });
    $('.po-slider-for-main').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.po-main-slider',
        dots: false,
        centerMode: false,
        focusOnSelect: true,
        draggable: true

    });

     $('.cc_7m').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        arrows: false,
        infinite: false,
        draggable: true,
        fade: false,
        dots: false,
        asNavFor: '.cc_7n'
    });
    $('.cc_7n').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        draggable: true,
        asNavFor: '.cc_7m',
        dots: false,
        centerMode: false,
        infinite: false,
        focusOnSelect: false
    });

    //�scroll up
    
    var offset = 180;
    var duration = 500;
    var btntop = $('.btn-top');
    $(window).scroll(function() {
        if ($(this).scrollTop() > offset) {
            btntop.fadeIn(duration);
        } else {
            btntop.fadeOut(duration);
        }
        
    });

    $('.btn-top').click(function(event) {
        event.preventDefault();
        $('html, body').animate({scrollTop: 0}, duration);
        return false;
    })
                
    //Product hover

    $('body').on('mouseenter',".carousel-product",
        function () {
            $(this).parent().css("z-index", "1");
            $(this).children('.element-hover').stop().slideDown(200);
            $(this).children('.quick-look-a').css('visibility', 'visible');
            $(this).children('.cc_4').children('.slick-prev').css('visibility', 'visible');
            $(this).children('.cc_4').children('.slick-next').css('visibility', 'visible');
        });
    $('body').on('mouseleave',".carousel-product", function () {
            $(this).children('.element-hover').stop().slideUp(200);
            var c = $(this).parent();
            setTimeout(function () {
                c.css("z-index", "0");
            }, 100);
            $(this).children('.quick-look-a').css('visibility', 'hidden');
            $(this).children('.cc_4').children('.slick-prev').css('visibility', 'hidden');
            $(this).children('.cc_4').children('.slick-next').css('visibility', 'hidden');
        }
    );

    $(".product .content-seen").hover(function(){
        $(this).css("z-index", "1");
    }, function () {
        var c = $(this);
        setTimeout(function(){
            c.css("z-index", "0");
        }, 100);
        
    });

    // Review hover
    $(".reviews-element").hover(
        function () {
            var h = $(this).find(".review-text")[0].scrollHeight;
            var gh = $(this).find(".review-gradient-wrapper")[0].scrollHeight;
            
            if (h >= 140) {

                $(this).stop().animate({
                    height: 33+h+gh
                }, 200);
            }
        },
        function () {
            $(this).stop().animate({
                height: "284px"
            }, 200);
        }
    );

    // Yandex maps   
    $("#mapOffline").one('click',
        function () {
            $('#mapOffline').hide();
            $("#mapLive").show();

            var myMap;

            // ������� �������� API � ���������� DOM.
            ymaps.ready(init);

            function init() {
                // �������� ���������� ����� � ��� �������� � ���������� �
                // �������� id ("map").
                myMap = new ymaps.Map('map', {
                    // ��� ������������� ����� ����������� ����� �������
                    // � ����� � ����������� ���������������.
                    center: [55.634662,37.439126], // ������
                    zoom: 16
                });
                myMap.geoObjects.add(new ymaps.Placemark([55.634662,37.439126], {
                    balloonContent: '<strong>�������� ������</strong> ��������-�������'
                }, {
                    preset: 'islands#dotIcon',
                    iconColor: '#85c240'
                }));
                //document.getElementById('destroyButton').onclick = function () {
                // ��� ����������� ������������ ����� destroy.
                //  myMap.destroy();
                //};

            }
        }
    );

    // header-fix 
    var c2 = false;
    $(window).on("scroll",
        function () {
            var c1 = $(window).scrollTop();
            if (!c2 && c1 >= 283) {
                $(".header-fix").stop().show();
                $(".header-fix").animate({
                    top: "0px"
                }, 200);
                $(".htl-basket-content").hide();
                c2 = true;
            } else if (c2 && c1 < 283) {
                $(".header-fix").stop().animate({
                    top: "-90px"
                }, 50, function(){
                    $(".header-fix").hide();
                    $(".htl-basket-content").hide();
                    $(".htl-basket-add").hide();
                    $(".htl-compare-add").hide();
                    $(".htl-like-add").hide();
                    c2 = false;
                });
            }
        }
    );

    // header basket hover
    $(".htl-basket").hover(function () {
        var c = $(this).children(".htl-basket-content");
        var a = $(".htl-basket-product");
        if (a.index() == -1 ) {
        	$(".basket-arrow").after('<div class="htl-basket-product temp">� ������� �����</dv>');
        }
        c.show();
        $(".htl-basket-add").hide();
      
    }, function(){
        var c = $(this).children(".htl-basket-content");
        if( $(".temp").index() > 0 ) { $(".temp").remove(); }
        c.hide();
    });

    // search menu
   /* $(".search-categories-menu div").hover(function () {
        var ch = $(this).data('ul');
        var c = $("#" + ch + " .first-li");
        var timeoutId2 = setTimeout(function () {
            $("#" + ch).removeClass("hidden");
        }, 0);
        $(this).data("timeoutId2", timeoutId2);

        $(".filter-button img, .ce-info, .brown .content-title").css("z-index", "0");

    }, function () {
        var ch = $(this).data('ul'),
            timeoutId = setTimeout(function () {
                $("#" + ch).addClass("hidden");
            }, 100);
        $(this).data("timeoutId", timeoutId);
        clearTimeout($(this).data("timeoutId2"));
        $(".filter-button img, .ce-info, .brown .content-title").css("z-index", "1");
    });

    $(".categories-hover").hover(function () {
        var m = $(this).attr("id"),
            e = $("div[data-ul='" + m + "']");
        clearTimeout(e.data('timeoutId'));
        e.addClass("active");
        $(".filter-button img, .ce-info, .brown .content-title").css("z-index", "0");
    }, function () {
        $(this).children().children(".first-li").addClass("active");
        $(this).addClass('hidden');
        $(".search-categories-menu div").removeClass("active");
        $(".filter-button img, .ce-info, .brown .content-title").css("z-index", "1");
    });

    $(".categories-hover ul.level_1 li.first-li").on("mouseenter", function () {
        $(this).parent().parent().removeClass("hidden");
        $(this).addClass("active");
    });
    $(".categories-hover ul.level_1 li:nth-child(2)").on("mouseenter", function () {
        $(this).parent().children(".first-li").removeClass("active");
    }); */

   

        // ��� ��������� ������� ������ ���� (������� ��������� ��� � 0 �������)
        
        	var $menu = $(".dropdown-menu");
            var $menu2 = $(".m_left");

        	// jQuery-menu-aim: <meaningful part of the example>
	        // Hook up events to be fired on menu row activation.
	        $menu.menuAim({
	            activate: activateSubmenu,
	            deactivate: deactivateSubmenu,
	            exitMenu: deactivateAll,
                submenuDirection: 'right'
	        });
            $menu2.menuAim({
                activate: activateSubmenu,
                deactivate: deactivateSubmenu,
                exitMenu: deactivateAll,
                submenuDirection: 'left'
            });
        
        
        // jQuery-menu-aim: </meaningful part of the example>

        // jQuery-menu-aim: the following JS is used to show and hide the submenu
        // contents. Again, this can be done in any number of ways. jQuery-menu-aim
        // doesn't care how you do this, it just fires the activate and deactivate
        // events at the right times so you know when to show and hide your submenus.
        function activateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId),
                li = $submenu.find('li'),
                height = $submenu.outerHeight() > $row.parent().outerHeight() ? $submenu.outerHeight() : $row.parent().outerHeight(),
                width = $menu.outerWidth();
                $row.css({
                    background: '#4b403d'
                });
                $row.find('a.level_1').css('color', '#fff');
                
            if(!$row.parent().hasClass('m_left')) {
               
                var w = width - 1;
                
            } else {
               
                var w = 2 - width;
            }

            // Show the submenu
                if (li.length > 0){
                    $submenu.css({
                        display: "block",
                        top: 0,
                        left: w,  // main should overlay submenu 20 - width
                        height: height - 2  // padding for main dropdown's arrow 
                    });
                    //$submenu.addClass('active');
                    $row.find('ul.level_3').css("height", height - 2 +"px");
                } else {
                    $submenu.css({
                        display: 'none'
                    });
                }
            /*
            var h = 0;
            
            if (!$row.hasClass('l_3')) {
                li.each(function(){
                    h += $(this).outerHeight();
                    
                    if (height < h ) {
                        $(this).addClass('l3');
                        li.css({width : '225px'});
                        
                        $submenu.css({width : '502px'});
                    }
                   
                });
                 var l3 = $('.l3');
                $submenu.parent().append('<ul class="level_3"></ul>');
                $('ul.level_3').append(l3);
                $row.addClass('l_3');  
            } 
            */
            
            

            // Keep the currently activated row's highlighted look
            $row.find("a").addClass("maintainHover");
        }

        function deactivateSubmenu(row) {
            var $row = $(row),
                submenuId = $row.data("submenuId"),
                $submenu = $("#" + submenuId);
            // Hide the submenu and remove the row's highlighted look
            $submenu.css("display", "none");
            $row.find("a").removeClass("maintainHover");
            $row.css({
                    background: '#fff'
                });
            $row.find('a.level_1').css('color', 'black');
        }

        function deactivateAll() {
            var ul = $menu.find('ul.active');
            
            if (ul.parent().parent().outerHeight() < 10){
                ul.css('display','');
                //ul.removeClass('active');
                $("a.maintainHover").removeClass("maintainHover");
                //console.log(ul.parent().parent().outerHeight());
            }
        }

        

        // Bootstrap's dropdown menus immediately close on document click.
        // Don't let this event close the menu if a submenu is being clicked.
        // This event propagation control doesn't belong in the menu-aim plugin
        // itself because the plugin is agnostic to bootstrap.
        $(".dropdown-menu li").click(function(e) {
            window.location = $(this).find('a').eq(0).attr('href');
            e.stopPropagation();
        });

       /* $(".menu-item").mouseleave(function() {
            // Simply hide the submenu on any click. Again, this is just a hacked
            // together menu/submenu structure to show the use of jQuery-menu-aim.
            $("ul.level_2").css("display", "none");
            $("a.maintainHover").removeClass("maintainHover");
        }); */

    

   

    // quick order popup
    $('body').on('click',".one-click-order", function(){
        $("#fast-order-pid").val($(this).data('product_id'));
        $("#fast-order-sid").val($(this).data('set_id'));
        $("#fast-order-sname").val($(this).data('set_name'));
        var modal = $('#product-quick-order');
        modal.find('div').html('������� �����');
        modal.find('input[name="isCredit"]').val('0');
    })

    // credit buy popup
    $('body .pqv-order').on('click',".credit-order", function(){
        $("#fast-order-pid").val($(this).data('product_id'));
        $("#fast-order-sid").val($(this).data('set_id'));
        $("#fast-order-sname").val($(this).data('set_name'));
        var modal = $('#product-quick-order');
        modal.find('div').html('������� � ������');
        modal.find('input[name="isCredit"]').val('1');
    })

    fastOrderClick();

     // back call popup

    $(".header-info-backcall a, #backcall").magnificPopup({
        items: {
            src: "#back-call",
            type: "inline"
        },
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: "auto",
        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
                $(".header-fix .htl-container").css("padding-right", "17px");
                $(".header-fix .search-wrapper").css("padding-right", "17px");
            },
            close: function () {
                // Will fire when popup is closed
                $(".header-fix .htl-container").css("padding-right", "0");
                $(".header-fix .search-wrapper").css("padding-right", "0");
                $(".response").hide();
                $(".request").show();
            }
        }
    });

    // basket delete popup
    basketDeletePopup();
    

    // compare delete popup

    $(".compare .l-close, .compare .ce-info div:nth-child(4) button").magnificPopup({
        items: {
            src: "#delete-compare",
            type: "inline"
        },
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: "auto",
        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
                $(".header-fix .htl-container").css("padding-right", "17px");
                $(".header-fix .search-wrapper").css("padding-right", "17px");
            },
            close: function () {
                // Will fire when popup is closed
                $(".header-fix .htl-container").css("padding-right", "0");
                $(".header-fix .search-wrapper").css("padding-right", "0");
                $(".content").find(".remove").removeClass("remove");
            }
        }
    });


    // like delete popup

    $(".like .l-close").magnificPopup({
        items: {
            src: "#delete-like",
            type: "inline"
        },
        fixedContentPos: true,
        fixedBgPos: true,
        overflowY: "auto",
        closeBtnInside: true,
        preloader: false,

        midClick: true,
        removalDelay: 300,
        mainClass: 'my-mfp-slide-bottom',
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
                $(".header-fix .htl-container").css("padding-right", "17px");
                $(".header-fix .search-wrapper").css("padding-right", "17px");
            },
            close: function () {
                // Will fire when popup is closed
                $(".header-fix .htl-container").css("padding-right", "0");
                $(".header-fix .search-wrapper").css("padding-right", "0");
                $(".content").find(".remove").removeClass("remove");
            }
        }
    });
    

    // popup button "No" action
    $(".delete-basket button:nth-child(4)").click(function(){
        var magnificPopup = $.magnificPopup.instance; 
        // save instance in magnificPopup variable
        magnificPopup.close(); 
        // Close popup that is currently opened

    });

    // compare add class remove to element
    $(".compare .l-close, .like .l-close").click(function(){
        var el = $(this).parent().parent();
        el.addClass("remove");
        $("#tab" + el.data("desc")).addClass("remove");
    });

    // header basket remove from cart button
    $(".htl-basket-product a:last-child").click(function(){
    	var i = $(this).parent().index() + 1;
    	$(".htl-basket-product:nth-child("+i+")").addClass("remove");
        $('tr[data-product_id="'+$(this).parent().data("product_id")+'"').addClass("remove");
    });

    // popup basket delete "Yes"
    $("#delete-basket button:nth-child(3)").click(function(){
        var productId = $(".remove").data("product_id"); // old selector .htl-basket-product.remove
        var price = $(".remove").data("product_price");
    	
        $('.buyLink[data-product_id="'+productId+'"]').removeClass("active");
        deleteProductFromBasket(productId, price, true);
        var value = $(".remove").find(".cart-product-amount").val()
        var amount = parseInt(value);
        // ���� ���������� ������ ������ 1 ��������� ����������, ���� ����� 1 �� ������� ��� ���������
       // if ( amount && amount > 1 ) {
        //    $(".remove").find(".cart-product-amount").val(value - 1);
       //     recalculateNewAmount();
       // } else {
            $(".remove").detach();
       // }

        // ������� ���� �����, ���� ���� ������ ������ "�������� �������"
        if ($(".cart-empty").hasClass("active")) {
            setCookie("total_amount", "", "", "/");
            setCookie("cart_products", "", "", "/");
            refreshBasketInfo();
            
        }
    	var magnificPopup = $.magnificPopup.instance; 
        // save instance in magnificPopup variable
        magnificPopup.close(); 
        // Close popup that is currently opened
    });
    
    //console.log(getCookie("compare_products").split(','));
    // popup compare delete "Yes"
    $("#delete-compare button:nth-child(3)").click(function(){

        //������� ����� �� ����
        var pId = $(".remove").data("product_id");
        if(pId == 'all'){
            setCookie('compare_products', [], true, '/');
            $(".htl-compare .htl-amount").text(0);
        } else {
        
        var cId = $(".remove").data("cat_id");
        
        
        var pc = getCookie("compare_products").split(',');
        
        var index = pc.indexOf(pId.toString());
        
        if (index > -1) {
            pc.splice(index, 1);
        }
        
        pc ? setCookie('compare_products', pc, '', '/') : setCookie('compare_products', [], true, '/');
        var a = $(".htl-compare .htl-amount");
        a.text(pc.length);

        // Delete category if no other products in it 
        var c = $("#cat_"+cId);
        var m = $('div[data-cat_id="'+cId+'"]');
        
        if (m.length < 2) { c.remove(); }
        }
        var ind = $(".remove").index();
        if ( $(".remove").eq(0).hasClass("content-carousel") ) {
            $(".remove").remove();
        } else {
            $(".cc_7m, .cc_7n").slickRemove(ind);
        }

        var l = $(".carousel-product").length;
        var text = l; 
        
        if(ind > (l-3) ) {
           $(".cc_7n").slickPrev(); 
        }
        if (l > 5 || l == 0) {
            text += " �������";
        }
        else if ( l < 5 && l != 1) {
            text += " ������";
        }
        else if ( l == 1) {
            text += " �����";
        }
        $(".ce-info div:nth-child(3)").text(text);
        
        // Delete unused rows
        var li = $('.cet-title').find('li');
        li.each(function(){
            var c = $(this).attr('class'),
                t = $('.cet-pinfo .'+c),
                r = $('.'+c);
                e = true;
                
                t.each(function(){
                    if ($.trim($(this).text()).length > 0) { e = false ;}
                });
                if (e) {r.remove();}
        });


        var magnificPopup = $.magnificPopup.instance; 
        // save instance in magnificPopup variable
        magnificPopup.close();
        // Close popup that is currently opened
    });

    // popup like delete "Yes"
    $("#delete-like button:nth-child(3)").click(function(){
        
        //������� ����� �� ����
        var pId = $(".remove").parent().find(".buyLink").data("product_id");
        //console.log(pId);
        var pl = getCookie("like_products").split(',');
        var index = pl.indexOf(pId.toString());
        if (index > -1) {
            pl.splice(index, 1);
        }
        pl ? setCookie('like_products', pl, '', '/') : setCookie('like_products', [], true, '/');
        var a = $(".htl-like .htl-amount");
        a.text(pl.length);

        

        $(".like-h4").removeClass("hidden");
        $(".remove").parent().remove();
        $(".cc-mrzero").removeClass("cc-mrzero");
        for (i = 1; i <= $(".carousel-element").length; i++) {

            if ( i % 4 === 0 ) {
                $(".cc-el:nth-child("+i+")").addClass("cc-mrzero");
            }
        }
        var magnificPopup = $.magnificPopup.instance; 
        // save instance in magnificPopup variable
        magnificPopup.close();
        // Close popup that is currently opened
    });

    // compare buttons
    $(".ced-b").click(function () {
        $(".ced-b").removeClass("active");
        $(this).addClass("active");
        var differ = $(this).hasClass("differ");
        var b = $(".cet-pinfo");
        var g = $(".cet-title");
        if( differ ) {
            var a = $(".cet-pinfo").length;
            var c = b.eq(0).find("li").length;
            
            b.find("li").addClass("hidden");
            g.find("li").addClass("hidden");

            for ( var z = a-1; z >= 1; z--) {
                for (var i = c; i >= 0; i--) {
                    var e = b.eq(0).find("li:nth-child("+i+")").text();
                    var f = b.eq(z).find("li:nth-child("+i+")").text();

                    if ( g.find("li:nth-child("+i+")").hasClass("hidden") && e != f) {
                        var k = b.find("li:nth-child("+i+")");
                        var l = g.find("li:nth-child("+i+")");
                        k.removeClass("hidden");
                        l.removeClass("hidden");
                        k.addClass("odd");
                        l.addClass("odd");
                    } 
                }
            }
            var d = $(".cet-title .odd");
            for ( i = 1; i <= d.length; i++) {
                if(i % 2 === 0 ) {
                    var m = d.eq(i-1).index()+1;
                    b.find("li:nth-child("+m+")").removeClass("odd").addClass("even");
                    g.find("li:nth-child("+m+")").removeClass("odd").addClass("even");
                }
            }

        } else {
            b.find("li").removeClass("hidden odd even");
            g.find("li").removeClass("hidden odd even");
        }
    });
    $(".ced-clear").click(function(){
        $(".content-carousel").addClass("remove");
        $(".ce-table").addClass("remove");
    });


    $(window).load(function(){

        //compare table height make equal
        var i = 1;
        $(".cet-title ul li").each(function () {
            var h = $(this).height();
            var a = $(this);
            var e =  $(".cet-pinfo ul li:nth-child(" + i + ")");
            e.each(function() {
                var eh = $(this).height();
                if (eh > h) { h = eh; }
            });

            a.css("height", h);
            e.css("height", h);

            i++;
        });
        $(".ce-table").css("height", $(".cet-title").height());

        correctHeight();
        
        // Correct top padding of content in catalog
        $(".product .p-one").css('padding-top', $('.content-title').height() - 41 +'px');

        setTimeout(function(){
            if($('a[data-view="tile"').hasClass("active")){
                $(".list-view").hide();
            }
        },1000);
     
   }); // window.load end

    // Side filter price slider

    $("#slider-range").slider({
        range: true,
        min: $("#amount1").data('min'),
        max: $("#amount2").data('max'),
        values: [$("#amount1").data('min'), $("#amount2").data('max')],
        slide: function (event, ui) {
            $("#amount1").val(ui.values[0]);
            $("#amount2").val(ui.values[1]);
        },
        stop: function( event, ui ) {
            reloadProducts();
        }
    });
    $("#amount1").val($("#slider-range").slider("values", 0));
    $("#amount2").val($("#slider-range").slider("values", 1));
    
    // price inputs change
    $(".sf-price input").change(function() {
        var i = $(this).attr("id").substr(-1,1) - 1;
        $("#slider-range").slider("values", i, $(this).val());
        reloadProducts();
    });

    // Side filter reset
    
    $(".filter-button img:first-child").click(function(){
        var min = $("#amount1").data("min");
        var max = $("#amount2").data("max");

        $('.sf-func input[type="checkbox"]').prop("checked",false);
        $("#slider-range").slider("values", [min, max]);
        $("#amount1").val(min);
        $("#amount2").val(max);
        reloadProducts();
    }); 

    // filter checkbox check
    $('.sf-func input[type="checkbox"]').click(function(){
        reloadProducts();
    });

    // left side advantages popup

    $(".advantages li").hover(function(){
        $(this).children(".a-popup").removeClass("hidden");
    }, function(){
        $(this).children(".a-popup").addClass("hidden");
    });

    // Check if input empty
    // param a object  input
    // return res boolean true if inputs not empty
    function inputCheck(a){
        var res = true;
        if (a.val() == "") {
            a.css("border-color", "red");
            res = false;
        }
        return res;
    }

    // Left side question check
    $(".a-popup button").click(function(){
        var a = $(".a-popup input:nth-child(2)");
        var b = $(".a-popup input:nth-child(3)");
        inputCheck(a);
        inputCheck(b);
    });
    // Quick order check
    $("#product-quick-order button:nth-child(5)").click(function(){
        var a = $("#product-quick-order input:nth-child(3)");
        var b = $("#product-quick-order input:nth-child(4)");
        inputCheck(a);
        inputCheck(b);
    });
    $(".pt-order .green-button").click(function(){
        var a = $('.pt-order input[name="name"]');
        var b = $('.pt-order input[name="phone"]');
        inputCheck(a);
        inputCheck(b);
    });
    // Back call check
    $("#back-call button:nth-child(5)").click(function(){
        var a = $("#back-call input:nth-child(3)");
        var b = $("#back-call input:nth-child(4)");
        inputCheck(a);
        inputCheck(b);
    });

    // product page tabs 

    $(".pt-title a").click(function(){
        $(".pt-title div.active").removeClass("active");
        $(".pt-text.active").removeClass("active");
        $(".pt-text."+$(this).data("tab")).addClass("active");
        $(this).parent().addClass("active");
    });

    // product quick view and catalog list tabs

    $('body').on('click',".carousel-other div",function(){

        $(this).parent().parent().find(".active").removeClass("active");
        $(this).parent().parent().find("."+$(this).data("tab")).addClass("active");
        $(this).addClass("active");

        var a = $(this).parent().parent().parent().parent(),
            b = $(this).parent().parent(),
            tab = $(this).data("tab");

        if ( a.hasClass("list") == false && b.hasClass("pqv-right") == false) {

            var product_id = $(this).parent().parent().find(".buyLink").data("product_id");
            
            loadProductQuickView(product_id,tab);
        }

    });


    // bx vertical slider 
     $('.slider8').bxSlider({
        mode: 'vertical',
        pager: false,
        slideWidth: 300,
        minSlides: 2,
        slideMargin: 10
      });

     // addToCart popup 

     $('body').on('click',".de-last-incart", function(){
        $(".htl-basket-add").show();
        setTimeout(function(){
            $(".htl-basket-add").hide();
        },3000);
     });
     $(".htl-basket-add .ba-close").click(function(){
        $(".htl-basket-add").hide();
     });

     // addToCompare popup 

     $('body').on('click',".eh-compare a", function(){
        var el = $(this).parent();
        var href = $(this).data("href");
        var pId = el.parent().find(".buyLink").data("product_id");
        if (!pId) { pId = el.parent().parent().find(".buyLink").data("product_id");  }
        var el2 =  $(".compare-"+pId);
        if(!el.hasClass("done")) {
            $(".htl-compare-add").show();
            el.addClass("done");
            el2.addClass("done");
            
            var p = getCookie("compare_products").split(',');
            if(p == "") { p = [];}
            
            if ($.inArray(pId.toString(), p) < 0) {
                p.push(pId);
            }
            
            setCookie('compare_products', p, '', '/');
            //console.log(getCookie("compare_products"));

            var a = $(".htl-compare .htl-amount");
            a.text(p.length);

            el.html('����� � <a href="'+href+'">���������</a>');
            el2.html('����� � <a href="'+href+'">���������</a>');    
            setTimeout(function(){
                $(".htl-compare-add").hide();
            },3000);
        }
     });

     // hide addToCompare popup
     $(".htl-compare-add .ba-close").click(function(){
        $(".htl-compare-add").hide();
     });

     // addTolike popup 
     $('body').on('click',".eh-like a", function(){
        var el = $(this).parent();
        var href = $(this).data("href");
        var pId = el.parent().find(".buyLink").data("product_id");
        if (!pId) { pId = el.parent().parent().find(".buyLink").data("product_id");  }
        var el2 =  $(".like-"+pId);
        if(!el.hasClass("done")) {
            $(".htl-like-add").show();
            el.addClass("done");
            el2.addClass("done");

            var p = getCookie("like_products").split(',');
            if(p == "") { p = [];}
            
            if ($.inArray(pId.toString(), p) < 0) {
                p.push(pId);
            }
            
            var a = $(".htl-like .htl-amount");
            a.text(p.length);

            setCookie('like_products', p, '', '/');

            el.html('����� � <a href="'+href+'">���������</a>');
            el2.html('����� � <a href="'+href+'">���������</a>');
            setTimeout(function(){
                $(".htl-like-add").hide();
            },3000);
        }
     });
     $(".htl-like-add .ba-close").click(function(){
        $(".htl-like-add").hide();
     });

     // Cat page left side slide
     $(".filter-button img:nth-child(2)").click(function(){
        var w = $(".aside").width();
        if (w == 0) {
            $(".aside").animate({width: "220px"}, 200,function(){
                $(".aside-wrapper").css("overflow", "visible");
            });
            $(".aside .filter-button img:last-child").attr("src","/theme/img/content/filter_button_2.png");
            $(".aside .filter-button").css("right", "0px");
            $(".aside .filter-button img:first-child").css("visibility", "visible");
            $(".section").removeClass("wide");
            $(".mr-norm").addClass("cc-mrzero");
            $(".content-seen").removeClass("wide2");
            $(".f-pages.count").removeClass("wide");
            $(".carousel-element.list").css("width", "97.5%");
            $(".list .l-text").css("width", "298px");
            $(".list .pod-right").css("width", "100px");
            $(".cc_6").unslick();
                $('.cc_6').slick({
                    dots: false,
                    autoplay: false,
                    autoplaySpeed: 5000,
                    speed: 800,
                    infinite: false,
                    draggable: false,
                    slidesToShow: 3,
                    adaptiveHeight: false,
                    slide: 'div'
                });
            
        } else {
            $(".aside-wrapper").css("overflow", "hidden");
            $(".aside").animate({width: "0px"}, 200, function(){
                $(".aside .filter-button img:last-child").attr("src","/theme/img/content/filter_button_7.png");
                $(".aside .filter-button").css("right", "24px");
                $(".aside .filter-button img:first-child").css("visibility", "hidden");
                $(".section").addClass("wide");
                $(".cc-mrzero").addClass("mr-norm");
                $(".cc-mrzero").removeClass("cc-mrzero");
                $(".f-pages.count").addClass("wide");
                $(".content-seen").addClass("wide2");
                $(".carousel-element.list").css("width", "97.3%");
                $(".list .l-text").css("width", "525px");
                $(".list .pod-right").css("width", "auto");
                $(".cc_6").unslick();
                $('.cc_6').slick({
                    dots: false,
                    autoplay: false,
                    autoplaySpeed: 5000,
                    speed: 800,
                    infinite: false,
                    draggable: false,
                    slidesToShow: 4,
                    adaptiveHeight: false,
                    slide: 'div'
                });
            });
            
        }
        
     });

    // Top title slideUp
    $(".wrapper2 div:first-child a.blue-dashed").click(function(){
        var h = $(".top-image.category").height();
        var e = $(this);
        if (h == 332) {    
            $(".top-image.category").animate({height: "213px"}, 200);
            $(".top-image.category div:nth-child(2), .top-image.category div:nth-child(3)").hide();
            $(this).text("����������");
            setCookie("top_title_slideUp", "1", "", "/");
        } else {
            $(".top-image.category").animate({height: "332px"}, 200, function() {
                $(".top-image.category div:nth-child(2), .top-image.category div:nth-child(3)").fadeIn();
                e.text("��������");
                setCookie("top_title_slideUp", "0", "", "/");
            });
        }
    });

    // Order page buttons: back, forward

    $(".step1 .o-button-next").click(function(){
        $(".order .step1").addClass("hidden");
        $(".order .step2").removeClass("hidden");
    });

    $(".step2 .o-button-back").click(function(){
        $(".order .step2").addClass("hidden");
        $(".order .step1").removeClass("hidden");
    });

    $(".step1 .o-button-back").click(function(){
        window.location.replace($(this).data("href"));
    });

    // redirect on product img click
    $('body').on('click',".cp-slide", function(){
        var a = $(this).parent().parent().parent();
        var h = a.data("href");
        a.attr("href", h);
    });

    // Buy link
    $('body').on('click','a.buyLink', function(){
        if(!$(this).is('.active')){
            $(this).addClass('active');
            // yaCounter21819568.reachGoal('BUY');
            addProductToBasket($(this).data('product_id'), $(this).data('product_price'));
            return false;
        };
    });

    // Cat page list or tile view change
    $(".tile-list").click(function(){
        var b = $(this).data("view");
        var t = false;
        if (b == 'list' && !$(this).hasClass("active")) {
           $(".products").removeClass("content-catalog");
           $(".list-view").show();
           $(".tile-view").hide();
           t = true;
        } else if (b == 'tile' && !$(this).hasClass("active")){
            $(".products").addClass("content-catalog");
            $(".tile-view").show();
           $(".list-view").hide();
           t = true;
        }
        if (t) {
            $(".tile-list").toggleClass("active");
            $(".list-block2").toggleClass("not-visible");
            $(".tl_img").toggleClass("hidden");
        }
    });

    // Feedback page write feedback button click
    $(".f-button .header-button").click(function(){
         $('html, body').animate({
                scrollTop: $(".f-title").offset().top - 100
            }, 500);
    });

    // Feedback page send feedback
    $(".send-button").click(function(){
        var form = $('#feed-back-form');
        var res = $('.send-result');

        $.ajax({ 
                url : '/ajax/system.php',
                data : form.serialize(),
                type : "post",
                dataType : "json",
                beforeSend : function () {
                    form.find('input, textarea').attr('disabled', 'disabled');
                    res.find('.error').hide();
                },
                success: function(data) {
                   // console.log(data);
                    if (!data.error) {
                        //win.find('.close').trigger('click');
                    
                        form.find('input, textarea').each(function(index, el){
                            $(el).val(el.defaultValue);
                        });
                        form.hide();
                        res.find('.success').show();
                        $('.send-button').hide();
                    } else {
                        form.find('.er-' + data.error_code).css('border-color', 'red');
                        res.find('.error-code-' + data.error_code).show();
                    }
                    
                },
                complete : function() {
                    form.find('input, textarea').removeAttr('disabled');
                }
            });
    });
    
    // Send mail footer form
    $("#send-mail").click(function(){
        var form = $('#mail-feedback-form');
        //var res = $('.send-result');

        $.ajax({ 
                url : '/ajax/system.php',
                data : form.serialize(),
                type : "post",
                dataType : "json",
                beforeSend : function () {
                    form.find('input, textarea').attr('disabled', 'disabled');
                    //res.find('.error').hide();
                },
                success: function(data) {
                    
                    if (!data.error) {
                        //win.find('.close').trigger('click');
                    
                        form.find('input, textarea').each(function(index, el){
                            $(el).val(el.defaultValue);
                        });
                        alert("��������� ������� ����������!");
                        //form.hide();
                        //res.find('.success').show();
                        //$('.send-button').hide();
                    } else {
                        //form.find('.er-' + data.error_code).css('border-color', 'red');
                        //res.find('.error-code-' + data.error_code).show();
                        alert("���������� ��������� ��������� ��� ����!");
                    }
                },
                complete : function() {
                    form.find('input, textarea').removeAttr('disabled');
                }
            });
    });
    
    // Backcall
    $('.backcall-button').bind('click', function () {
        var wrap = $(this).data("onpage") ? $('#back-call2') : $('#back-call');
        if ($(this).data("onpage") == "side") { wrap = $("#back-call3"); }
        var phone = wrap.find('input[name="phone"]');
        var name = wrap.find('input[name="name"]');
        var request = wrap.find('.request');
        var response = wrap.parent().find('.response');

        if (
            name.val().length <= 1 ||
                phone.val().length < 5 ||
                name.val() == name.prop('defaultValue') ||
                phone.val() == phone.prop('defaultValue')
            ) {
            alert('������� ���� ��� � �������');
            return false;
        }

        if (wrap.hasClass('in-process')) {
            return false;
        }

        $.ajax({
            url: '/ajax/basket.php?method=make_order',
            type: 'POST',
            dataType: 'json',
            data: {
                'name': name.val(),
                "phone": phone.val(),
                "backcall": 'true'
            },
            beforeSend: function () {
                name.attr('disabled', 'disabled');
                phone.attr('disabled', 'disabled');

                wrap.addClass('in-process');
            },
            success: function (data, textStatus, jqXHR) {
                response.show();
                request.hide();
                
            },
            complete: function () {
                name.removeAttr('disabled');
                phone.removeAttr('disabled');
                wrap.removeClass('in-process');
            }
        });

        return false;

    });


    // Page next, page prev click
    $(".page-prev, .page-next").click(function(){
        var cp = parseInt($(".page-num.active a").text());
        var href = $(".page-num.active").data("href");
        var p = parseInt($(this).data("plus"));
        var max = $(".page-num.active").data("pages");
        var sum = cp + p;
        //console.log(sum);
        if(sum > 0 && sum <= max) {
            var uri = $(".page_"+sum +" a").data("uri");
            //console.log(uri + "/" + sum);
            window.location = uri;
        }
    });

    // Sort by popularity and price
    $("#sort-filter a, .page-num a, .f-pages.count a").bind('click',function(){
        //console.log('click');
        var uri = $(this).data('uri');
        window.location = uri;
    });
    
    // Feedback page write feedback button click
    $("#more-about").click(function(){
         $('html, body').animate({
                scrollTop: $(".aboutUs-content").offset().top - 100
            }, 500);
    });

    //Product quic view click 
    
    $(".quick-look-a").click(function(){
         var product_id = $(this).parent().find(".buyLink").data("product_id");
         //console.log(product_id);
        loadProductQuickView(product_id);
    });

    $('body').on('click',"#pqv-prev,#pqv-next", function(){
        var product_id = $(this).data('product_id');
        loadProductQuickView(product_id);
    });

    // ������������ �����
    // Quick buy
                    
    $('#fast-order-send, #fast-order-send2').bind('click', function(){
            var wrap = $(this).parent();
            var name =  wrap.find('input[name="name"]');
            var phone = wrap.find('input[name="phone"]');
            var product_id = wrap.find('input[name="product_id"]');
            var set_id = wrap.find('input[name="set_id"]');
            var set_name = wrap.find('input[name="set_name"]');
            var request = wrap.find('.request');
            var response = wrap.find('.response');
            var button = $(this);
            var isCredit = (wrap.find('input[name="isCredit"]').length) ? wrap.find('input[name="isCredit"]').val() : 0 ;
            

            if ( name.val().length <= 1 || phone.val().length < 5 || name.val() == '���:' || phone.val() == '�������:') {
                alert('������� ���� ��� � �������');
                return false;
            }
            if ( wrap.hasClass('in-process') ) {
                return false;
            }
            $.ajax({
                url: '/ajax/basket.php?method=make_order',
                type : 'POST',
                dataType : 'json',
                data : {
                    'name' : name.val(),
                    "phone" : phone.val(),
                    "product_id" : product_id.val(),
                    "set_name" : set_name.val(),
                    "set_id" : set_id.val(),
                    "fast_order" : 1,
                    "is_credit": isCredit,
                },
                beforeSend : function () {
                    name.attr('disabled', 'disabled');
                    phone.attr('disabled', 'disabled');
                    button.attr('disabled', 'disabled');
                    wrap.addClass('in-process');
                },
                success : function (data, textStatus, jqXHR) {
                    request.hide();
                    response.show();
                    
                    
                    // Order accepted popup
                    $('#order-num').text(data.order_code);
                    $('#order-num2').text(data.order_code);
                    /*
                    var goodsArray = new Array();
                        for(var i in data.products){
                            goodsArray.push ({
                            'id':data.products[i].id,
                            'price':data.products[i].price,
                            'name':data.products[i].converted_title,
                            'quantity':data.products[i].amount,
                            });
                        }
                        var yaParams = {
                                              order_id: data.order_id,
                                              order_price: data.price, 
                                              currency: "RUR",
                                              exchange_rate: 1,
                                              goods: goodsArray
                                            };
                        yaCounter21819568.reachGoal('ORDER', yaParams); */
                },
                complete : function () {
                    name.removeAttr('disabled');
                    phone.removeAttr('disabled');
                    button.removeAttr('disabled');
                    wrap.removeClass('in-process');
                } 
            });
            return false;
       });

        $(".search-categories-menu div").click(function(){
            window.location = $(this).find('a').attr('href');
        });

    openOriginalImage();
    

    youtubeOpen();
 // filter tags expand/colapse 
     $('.more_tags a').click(function(){
        var tag = $('.tags_list');
        if (tag.hasClass('colapse')){
            tag.animate({'max-height' : "200px"},  500);
        }else{
            tag.animate({'max-height' : '2000px'}, 500);
        }
        tag.toggleClass('colapse');
     });


     // Product page scroll to full description and open appropriate tab
     $('.product_more').click(function(){
            $('html, body').animate({
                scrollTop: $("#more").offset().top -100
            }, 500);
            $('.pt-text.active, .pull-left.active').removeClass('active');
            $('#more').find(".tab"+$(this).data('tab')).addClass('active');
            $('#more').find(".t"+$(this).data('tab')).addClass('active');
            //console.log($(this).data('tab'));
     });

    // �������� ����� �������� � ������� ���������� ������
    $('.wholesaler-request').find('input[name="phone"]').mask('+7 (999) 999-99-99');

    // ���������� ������� �������� ������
    $('form.wholesaler-request').on('submit',function(e){
        e.preventDefault();

        var form = $(this),
            name = form.find('input[name="name"]').val(),
            phone = form.find('input[name="phone"]').val(),
            email = form.find('input[name="email"]').val(),
            extra = form.find('textarea').val();

        if (form.find('input[type="submit"]').hasClass('disabled')) {
            return false ;
        }

        var params = {
            name:name,
            phone:phone,
            email:email,
            extra_information:extra,
        };

        $.ajax({
            url:'/ajax/basket.php?method=wholesaler',
            data:params,
            method:'post',
            dataType:'json',
            beforeSend:function(){
                form.find('input[type="submit"]').addClass('disabled');
            },
            success:function(data){
                console.log(data);
                if (data.orderId) {
                    alert(name+', ��� ������ ������� ���������!');
                    setTimeout(function(){
                        location.reload();
                    }, 1500);
                } else {
                    alert(name+', ������ �������! ������� ��������� ������, ��� ��������� ������ �����.');
                    console.log(data);
                }
            },
            complete:function(){
                form.find('input[type="submit"]').removeClass('disabled');
            }
        });
    });

});
// end  document.ready fucntion

// Functions

// Product original photo gallery
function openOriginalImage() {
    $('.po-main-slider .slick-track').magnificPopup({
        delegate: 'a',
        type: 'image',
        closeOnContentClick: false,
        closeBtnInside: false,
        mainClass: 'mfp-with-zoom mfp-img-mobile',
        image: {
            verticalFit: true
            //titleSrc: function(item) {
            //    return item.el.attr('title') + ' &middot; <a class="image-source-link" href="'+item.el.attr('data-source')+'" target="_blank">image source</a>';
            //}
        },
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true,
            duration: 300, // don't foget to change the duration also in CSS
            opener: function(element) {
                return element.find('img');
            }
        }
        
    });
}


// basket delete popup
function basketDeletePopup() {
        $(".htl-basket-product a:last-child, .cart-delete, .cart-empty").magnificPopup({
            items: {
                src: "#delete-basket",
                type: "inline"
            },
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: "auto",
            closeBtnInside: true,
            preloader: false,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-slide-bottom',
            callbacks: {
                open: function () {
                    // Will fire when this exact popup is opened
                    // this - is Magnific Popup object
                    $(".header-fix .htl-container").css("padding-right", "17px");
                    $(".header-fix .search-wrapper").css("padding-right", "17px");
                },
                close: function () {
                    // Will fire when popup is closed
                    $(".header-fix .htl-container").css("padding-right", "0");
                    $(".header-fix .search-wrapper").css("padding-right", "0");
                    $(".remove").removeClass("remove");
                }
            }
        });
    }


    // change page
    function loadProductQuickView(product_id,tab) {
        if (product_id == undefined) return false;
        if (product_id == undefined) tab = false;

        $("#quick-view_"+product_id).ajaxSubmit({
                //data : {"product_id": product_id},
                beforeSend : function () {
                    
                },
                success : function (data, textStatus, jqXHR) {
                    //console.log(data);
                    $(".quick-view-div").empty().append(data);
                    quickProductSlider();
                    //productQuickView();
                    productQuickViewOpen(tab);
                    fastOrderClick();
                    ajaxMode = true;
                    return false;
                }
        });

    }

    // Quick product slider
    function quickProductSlider() {
         $('.po-main-slider2').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            infinite: false,
            fade: true,
            asNavFor: '.po-slider-for-main2'
        });
        $('.po-slider-for-main2').slick({
            slidesToShow: 3,
            slidesToScroll: 1,
            draggable: true,
            asNavFor: '.po-main-slider2',
            dots: false,
            centerMode: false,
            infinite: false,
            focusOnSelect: true
        });
    }

    function productQuickViewOpen (tab) {
        if (tab == undefined) tab = false;
        $.magnificPopup.open({
              items: {
                    src: '#product-quick-view',
                    type: 'inline'
                },

                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-slide-bottom',
                callbacks: {
                    open: function () {
                        // Will fire when this exact popup is opened
                        // this - is Magnific Popup object
                        $(".header-fix .htl-container").css("padding-right", "17px");
                        $(".header-fix .search-wrapper").css("padding-right", "17px");
                        $('.po-main-slider2').slickPrev();

                        if (tab){
                            // Activate clicked tab
                            var e = $(".pqv-right");
                            e.find(".active").removeClass("active");
                            e.find("." + tab).addClass("active");
                            e.find("[data-tab='" + tab + "']").addClass("active");
                        }
                    },
                    close: function () {
                        // Will fire when popup is closed
                        $(".header-fix .htl-container").css("padding-right", "0");
                        $(".header-fix .search-wrapper").css("padding-right", "0");
                    }
                }
            });
    }

    // Category product tile height correct for long product name
        // product name height correct
        function correctHeight() {
            function maxNameheight(el, correct){
                var h_max = 0;
                if (typeof correct === "undefined" || correct === null) {
                    correct = true;
                }
                el.each(function(){
                    var h = $(this).height();
                    if (h>h_max) {
                        h_max = h;
                    }
                });
                if(correct){ el.height(h_max); }
                return h_max;
            }

            var el1 = $(".content-popular").find(".carousel-product-name");
            var el4 = $(".content-seen").find(".carousel-product-name");
            var el2 = $(".discount-element").find(".carousel-product-name");
            var el3 = $(".tile-view .carousel-element");
            var el5 = $(".tile-block").find(".carousel-product-name");
            maxNameheight(el1);
            maxNameheight(el2);
            maxNameheight(el4);
            var h_max = maxNameheight(el5);
            $(".tile-view").height(418 + h_max - 60);
            $(".wide.tile-view").height(416 + h_max - 60);
            //el3.css("min-height", h_max);
        }

    function cc_4Slick(){
        $('.cc_4').slick({
            dots: false,
            autoplay: false,
            autoplaySpeed: 5000,
            speed: 800,
            infinite: true,
            draggable: false,
            slidesToShow: 1,
            adaptiveHeight: false,
            slide: 'div',
            lazyLoad: 'ondemand'
        });
    }

    function fastOrderClick(){
        $(".eh-fast-order .header-button-top, .de-fast-order .header-button-top, .de-last-incart-set").magnificPopup({
            items: {
                src: "#product-quick-order",
                type: "inline"
            },
            fixedContentPos: true,
            fixedBgPos: true,
            overflowY: "auto",
            closeBtnInside: true,
            preloader: true,

            midClick: true,
            removalDelay: 300,
            mainClass: 'my-mfp-slide-bottom',
            callbacks: {
                open: function () {
                    // Will fire when this exact popup is opened
                    // this - is Magnific Popup object
                    $(".header-fix .htl-container").css("padding-right", "17px");
                    $(".header-fix .search-wrapper").css("padding-right", "17px");
                },
                close: function () {
                    // Will fire when popup is closed
                    $(".header-fix .htl-container").css("padding-right", "0");
                    $(".header-fix .search-wrapper").css("padding-right", "0");
                    $(".response").hide();
                    $(".request").show();
                }
            }
        });
    }

function youtubeOpen() {
   // youtube video popup

    $('.popup-youtube').magnificPopup({
        disableOn: 700,       
        type: "iframe",
        mainClass: 'my-mfp-slide-bottom',
        removalDelay: 160, 
        preloader: false,

        fixedContentPos: true,
        callbacks: {
            open: function () {
                // Will fire when this exact popup is opened
                // this - is Magnific Popup object
				var src = $('.popup-youtube').attr('data_href') ;
				$('.my-mfp-slide-bottom').find('iframe').attr('src',src);
				
                $(".header-fix .htl-container").css("padding-right", "17px");
                $(".header-fix .search-wrapper").css("padding-right", "17px");
            },
            close: function () {
                // Will fire when popup is closed
                $(".header-fix .htl-container").css("padding-right", "0");
                $(".header-fix .search-wrapper").css("padding-right", "0");
            }
        }
    });
}

// product quick view popup
    function productQuickView() {
             
            //$('.quick-look-a')
            $('.quick-look-a').magnificPopup({
                items: {
                    src: '#product-quick-view',
                    type: 'inline'
                },

                fixedContentPos: true,
                fixedBgPos: true,

                overflowY: 'auto',

                closeBtnInside: true,
                preloader: false,

                midClick: true,
                removalDelay: 300,
                mainClass: 'my-mfp-slide-bottom',
                callbacks: {
                    open: function () {
                        // Will fire when this exact popup is opened
                        // this - is Magnific Popup object
                        $(".header-fix .htl-container").css("padding-right", "17px");
                        $(".header-fix .search-wrapper").css("padding-right", "17px");
                        $('.po-main-slider2').slickPrev();
                        
                    },
                    close: function () {
                        // Will fire when popup is closed
                        $(".header-fix .htl-container").css("padding-right", "0");
                        $(".header-fix .search-wrapper").css("padding-right", "0");
                    }
                }
            });
            
            $('#product-quick-view').addClass("mfp-hide");

      //  },1000);
       //$('#product-quick-view').addClass("mfp-hide");


    //--->  


    }

	


 // autocomplet : this function will be executed every time we change the text
function autocomplet(e,index) {
    var min_length = 2; // min caracters to display the autocomplete
    var keyword = e.val();
    
    if (keyword.length >= min_length) {
        var post_data = {keyword : keyword};

        $.ajax({
            url: '/ajax/products.php?method=autocomplete',
            type: 'POST',
            data: post_data,
            success:function(data){
				
				//console.log(data);
				
                $('#search_sugestion_'+index).html(data);
                $('#search_sugestion_'+index).slideDown(400);
                $('#search_sugestion_'+index).mouseleave(function(){
                    $(this).slideUp(400);
                    return false;
                });

                $('.search-categories-menu').mouseenter(function(){
                    $('#search_sugestion_'+index).slideUp(0);
                    return false;
                });
				
            }
        });
    } else {
        
        $('#search_sugestion').hide();
    }
}
 
// set_item : this function will be executed when we select an item
function set_item(item, href) {
    // change input value
    $('#product_search').val(item);
    // hide proposition list
    $('#search_sugestion').hide();
    window.location = href;
}