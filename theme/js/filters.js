function reloadProducts(page, sort, onPage) {
	var formId = '#filter-form';
	var productsFieldId = '#products-list';
	
	//var loadingImage = $('#filter-loading').clone();
	//var ss;
	
	if (page == undefined) 
		page = (defaultPage == undefined) ? 1 : defaultPage;
	if (sort == undefined || sort == null) {
		sort = (defaultSort == undefined) ? 'none' : defaultSort;
	}

	
	$(formId).ajaxSubmit({
		data : {"page" : page, "sort" : sort, "onPage" : onPage},
		beforeSend : function () {

			$(productsFieldId).css('opacity', .3);
			//show loading
			//loadingImage.appendTo('body');
			/*loadingImage.css({
				'display' : 'block',
				'position' : 'absolute',
				'left' : $(productsFieldId).position().left + 20,
				'top' : $(productsFieldId).position().top
			});
			ss = $('body, html').mousemove( function (e) {
				loadingImage.css({
					'left' : (e.pageX + 10) + 'px',
					'top' : (e.pageY + 20) + 'px'
				});
			}); */
		},
		success : function ( data, statusText, xhr, element) {
				//console.log(data);
				$(productsFieldId).empty().append(data);
				
				//Replace old paginator
				$('#paginator-container').empty();
				var paginDiv = $(".f-pages").clone();
				$('.f-pages').remove();
				
				$('#paginator-container').html(paginDiv);

				$(productsFieldId).css('opacity', 1);
				
				//Hide loading
				//ss.unbind('mousemove');
				//loadingImage.remove();
				
				ajaxMode = true;
				
				correctHeight();
				$(".quick-look-a").click(function(){
			         var product_id = $(this).parent().find(".buyLink").data("product_id");
			         //console.log(product_id);
			        loadProductQuickView(product_id);
			    });
				//productQuickView();
				$(".cc_4").unslick();
				cc_4Slick();
				fastOrderClick();
				if ($(".tile-list.active").data("view") == 'list'){
		            $(".tile-view").hide();
		            $(".list-view").show();
		            $(".list-block2").toggleClass("not-visible");
				}

				$(".page-num a, .f-pages.count a").unbind('click');
				$(".page-prev, .page-next").unbind('click');
				//������������ ������� �� ����������
				$('#sort-filter a').unbind('click').bind('click', function() {
					var sort = $(this).data('value');
					if (sort == 'price_asc'){
						$(this).data('value','price_desc');
					}else{
						$(this).data('value','price_asc');
					}
					reloadProducts(page, sort);
					defaultSort = sort;
					return false;
				}); 
		}
	});
	return false;
}

// change page
function reloadPage(page, sort) {
	if (page == undefined) 
		page = (defaultPage == undefined) ? 1 : defaultPage;
	if (sort == undefined) {
		sort = (defaultSort == undefined) ? 'none' : defaultSort;
	}

	$.ajax({
			url: '/ajax/products.php?method=cat',
			type : 'POST',
			dataType : 'json',
			data : {"page" : page,'sort': sort},
			beforeSend : function () {
				
			},
			success : function (data, textStatus, jqXHR) {
				
			}
	});

}