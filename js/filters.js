function reloadProducts(page, sort) {
	var formId = 'filter-form';
	var productsFieldId = 'products-field';
	
	var loadingImage = $('#filter-loading').clone();
	var ss;
	
	if (page == undefined) 
		page = (defaultPage == undefined) ? 1 : defaultPage;
	if (sort == undefined) {
		sort = (defaultSort == undefined) ? 'none' : defaultSort;
	}
	$('#' + formId).ajaxSubmit({
		data : {"page" : page, "sort" : sort},
		beforeSend : function () {
			$('#' + productsFieldId).css('opacity', .5);
			//show loading
			loadingImage.appendTo('body');
			loadingImage.css({
				'display' : 'block',
				'position' : 'absolute',
				'left' : $('#' + productsFieldId).position().left + 20,
				'top' : $('#' + productsFieldId).position().top
			});
			ss = $('body, html').mousemove( function (e) {
				loadingImage.css({
					'left' : (e.pageX + 10) + 'px',
					'top' : (e.pageY + 20) + 'px'
				});
			});
		},
		success : function ( data, statusText, xhr, element) {
				$('#' + productsFieldId).html(data);
				$('#' + productsFieldId).css('opacity', 1);
				
				//Hide loading
				ss.unbind('mousemove');
				loadingImage.remove();
				$('#sort-panel .sort-asc, #sort-panel .sort-desc, #sort-panel .sort-news-sale').unbind('click');
				//������������ ������� �� ����������
				$('#sort-panel .sort-asc').click(function () {
					reloadProducts(page, 'price_asc');
					defaultSort = 'price_asc';
					return false;
				});
				$('#sort-panel .sort-desc').click(function () {
					reloadProducts(page, 'price_desc');
					defaultSort = 'price_desc';
					return false;
				});
				$('#sort-panel .sort-news-sale').click(function () {
					reloadProducts(page, 'news_sale');
					defaultSort = 'news_sale';
					return false;
				});
		}
	});
	return false;
}

$(function() {
	$( "#slider-range" ).slider({
		range: true,
		min: price.min,
		max: price.max,
		step: 50,
		values: [ price.min, price.max ],
		slide: function( event, ui ) {
			$( "#amount1" ).val( ui.values[ 0 ]);
			$( "#amount2" ).val( ui.values[ 1 ]);
		},
		stop: function() { reloadProducts(); }
	});
	$( "#slider-range" ).slider( "values",0, $("#amount1").val() );
	$( "#slider-range" ).slider( "values",1, $("#amount2").val() );
	$('#amount1').keypress(function(event){
			var keyCode = event.keyCode ? event.keyCode :
			event.charCode ? event.charCode :
			event.which ? event.which : void 0;
	 
			//if pressed "Enter" key
			if(keyCode == 13){
				if(parseInt($(this).val())>parseInt($("#amount2").val().replace(' ',''))){$(this).val(parseInt($("#amount2").val().replace(' ','')));}
				$( "#slider-range" ).slider( "values",0, $(this).val() );
				defaultSort = 'price_asc';
				reloadProducts();
			}
	});
	$('#amount2').keypress(function(event){
			var keyCode = event.keyCode ? event.keyCode :
			event.charCode ? event.charCode :
			event.which ? event.which : void 0;
	 
			//if pressed "Enter" key
			if(keyCode == 13){
				if(parseInt($(this).val())<parseInt($("#amount1").val().replace(' ',''))){$(this).val(parseInt($("#amount1").val().replace(' ','')));}
				$( "#slider-range" ).slider( "values",1, $(this).val() );
				defaultSort = 'price_asc';
				reloadProducts();
			}
	});
	$( "#amount1" ).change(function(){
		if(parseInt($(this).val())>parseInt($("#amount2").val().replace(' ',''))){$(this).val(parseInt($("#amount2").val().replace(' ','')));}
		$( "#slider-range" ).slider( "values",0, $(this).val() );
		defaultSort = 'price_asc';
		reloadProducts();
	});
	$( "#amount2" ).change(function(){
		if(parseInt($(this).val())<parseInt($("#amount1").val().replace(' ',''))){$(this).val(parseInt($("#amount1").val().replace(' ','')));}
		$( "#slider-range" ).slider( "values",1, $(this).val() );
		defaultSort = 'price_asc';
		reloadProducts();
	});
	
	//Checkbox
	$('.hidden-checkbox').css('opacity', 0);
	$('.hidden-checkbox').click(function() {
		if ($(this).is(':checked')) {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-on');
		} else {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-off');
		}
		reloadProducts();
	});
});