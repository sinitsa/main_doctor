/**
 * Базовая функция валидации почты
 */
function _jqFFVEmail(v){
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(v);
}

/**
 * Базовая функция валидации телефона
 */
 function _jqFFVPhone(v){
  if (!v) return false
  return /^[\d\+\s()-]*$/.test(v)
}

/**
 * Функция инициализирует хинт для форм валидации да и вообще
 */
 function _initFormValidHint(pF){
 	var hint = $('<div class="gbph-hint">').css({
		'background-color'	: '#FFFFFF',
		'border'			: '1px solid #db4a4b',
		'color'				: '#333',
		//'font-weight'		: 'bold',
		'padding'			: '2px 9px',
		'position'			: 'absolute',
		'z-index'			: '1000'
	 })
	 hint.append(
 		hint.hcaption = $('<div class="gbph-caption">').css({
 			'color'			: '#000',
			'font-size'		: '11px',
			//'font-weight'	: 'bold',
			'text-align'	: 'center'
 		}), 
 		$('<div class="gbph-top-arrow">').css({
 			'top'			: '-6px',
			'position'		: 'absolute'
 		}).append(
 			$('<div class="gbph-ta D">').css({
 				'content'			: '""',
				'display'			: 'block',
				'height'			: '0',
				'position'			: 'absolute',
				'width'				: '0',
				'border'			: '6px solid',
				'border-color'		: '#FFFFFF transparent',
				'left'				: '-6px',
				'border-top-width'	: '0'
 			}), 
			$('<div class="gbph-ta C">').css({
				'content'			: '""',
				'display'			: 'block',
				'height'			: '0',
				'position'			: 'absolute',
				'width'				: '0',
				'border'			: '5px solid',
				'border-color'		: '#fb4400 transparent',
				'left'				: '-5px',
				'border-top-width'	: '0',
				'top'				: '1px'
			})
 		)
 	)
 	$(function(){
 		$('body').append(hint)	
 	})
 	/*var pos = pF.offset()
 	pF.css('resize', 'none')
 	hint.css({
 		'top'	: pos.top + pF.outerHeight(), 
 		'left'	: pos.left + 2
 	})*/
 	hint._hide = hint.hide
 	var pFbColor = 
 	hint.hide = function(){
 		pF.css({
 			'background': 'white'
 		}).removeAttr('data-unvalid')
 		hint._hide()
 	}
 	hint._show = hint.show
 	hint.show = function(){
 		var pos = pF.offset()
 		//console.log(pos)
 		hint.css({
	 		'top'	: pos.top + pF.outerHeight(), 
	 		'left'	: pos.left + 2
 		})
 		pF.css({
 			'background': '#fff'
 		}).attr('data-unvalid', 'yes')
 		hint._show()
 	}
 	hint.hide()
 	return hint
 }

/**
 * Функция валидации полей форм
 * @param jQuery запрос jQuery на требуемое поле
 * @param validFunc {Function} функция проверки валидации (Enter: Field value, return true/false)
 * @param errMsg {string} сообщение которое появляется рядом с полем  в случае ошибки
 */
function _jqFormFieldValidInit(jQuery, validFunc, errMsg){
	//var hint = _initOnceFormValidDom()
	function errState(state){
		if(state){
			field._errHint.hide()
		}else{
			field._errHint.show()	
		}
	}
	$(jQuery).each(function(){
		var me = $(this)
		me.blur(function(){
			if(me.length){
				validFunc(me.val()) ? me._errHint.hide() : me._errHint.show()
			}
		})
		me._errHint = _initFormValidHint(me)
		me._errHint.hcaption.html(errMsg)
	})
}


/**
 * Устанавливает максимальную длину значения поля
 * @param jQuery запрос jQuery на требуемое поле
 * @param fieldLen {integer} макс длина значения поля
 */
function _jqSetFormFieldValLength(jQuery, fieldLen){
	$(jQuery).attr('maxlength', fieldLen)
}

/**
 * инициализация формы на валидацию при попытке отправки
 * @param jQuery запрос jQuery на форму
 * @param fieldLen {integer} макс длина значения поля
 */
function _initFormValidOnSubmit(jQuery){
	var me = $(jQuery)
	me.submit(function(){
		var 
		haveErr = 0, 
		inputF = me.find('input, select, textarea')
			.trigger('blur')
			.each(function(){
				$(this).attr('data-unvalid') ? haveErr = 1 : 0
			})
		//console.log(inputF)
		//return false
		return !haveErr
	})
}