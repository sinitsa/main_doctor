/*
 * Copyright (c) 2011 Maksym Mukovoz (inshop.com.ua)
 *
 * 1. ����� ������� ���������� ������
 *  ���� �������� after �� before
 * 2. ����� ��� ����������� �������
 * error_message
 * 3. �������� ���� �������
 * .html('������������ ����'));
 *
 * jQuery Simple Form Validator
 */

$(function() {
  $('form').each(function(i, form) {
    $(form).submit(function() {

      var valid = true;
      $('div.error_message').remove();

      $(form).find(':input:visible:not(input[type=image], input[type=submit])').each(function(j, field) {
        $(field).css('border-color', '#DADADA');

        var required = $(field).attr('required');
        required = (required === true) ? 'no' : required;

        if ((required == 'undefined' || required != 'no') && $(field).val() == '') {
          valid = false;

          $(field).css('border-color', 'red').after($('<div>')
            .attr('class', 'error_message')
            .html('������������ ����'));
        }
      });


      if (!valid) {
        return false;
      }
    });
  });
});