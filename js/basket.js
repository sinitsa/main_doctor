function setCookie (name, value, expires, path, domain, secure) {
      document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}
function getCookie(c_name) {
	if (document.cookie.length > 0 ) {
		c_start = document.cookie.indexOf(c_name + "=");
		if (c_start != -1) {
			c_start = c_start + c_name.length+1;
			c_end = document.cookie.indexOf(";", c_start);
			if (c_end == -1) c_end = document.cookie.length;
			return unescape(document.cookie.substring(c_start,c_end));
		}
	}
	return "";
}

function parseProductsString( string ) {
	var returnArray = new Array();
	
	if (string.length > 0 ) {
		var products = string.split('|');
		for (var i in products) {
			var temp = products[i].split(':');
			
			returnArray.push({
				'id' : parseInt(temp[0]),
				'amount' : parseInt(temp[1])
			});
		}
	}
	return returnArray;
}
function generateProductsString( products ) {
	var tempArray = new Array();
	for (var i in products) {
		tempArray.push(products[i].id + ':' +  products[i].amount);
	}
	return tempArray.join('|');
}
function addProductToBasket(productId, price, type, _this) {
	//������� ������ �� ���
	var products = parseProductsString( getCookie('cart_products') );
	var totalAmount = parseInt(getCookie('total_amount'));
	if (!isNaN(totalAmount)) {
		totalAmount += price;
	} else {
		totalAmount = price;
	}
	
	//���� ����� ��� ���� � �����, ��������� ��� ����������
	//������ ���������� ���������� ���� �������
	var foundSame = false;
	var productsCount = 0;
	for (var i in products) {
		if (products[i].id == productId) {
			products[i].amount++;
			foundSame = true;
		}
		productsCount += products[i].amount;
	}
	if (!foundSame) {
		products.push({
				"id" : productId,
				"amount" : 1
			});
		productsCount += 1;
	}
	
	/*//�������������� ������ ����� �������� ���
	var date = new Date();
	date.setTime(date.getTime()+(7200));
	var expires = date.toGMTString();
	*/
	
	setCookie('cart_products', generateProductsString(products), '', '/');
	setCookie('total_amount', totalAmount, '', '/');
	//�������� ����������
	setBasketInfo(productsCount, totalAmount);
	
	if (type == 'list') {
		//������� ��������
		var image = $(_this).closest('.divkatlist').find('img.imglist');
	} else if (type == 'catalog') {
		var image = $('#imgcatalog');
	}
	animateAdding(image, type);
}

function setBasketInfo(amount , summ) {
	$('#basket-summ-price').text(summ);
	$('#basket-summ-products').text(amount);
}
//addProductToBasket(Math.round(Math.random() * 10), 100);

function animateAdding(imageElement, type) {
	var pic = imageElement;
	var pX = $('#basket').position().left;
	var pY = $('#basket').position().top;
	var basketImage = $('#basket img');
	var basketImageClone = basketImage.clone();
	
	basketImageClone.appendTo('body').css({
		"position" : "absolute",
		"top" : basketImage.position().top,
		"left" : basketImage.position().left
	});
	
	var size = getNewSize(pic.width(), pic.height(), $('#basket').width(), $('#basket').height());
	var destWidth = size.width;
	var destHeight = size.height;
	
	
	var scrollElement = $("html,body");
	var scroll = $("html").scrollTop() || $("body").scrollTop() ;

	var img = pic.clone();
	img.css({
		"position" : "absolute",
		"left" : pic.position().left + 'px',
		"top" : pic.position().top + 'px',
		"opacity" : 0.4
	});

	scrollElement.animate({
		"scrollTop" : 0
	}, 400, function() {
		img.appendTo('body');
		basketImageClone.animate( {
			"width" : "+=10px",
			"left" : "-=5px"
		}, 150);
		img.animate({
			"left" : pX,
			//hack with height
			"top" : pY + (type == 'list' ? destHeight : 0),
			"width" : destWidth,
			"height" : destHeight
		},
		150,
		function () {
			basketImageClone.animate( {
				"width" : "-=10px",
				"left" : "+=5px"
			}, 150);
			img.animate({
				"opacity" : 0
			}, 150,
			function () {
				img.remove();
				basketImageClone.remove();
				
				scrollElement.animate({
						"scrollTop" : scroll
				}, 400);
			});
		});
	});
}
function recalculateBasket() {
	var productsCount = 0;
	var priceSumm = 0;
	var products = new Array();
	
	var _break = false;
	$('.product-item').each(function (index, element) {
		if (_break == false) {
			var amount = $(element).find('.kolvoinput').val();
			var price = $(element).find('.priceinput').val();
			var pid = $(element).find('.idinput').val();
			
			if (amount == '0') {
				$(element).find('.kolvoinput').val(1);
				amount = '1';
			}
			
			if (amount.match(/^[0-9]{1,3}$/)) {
				amount = parseInt(amount);
				productsCount += amount;
				priceSumm += parseInt(price) * amount;
				products.push({
					"id" : pid,
					"amount" : amount
				});
			} else {
				productsCount = '-';
				priceSumm = '-';
				_break = true;
			}
		}
	});
	//�������� ������ �� �������� �������
	$('#basket-products-count').text(productsCount);
	$('#basket-price-summ').text(priceSumm);
	//���� ������ �� ����
	if (_break == false) {
		//�������� ������ � ����� �������
		setBasketInfo(productsCount, priceSumm);
		//���������� ����
		setCookie('cart_products', generateProductsString(products), '', '/');
		setCookie('total_amount', priceSumm, '', '/');
		
		//������ �� ������
		$.ajax({
			url: '/ajax/price.php?method=getpriceanddiscount',
			type : 'POST',
			dataType : 'json',
			data : {"price" : priceSumm, "amount" : productsCount},
			beforeSend : function () {
				$('#discount, #discount_value, #price_total').html('<img src="/img/ajax_loading.gif" />');
			},
			success : function (data, textStatus, jqXHR) {
				$('#discount').text(data.global_discount_string);
				$('#discount_value').text(data.price_diff);
				$('#price_total').text(data.price_total);
			},
			error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
	if (_break == false && productsCount > 0) {
		$('#continue-order').removeAttr('disabled');
	} else {
		$('#continue-order').attr('disabled','disabled');
	}
}
function deleteProductsFromBasket(id) {
			
	$('#delete-confirm').appendTo('body').show();
	
	var background = $('<div></div>').css({
		"position" : "fixed",
		"width" : "100%",
		"height" : "100%",
		"top" : "0",
		"left" : "0",
		"background" : "#000",
		"opacity" : 0.5,
		"zIndex" : "3"
	}).appendTo('body');
	
	var name = $('#product-item-' + id + ' .product-link').text();
	var src = $('#product-item-' + id + ' .product-image').attr('src');
	$('#delete-confirm .name').text(name);
	$('#delete-confirm .image img').attr('src', src);
	
	$('#delete-confirm .delete').click(function () {
		$('#product-item-' + id).remove();
		recalculateBasket();
	
		//
		background.remove();
		$('#delete-confirm').hide();
		$('#delete-confirm .delete').unbind('click');
		$('#delete-confirm .cancel').unbind('click');
	});
	$('#delete-confirm .cancel').click(function () {
		//
		background.remove();
		$('#delete-confirm').hide();
		$('#delete-confirm .delete').unbind('click');
		$('#delete-confirm .cancel').unbind('click');
	});
}

function selectDeliveryType(type) {
	$('.radio-button').each(function(index, element) {
		element.src = '/img/gal.gif';
	});
	$('#delivery-type-' + type).find('.radio-button').attr('src', '/img/galvkl.gif');
	$('#delivery-type-input').val(type);
}

function getNewSize(oldWidth, oldHeight, destWidth, destHeight) {
	if (destWidth) {var iScaleW = destWidth / oldWidth;} else {var iScaleW = 1;}
	if (destHeight) {var iScaleH = destHeight / oldHeight;} else {var iScaleH = 1;}

	var iSizeRelation = ( (iScaleW < iScaleH) ? iScaleW : iScaleH);
	var iWidthNew = Math.round(oldWidth * iSizeRelation);
	var iHeightNew = Math.round(oldHeight * iSizeRelation);

	var iSizeW = iWidthNew;
	var iSizeH = iHeightNew;
	iDestX =0;
	iDestY =0;
	
	return {"width" : iSizeW, "height" : iSizeH};
}