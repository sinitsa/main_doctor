<?php

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;

class SearchModel extends BaseModel 
{
    private $cl;
    
    function __construct() {
        $this->sphinxInit();
    }
    
    /**
     * �������������  sphinx
     */
    private function sphinxInit() {
        
        // ��������� ����������      
        require_once (ROOT_DIR . 'oc_app/components/sphinxapi.php');
       
        $this->cl = new \SphinxClient();
		// ���������
        $this->cl->SetServer("127.0.0.1", 3312);
		$this->cl->SetConnectTimeout( 1 );
		$this->cl->SetMaxQueryTime(1000);
		$this->cl->SetLimits(0, 5, 5);
		$this->cl->SetMatchMode(SPH_MATCH_ANY);
		// $cl->SetMatchMode(SPH_MATCH_EXTENDED2);
		$this->cl->SetSortMode(SPH_SORT_RELEVANCE);
		$this->cl->SetFieldWeights(array('title' => 20, 'des' => 10));
    }
    /**
     * �����  sphinx 
     * @param type $query
     * @return array (������ id �������� �����)
     */
    private function sphinxSearch($query) {
        
		$result = $this->cl->Query($query,"armed");
		if( $result === false ){
			if( $this->cl->GetLastWarning() ) {
				die('WARNING: '.$this->cl->GetLastWarning());
			}
            return [];
			//die('ERROR: '.$this->cl->GetLastError());
		}
        return array_keys($result['matches']);
    } 
    
    /**
     * ����� ������ ������� �� ������� �� ������
     * @param string $query
     * @return array
     */
    public function getProducts($query) {
        
        if (empty($query)) {
            return [];
        } 
            
        //$sphinxResult = $this->sphinxSearch($query);
        
       

              
			//$search_text = iconv('windows-1251','utf8',$query);	  
			$keyword = '%'.$query.'%';
			 $products=[];
			$query = "SELECT `id` FROM `catalog` WHERE `title` LIKE '{$keyword}'";
			$sql = DB::query($query);
			
			while ($row = $sql->fetch(\PDO::FETCH_ASSOC)) {
				
                $list[] = $row["id"];
            } 
			
			
			
			 // if (count($sphinxResult)) {
           $list = implode(',', $list);
            $sql = DB::query(
            "SELECT
                P.`id`,
                P.`title`,
                P.`price`,
                P.`chpu`,
                P.`fake_in_stock`,
                R.`amount` 
            FROM
                `catalog` AS P
            LEFT JOIN 
                storage_rests AS R
            ON 
                R.`catalog_id` = P.`id`    
            WHERE 
                `id` in ({$list}) 
            ORDER BY FIELD (`id`, {$list})");
			
			
			
			//var_dump($sql);
			while ($row = $sql->fetch(\PDO::FETCH_ASSOC)) {
				
                $products[] = $row;
            } 
			
			
            return $products;
        //}
       // return [];   
    }   
}
