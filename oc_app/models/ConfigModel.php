<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
/**
* 
*/
class ConfigModel extends BaseModel
{
	
	public function get($string) {
		$string = explode('.', $string);
		$query = DB::prepare("SELECT `value` FROM `config` WHERE `key_group` = :group AND `key` = :key LIMIT 1");
		$query->execute([':group' => $string[0], ':key' => $string[1]]);
		$result = $query->fetch(\PDO::FETCH_ASSOC);
		return $result['value'];
	}

	public function set() {
		return false;
	}
}