<?php 

namespace app\models;

use core\lib\BaseModel;
use core\lib\DB;
/**
* 
*/
class SamovyvozModel extends BaseModel {

	public $dbdata;
	
	function __construct($index) {
		
		if(!empty($index))
		{
			$this->getPointDelivery($index);
		}	
		else
		{
			$this->getPointsDelivery();
		}	
		
	}
	
	//Получение списка всех пунктов самовывоза
	public function getPointsDelivery() {
		$points = [];
		$query = DB::query("SELECT * FROM `deliveryPickups`");
		while ($row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$points[] = $row;
		}
		
		$this->dbdata['points'] = $points;
		return $points;
	}

	// Получение конкретной точки самовывоза
	public function getPointDelivery($index) {
		$key = 'chpu';  
		$query = DB::prepare(
			"SELECT 
				*
			FROM
				`deliveryPickups`
			WHERE 
				`{$key}` = ?
			LIMIT 1");
		$query->execute([$index]);
		$this->dbdata['point'] = $query->fetch(\PDO::FETCH_ASSOC);
		return $query->fetch(\PDO::FETCH_ASSOC);
	} 

}