	<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">�����</a>
		<!-- <a href="javascript: void(0);">������</a> -->
	</div>
</div>
<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">�������</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<h2 class="cat_title">���� ������� (<span class="count_order"><?=$data['title'] ?></span>)</h2>
</div>
<div>
	<form action="" method="POST" class="make_order" style="<?=($data['basket']['count'] <= 0) ? "display:none;" : ""?>">
		<div class="list">
			<?php foreach ($data['products'] as $key => $item): ?>
				<div class="item">
					<div class="head">
						<?=$item['title']?>
					</div>
					<div class="body">
						<a href="/medtehnika/<?=$item['chpu']?>" class="image">
							<img  class="lazy" src="http://www.doctor-center.ru/upload/small/<?=$item['id']?>.jpg" alt="<?=$item['title']?>">
						</a>
						<div class="control">
							<div>
								<a href="javascript: void(0);" class="dec"></a>
								<input type="text" data-id="<?=$item['id'] ?>" name="count" value="<?=$item['count'] ?>">
								<a href="javascript: void(0);" class="inc"></a>
							</div>
							<div>
								<a class="del_product" data-title="<?=$item['title']?>" data-cat_title="<?=$item['cat_title']?>" data-id="<?=$item['id']?>" data-price="<?=$item['price']?>"href="javascript: void(0);">������� �����</a>
							</div>
						</div>
						<div class="price">
							<?=number_format($item['price'], 0, ',', ' ')?> ���.
						</div>
					</div>
				</div>
			<?php endforeach ?>
		</div>
		<div class="list_summ">
			<div></div>
			<div>
				<p>����� ������:</p>
				<p><span class="tsumm"><?=number_format($data['products_summ'], 0, ',', ' ')?></span> ���.</p>
			</div>
		
		</div>
		
		<div class="container">
			<h3>����������:</h3>
			<div class="info">
				<input type="text" class="inp_name" name="name" placeholder="��� *">
				<input type="tel" class="inp_phone" name="phone" placeholder="+7 (___) ___-__-__*">
				<input type="email" class="inp_email" name="email" placeholder="�mail:">
				<input type="text" class="inp_adress" name="adress" placeholder="����� ��������:">
				<!--<label>���� ��������:</label>
				<input type="date" class="inp_date_delivery" name="date_delivery" placeholder="���� � ����� ��������:">
				<label>����� �������� c:</label>
				<input type="time" class="inp_time_ot" name="time_ot" placeholder="���� � ����� ��������:">
				<label>��:</label>
				<input type="time" class="inp_time_do" name="time_do" placeholder="���� � ����� ��������:">-->
				<input type="number" class="inp_index" name="index" placeholder="������:">
			</div>
			<h3>������:</h3>
			<div class="pay_type">
				<div class="check_pay_type" data-type="2">
					<div class="icon bk"></div>
					<p>����������� ������</p>
				</div>
				
				<div class="check_pay_type active" data-type="1">
					<div class="icon nal"></div>
					<p>������ ���������</p>		
				</div>	
			</div>
			<h3>��������:</h3>
			<div class="delivery_type">
				<div class="delivery_wrapper min">
					<a href="javascript: void(0)" class="control2"></a>
					<ul class="dropdown ">
						<li data-id="1" class="del_type_item active">�������� ��������</li>
						<!--<li data-id="2" class="del_type_item">�������� �� ���������� �������</li>-->
					<!--	<li data-id="4" class="del_type_item">����������� ���������</li>-->
						<li data-id="5" class="del_type_item">���������</li>
					</ul>
				</div>
			</div>
			<h3>����������� � ������:</h3>
			<div class="comment_order">
				<textarea name="order_comment" cols="30" rows="10" placeholder="����������� � ������"></textarea>
			</div>
		
			<div class="finish_summ">
				<div>
					<div>
						����� �����:
					</div>
					<div>
						<span class=""><span class="tsumm red"><?=number_format($data['products_summ'], 0, ',', ' ')?></span> ���.</span>
					</div>
				</div>
				<div>
					<div>
						��������:
					</div>
					<div class="delivery_summ">
						<?php if ($data['products_summ'] < 2000): ?>
							<span class="red">300</span> ���.
						<?php else: ?>
							���������
						<?php endif ?>
					</div>
				</div>
			</div>
			<p class="error_warrning">
				��� ���������� ����� ���� �������� ������:
			</p>
			<ul class="error_messages">
				<li class="name_mes_warrning">��� ������� �� �����, ��� �����������</li>
				<li class="phone_mes_warrning">������� ������ �� �����</li>
				<li class="email_mes_warrning">email ������ �� �����</li>
				<li class="email2_mes_warrning">��� ������ ������ ���������� ������ ����������� ����������� Email</li>
			</ul>
			<a href="javascript: void(0);" class="finish_order_btn">��������</a>
			<input type="hidden" name="orderid" value="">
		</div>
	</form>
</div>
<div class="bottom_separator"></div>
<div class="container">
	<?php
        //  ���� ���������� � �����: ��������, ����� ������, ������� ������
        include 'widgets/site_info.tpl.php'
    ?>
</div>