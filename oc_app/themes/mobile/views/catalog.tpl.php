<?php /*
<div class="top">
	<div>
		<a class="back" href="javascript: void(0);">�����</a>
		<a class="next" href="javascript: void(0);">��������� �����</a>
	</div>
</div>
 */
?>

<div class="container">
	<ul class="breadcrumbs">
		<li><a href="/">�������</a> </li>
		<?php foreach ($data['breadcrumbs'] as $value): ?>
			<li>&nbsp;- <a href="<?=$value['link']?>"><?=$value['title']?></a></li>
		<?php endforeach ?>
	</ul>
	<h1 class="cat_title"><?=$data['title'] ?></h1>
    <div class="cart">
        <div class="info">
            <div class="price">
                <span>����: </span>
                <span> <?=$data['price']?></span>
                <span>���. </span>
            </div>
            <div class="available">
                <?php if ((filter_var($data['amount']) > 0) || (filter_var($data['fake_in_stock']) == 1)): ?>
				<div class="info_flags_true available">� �������</div>
				<?php else: ?>
				<div class="info_flags_false available">�� �����</div>
				<?php endif ?>
            </div>
        </div>
        <div class="lazy_slider">
            <?php foreach ($data['images'] as $image): ?>
            <div class="image"><img class="s_element" data-lazy="<?=$image ?>"></div>
            <?php endforeach ?>               
        </div>
        <a class="in_basket" data-id="<?=$data['id']?>" data-title="<?=$data['title']?>"  data-cat_title="<?=$data['cat_title']?>"  data-price="<?=$data['price']?>" href="javascript: void(0);">� �������</a>
    </div>
</div>
<div class="panel2">
    <div class="block">
        <div class="delivery">
            <span>�������� �� ������:</span>
            <?php if($data['price'] >= 2000): ?>
            <span>���������</span>
            <?php else: ?>
            <span>�� 300 ���.</span>    
            <?php endif; ?>   
        </div>
        <div class="time_delivery">
            <span>� �������:</span>
            <span>1-3 ����</span>
        </div>
    </div>
    <div class="separator">
        
    </div>
</div>
<div class="product_des">
    <div class="subtitle">
        <h3>��������</h3>
    </div>
    <div class="wrapper">
        <div class="t1 active">
            <div class="des_area">
                <?=$data['describtion'];?>
            </div>
        </div>
    </div>
    <?php if (!empty($data['features'])):?>
        <div class="subtitle">
            <h3>�����������</h3>
        </div>
    <div class="wrapper">
        <div class="t1 feat_area active">
            <?php foreach($data['features'] as $key => $list): ?>
            <h4><?=$key?></h4>
            <ul>
                <?php foreach($list as $item): ?>
                <li>
                    <span><?=$item ?></span>
                </li>
                <?php endforeach;?>
            </ul>
            <?php endforeach; ?>
        </div>
    </div>
    <?php endif;?>
</div>

<!-- Callback button -->



<!-- feedbacks -->
<?php if (!empty($data['feedbacks'])): ?>
<div class="subtitle">
    <h3>������ �����������</h3>
</div>    
<div class="feedbacks">
    <?php foreach ($data['feedbacks'] as $item): ?>
    <div class="wrap">
        <div class="head">
            <span class="name"><?=$item['name']?></span>
            <span class="date"><?=$item['date']?></span>
        </div>
        <div class="body">
            <?=$item['content']?>
        </div>
    </div>   
    <?php endforeach ?>
</div>   
<?php endif ?>
<!-- Personal recommendation -->
<?php if (!empty($data['recommendProducts'])): ?>
<div class="subtitle">
    <h3>������������ ������������</h3>
</div>    
<div class="bot_listing">
    <?php foreach ($data['recommendProducts'] as $item): ?>
    <div class="element">
        <a class="img" href="/medtehnika/<?=$item['chpu']?>.html">
            <img class="lazy" data-src="http://www.doctor-center.ru/upload/medium/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
        </a>
        <p class="name"><?=$item['title']?></p>
        <p class="price"><?=$item['price']?> ���.</p>
    </div>    
    <?php endforeach ?>
</div>
<?php endif ?>
<!-- Another Products -->
<?php if (!empty($data['anotherProducts'])): ?>
<div class="subtitle">
    <h3>������� ������</h3>
</div>    
<div class="bot_listing">
    <?php foreach ($data['anotherProducts'] as $item): ?>
    <div class="element">
        <a class="img" href="/medtehnika/<?=$item['chpu']?>.html">
            <img class="lazy" data-src="http://www.doctor-center.ru/upload/medium/<?=$item['id']?>.jpg" src="" alt="<?=$item['title'] ?>">
        </a>
        <p class="name"><?=$item['title']?></p>
        <p class="price"><?=$item['price']?> ���.</p>
    </div>    
    <?php endforeach ?>
</div>
<?php endif ?>

<div class="bottom_separator"></div>

<div class="container">
    <?php
        //  ���� ���������� � �����: ��������, ����� ������, ������� ������
        include 'widgets/site_info.tpl.php'
    ?>
</div>