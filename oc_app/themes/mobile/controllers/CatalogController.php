<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\ProductModel;
use app\models\BasketModel;

/**
* 
*/
class CatalogController extends BaseController
{

	function action_index(){

		$config = new ConfigModel();	
		$menu = new MenuModel();
        $product = new ProductModel($this->content);
        
		$data = [
			'basket' => BasketModel::getData(),
			'phone' => $config->get('site.phone'),
			'phone_spb' => $config->get('site.phone2'),
			'phone_ru' => $config->get('site.phone_ru'),
			'work_mode'=> $config->get('site.work_mode'),
			'id' => $product->dbdata->id,
            'title' => $product->dbdata->title,
            'images' => $product->getImages(),
            'fake_in_stock' => $product->dbdata->fake_in_stock,
            'amount' => $product->dbdata->amount,
            'price' => $product->dbdata->price,
            'cat_title' => $product->dbdata->cat_title,
			'menu' => $menu->getMenu(),
			'pagesMenu' => PageModel::getMenu(),
			'breadcrumbs' => $product->getBreadcrumbs(),
			'seo' => [
				'title' => $product->dbdata->seo_title, 
				'describtion' => $product->dbdata->seo_des, 
				'keywords' => $product->dbdata->seo_key
			],
            'features' => $product->getFeatures(),
            'describtion' => $product->dbdata->des,
            'feedbacks' => $product->getFeedbacks(),
            'anotherProducts' => $product->getAnotherProducts(4),
            'recommendProducts' => $product->getRecommendProducts($product->dbdata->id, 4),
		];
        // echo '<pre>';
        // print_r($product->getRecommendProducts($product->dbdata->id, 4));
        // echo '</pre>';

		$this->view->render('index', 'catalog', $data);
	} 
}