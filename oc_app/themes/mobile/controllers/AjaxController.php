<?php 
namespace app\themes\mobile\controllers;
use core\lib\BaseController;
use app\models\BasketModel;

/**
* 
*/
class AjaxController extends BaseController {

	public static function iconvArray ($inputArray,$newEncoding){

			$outputArray=array ();

			if ($newEncoding!=''){

			if (!empty ($inputArray)){

			foreach ($inputArray as $key => $element){

			if (!is_array ($element)){

			$element=iconv ("windows-1251", $newEncoding,$element);

			} else {

			$element = self::iconvArray ($element, $newEncoding);

			}

			$outputArray[$key]=$element;

			}

			}

			}

			return $outputArray;

		}
	function action_makeorder(){ 

		header('Content-Type: application/json;');
		// ���������� ������� ������
		$args = [
			'name' 		=> FILTER_SANITIZE_MAGIC_QUOTES,
			'phone' 	=> FILTER_SANITIZE_MAGIC_QUOTES,
			'email' 	=> FILTER_SANITIZE_EMAIL,
			'adress' 	=> FILTER_SANITIZE_MAGIC_QUOTES,
			'date_delivery' => FILTER_SANITIZE_MAGIC_QUOTES,
			'time_ot' => FILTER_SANITIZE_MAGIC_QUOTES,
			'time_do' => FILTER_SANITIZE_MAGIC_QUOTES,
			'index' 	=> FILTER_VALIDATE_INT,
			'ptype' 	=> FILTER_VALIDATE_INT,
			'dtype' 	=> FILTER_VALIDATE_INT,
			'ref'		=> FILTER_VALIDATE_URL,
			'comment' 	=> FILTER_SANITIZE_MAGIC_QUOTES, 
			'list' 		=> [
				'filter'	=> FILTER_VALIDATE_INT,
                'flags'     => FILTER_REQUIRE_ARRAY,
			],	
			'other' 		=> [
				'filter'	=> FILTER_SANITIZE_MAGIC_QUOTES,
                'flags'     => FILTER_REQUIRE_ARRAY,
			],	  	
		];
		$input = filter_input_array(INPUT_POST, $args);

		//echo "dfds";
		// 
		$result = BasketModel::makeOrder(
			$input['name'],
			$input['phone'],
			$input['email'],
			$input['adress'],
			$input['date_delivery'],
			$input['time_ot'],
			$input['time_do'],
			$input['index'],
			$input['ptype'],
			$input['dtype'],
			$input['comment'],
			$input['ref'],
			$input['list'],
			$input['other']);
			;
		echo json_encode(self::iconvArray($result, "UTF-8"));
	}

	/**
	 * [action_changeTheme ��������� ������ �����]
	 */
	function action_changeTheme() {

		session_start();

		$theme = filter_input(INPUT_GET, 'theme');

		if ($theme == 'default') {
			$_SESSION['mobile'] = 0;
		}elseif ($theme == 'mobile') {
			$_SESSION['mobile'] = 1;
		}
		return false;
	}

}