<?php
namespace app\themes\mobile\controllers;

use core\lib\BaseController;
use app\models\ConfigModel;
use app\models\MenuModel;
use app\models\PageModel;
use app\models\BasketModel;
use app\models\SearchModel;

class SearchController extends BaseController {
    
    function action_index() {
        $config = new ConfigModel();
        //$limit = $config->get('catalog.products_on_page_mobile');
        $menu = new MenuModel();
        $query = filter_input(INPUT_GET, 'search_text');
        $search = new SearchModel(); 
        
        $data = [
            'basket' => BasketModel::getData(),
            'phone' => $config->get('site.phone'),
            'phone_spb' => $config->get('site.phone2'),
            'phone_ru' => $config->get('site.phone_ru'),
            'work_mode' => $config->get('site.work_mode'),
            'products' => $search->getProducts($query),
            'title' => "���������� ������ \"{$query}\"" ,
            'menu' => $menu->getMenu(),
            'pagesMenu' => PageModel::getMenu(),
            'seo' => [
                'title' => '�����',
                'describtion' => '�����',
                'keywords' => ''
            ],
        ];

        $this->view->render('index', 'search', $data);
    }
    
}