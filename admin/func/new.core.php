<?
	//	����� ��� ������ � ��
	require_once('db.php');
	//	����� ��� ������ � ����������
	require_once('image.php');
	
	define('_DS', DIRECTORY_SEPARATOR);
	
	class App {
		static private $instance = NULL;
		
		//	�������� ����������:
		static $db;
	
		//	�������� ���������:
		static private $name;
		static private $email;
		static private $phone;
		static private $rezhim;
		
		//	��������� �����:
		static $root_dir;
		static $upload_dir;
		
		function __construct() {
			self::$db = db::getInstance();
			self::readConfig();
		}
		
		static function getInstance() {
			if(is_null(self::$instance)) {
				self::$instance = new App();
			}
			return self::$instance;
		}
		
		//	������ MySQL.config � MySQL.config_img
		static private function readConfig() {
			$query = 'SELECT * FROM `config` WHERE `id`=1';
			$rows = self::$db->execute($query);
			
			self::$root_dir = $rows[0]['path'] . _DS;
			
			self::$name = $rows[0]['name'];
			self::$email = $rows[0]['email'];
			self::$phone = $rows[0]['tel'];
			self::$rezhim = $rows[0]['rezhim'];
			
			$query = 'SELECT * FROM `config_img` WHERE `id`=1';
			$rows = self::$db->execute($query);
			self::$upload_dir = self::$root_dir . $rows[0]['upload_dir'] . _DS;
			
			//	���� �����
			image::$big_dir = self::$upload_dir . $rows[0]['big_dir'] . _DS;
			image::$med_dir = self::$upload_dir . $rows[0]['med_dir'] . _DS;
			image::$small_dir = self::$upload_dir . $rows[0]['small_dir'] . _DS;
			image::$hit_dir = self::$upload_dir . $rows[0]['hit_dir'] . _DS;
			image::$dop_dir = self::$upload_dir . $rows[0]['dop_dir'] . _DS;
			image::$dop_small_dir = image::$dop_dir . $rows[0]['dop_small_dir'] . _DS;
			
			//	������� �����:
			//	�������
			image::$w_big = $rows[0]['w_big'];
			image::$h_big = $rows[0]['h_big'];
			//	�������
			image::$w_med = $rows[0]['w_med'];
			image::$h_med = $rows[0]['h_med'];
			//	���������
			image::$w_small = $rows[0]['w_small'];
			image::$h_small = $rows[0]['h_small'];
			//	���
			image::$w_hit = $rows[0]['w_hit'];
			image::$h_hit = $rows[0]['h_hit'];
			//	���. ����� �����������
			image::$w_dop = $rows[0]['w_dop'];
			image::$h_dop = $rows[0]['h_dop'];
			//	��������
			image:: $quality = $rows[0]['quality'];
		}
	}
?>