<?php 

namespace core\lib;

/**
* 
*/
class BaseView
{
	public $template = 'main';

	function render($template, $content, $data = null, $theme = THEME) {
		$canonical = $this->canonicalCheck();
		include "oc_app/themes/" . $theme . "/views/" . $template . ".tpl.php";
	}

	// создание канонической ссылки если  надо
	private function canonicalCheck() {

		// глушим, возможно не нужно!!!
		/*$query = filter_input(INPUT_SERVER, 'REQUEST_URI');

		$query = explode('?', $query);

		if (count($query) > 1) {
			return  'http://' . $_SERVER['SERVER_NAME'] . $query[0];
		} */
		return false;

	}
}