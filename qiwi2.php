#!/usr/bin/php
<?php
ini_set("display_errors",1);
error_reporting(E_ALL);

require_once("connect.php");

class QIWIgeter{
	private $xmlpoint;
	private $curl;
	private $tablePrice;
	private $tablePoint;
	private $key = "13c8a20ba9a8faf16fc741793c38928b530c5b3564d47"; // ключ от http://calc.qiwipost.ru/rules.html, при изменении пароля следует перегенерить
	private $calcArr = array("type=json", "encoding=UTF-8", "nds=1", "tens=1", "pay=cash"); // строка запроса для http://wt.qiwipost.ru/calc, расчитывает стоимость.
	private $urlAPI = "http://api.qiwipost.ru/?do=listmachines_xml"; // url API qiwiposta
	private $paramsAPI = "telephonenumber=9263737552&password=H4$4dID$1x"; // логин и пасс от кабинета

	/**
	 * Получает на вход 2 базы данных	
	 * TODO: можно добавить if not exist create db
	 * @param type string $tablePoint 
	 * @param type string $tablePrice 
	 * @return type
	 */
	public function __construct($tablePoint, $tablePrice){
		$this->curl = curl_init();
		$this->tablePoint = $tablePoint;
		$this->tablePrice = $tablePrice;
	}

	/**
	 * Ну это медот значится получает XML со всеми точками доставки, на сегоднящний день.
	 * @param type string $url 
	 * @param type string $params 
	 * @return type string
	 */
	public function getXMLPoint(){
		$url = $this->urlAPI;
		$params = $this->paramsAPI;
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_POST, true);
		curl_setopt($this->curl, CURLOPT_POSTFIELDS, $params);
		$this->xmlpoint = curl_exec($this->curl);
	}

	/**
	 * Калькулятор цены, простой метод котаны.
	 * @param type string $url 
	 * @return type source
	 */
	public function caclPrice($url){
		curl_setopt($this->curl, CURLOPT_URL, $url);
		curl_setopt($this->curl, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->curl, CURLOPT_POST, false);
		return curl_exec($this->curl);
	}

	/**
	 * Итератор для полученого xmlника, прогоняет его через цикл, заносит данные в QIWI_POINT, а цену и тип доставки в QIWI_PRICE
	 * @return type
	 */
	public function IterPointer(){
		$xml = simplexml_load_string($this->xmlpoint, 'SimpleXMLElement', LIBXML_NOCDATA);
		try{
			mysql_query("TRUNCATE {$this->tablePoint}");
			mysql_query("TRUNCATE {$this->tablePrice}");
		}catch(Exception $e){
			echo 'Ошибка: ',  $e->getMessage(), "\n";
		}
		$id_point = 1;
		$pointStr = "";
		foreach ($xml->machine as $key => $value) {
			$json = json_encode($value);
			$array = json_decode($json, TRUE);
			$namePoint = $array['name'];
			foreach (array("A", "B", "C") as $char) {
				$line = $this->getLineToCalc($namePoint, $char);
				$price = $this->caclPrice($line);
				$price = json_decode($price, TRUE);
				if("0" === $price['deliverytime']){
					// консальная утилита, не понимает эксепшена, пичаль
					//throw new Exception("deliverytime равно 0, это ошибка, видимо что-то не так", 1); 
					continue; 
				}
				$query = "INSERT INTO {$this->tablePrice}(id_point, `type`, price, `time`) VALUES ({$id_point}, '{$char}', {$price['price']}, '{$price['deliverytime']}')";
				mysql_query($query) or die(mysql_error());
			}
			$point = serialize($array);
			$point = mysql_real_escape_string($point);
			$pointStr .= "('{$point}'),";
			$id_point++;
		}
		$query = "INSERT INTO {$this->tablePoint} (`point`) VALUES {$pointStr}";
		$query = substr($query, 0, -1);
		mysql_query($query) or die(mysql_error());
	}

	/**
	 * Генерит строку для API сервиса qiwiposta
	 * @param type string $id 
	 * @param type string $size 
	 * @return type $string
	 */
	public function getLineToCalc($id, $size){
		$line = "http://wt.qiwipost.ru/calc?";
		$line .= "key={$this->key}";
		$line .= "&id={$id}";
		$line .= "&size={$size}";
		foreach ($this->calcArr as $value) {
			$line .= "&{$value}";
		}
		return $line;
 	}

 	/**
 	 * Закрывает ресурс курла.
 	 * @return type
 	 */
 	public function __destruct(){
 		curl_close($this->curl);
 	}
}

$a = new QIWIgeter("QIWI_POINT", "QIWI_PRICE");
$a->getXMLPoint();
$a->IterPointer();
