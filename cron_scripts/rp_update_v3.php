<?php 

$debug = false; 

$phptime = microtime(true);

if ($debug) {
	ini_set('error_reporting', E_ALL);
	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
}
/**
* 	Класс реализующий синхронизацию данных (цена, наличие) с 10med
*	Работает для Olesya CMS
*/
class dataUpdater10med {

	private $token;
	private $url;
	private $limit;
	private $myCurl;
	private $db;
	private $time;
	public $count_rows;

	function __construct($curlArr, $dbArr) {

		// $this->limit = $limit;

		$this->time = time();
		$this->count_rows = 0;
		$this->token = $curlArr['token'];
		$this->url = $curlArr['url'];
		$this->myCurl = curl_init();
		
		$this->db = new mysqli($dbArr['host'], $dbArr['login'], $dbArr['pass'], $dbArr['db']);
		if ($this->db->connect_error) {
		    die('Ошибка подключения (' .$this->db->connect_errno . ') '. $this->db->connect_error);
		}
	}

	function __destruct() {
		$this->db->close();
	}

	// Запрос данных к 10Med
	public function request($action, $params) {

		$urlString = $this->url . $action . '?access-token=' . $this->token;

		curl_setopt_array($this->myCurl, array(
		    CURLOPT_URL => $urlString,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params),
		));

		$res = curl_exec($this->myCurl);
		$res = json_decode($res);
		
		return $res;
	}

	// выборка 10med id из всех, товаров сайта, которые имею её
	public function getSiteProducts() {
		$output = array();
		$query = "	SELECT id, linked_with_10med
					FROM catalog
					WHERE linked_with_10med > 0";
		if ($result = $this->db->query($query)) {
			while ($row = $result->fetch_assoc()) { 
				$output[] = $row;
			}
			return $output;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}
	}

	// обновление цены, наличия
	public function updateProductsRests($storage, $id10med, $rests, $price) {
		if ($rests > 0) {
			$query = "	INSERT INTO storage_rests (storage_id, catalog_id, amount, last_update, newPrice) 
						VALUES (
							{$storage},
							(SELECT id FROM catalog WHERE linked_with_10med = {$id10med}),
							{$rests},
							{$this->time},
							{$price}
						)";
			if ($result = $this->db->query($query)) {
				return true;
			} else {
				echo "Errormessage: " . $this->db->error;
				return false;
			}	
		} else {
			return true;
		}
	} 

	public function updateProductsPrice($id10med, $price) {
		$this->count_rows++;
		$query = "UPDATE catalog SET price = {$price}, price_after_discount = {$price} WHERE linked_with_10med = {$id10med}";
		if ($result = $this->db->query($query)) {
			return true;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}	
	}

	// очистка таблицы storage_rests
	public function clearStorageRests() {
		$query = "TRUNCATE TABLE storage_rests";
		if ($result = $this->db->query($query)) {
			return true;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}
	}
	// очистка таблицы по массиву id товаров
	public function clearProductRests($productIdsArray){
		$productIds = implode(' ,' , $productIdsArray);
		$q = "DELETE FROM storage_rests WHERE catalog_id in ( {$productIds} ) ;" ;
		if ($result = $this->db->query($q)) {
			return true;
		} else {
			echo "Errormessage: " . $this->db->error;
			return false;
		}
	}
	
	public function updateMassiveRests($array, $products_array){
	    $insert_query = "INSERT INTO `storage_rests` (`storage_id`, `catalog_id`, `amount`, `last_update`, `newPrice`) VALUES " ;
	    $prods = [] ;
		foreach ($array as $prod) {
		    if ($prod->rests < 1) continue ;

		    $prods[] = " ({$prod->storageId}, {$products_array[$prod->id10med]}, {$prod->rests}, {$this->time}, {$prod->price} ) " ;
        }
        $insert_query .= implode(" , ", $prods);

		//return $insert_query ;
		return $this->db->query($insert_query) ;
	}

    public function updateMassivePrice($array, $products_array){

	    $query = $this->db->prepare("UPDATE catalog SET price = ?, price_after_discount = ? WHERE id = ? ") ;
        $query->bind_param('iii', $price, $price, $id);
        foreach ($array as $prod) {
            $price = $prod->price ;
            $id = $products_array[$prod->id10med] ;
            $query->execute();
        }

        $query->close();

        return true ;
    }
	
}


echo '<pre>';

// Данные подключения к БД
//require_once('../config.db.php');
require_once('../config.db.php');


// Формиррование служебных  данных
$limit = 500;
$curlArr = array(
	'token' => '5RJrtyWrOf7KlNc4Hzzj65NQ7Ud0AOKS',
	'url'	=> 'http://46.101.204.133/api/',
);
$dbArr = array(
	'host'	=> $host,
	'login' => $login,
	'pass'	=> $pass,
	'db'	=> $db,
);

$updater = new dataUpdater10med($curlArr, $dbArr);

$products = $updater->getSiteProducts();
//$updater->clearStorageRests();

if ($debug) {	
	print_r($products);
	echo "---------------------------------------- \n";	
	$inputDebug = array();
}

// разбиение на порции
$products = array_chunk($products, $limit);
foreach ($products as $part) {
	
	// Массив для записи id из 10med
	$params = array();
	
	$ids = array();
	$prods = array();

	foreach ($part as $item) {
		$ids[] = $item['id'];
		$params['id10med'][] = $item['linked_with_10med'];
		$prods[$item['linked_with_10med']] = $item['id'];
	}
	// запрос к 10мед
	$input10medData = $updater->request('rest', $params);
	
	if (!is_array($input10medData)){
		printf(" Bad response from 10med - lost " . count($params['id10med']) . " products rests \n");
		unset($ids);
		continue;
	}
	
	// чистим остатки после ответа 10меда во избежании ошибок
	$updater->clearProductRests($ids);
	
	if ($debug) {	
		$inputDebug = array_merge($inputDebug, $input10medData); 
	}
	
	echo $updater->updateMassiveRests($input10medData, $prods);

    $updater->updateMassivePrice($input10medData, $prods);

	foreach ($input10medData as $value) {
		//$updater->updateProductsRests($value->storageId, $value->id10med, $value->rests, $value->price);
		//$updater->updateProductsPrice($value->id10med, $value->price);
		printf("[update row]  1C_key: %' 12s  |   sclad: %' 3d   |   10medId:%' 6d   |   price:%' 7d   |   amount: %' 4d|\n",$value->nomenclature , $value->storageId, $value->id10med, $value->price, $value->rests);
	}
}

printf("------------------------------ \n %d rows update | PHP time: %F \n", $updater->count_rows, (microtime(true)-$phptime));

if ($debug) {
	print_r($inputDebug);
}

unset($updater);


?>