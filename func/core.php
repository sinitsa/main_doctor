<?php
$my_ip = $_SERVER['REMOTE_ADDR'];
$debug = false;
if($my_ip == "217.194.255.193"){
	$debug = false;
}
include('constants.php');
define('_DS', DIRECTORY_SEPARATOR);
date_default_timezone_set('Europe/Moscow');

include(ROOT_DIR . 'config.php');
include('api_10med.php');

//���������� ���. ���� ����
if (file_exists(ROOT_DIR . '/hooks/core.php')) {
	include(ROOT_DIR . '/hooks/core.php');
}

//���������� ������ ��� ������ � �������� ��������

include('classes/cart.php');
include('classes/db.php');
include('classes/deliveryPrice.php');
include('classes/discountPrice.php');

// ---- /config system/ ------
// key example: catalog.products_on_page
class Config {
	//���������� ��� ����������� ��������
	public static $data = array();
	
	protected static function parseKey($key) {
		if (empty($key)) return false;
		$temp = explode('.', $key);
		$count = count($temp);
		if ($count == 2) {
			$keyGroup = $temp[0];
			$key = $temp[1];
		} elseif ($count == 1) {
			$keyGroup = '';
			$key = $temp;
		} else {
			return false;
		}
		return array('keyGroup' => $keyGroup, 'key' => $key);
	}
	public static function get($key) {
		if($key === "credit.partner_id"){
			return "1-178YO4Z";
		}
		//��������� �������� �������������
		if ($key == 'site.server_dir')
			return ROOT_DIR;
		//������� �� "����"
		if (isset(self::$data[$key])) {
			return self::$data[$key];
		}
		$k = self::parseKey($key);
		if ($k == false) return false;
		
		$temp = mysql_fetch_assoc(mysql_query("SELECT `value` FROM `config` WHERE `key_group` = '{$k['keyGroup']}' AND `key` = '{$k['key']}'"));
		//������� � "���"
		self::$data[$key] = $temp['value'];
		return $temp['value'];
	}
	public static function set($keyFull, $value) {
		$key = self::parseKey($keyFull);
		if ($key == false) return false;
		
		$value = mysql_real_escape_string($value);
		
		mysql_query("UPDATE `config` SET `value` = '{$value}' WHERE `key_group` = '{$key['keyGroup']}' AND `key` = '{$key['key']}' LIMIT 1");
		
		if (mysql_affected_rows() <= 0) {
			mysql_query("INSERT INTO `config` SET
				`value` = '{$value}', 
				`key_group` = '{$key['keyGroup']}',
				`key` = '{$key['key']}'
				");
		}
		self::$data[$keyFull] = $value;
		return true;
	}
	/* deprecated
	public static function add($key, $value, $name = '', $desc = '') {
		$key = mysql_real_escape_string($key);
		$value = mysql_real_escape_string($value);
		$name =  mysql_real_escape_string($name);
		$desc =  mysql_real_escape_string($desc);
		
		if(mysql_fetch_assoc(mysql_query("SELECT * FROM `config_main` WHERE `key` = '{$key}'"))) {
			return false;
		} else {
			mysql_query("INSERT INTO `config_main` SET `value` = '{$value}', `key` = '{$key}', `name` = '{$name}', `description` = '{$desc}'");
			return true;
		}
	}
	*/
	public static function getAll() {
		$array = array();
		
		$sel = mysql_query("SELECT * FROM `config` ORDER BY `key_group`");
		while ($res = mysql_fetch_assoc($sel)) {
			$array[] = $res;
		}
		return $array;
	}
}


//������������� �� ������������ � edit
function isEditAuth() {
	return isset($_SESSION['auth']) && $_SESSION['auth'] == 'edit';
}
function getLinkEditProduct($id) {
	if (is_array($id)) $id = $id['id'];
	return '/edit/m_catalog/edit.php?id=' . $id;
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
// ������ ����������� �����
function query_email() {
	global $site_email;

	$query = ("SELECT * FROM config WHERE id= '1' LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		$site_email = $line['email'];
	};
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
// -------------------------------------------------------------------------------- /config/ ------------------------------------------------------------------------
// ������ ���������������� ���������� �����
function query_config() {
	$query = ("SELECT * FROM config WHERE id= '1' LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {

		global $config_name;
		global $config_path;
		global $config_email;
		global $config_tel;
		global $config_rezhim;
		global $config_banner;
		global $config_des;
		global $config_key;
		global $seo_title_text;
		global $seo_text;
		global $config_url;
		global $config_ga;


		$config_name = $line['name'];
		$config_path = $line['path'];
		$config_email = $line['email'];
		$config_tel = $line['tel'];
		$config_rezhim = $line['rezhim'];
		$config_banner = $line['banner'];
		$config_des = $line['seo_des'];
		$config_key = $line['seo_key'];
		$seo_title_text = $line['seo_title_text'];
		$seo_text = $line['seo_text'];
		$config_url = $line['url'];
		$config_ga = $line['ga'];
	};
}
// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//  ----------------  �������� � ID
// �������� ����� id
function get_id() {
	if (isset($_REQUEST['id']) && preg_match("|^\d+$|", $_REQUEST['id']))
		$id = $_REQUEST['id'];
	return $id;
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//  ------------------------------------------------------------------------------------ /foto/ --------------------------------------------------------

function query_foto($id) {
	$query = ("SELECT * FROM foto WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		global $cat;
		global $title;

		$cat = $line['cat'];
		$title = $line['title'];
	};
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//  ------------------------------------------------------------------------------------ /pages/ ---------------------------------------------------------------------------------
//���������� ������ � ����������� ��������
// $forceChpu �������������� ����� �������� ������ �� ���
function getStaticPage($idOrChpu, $forceChpu = false) {
	if (is_numeric($idOrChpu) && $forceChpu == false) {
		$where = "`id` = '{$idOrChpu}'";
	} else {
		$idOrChpu = mysql_real_escape_string($idOrChpu);
		$where = "`chpu` = '{$idOrChpu}'";
	}
	$sel = mysql_query("SELECT * FROM `pages` WHERE {$where}");
	return mysql_fetch_assoc($sel);
}
//���������� ������ ����������� �������
function getStaticPages() {
	$pages = array();
	$sel = mysql_query("SELECT * FROM `pages`");
	while ($row = mysql_fetch_assoc($sel)) {
		$pages[] = $row;
	}
	return $pages;
}

//������� ����������� ��������
function deleteStaticPage($id) {
	if (!is_numeric($id)) return false;
	mysql_query("DELETE FROM `pages` WHERE `id`='{$id}'");
	return mysql_affected_rows() > 0;
}
// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//  ---------------- �������� � �������
function query_path() {
	global $folder;
	$request = ("SELECT path FROM config WHERE id='1' LIMIT 1");
	$result = mysql_query($request);
	while ($row = mysql_fetch_row($result)) {
		$folder = $row[0];
	};
}

// --------------------------------------------------------------------------------  ����� ------------------------------------------------------------------------
// ------------------  ������� �������
function imageresize($src, $dest, $w, $quality) {

	$im = imagecreatefromjpeg($src);
	$w_src = imagesx($im);
	$h_src = imagesy($im);
	echo $w_src;
	echo "<br>";
	echo $h_src;
	echo "<br>";
	$ratio = $w_src / $w;
	$width = round($w_src / $ratio);
	$height = round($h_src / $ratio);

	$im1 = imagecreatetruecolor($width, $height);
	imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

	imagejpeg($im1, $dest, $quality);

	imagedestroy($im);
	imagedestroy($im1);
}

// --------------------------------------------------------------------------------  ����� ------------------------------------------------------------------------
// ------------------  ������� ������� 22222222222222222222222222222222222222222222222222222222
function imageres($src, $dest, $w, $quality) {

	$im = imagecreatefromjpeg($src);
// ������ ������ ���������
	$w_src = imagesx($im);
// ������ ������ ���������
	$h_src = imagesy($im);



	if ($w_src > $h_src) {

// ������� ���������� ���������
		$ratio = $w_src / $w;
		$width = round($w_src / $ratio);
		$height = round($h_src / $ratio);
		echo $ratio;
		echo "<br>";
		echo $w_src;
		echo "<br>";
		echo $h_src;
		echo "<br>";
		echo $width;
		echo "<br>";
		echo $height;
		echo "<br>";
		echo "<br>";

		$im1 = imagecreatetruecolor($width, $height);
		imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

		imagejpeg($im1, $dest, $quality);

		imagedestroy($im);
		imagedestroy($im1);
	} else {

// ������� ���������� ���������
		/* $ratio = $h_src/$h;
		  $width = round($w_src/$ratio);
		  $height = round($h_src/$ratio);
		 */
		if ($w_src > $w) {
			$ratio = $w_src / $w;
			$width = round($w_src / $ratio);
			$height = round($h_src / $ratio);
		}

		echo $ratio;
		echo "<br>";
		echo $w_src;
		echo "<br>";
		echo $h_src;
		echo "<br>";
		echo $width;
		echo "<br>";
		echo $height;
		echo "<br>";
		echo "<br>";

		$im1 = imagecreatetruecolor($width, $height);
		imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

		imagejpeg($im1, $dest, $quality);

		imagedestroy($im);
		imagedestroy($im1);
	}
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function image_res($src, $dest, $w_max, $h_max, $quality) {

	$im = imagecreatefromjpeg($src);

// ������ ������ ���������
	$w_src = imagesx($im);

// ������ ������ ���������
	$h_src = imagesy($im);

	if ($h_src > $h_max and $h_max != '0') {
		$ratio = $h_src / $h_max;
		$width = round($w_src / $ratio);
		$height = round($h_src / $ratio);

		if ($width > $w_max) {
			$ratio = $width / $w_max;
			$width = round($width / $ratio);
			$height = round($height / $ratio);
		}
	} else {
		if ($w_src > $w_max) {
			$ratio = $w_src / $w_max;
			$width = round($w_src / $ratio);
			$height = round($h_src / $ratio);
		} else {
			$width = $w_src;
			$height = $h_src;
		}
	}
// ������ ����� ����������� true color.
	$im1 = imagecreatetruecolor($width, $height);

//
	imagecopyresampled($im1, $im, 0, 0, 0, 0, $width, $height, $w_src, $h_src);

	imagejpeg($im1, $dest, $quality);

	imagedestroy($im);
	imagedestroy($im1);
}
//����� ����� ������� ������ � ������� � ����� �������

define('IMG_RESIZE_FIT', 0); // ���������� � ������� (���������� �� ������������ �������)
define('IMG_RESIZE_MAX', 1); // ���������� �� ������������ �������
define('IMG_RESIZE_MIN', 2); // ���������� �� ����������� �������
define('IMG_RESIZE_CROP', 3); // ������� �������� ��������: ���� W>H, �� ������� �� �����, ���� W<H, �� �����

function uploadAndResize($file,$sDirDest,$sFileDest,$iWidthDest=null, $iHeightDest=null, $iResizeMode=1, $quality = 100) {
	if (!empty($file['error'])) return false;
	if (empty($file['tmp_name']) || $file['tmp_name'] == 'none') return false;

	$sFileSrc = $file['tmp_name'];
	//$quality = 100;

	if (!($aSize=getimagesize($sFileSrc))) {
		return false;
	}
	$img_src=false;
	switch ($aSize[2]) {
		case 3:
			$img_src=imagecreatefrompng($sFileSrc);
			$sFileDest.='.png';
			break;
		case 1:
			$img_src=imagecreatefromgif($sFileSrc);
			$sFileDest.='.gif';
			break;
		case 2:
			$img_src=imagecreatefromjpeg($sFileSrc);
			$sFileDest.='.jpg';
			break;
		default:
			return false;
			break;
	}
	if (!$img_src) {
		return false;
	}

	if ($iWidthDest) {$iScaleW=$iWidthDest/$aSize[0];} else {$iScaleW=1;}
	if ($iHeightDest) {$iScaleH=$iHeightDest/$aSize[1];} else {$iScaleH=1;}

	if ($iResizeMode==IMG_RESIZE_MAX) {
			$iSizeRelation=(($iScaleW<$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeW=$iWidthNew;
			$iSizeH=$iHeightNew;
			$iDestX=0;
			$iDestY=0;
	} elseif ($iResizeMode==IMG_RESIZE_MIN) {
			$iSizeRelation=(($iScaleW>$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeW=$iWidthNew;
			$iSizeH=$iHeightNew;
			$iDestX=0;
			$iDestY=0;
	} elseif ($iResizeMode==IMG_RESIZE_CROP) {
			$iSizeRelation=(($iScaleW>$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeH=$iSizeW=(($iWidthNew<$iHeightNew)?$iWidthNew:$iHeightNew);
			$iDestX=round(($iSizeW-$iWidthNew)/2);
			$iDestY=0;
	} else {
			$iSizeRelation=(($iScaleW<$iScaleH)?$iScaleW:$iScaleH);
			$iWidthNew=round($aSize[0]*$iSizeRelation);
			$iHeightNew=round($aSize[1]*$iSizeRelation);

			$iSizeH=$iSizeW=(($iWidthNew>$iHeightNew)?$iWidthNew:$iHeightNew);
			$iDestX=round(($iSizeW-$iWidthNew)/2);
			$iDestY=round(($iSizeH-$iHeightNew)/2);
	}

	$sFileFullPath = rtrim($sDirDest, '/').'/'.$sFileDest;

	if (($iWidthDest and $iWidthDest!=$aSize[0]) && !($iWidthDest > $aSize[0] && $iHeightDest > $aSize[1] && $iResizeMode==IMG_RESIZE_MAX)) {
	$img_dest=imagecreatetruecolor($iSizeW,$iSizeH);
	//$clr = imagecolorallocate($img_dest,255,255,255);
	//imagefill($img_dest,0,0,$clr);
	imagesavealpha($img_dest,true);
	imagealphablending($img_dest,false);
	if (imagecopyresampled($img_dest, $img_src, $iDestX, $iDestY, 0, 0,
                           $iWidthNew, $iHeightNew, $aSize[0], $aSize[1])) {
	  imagedestroy($img_src);
      switch ($aSize[2]) {
        case 3:
          if (imagepng($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        case 1:
          if (imagegif($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        case 2:
          if (imagejpeg($img_dest,$sFileFullPath,$quality)) {
            chmod($sFileFullPath,0666);
          }
          break;
        }
      imagedestroy($img_dest);
      return $sFileDest;
    }
  } else {
    if (copy($sFileSrc,$sFileFullPath)) {
      return $sFileDest;
    }
  }
return false;

}
// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//  ----------------  �������� � ���������
// ���������� ����

function query_cat_full($id) {
	$query = ("SELECT * FROM cat WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		global $title;
		global $pod;
		global $seo_title;
		global $seo_des;
		global $seo_key;
		global $seo_title_text;
		global $seo_text;
		global $chekis;
		global $chpu;
		global $cat_filters_state;


		$title = $line['title'];
		$pod = $line['pod'];
		$seo_title = $line['seo_title'];
		$seo_des = $line['seo_des'];
		$seo_key = $line['seo_key'];
		$seo_title_text = $line['seo_title_text'];
		$seo_text = $line['seo_text'];
		$chpu = $line['chpu'];
		$cat_filters_state = $line['filters_state'];
	};
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function query_catalog($id) {
	$query = ("SELECT * FROM catalog WHERE id=$id LIMIT 1");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		global $adres;
		global $short;
		global $des;
		global $price;
		global $tip;
		global $cat;
		global $bal;
		global $et;
		global $gaz;
		global $gvoda;
		global $kom;
		global $mkad;
		global $naz;
		global $oto;
		global $sd;
		global $shosse;
		global $sk;
		global $srok;
		global $su;
		global $suz;
		global $svet;
		global $sz;
		global $tipd;
		global $tipk;
		global $voda;
		global $lico;
		global $lat;
		global $lng;
		global $spec;
		global $img;
		global $chekis;

		$adres = $line['adres'];
		$short = $line['short'];
		$des = $line['des'];
		$price = $line['price'];
		$tip = $line['tip'];
		$cat = $line['cat'];
		;
		$bal = $line['bal'];
		$et = $line['et'];
		$gaz = $line['gaz'];
		$gvoda = $line['gvoda'];
		$kom = $line['kom'];
		$mkad = $line['mkad'];
		$naz = $line['naz'];
		$oto = $line['oto'];
		$sd = $line['sd'];
		$shosse = $line['shosse'];
		$sk = $line['sk'];
		$srok = $line['srok'];
		$su = $line['su'];
		$suz = $line['suz'];
		$svet = $line['svet'];
		$sz = $line['sz'];
		$tipd = $line['tipd'];
		$tipk = $line['tipk'];
		$voda = $line['voda'];
		$lico = $line['lico'];
		$lat = $line['lat'];
		$lng = $line['lng'];
		$spec = $line['spec'];
		$img = $line['img'];
	};
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
// ������� ���������
function translit($stroka) {

	global $trans;
	$stroka = str_replace(") .", "", $stroka);
	$stroka = rtrim($stroka);
	$stroka = htmlspecialchars($stroka, ENT_QUOTES);
	$stroka = mb_strtolower($stroka);

	$stroka = str_replace("&quot;", "", $stroka);

	$stroka = str_replace("�", "", $stroka);

	$stroka = str_replace("\"", "-", $stroka);
	$stroka = str_replace("�", "-", $stroka);
	$stroka = str_replace("�", "-", $stroka);

	$stroka = str_replace("&laquo;", "-", $stroka);
	$stroka = str_replace("&raquo;", "-", $stroka);
	$stroka = str_replace("*", "-", $stroka);
	$stroka = str_replace("&", "-", $stroka);
	$stroka = str_replace("�", "", $stroka);
	$stroka = str_replace("(", "-", $stroka);
	$stroka = str_replace(")", "", $stroka);
	$stroka = str_replace(" .", "", $stroka);
	$stroka = str_replace("�", "a", $stroka);
	$stroka = str_replace("�", "b", $stroka);
	$stroka = str_replace("�", "v", $stroka);
	$stroka = str_replace("�", "g", $stroka);
	$stroka = str_replace("�", "d", $stroka);
	$stroka = str_replace("�", "e", $stroka);
	$stroka = str_replace("�", "e", $stroka);
	$stroka = str_replace("�", "zh", $stroka);
	$stroka = str_replace("�", "z", $stroka);
	$stroka = str_replace("�", "i", $stroka);
	$stroka = str_replace("�", "i", $stroka);
	$stroka = str_replace("�", "k", $stroka);
	$stroka = str_replace("�", "l", $stroka);
	$stroka = str_replace("�", "m", $stroka);
	$stroka = str_replace("�", "n", $stroka);
	$stroka = str_replace("�", "o", $stroka);
	$stroka = str_replace("�", "p", $stroka);
	$stroka = str_replace("�", "r", $stroka);
	$stroka = str_replace("�", "s", $stroka);
	$stroka = str_replace("�", "t", $stroka);
	$stroka = str_replace("�", "u", $stroka);
	$stroka = str_replace("�", "f", $stroka);
	$stroka = str_replace("�", "h", $stroka);
	$stroka = str_replace("�", "ts", $stroka);
	$stroka = str_replace("�", "ch", $stroka);
	$stroka = str_replace("�", "ch", $stroka);
	$stroka = str_replace("�", "sh", $stroka);
	$stroka = str_replace("�", "sh", $stroka);
	$stroka = str_replace("�", "", $stroka);
	$stroka = str_replace("�", "y", $stroka);
	$stroka = str_replace("�", "", $stroka);
	$stroka = str_replace("�", "e", $stroka);
	$stroka = str_replace("�", "yu", $stroka);
	$stroka = str_replace("�", "ya", $stroka);
	$stroka = str_replace("�", "ya", $stroka);
	$stroka = str_replace(" ", "-", $stroka);
	$stroka = str_replace(".", "", $stroka);
	$stroka = str_replace(",", "-", $stroka);
	$stroka = str_replace("+", "-", $stroka);
	$stroka = str_replace("/", "-", $stroka);
	$stroka = str_replace('"', "-", $stroka);
	$stroka = str_replace("*", "-", $stroka);
	$stroka = str_replace(":", "-", $stroka);
	$stroka = str_replace('�', "-", $stroka);
	$stroka = str_replace("---", "-", $stroka);
	$stroka = str_replace("--", "-", $stroka);
	$stroka = str_replace("--", "-", $stroka);


	$trans = $stroka;
//echo $stroka;
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function query_cat_novinki($id) {


	global $novinki_id;
	global $novinki_title;
	global $novinki_chpu;
	global $novinki_short;
	global $novinki_cat;
	global $novinki_price;
	global $num_cat;
	global $num_per;

	global $q;
	global $chekis;

	$query = (" SELECT * FROM catalog WHERE novinka='1' ");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		$novinki_id[] = $line['id'];
		$novinki_title[] = $line['title'];
		$novinki_chpu[] = $line['chpu'];
		$novinki_cat[] = $line['cat'];
		$novinki_price[] = $line['price'];


		$t_title = $line['title'];
		$num_title = strlen($t_title);
		$n_short = 150 - $num_title;

		$novinki_short[] = substr($line['short'], 0, $n_short);
	};

	$num_cat = count($novinki_id);
	$num_pol = $num_cat / 2;
	$num_pol = intval($num_pol);
	$num_per = $num_cat - $num_pol;


	$q = count($novinki_id);
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function query_cat_spec($id) {
	global $spec_id;
	global $spec_title;
	global $spec_chpu;
	global $spec_short;
	global $spec_cat;
	global $spec_price;
	global $spec_cat;
	global $spec_per;
	global $num_cat;
	global $num_per;
	global $spec_novinka;

	global $q;
	global $chekis;

	$query = (" SELECT * FROM catalog WHERE hit = '1'");
	$result = @mysql_query($query);
	$chekis = mysql_num_rows($result);
	while ($line = @mysql_fetch_assoc($result)) {
		$spec_id[] = $line['id'];
		$spec_title[] = $line['title'];
		$spec_chpu[] = $line['chpu'];
		$spec_cat[] = $line['cat'];
		$spec_price[] = $line['price'];
		$spec_novinka[] = $line['novinka'];
		$t_title = $line['title'];
		$num_title = strlen($t_title);
		$n_short = 250 - $num_title;

		$spec_short[] = substr($line['short'], 0, $n_short);
	};

	$num_cat = count($spec_title);
	$num_pol = $num_cat / 2;
	$num_pol = intval($num_pol);
	$num_per = $num_cat - $num_pol;

	$q = count($spec_title);
}

function query_catalog_spec() {

	global $catalog_id;
	global $catalog_title;
	global $catalog_chpu;
	global $catalog_price;
	global $catalog_short;
	global $catalog_des;
	global $catalog_novinka;
	global $catalog_prior;

	global $num_cat;
	global $num_per;
	global $q;
	global $chekis;

	global $_cat_min_price;
	global $_cat_max_price;
	global $_cat_mid_price;
	global $_cat_count;
	global $_pf_cat_id;
	global $_cur_pf_min_price;
	global $_cur_pf_max_price;
	global $_pf_cat_name;

	// ----------- ��������� ������ �� ����������
	// ���� ������
	if (isset($_REQUEST["pf_min_price"]) && (isset($_REQUEST["pf_max_price"]))) {
		// ���������� ���������� �� � ��
		$_cur_pf_min_price = $_REQUEST["pf_min_price"]; // ��
		$_cur_pf_max_price = $_REQUEST["pf_max_price"]; // ��

		$add_where = " AND ((price>=$_cur_pf_min_price) AND (price<=$_cur_pf_max_price))"; // ��������� �������

		$_cat_min_price = 9999999999;
		$_cat_max_price = -1;
		$_cat_count = 0;
		$_pf_cat_id = $now_cat_id;
		$_pf_cat_name = $id;

		// ������ ������ �� ������� �������
		$query = ("SELECT * FROM catalog WHERE (hit = '1')  ORDER BY spec_rang");
		$result = @mysql_query($query);
		$num_cat = mysql_num_rows($result);
		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
		};

		$query = ("SELECT * FROM catalog WHERE (hit = '1') $add_where order by price");
		$result = @mysql_query($query);
		$num_cat = mysql_num_rows($result);

		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
			$catalog_id[] = $line['id'];
			$catalog_title[] = $line['title'];
			$catalog_chpu[] = $line['chpu'];
			$catalog_price[] = $line['price'];
			$catalog_art[] = $line['art'];
			$catalog_des[] = $line['des'];
			$catalog_novinka[] = $line['novinka'];
			$catalog_prior[] = $line['prior'];
			$tmp_short = $line['short'];

			$t_title = $line['title'];
			$num_title = strlen($t_title);
			$n_short = 150 - $num_title;

			$catalog_short[] = substr($line['short'], 0, $n_short);
			$_cat_count++;
		};
	}
	else {
		$add_where = "";
		$_cat_min_price = 9999999999;
		$_cat_max_price = -1;
		$_cat_count = 0;
		$_pf_cat_id = $now_cat_id;
		$_pf_cat_name = $id;
		$query = ("SELECT * FROM catalog WHERE (hit = '1') $add_where  ORDER BY spec_rang");
		$result = @mysql_query($query);
		$num_cat = @mysql_num_rows($result);

		while ($line = @mysql_fetch_assoc($result)) {
			$price = ($line['price']) * 1;
			if ($price > $_cat_max_price)
				$_cat_max_price = $price;
			if ($price < $_cat_min_price)
				$_cat_min_price = $price;
			$catalog_id[] = $line['id'];
			$catalog_title[] = $line['title'];
			$catalog_chpu[] = $line['chpu'];
			$catalog_price[] = $line['price'];
			$catalog_novinka[] = $line['novinka'];
			$catalog_prior[] = $line['prior'];
			//$price = $line['price'];
			//$sum = strlen($price);
			//$tochka = $sum  - '3';
			//if ($sum > '3' )
			//{
			//	$first_price = substr($price, 0, $tochka);
			//	$last_price = substr ($price, $tochka, $sum);
			//	$catalog_price[] = $first_price." ".$last_price;
			//}
			//$catalog_price[] = $price;


			$catalog_des[] = $line['des'];

			$tmp_short = $line['short'];

			$t_title = $line['title'];
			$num_title = strlen($t_title);
			$n_short = 150 - $num_title;

			$catalog_short[] = substr($line['short'], 0, $n_short);
			$_cat_count++;
		};
		$_cur_pf_min_price = $_cat_min_price;
		$_cur_pf_max_price = $_cat_max_price;
	}


	$_cat_mid_price = round(($_cat_max_price - $_cat_min_price) / 2) + $_cat_min_price;
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function check_pod($id) {

	global $num_pod;

	$query = ("SELECT * FROM cat WHERE pod=$id");
	$result = @mysql_query($query);
	$num_pod = mysql_num_rows($result);
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------


function query_all($tab) {
	global $all;

	$all = Array();
	$query = ("SELECT * FROM $tab ");
	$result = @mysql_query($query);
	while ($line = @mysql_fetch_assoc($result)) {
		$all[count($all)] = $line;
	};
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------

function add_monitor($act, $razd, $act_id, $title, $chpu) {
	$date = date("m.d.y");
	$time = date("H:i:s");
	$id = '0';
	$query = "INSERT INTO monitor VALUES('$id','$date','$time','$act','$razd','$act_id','$title','$chpu')";
	$result = mysql_query($query);
}

// --------------------------------------------------------------------------------  ������ ------------------------------------------------------------------------
//Depreceted
function add_monitor_sm($act, $act_id, $title, $dostavka) {
	$date = date("m.d.y");
	$time = date("H:i:s");
	$id = '0';
	$query = "INSERT INTO monitor_sm VALUES('$id','$date','$time','$act','$act_id','$title','$dostavka')";
	$result = mysql_query($query);
}

// ------------------------------------------------------------- ������ ������
function sql_catalogFilterDes($des) {
	$res = $des;

	preg_match_all("/(?:&lt;)(?:.*)a(?:.*)href=(?:&quot;)(?:.*)(http:\/\/)(?:.*)(?:&quot;)(?:.*)(?:&gt;)(.*)(?:&lt;)\/a(?:&gt;)/i", $res, $matches);

	foreach ($matches[0] as $key => $str) {
		$res = str_replace($str, $matches[2][$key], $res);
	}

	preg_match_all("/(?:<)(?:.*)a(?:.*)href=(?:\")(?:.*)(http:\/\/)(?:.*)(?:\")(?:.*)(?:>)(.*)(?:<)\/a(?:>)/i", $res, $matches);

	foreach ($matches[0] as $key => $str) {
		$res = str_replace($str, $matches[2][$key], $res);
	}
	return $res;
}

// --------------------------------------------------------------------------------  ������ ��������� ������  ------------------------------------------------------------------------

function sql_catalogFilterImages($des) {
	$res = $des;
	//preg_match_all ("/(?:&lt;)(?:.*)img(?:.*)src=(?:&quot;)(?:.*)(http:\/\/.*)(?:&quot;)(?:.*)(?:&gt;)/i", $res, $matches);
	preg_match_all("/(?:&lt;)(?:.*)img(?:.*)((?<=src=&quot;)http:\/\/.*)(?:(?:&quot;)(?=\s&nbsp;))/i", $res, $matches);
	foreach ($matches[1] as $str) {
		$image = new GetImage;

		$image->source = $str;
		$image->save_to = '../../img_about/'; // ���� �� ����� �����������
		$get = $image->download('gd', true); // ����������  GD
		$new_url = '/img_about/' . $image->new_name;

		if ($get) {
			$res = str_replace($str, $new_url, $res);
		}
	}

	preg_match_all("/(?:\<)(?:.*)img(?:.*)src=(?:\")((?:.*)(?:http:\/\/)(?:[^\"]*))(?:\")(?:.*)(?:\>)/i", $res, $matches);
	foreach ($matches[1] as $str) {
		$image = new GetImage;

		$image->source = $str;
		$image->save_to = '../../img_about/'; // ���� �� ����� �����������
		$get = $image->download('gd', true); // ����������  GD
		$new_url = '/img_about/' . $image->new_name;

		if ($get) {
			$res = str_replace($str, $new_url, $res);
		}
	}
	return $res;
}

// ������� ������� � ������ � ���������
function num_simvol($text) {
	$text = strip_tags($text);
	$text = trim($text);
	//$text = str_replace(".", "", $text);
	//$text = str_replace(",", "", $text);
	//$text = str_replace("-", "", $text);
	//$text = str_replace("!", "", $text);
	//$text = str_replace(":", "", $text);
	//$text = str_replace(";", "", $text);
	$text = str_replace("&nbsp;", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	$text = str_replace("  ", " ", $text);
	//$text = str_replace('\r\n', "", $text);
	//$text = str_replace('\n', "", $text);
	//$text = str_replace("\t", "", $text);
	$text = str_replace("  ", " ", $text);
	//echo $text;
	$col_simvol = strlen($text);
	echo $col_simvol;
}
//������������ ������ ������ � ������, ��� ������������� � mysql ������� set, update
//�� ����� �������
//array(
//	"column" => "value",
//	...
//);
//

//������� ������ � ������
function getProductFeedback($catalog_id, $all = false) {
	if (!is_numeric($catalog_id) || $catalog_id <= 0) return false;
	
	$products = array();
	$confirm = $all ? '' : "AND `confirm` = '1'";
	
	$sel = mysql_query("SELECT * FROM `otzyv` WHERE `catalog_id` = '{$catalog_id}' {$confirm} ORDER BY `id` ASC");
	while ($res = mysql_fetch_assoc($sel)) {
		$products[] = $res;
	}
	
	return $products;
}
function getProductsFeedback($type = 'new', $confirm = 0) {
	switch ($type) {
		case 'new' :
		default :
			$sel = mysql_query("
				SELECT
					`otzyv`.*,
					`catalog`.`id` as `catalog_id`,
					`catalog`.`title` as `catalog_title`
				FROM
					`otzyv`
					LEFT JOIN `catalog` ON `otzyv`.`catalog_id` = `catalog`.`id`
				WHERE
					`otzyv`.`confirm` = '{$confirm}'
				ORDER BY `otzyv`.`date` DESC");
			while ($res = mysql_fetch_assoc($sel)) {
				$products[] = $res;
			}
		break;
	}
	return $products;
}
function addProductFeedback($catalog_id, $data) {
	if (!is_numeric($catalog_id)) return false;
	$data['name'] = mysql_real_escape_string(htmlspecialchars($data['name']));
	$data['email'] = mysql_real_escape_string(htmlspecialchars($data['email']));
	$data['comment'] = mysql_real_escape_string(htmlspecialchars($data['comment']));
	$date = time();
	
	mysql_query("
		INSERT INTO
			`otzyv`
		SET
			`catalog_id` = '{$catalog_id}',
			`name` = '{$data['name']}',
			`email` = '{$data['email']}',
			`comment` = '{$data['comment']}',
			`date` = '{$date}'
		");
	return mysql_affected_rows() > 0;

}

//�������� ��� ������
function getOrderCode($id) {
	return Config::get('order.prefix').$id;
}
function fillProductsWithInfo( &$orderProducts ) {
	//������� id ������� ��� ��������� ���������� � ���
	$productsIds = array();
	foreach ( $orderProducts as $p) $productsIds[] = $p['id'];
	
	$products = getProducts(false, array('id' => $productsIds));
	$products = $products['products'];
	
	//��������� $order['products'] ������� � ������
	foreach ($orderProducts as &$orderP) {
		foreach ($products as $p) {
			if ($orderP['id'] == $p['id']) {
				$orderP['info'] = $p;
				break;
			}
		}
	}
}
//���������� url ������������ �������������� �������
function getTemplateLink($linkData, $linkType, $fullUrl = false) {
	$template = Config::get('template.link_' . $linkType);
	if ($template === false) return '';
	if (!isset($linkData['id'])) $linkData['id'] = '';
	if (!isset($linkData['chpu'])) $linkData['chpu'] = '';
	//� ������� ������ ���������� id ��� chpu
	$rep = array(
		'[id]' => $linkData['id'],
		'[chpu]' => $linkData['chpu']
	);
	
	return ($fullUrl ? rtrim(Config::get('site.web_addr'), '/') : '').strtr($template , $rep);
}
function getSetString($data) {
	$setArray = array();
	
	foreach($data as $column => $value) {
		$setArray[] = "`{$column}` = '{$value}'";
	}
	
	return implode("," , $setArray);
}
//����� ������ ������� �������� E-Mail. Yeah!
function sendMail($params) {
	//��������� ���������
	$defaultParams = array(
		'emailTo' => '',
		'emailFrom' => Config::get('site.email_from'),
		'subject' => '��������� �� "'.Config::get('site.name').'"',
		'body' => ''
	);
	//��������� ��������� ���������
	$params = array_merge($defaultParams, $params);
	
	return  mail($params['emailTo'], $params['subject'], $params['body'],
				join("\r\n", array(
				"From: {$params['emailFrom']}",
				//"Reply-To: admin@med-serdce.ru",
				"Content-type: text/html;\n\t charset=windows-1251",
				"X-Mailer: PHP/" .phpversion() ) )
				); 
}
//���������� ��������� ���� �� �������� � ������������� 
function getImagePath($imageType) {
	return $_SERVER['DOCUMENT_ROOT'].'/'.Config::get('path.images_upload').Config::get('path.images_'.$imageType);
}
//���������� ��� ���� �� �������� � �������������
function getImageWebPath($imageType, $fullUrl = true) {
	if ($fullUrl) {
		return Config::get('site.web_addr').Config::get('path.images_upload').Config::get('path.images_'.$imageType);
	} else {
		return '/'.Config::get('path.images_upload').Config::get('path.images_'.$imageType);
	}
}
//���������� ������� ����������� �� �������
function getConfigImageSize($imageType) {
	$size = Config::get('image_size.'.$imageType);
	//TODO ([0-9]+)(?:\*|x|X|�|�|:){1}([0-9]+)
	$size = explode('x', $size);
	if (count($size) == 2) {
		$size = array(
			'width' => $size[0],
			'height' => $size[1]
		);
		return $size;
	} else {
		return false;
	}
}
//���������� �������� ����������� �� �������
function getConfigImageQuality($imageType) {
	return Config::get('image_quality.'.$imageType);
}
//���������� ������ ����������� �����������
function getConfigImageAutoresize($imageType) {
	$t = Config::get('image_autoresize.'.$imageType);
	return $t == '1';
}
//Deprecated. Use getMenu()
function zh_getMenuResource() {
	$maxpodcount = 9999;
	$query_str = ("
	SELECT
		CASE WHEN menuleft_el.pod=0 THEN
				1
			ELSE
				0
			END AS is_group,

		CASE WHEN menuleft_el.pod>0 THEN
				(menuleft_parent.rang + 1)*" . ($maxpodcount + 1) . "+menuleft_el.rang
			ELSE
				(menuleft_el.rang + 1)*" . ($maxpodcount + 1) . "
			END AS ex_rang,

		CASE WHEN menuleft_el.pod>0 THEN
				menuleft_el.pod
			ELSE
				menuleft_el.id
			END AS ex_pod,
		
		CASE
			WHEN menuleft_el.pod > 0 THEN (SELECT COUNT(*) FROM catalog WHERE cat = menuleft_el.cat_id)
			ELSE 0
		END as products_count,
		
		menuleft_el.pod,
		menuleft_el.title,
		menuleft_el.cat_id,
		menuleft_el.rang,
		menuleft_el.id,
		menuleft_el.chpu,
		cat.chpu as cat_chpu
	FROM
		menuleft as menuleft_el
			LEFT JOIN menuleft AS menuleft_parent
			ON menuleft_el.pod = menuleft_parent.id
			LEFT JOIN cat AS cat
			ON cat.id = menuleft_el.cat_id
	WHERE
		CASE WHEN menuleft_el.pod>0 THEN
				(menuleft_parent.rang + 1)*" . ($maxpodcount + 1) . "+menuleft_el.rang
			ELSE
				(menuleft_el.rang + 1)*" . ($maxpodcount + 1) . "
			END >= " . ($maxpodcount + 1) . "
	ORDER BY
		ex_rang,
		ex_pod
	");

	return mysql_query($query_str);
}

#���������� ��� ����
function getTags() {
	$tags = array();
	$res = mysql_query("SELECT * FROM `tags` ORDER BY `title`");
	while ($row = mysql_fetch_assoc($res)) {
		$tags[] = $row;
	}
	return $tags;
}

//���������� ��������� ����\��������������� � ���������
function getTagsInCat($id) {
	if ( !is_numeric($id) ) return false;
	$tags = array();
	
	$sel = mysql_query("SELECT * FROM `tags` WHERE `cat_id` = '{$id}' ORDER BY `rang`");
	while($row = mysql_fetch_assoc($sel)) {
		$tags[] = $row;
	}
	return $tags;
}
//���������� ���������� � ����\��������������� �� ��� ��� id
function getTagInfo($chpu) {
	if ( is_numeric($chpu) ) {
		return sqlFetch("SELECT * FROM `tags` WHERE `id` = '{$chpu}'");
	} else {
		$chpu = mysql_real_escape_string($chpu);
		return sqlFetch("SELECT * FROM `tags` WHERE `chpu` = '{$chpu}'");
	}
}
//���������� ����, ������� ������������ � ������
function getTagsInCatalog($catalogId) {
	if (!is_numeric($catalogId)) return false;
	
	$sel = mysql_query("
		SELECT
			`tags`.*
		FROM
			`tags_links`,
			`tags`
		WHERE
			`tags_links`.`tag_id` = `tags`.`id`
			AND
			`tags_links`.`catalog_id`  = '{$catalogId}'
	");
	$tags = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$tags[] = $row;
	}
	return $tags;
}

function getBrands()
{
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`cat`
		WHERE
			`is_brand`  =  1
		ORDER BY 'title'	
	");
	$brands = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$brands[] = $row;
	}
	return $brands;
}

//����������� ������� �� ���������������
function unlinkCatalogAndTags($catalogId, $tags = 'all') {
	if ($tags == 'all') {
		$whereString = "`catalog_id` = '{$catalogId}'";
	} elseif (is_numeric($tags)) {
		$whereString = "`catalog_id` = '{$catalogId}' AND `tag_id` = '{$tags}'";
	} elseif (is_array($tags)) {
		$whereString = "`catalog_id` = '{$catalogId}' AND `tag_id` IN ('".implode("','", $tags)."')";
	} else {
		return false;
	}
	
	mysql_query ("
		DELETE FROM
			`tags_links`
		WHERE
			{$whereString}
	");
	return true;
}
//
function linkCatalogAndTags($catalogId, $tags) {
	$tags = (array) $tags;
	
	foreach ($tags as $tag) {
		if (!is_numeric($tag)) continue;
		mysql_query("
			INSERT INTO
				`tags_links`
			SET
				`catalog_id` = '{$catalogId}',
				`tag_id` = '{$tag}'
		");
	}
	return true;
}
//���������� ������ � ��������� ����, ��� ��������� ���������� � ���-�� ������ � ���
//$allowSeparator - ����, ������������ �� ��������� ������������ � ������ � ������
//���� $allowSeparator = false, ����������� �� ����� �������� � �������.
//������� ����������� � ���� pod != 0, cat_id = 0
function getMenu($allowSeparator = false) {
	$items = array();
	$sel = mysql_query("SELECT * FROM `menuleft` WHERE `cat_id` = '0' AND `pod` = '0' ORDER BY `rang`");
	while($row = mysql_fetch_assoc($sel)) {
		$items[$row['id']] = array(
			'id' => $row['id'],
			'title' => $row['title'],
			'chpu' => $row['chpu'],
			'submenu' => array()
		);
	}
	
	$sel = mysql_query("
		SELECT
		*,
		(SELECT COUNT(*) FROM `catalog` WHERE `cat` = `menuleft`.`cat_id`) as `products_count`,
		(SELECT `chpu` FROM `cat` WHERE `id` = `menuleft`.`cat_id`) as `chpu`,
		(SELECT `title` FROM `cat` WHERE `id` = `menuleft`.`cat_id`) as `title`
		FROM
			`menuleft`
		WHERE `pod` <> '0' ORDER BY `rang`
	");
	while($row = mysql_fetch_assoc($sel)) {
		$row['separator'] = false;
		//���� ��� �����������
		if ($row['cat_id'] == '0' && $row['pod'] != '0') {
			if ($allowSeparator)
				$row['separator'] = true;
			else
				continue;
		}
		$items[$row['pod']]['submenu'][] = $row;
	}
	return $items;
}
//���������� ���������, ������� ��� � ����
function getCatsNotInMenu() {
	$sel = mysql_query("
		SELECT 
			*,
			(SELECT COUNT(*) FROM `catalog` WHERE `catalog`.`cat` = `cat`.`id`) as `products_count`
		FROM
			`cat`
		WHERE
			`id` NOT IN (
				SELECT cat_id FROM menuleft
			)
	");
	$cats = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$cats[] = $row;
	}
	return $cats;
}
//���������� ������ ��������� � ������ ����
function getCatsInMenuItem($chpu) {
	$chpu = mysql_real_escape_string($chpu);
	$cats = array();
	$sel = mysql_query("
		SELECT
		*,
		(SELECT `title` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `title`
		FROM `menuleft`
		WHERE `chpu` = '{$chpu}'
	");
	if ($res = mysql_fetch_assoc($sel)) {
		$sel = mysql_query("
			SELECT
				`id`, `pod`,`cat_id`,`rang`,
				(SELECT `title` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `title`,
				(SELECT `chpu` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`) as `chpu`,
				IFNULL(`menuleft`.`seo_title`, (SELECT `seo_title` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`)) as `seo_title`,
				IFNULL(`menuleft`.`seo_text`, (SELECT `seo_text` FROM `cat` WHERE `cat`.`id` = `menuleft`.`cat_id`)) as `seo_text`,
				(SELECT MIN(`price_after_discount`) FROM `catalog` WHERE `catalog`.`cat` = `menuleft`.`cat_id`) as `min`,
				(SELECT MAX(`price_after_discount`) FROM `catalog` WHERE `catalog`.`cat` = `menuleft`.`cat_id`) as `max`,
				(SELECT count(`price_after_discount`) FROM `catalog` WHERE `catalog`.`cat` = `menuleft`.`cat_id`) as `products_count`
			FROM `menuleft`
			WHERE `pod` = '{$res['id']}'
			AND `menuleft`.`cat_id` != -1 ORDER BY `rang`");
	} else {
		return false;
	}
	while ($res = mysql_fetch_assoc($sel)) {
		$cats[] = $res;
	}
	return $cats;
}
//���������� ���������� � ������ ����
function getMenuItemInfo($chpu) {
	if (is_numeric($chpu)) {
		$sel = mysql_query("SELECT * FROM `menuleft` WHERE `id` = '{$chpu}' ");
	} else {
		$sel = mysql_query("SELECT * FROM `menuleft` WHERE `chpu` = '".mysql_real_escape_string($chpu)."'");
	}
	if ($res = mysql_fetch_assoc($sel)) {
		return $res;
	} else {
		return false;
	}
}
//���������� �������� � ����������� � ������������ ���������
/*
[params] => Array
        (
            [0] => Array
                (
                    [id] => 10
                    [param_id] => 3
                    [value] => 
                    [value_float] => 1
                    [cat_id] => 0
                )
			...
        )
*/
function getMinAndMaxParams($params) {
	$params = (array) $params;
	if (count($params) == 0)
		return false;
	if (count($params) == 1)
		return array('min' => $params[0], 'max' => $params[0]);
	else {
		$min = $max = $params[0];
		foreach ($params as $p) {
			if ($min['value_float'] > $p['value_float'])
				$min = $p;
			if ($max['value_float'] < $p['value_float'])
				$max = $p;
		}
		return array('min' => $min, 'max' => $max);
	}
}
//���������� ������ �������
//function getProducts ($page = false, $filter = false, $sort = false) {
function getProducts ($select = false, $filter = false, $sort = false, $page = false) {
	global $debug;
	if ($select == false) $select = 'catalog.*';
	$select = (array) $select;

	$fromString = '`catalog`';

	$products = array();
	$pagesCount = 0;
	$productsCount = 0 ;
	
	//������� WHERE �������� ������
	$compareType = array('!', '<', '>', '>=', '<=');
	$whereClause = array();
	$useParams = false;
	
	if ($filter != false) {
		foreach ($filter as $name => $value) {
			//���� ������������ ���������� �� ����(���������������), � �� ���������
			if ($name == 'tag') {
				$value = (array) $value;
				$value = "'" . implode("','", $value) . "'";

				$fromString = "
					(SELECT * FROM `catalog`, `tags_links` WHERE
					`id` = `tags_links`.`catalog_id` AND `tags_links`.`tag_id` IN ( {$value} )
					) as `catalog`";
					
				continue;
			}
			
			if (is_array($value)) {
				$whereClause[] = "{$name} IN ('".implode("','", $value)."')";
			} else {
				//�������� ��� ������� � ��� ���������
				$compare = '=';
				$type = substr($name, 0, 2);
				if (in_array($type, $compareType)) {
					$compare = $type;
					$name = substr($name, 2);
				} elseif (in_array($type[0], $compareType)) {
					$compare = ($type[0] == '!' ? '<>' : $type[0]);
					$name = substr($name, 1);
				}
				$whereClause[] = "{$fromString}.{$name} {$compare} '{$value}'";
			}
		}
	}

	$whereString = '';
	if (count($whereClause) > 0) {
		$whereString = 'WHERE '.implode(' AND ', $whereClause);

		// ������� ��� id ���������
		$whereString2 = strstr($whereString, ' AND `catalog`.cat', true);
		$whereString2 = str_replace('WHERE', 'AND', $whereString2);
	}
	// ������� ��� ���������������
	
	// TODO: ��������� �� ����. 
	if(isset($filter['cat']) && $filter['cat'])
	{	
		$query_s = mysql_query("SELECT catalog_id FROM catalog_2_cat WHERE cat_id = {$filter['cat']}") or die(mysql_error());
		while ($res = mysql_fetch_assoc($query_s)) {
			
			$arrProductsDop[] = $res['catalog_id'];
			
		}
		
		if(!empty($arrProductsDop))
		{
			$prepare_ids = implode(", ", $arrProductsDop);
			//var_dump($prepare_ids);
			
			$whereString .= " OR id IN ({$prepare_ids}) {$whereString2}"; 
			
		}
		
		
		
	}	
		
		//$queryCatys = "SELECT catalog_id FROM catalog_2_cat WHERE cat_id = {$filter['cat']}";
		// i$whereString .= " OR id IN (SELECT catalog_id FROM catalog_2_cat WHERE cat_id = {$filter['cat']}) {$whereString2}"; 
	
	// ����� ������� 
	
	//����������
	$sortClause = '';
	switch ($sort) {
		case 'price_desc' : $sortClause = 'ORDER BY `price_after_discount` DESC';
			break;
		case 'price_asc' : $sortClause = 'ORDER BY `price_after_discount` ASC';
			break;
		case 'news_sale' : $sortClause = 'ORDER BY `novinka` DESC, `discount_value` DESC';
			break;
		//default : $sortClause = 'ORDER BY `spec_rang` ASC';
		default : $sortClause = 'ORDER BY `best_fake` DESC, `active_rests` DESC , `spec_rang` ASC ';
		//default : $sortClause = 'ORDER BY `cat`, `spec_rang` ASC';
	}
	
	//����� ��� ��������� �� �������� � ����������� ���
	$q2 = mysql_query("
				SELECT 
					COUNT(*) as amount,
					MAX(`price_after_discount`) as max_price,
					MIN(`price_after_discount`) as min_price
				FROM
					{$fromString}
				{$whereString}
			") or die(mysql_error()); 
	$res = mysql_fetch_assoc($q2);
	$outOfBounds = false;
	if ($page == false || !isset($page['page']) || $page['page'] == 'all' || !isset($page['onPage']) || !is_numeric($page['onPage'])) {
		$limit = '';
	} else {
		$pagesCount = ceil($res['amount'] / $page['onPage']);
        $productsCount = $res['amount'];
		if ($page['page'] <= $pagesCount || $pagesCount == 0) {
			$limit = 'LIMIT '.(($page['page'] - 1) * $page['onPage']).",{$page['onPage']}";
		} else {
			//������� �������� �� ��������� ���������
			$outOfBounds = true;
			$limit = 'LIMIT 0,'.$page['onPage'];
		}
	}
	$price = array(
		'min' => $res['min_price'],
		'max' => $res['max_price']
	);
	
	//������� ������ ������� ��� SELECT
	$selectString = implode(',', $select);
	$str = "
		SELECT
			(SELECT `amount` FROM `storage_rests` WHERE `catalog`.`id` = `storage_rests`.`catalog_id` AND `storage_rests`.`storage_id` = '6' LIMIT 1) as `rest_main`,
			(SELECT `amount` FROM `storage_rests` WHERE `catalog`.`id` = `storage_rests`.`catalog_id` AND `storage_rests`.`storage_id` = '7' LIMIT 1) as `rest_extra`,
			
			IFNULL((select SUM(d.`amount`) from `storage_rests` as d 
    left join `storages_new` as e on e.storage_id = d.storage_id 
    where e.is_active = 1 and d.catalog_id = `catalog`.id ),0) as `active_rests`, 
    
            IFNULL((select SUM(d.`amount`) from `storage_rests` as d 
    left join `storages_new` as e on e.storage_id = d.storage_id 
    where e.is_active = 1 and d.catalog_id = `catalog`.id  and d.storage_id = 1 ),0) as `active_main`,
    
            IFNULL((select SUM(d.`amount`) from `storage_rests` as d 
    left join `storages_new` as e on e.storage_id = d.storage_id 
    where e.is_active = 1 and d.catalog_id = `catalog`.id  and d.storage_id <> 1 ),0) as `active_extra`,
	
			( `fake_in_stock` > 0  AND `best` > 0 ) as `best_fake`,
			 
			{$selectString}
		FROM
		{$fromString}
		{$whereString}
		{$sortClause}
		{$limit}
		";
		
		//print_r($str);
	
	//die();
	$sel = mysql_query($str) or die(mysql_error());
	while ($res = mysql_fetch_assoc($sel)) {
			$res['rests_main'] = array(
			//�������� �� ������ � �������
			//'available' => $res['rest_main'] != '' || $res['rest_extra'] != '' ? true : false,
			'available' => ($res['active_rests'] != 0 )? true : false,
			//����� �� �������
			'summ' => $res['active_rests'] ,
			//�������� �����
			'main' => $res['active_main'] ,
			//��������������
			'extra' => $res['active_extra'] ,
			);
		$res['discount_summ_value'] = $res['price'] - getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
		$res['discount_summ_percent'] = $res['discount_type'] == 'percent' ? $res['discount_value'] : 100 - round((getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']) * 100) / $res['price']);
		
		// ��������� ��������� ������
		$query = "SELECT `pa`.`title`, `pav`.`value`, `pav`.`value_float`, `pa`.`unit`
					FROM `params_available` AS `pa`
					LEFT JOIN `params_available_values` AS `pav`
					ON `pa`.`id` = `pav`.`param_id`
					LEFT JOIN `params_catalog_links` AS `pcl`
					ON `pcl`.`param_id` = `pav`.`id`
					WHERE `pcl`.`catalog_id` = {$res['id']}";
	    $rs = mysql_query($query);
	    while ($res2 = mysql_fetch_assoc($rs)) {
	    	$res['params'][$res2['title']]['value'] = $res2['value'];
	    	$res['params'][$res2['title']]['value_float'] = $res2['value_float'];
	    	$res['params'][$res2['title']]['unit'] = $res2['unit'];
	    }
		$products[] = $res;
	}

	return array( 
		'products' => $products,
		'page' => array('pagesCount' => $pagesCount, 'currentPage' => $page['page'], 'outOfBounds' => $outOfBounds, 'p_count' => $productsCount),
		'price' => $price
		);
}


// ��������� ������� ������� �����������
function getAmount($product_id) {
	
	$result = 0 ;
    $q = mysql_query("select id, IFNULL((select SUM(d.`amount`) from `storage_rests` as d 
    left join `storages_new` as e on e.storage_id = d.storage_id 
    where e.is_active = 1 and d.catalog_id = {$product_id} ),0) as `active_rests`
    from `catalog` as `catalog`
    where `catalog`.id = {$product_id};") ;
    while ($res = mysql_fetch_assoc($q)) {
        $result = $res['active_rests'];
    }
    return $result;
}


// �������� ������� �� ������� �� id ������
function getActiveStoragesAmount($productId){
    
    $info = [];
    
    $q_get_storages = mysql_query("select storage_id from `storages_new` where is_active = 1 ;") ;
    while ($res1 = mysql_fetch_assoc($q_get_storages)) {
        $info[$res1['storage_id']] = 0;
    }

    $q_get_amount = "select a.storage_id,  a.amount
                    from storage_rests as a
                    left join storages_new as b on a.storage_id = b.storage_id
                    where catalog_id = {$productId} and b.is_active = 1 ;";
    $query = mysql_query($q_get_amount);
    
    while ($res = mysql_fetch_assoc($query)) {
        $info[$res['storage_id']] = $res['amount'];
    }
    return $info;
}



//���������� ������ �������, ��������� � ������� ��������������� ������� �� �������
function getProductsWithExtraFilter($select = false, $filter = false, $sort = false, $page = false) {
	if ($select == false) $select = '*';
	$select = (array) $select;

	$fromString = '`catalog`';
	$products = array();
	$pagesCount = 0;
	
	//������� WHERE �������� ������
	$compareType = array('!', '<', '>', '>=', '<=');
	$whereClause = array();
	$useParams = false;
	


	if ($filter != false) {
		foreach ($filter as $name => $value) {
			//���� ������������ ���������� �� ����(���������������), � �� ���������
			if ($name == 'tag') {
				$value = (array) $value;
				$value = "'" . implode("','", $value) . "'";
				
				$fromString = "
						(SELECT * FROM `catalog`, `tags_links` WHERE
						`id` = `tags_links`.`catalog_id` AND `tags_links`.`tag_id` IN ( {$value} )
						) as `catalog`";
				
				continue;
			}
			if (is_array($value)) {
				if ($name != 'params') {
					$whereClause[] = "{$name} IN ('".implode("','", $value)."')";
				}
			} else {
				//�������� ��� ���������
				$compare = '=';
				$type = substr($name, 0, 2);
				if (in_array($type, $compareType)) {
					$compare = $type;
					$name = substr($name, 2);
				} elseif (in_array($type[0], $compareType)) {
					$compare = ($type[0] == '!' ? '<>' : $type[0]);
					$name = substr($name, 1);
				}
				
				$whereClause[] = "{$name} {$compare} '{$value}'";
			}
		}
	}
	$whereString = '';
	if (count($whereClause) > 0) {
		$whereString = 'WHERE '.implode(' AND ', $whereClause);
	}
	
	//������� id �������� ����������, ����� ����� ���������� ����� ����������
	//����� ��������
	$valuesIds = array();
	/*
		$p = 
			array('type' => PARAM_VALUE, 'data' => 2)
			array('type' => PARAM_RANGE, 'data' => array('id' => 2, 'min' => 2, 'max' => 3))
	*/

	foreach($filter['params'] as $p) {
		if ($p['type'] == PARAM_VALUE || $p['type'] == PARAM_SET) {
			$valuesIds[] = $p['data'];
		} elseif ($p['type'] == PARAM_RANGE) {
			$paramsRange[] = $p;
		}
	}
	

	$paramsIn = "'".implode("','", $valuesIds)."'";
	
	//�������� id ���������� �� ������� (������ = id ���� ���������; ����, ���, ������ � ��.)
	$sel = mysql_query("
		SELECT 
			params_catalog_links.*,
			params_available_values.param_id as param_type_id
		FROM
			params_catalog_links
		LEFT JOIN params_available_values
			ON params_catalog_links.param_id = params_available_values.id
		WHERE
			params_catalog_links.param_id IN ({$paramsIn})
	");
	$groups = array();
	while ($res = mysql_fetch_assoc($sel)) {
		$groups[$res['param_type_id']][] = $res['param_id'];
	}
	//������� ���������
	foreach ($groups as $key => $value) {
		$groups[$key] = array_unique($value);
		$newArray = array();
		$newArray['type'] = PARAM_VALUE;
		foreach ($groups[$key] as $z) {
			$newArray['data'][] = $z;
		}
		$groups[$key] = $newArray;
	}
	//��������� � ������� ���� ���������
	foreach ($paramsRange as $p) {
		$groups[$p['data']['id']] = array('type' => PARAM_RANGE, 'data' => $p['data']);
	}
	
	//����������
	$sortClause = '';
	switch ($sort) {
		case 'price_desc' : $sortClause = 'ORDER BY `price_after_discount` DESC';
			break;
		case 'price_asc' : $sortClause = 'ORDER BY `price_after_discount` ASC';
			break;
		case 'news_sale' : $sortClause = 'ORDER BY `novinka` DESC, `discount_value` DESC';
			break;
		//default : $sortClause = 'ORDER BY `spec_rang` ASC';
	}
	
	//���������������  JOIN'� ��� �������
	$inner = '';
	$field = 'catalog.id';
	$onString = array();
	foreach ($groups as $key => $value) {
		if ($value['type'] == PARAM_VALUE) {
			$inner .= " INNER JOIN ( SELECT catalog_id FROM `params_catalog_links` WHERE `param_id` IN ('".implode("', '", $value['data'])."') ) as main".$key;
		} else {
			$inner .= " INNER JOIN ( SELECT catalog_id FROM `params_catalog_links` WHERE `param_id` IN 
				( SELECT id FROM `params_available_values` WHERE `value_float` >= '{$value['data']['min']}' AND `value_float` <= '{$value['data']['max']}' AND  `param_id` = '{$key}' ) 
			) as main".$key;
		}
		$onString[] = $field . ' = main' . $key . '.catalog_id';
		$field = 'main' . $key . '.catalog_id';
	}
	$onString = implode(' AND ', $onString);
	
	//����� ��� ��������� �� �������� � ���� � ��� ����
	//��������� �������� ������� ��������� �������
	$temp = mysql_fetch_assoc(mysql_query("
			SELECT 
				COUNT(DISTINCT catalog.id) as c,
				MAX(`price_after_discount`) as max_price,
				MIN(`price_after_discount`) as min_price
			FROM 
				{$fromString}
			{$inner}
			ON {$onString}
			{$whereString}
	"));
	if ($page == false || !isset($page['page']) || $page['page'] == 'all' || !isset($page['onPage']) || !is_numeric($page['onPage'])) {
		$limit = '';
	} else {
		$pagesCount = ceil($temp['c'] / $page['onPage']);
		if ($page['page'] <= $pagesCount) {
			$limit = 'LIMIT '.(($page['page'] - 1) * $page['onPage']).",{$page['onPage']}";
		} else {
			$limit = 'LIMIT 0,'.$page['onPage'];
		}
	}
	
	$price = array(
		'min' => $res['min_price'],
		'max' => $res['max_price']
	);
	
	//������� �� ������, ������� �������� ��� �������� �������������, ��������� � �������
	$sel = mysql_query ("
			SELECT 
				DISTINCT(catalog.id),
				catalog.*
			FROM 
				{$fromString}
			{$inner}
			ON {$onString}
				{$whereString}
				{$sortClause}
				{$limit}
	");

	while ($res = mysql_fetch_assoc($sel)) {
		$res['discount_summ_value'] = $res['price'] - getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
		$res['discount_summ_percent'] = $res['discount_type'] == 'percent' ? $res['discount_value'] : 100 - round((getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']) * 100) / $res['price']);
		$products[] = $res;
	}
	return array( 
		'products' => $products,
		'page' => array('pagesCount' => $pagesCount, 'currentPage' => $page['page']),
		'price' => $price
		); 
}
//���������� ���������� � ������
function getProduct($idOrChpu) {
	$data = array();
	
	if (is_numeric($idOrChpu)) {
		$sel = mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$idOrChpu}'");
	} else {
		$chpu = mysql_real_escape_string($idOrChpu);
		$sel = mysql_query("SELECT * FROM `catalog` WHERE `chpu` = '{$chpu}'");
	}
	
	//������ �� ������
	if ($line = mysql_fetch_assoc($sel)) {
		$data = $line;
	} else {
		return false;
	}

	//�������������� ����
	$data['extra_photo'] = array();
	
	$sel = mysql_query("SELECT * FROM `foto` WHERE `catalog_id` = '{$data['id']}' ORDER BY `rang`");
	while ($row = @mysql_fetch_assoc($sel)) {
		$data['extra_photo'][] = $row['id'];
	}
	
	//���������� � ���������
	$data['cat_info'] = getCatInfo($data['cat']);
	
	//���������� � ����� (��������������) ���������
	$data['tags'] = getTagsInCat($data['cat_info']['id']);
	//������� ������
	$data['near'] = array();
	
	$productsCount = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE	`cat` = '{$data['cat']}'");
	
	//��� �������, �� ����� �� ����
	if ($productsCount <= 5) {
		$sel = mysql_query("SELECT `id` FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
			ORDER BY `spec_rang`
			LIMIT 4
		");
	} else {
		//������� ������� �� ���� "������"
		$sel = mysql_query("SELECT `id` FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` >= '{$data['spec_rang']}'
			ORDER BY `spec_rang`
			LIMIT 4
		");
	}
	
	while ($row = mysql_fetch_assoc($sel)) {
		$data['near'][] = $row['id'];
	}
	//��������� �������� �� ������ (�� ����������)
	if (count($data['near']) < 4 && $productsCount > 5) {
		$lim = 4 - count($data['near']);
		$sel = mysql_query("SELECT `id` FROM `catalog` WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` >= '0'
			ORDER BY `spec_rang`
			LIMIT {$lim}
		");
		while ($row = mysql_fetch_assoc($sel)) {
			$data['near'][] = $row['id'];
		}
	}
	$tmp = getProducts(false, array('id' => $data['near']));
	$data['near'] = $tmp['products'];
	
	//����������\��������� �����
	$prev = sqlFetch("SELECT
				`id`, `title`, `chpu`, `price`, `price_after_discount`
			FROM `catalog`	WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
				AND `spec_rang` <= '{$data['spec_rang']}'
			ORDER BY `spec_rang` DESC
			LIMIT 1
		");
	if ($prev == false) {
		$prev = sqlFetch("SELECT 
				`id`, `title`, `chpu`, `price`, `price_after_discount`
				FROM `catalog` WHERE
				`cat` = '{$data['cat']}'
				AND `id` <> '{$data['id']}'
			ORDER BY `spec_rang` DESC
			LIMIT 1
		");
	}
	$next = $data['near'][0];
	
	if (($prev['id'] == $next['id']) && $productsCount > 2) {
		$next = $data['near'][1];
	}
	
	$data['around'] = array(
		'prev' => $prev,
		'next' => $next
	);
	
	$recCount = 0;
    $notArr = array();
    $recQ = mysql_query('select `catalog_id2`,`catalog`.* from `same_products_final` left join `catalog` on `catalog`.`id`=`same_products_final`.`catalog_id2` where `count`<>1 and `catalog_id1`=' . $data['id'] . ' order by `count` desc limit 4');
    // echo 'select `catalog_id2`,`catalog`.* from `same_products_final` left join `catalog` on `catalog`.`id`=`same_products_final`.`catalog_id2` where `count`<>1 and `catalog_id1`=' . $data['id'] . ' order by `count` desc limit 4';
	while ($recR = mysql_fetch_array($recQ)) {
      $data['recomm'][] = $recR;
      $notArr[] = $recR['id'];
      $recCount++;
    }
    unset($recR);
    unset($recQ);
    if ($recCount < 4) {
      $limit = 4 - $recCount;
      $notArr[] = $data['id'];
      $recQ = mysql_query('select `catalog_id`,`catalog`.* from `same_products_cat` left join `catalog` on `catalog`.`id`=`same_products_cat`.`catalog_id` where `cat_id`=' . $data['cat'] . ' and `catalog_id` not in (' . implode(',', $notArr) . ')  order by `count` desc limit ' . $limit);
      while ($recR = mysql_fetch_array($recQ)) {
        $data['recomm'][] = $recR;
      }
    }
    unset($recR);
    unset($recQ);
	
	//��������� ���������
	$sel = mysql_query("
		SELECT
			*,
			`params_available_values`.`param_id` as `param_type_id`,
			`params_available_values`.`id` as `param_available_id`
		FROM `params_catalog_links`
		INNER JOIN `params_available_values`
			 ON `params_catalog_links`.`param_id` =  `params_available_values`.`id`
		INNER JOIN `params_available`
			ON `params_available_values`.`param_id` =  `params_available`.`id`
			
		WHERE
			`catalog_id` = '{$data['id']}'
	");
	$params = array();
	while ($row = mysql_fetch_assoc($sel)) {
		$params[$row['param_type_id']]['title'] = $row['title'];
		$params[$row['param_type_id']]['type'] = $row['type'];
		$params[$row['param_type_id']]['type_id'] = $row['param_type_id'];
		$params[$row['param_type_id']]['unit'] = $row['unit'];
		$params[$row['param_type_id']]['values'][] = array(
															'value' => $row['type'] == PARAM_RANGE ? $row['value_float'] : $row['value'],
															'id' => $row['param_available_id']);
	}
	//������ �����
	$params = array_merge($params);
	
	$data['params'] = $params;
	
	//��������� �������
	$rests = array();
	$sel = mysql_query("SELECT * FROM `storage_rests` WHERE `catalog_id` = '{$data['id']}'");
	while ($row = mysql_fetch_assoc($sel)) {
		$rests[$row['storage_id']] = $row['amount'];
	}
        if (!isset($rests[1])) $rests[1] = 0;
        if (!isset($rests[2])) $rests[2] = 0;
	$data['rests'] = $rests;
	$data['active_rests'] = getAmount($data['id']);
	$data['rests_main'] = array(
		//�������� �� ������ � �������
		'available' => isset($rests[1]) || isset($rests[2]),
		//����� �� �������
		'summ' => $rests[1] + $rests[2],
		//�������� �����
		'main' => $rests[1],
		//��������������
		'extra' => $rests[2]
	);
	
	//����������� ��������� �������� �� ������
	if ($data['cat'] > 0 && $data['price_after_discount'] >= 0) {
		$data['deliveryPrice'] = deliveryPrice::getByCatAndPrice($data['cat'], $data['price_after_discount'], $data['id']);
	} else {
		$data['deliveryPrice'] = false;
	}
	
	//�������������� ������ � ������ �� �����
	$data['discount_summ_value'] = $data['price'] - getPriceAfterDiscount($data['price'], $data['discount_type'], $data['discount_value']);
	$data['discount_summ_percent'] = $data['discount_type'] == 'percent' ? $data['discount_value'] : 100 - round((getPriceAfterDiscount($data['price'], $data['discount_type'], $data['discount_value']) * 100) / $data['price']);
	
	return $data;
}
//���������� ������ � ��������� �� id ��� chpu
function getCatInfo($idOrChpu) {
	$where = (is_numeric($idOrChpu) ? 'id' : 'chpu') . " = '".mysql_real_escape_string($idOrChpu)."'";
	return mysql_fetch_assoc(mysql_query("SELECT * FROM `cat` WHERE " . $where));
}
//���������� ������ ��������� � ��������������
//2 ������ �����������
function getCats() {
	$cats = array();
	
	$sel = mysql_query("SELECT * FROM `cat` ORDER BY `pod`, `title`");
	while ($line = mysql_fetch_assoc($sel)) {
		if ($line['pod'] == 0) {
			$cats[ $line['id'] ] = $line;
		} else {
			$cats[ $line['pod'] ]['subcats'][] = $line;
		}
	}
	return $cats;
}
/*
��������� ����� � ����, ���������� ������, ������ ������� ����������

$productsInCart[ $productTemp[0] ] = array(
					'id' => $productTemp[0],
					'amount' => $productTemp[1]
					);
*/
function makeOrder($productsInCart, $orderInfo, $set_discount) {
	//���� ������� � ������� ���, �� ���������� ������
	if (count($productsInCart) <= 0 && !$orderInfo['backcall']) {
		return array(
			'error' => array(
				'code' => 1,
				'message' => 'empty cart'
			)
		);
	}
	$data['date'] = time();
	$data['name'] = mysql_real_escape_string(iconv("utf-8", "windows-1251", $orderInfo['name']));
	$data['phone'] = mysql_real_escape_string(iconv("utf-8", "windows-1251", $orderInfo['phone']));
	$data['email'] = mysql_real_escape_string(iconv("utf-8", "windows-1251", $orderInfo['email']));
	$data['adress'] = mysql_real_escape_string(iconv("utf-8", "windows-1251", $orderInfo['post_index'].' '.$orderInfo['adress']));
	$data['extra_information'] = mysql_real_escape_string(iconv("utf-8", "windows-1251", $orderInfo['extra_information']));
	$data['delivery_type'] = mysql_real_escape_string($orderInfo['delivery_type']);
	$data['payment_type'] = mysql_real_escape_string($orderInfo['payment_type']);
	if (empty($data['delivery_type'])) $data['delivery_type'] = 1;
	if (empty($data['payment_type'])) $data['payment_type'] = 1;
	
	if ($orderInfo['backcall']) {
		$data['delivery_type'] = 0;
		$data['extra_information'] = '�������� ������';
	}
	
	//���� "������ ������ ������������"	
	$data['h'] = mysql_real_escape_string($orderInfo["ref"]);
	
	
	if (strlen($data['name']) >= 3 && strlen($data['phone']) >= 5) {

		//������, ������� ������ � ����
		$productsToBase = array();
		//������ � ���� � ���, ������� ������ � ������ ���������
		$productsInfo = array();
		//������, ������� ����� �������������� ��� �������� � JS
		$productsToResponse = array();
		//��������� ��������� � ������ ������ �� ������
		$priceSumm = 0;
		$productsAmount = 0;
		
		$sel = mysql_query("
			SELECT
				*
			FROM
				`catalog`
			WHERE
				`id` IN ('". implode("','", array_keys($productsInCart)) ."')
		");
		while ($line = mysql_fetch_assoc($sel)) {
			$productsToBase[] = array(
				'id' => $line['id'],
				'amount' => $productsInCart[ $line['id'] ]['amount'],
				'price' => $line['price'],
				'price_after_discount' => $line['price_after_discount']
			);
			$productsToResponse[] = array(
				'id' => $line['id'],
				'amount' => $productsInCart[ $line['id'] ]['amount'],
				'price' => $line['price'],
				'price_after_discount' => $line['price_after_discount'],
				'converted_title' =>  iconv("windows-1251", "utf-8", $line['title']),
				//'title' => $line['title']
			);

			// ���� � ���� price_after_discount == null ����� price
			$price = isset($line['price_after_discount']) ? $line['price_after_discount'] : $line['price'];

			$priceSumm += $price * $productsInCart[ $line['id'] ]['amount'];
			$productsAmount += $productsInCart[ $line['id'] ]['amount'];
			
			//������ ������ � ������� ��� ������ � ������ ���������
			$productsInfo[] = array_merge(
								$line,
								array('amount' => $productsInCart[ $line['id'] ]['amount'])
							);
		}
		
		//��������� ������ � ���� �������� ��� ������� �� ������
		$temp = cart::getDeliveryAndDiscount($productsToBase);
		
		$deliveryPrice = $temp['deliveryPrice'];
		$discount['value'] = $set_discount ? $set_discount : $temp['discount'];
		$discount['type'] = 'percent';

		$price = array(
			'discount_type' => $discount['type'],
			'discount_value' => $discount['value'],
			'price_after_global_discount' => getPriceAfterDiscount($priceSumm, $discount['type'], $discount['value']),
			'price_before_global_discount' => $priceSumm,
			'delivery_price' => $temp['deliveryPrice']
		);
		$data['products'] = serialize($productsToBase);
		$data['order_price'] = serialize($price);

		/*
			products = 
			array(
				'id' =>
				'amount' =>
				'price' =>
				'price_after_discount' =>
			)
			order_price = 
			array(
				'discount_type' =>
				'discount_value' =>
				'price_after_global_discount' =>
			)
		*/

		$sqlParams = array();
		foreach ($data as $key => $value) {
			$sqlParams[] = "`{$key}` = '{$value}'";
		}
		mysql_query("
			INSERT INTO
				`orders`
				SET ".(implode(', ', $sqlParams))."
			");

		$orderId = mysql_insert_id();
		$orderCode = getOrderCode($orderId);
		
		
		//�������������� �������� ������ ���������
		query_config();

		$conf['site_url'] = Config::get('site.web_addr');
		$conf['support_email'] = Config::get('site.email_support');
		$conf['site_name'] = Config::get('site.name');
		//���� ������
		$subject = "����� � ����� {$config_url} {$orderCode}";

		// ��������� ��������� �� ����
		$header1 = "From: {$data['name']} <{$data['phone']}>\r\n";
		//��������� ����� ������
		$body = '<html>';
		
		//�������� �������
		$sd = mysql_query("SELECT * FROM `order_delivery_types` WHERE `id` = '{$data['delivery_type']}'");
		$deliveryName = mysql_fetch_assoc($sd);
		$deliveryName = $deliveryName['name'];
		
// mail comment begining

		foreach ($productsInfo as $p) { 

			$body .= '
			<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
			  <tr>
				<td width="80" align="left" valign="top"><a href="' . $conf['site_url'] . 'medtehnika/' . $p['chpu'] . '.html"><img src="'.getImageWebPath('images_product_small',true). '.jpg"  border="0" align="left" /></a></td>
				<td align="left" valign="top"><a href="' . $conf['site_url'] . 'medtehnika/' .$p['chpu']. '.html" style="color:#333;">' . $p['title'] . '</a> <span style="font-size: 1.3em;">(' . $p['amount'] . ' ��.)</span><br />
				  <br />
				  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;">' . $p['price_after_discount'] . '</span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">���.</span>
				 </td>
				</tr>
			</table>';
		}
		$body .= '
		<span style="font-size: 1.2em;">����� �����: ' . $price['price_after_global_discount'] . ' (������ ' . $price['discount_value']. ' ' .getDiscountTypeString($price['discount_type']) . ')</span><br />
		<br />
		<strong>���:</strong> ' . $data['name'] . '<br />
		<strong>���.:</strong> ' . $data['phone'] . '<br />
		<strong>��������:</strong> ' . $deliveryName . '<br />
		<strong>�����:</strong> ' . $data['adress'] . '<br />';

		if ($data['email'] != '') $body .= ('<strong>E-mail:</strong> ' . $data['email'] . '<br />');
		$body .='<strong>�������������:</strong> ' . $data['adress'] . '<br />';
		
		$body .= '</html>';


		// ���������� ����� �� ����� �����
		sendMailPHPMailer($subject, $body, Config::get('site.email_manager'));
		/*sendMail(array(
			'emailTo' => Config::get('site.email_manager'),
			'subject' => $subject,
			'body' => $body
		));*/
		
		// --- ���������� ������ ������������ � ���, ��� ����� ��������
		// ���� ���������� ���� email, �� ���������� ��� ������ � ������ 
		if (validateEmail($data['email'])) {
			$tema = "������ ������ �������� ����� {$orderCode} ({$conf['site_url']})";
			$pismo = "<html>".
			"�� ����, ��� �� ������� ������ ��� �������� �������.".
			"<br>".
			"������ ������ �������� ����� <strong>{$orderCode}</strong><br>".
			"� ��������� �����, ��� ������������� ������, � ���� �������� ��� ��������.".
			"<br>".
			"���� � ���������� ������ ������ �������� �������� - ������ �� {$conf['support_email']} .".
			"<br>".
			"� ���������, {$conf['site_name']}.".
			"</html>";

			sendMailPHPMailer($tema, $pismo, $data['email']);
			/*
			sendMail(array(
				'emailTo' => $data['email'],
				'subject' => $tema,
				'body' => $pismo
			));*/
		}
// comment end

		return array(
			'orderId' => $orderId,
			'orderCode' => $orderCode,
			'deliveryName' => $deliveryName,
			'productsToBase' => $productsToBase,
			'price' => $price,
			'productsToResponse' => $productsToResponse
		);


	} else {
		return array(
			'error' => array(
				'code' => 2,
				'message' => 'empty main client info (name or phone)'
			)
		);
	}
}


// ������������ ������� �������� ������
function makeWholeSalerOrder($orderInfo){

    $data['date'] = time(); //iconv("utf-8", "windows-1251", $orderInfo['name']);
    $data['name'] = mysql_real_escape_string(iconv("utf-8", "windows-1251",$orderInfo['name']));
    $data['phone'] = mysql_real_escape_string(iconv("utf-8", "windows-1251",$orderInfo['phone']));
    $data['email'] = mysql_real_escape_string($orderInfo['email']);
    $data['extra_information'] = mysql_real_escape_string("����� �������� ������ " . iconv("utf-8", "windows-1251",$orderInfo['extra_information']));

    $data['adress'] = '';
    $data['delivery_type'] = 1;
    $data['payment_type'] = 1;

    $data['products'] = serialize(array());
    $data['order_price'] = serialize(array());

    if (strlen($data['name']) >= 3 ) {

        $sqlParams = array();
        foreach ($data as $key => $value) {
            $sqlParams[] = "`{$key}` = '{$value}'";
        }

        mysql_query("
			INSERT INTO
				`orders`
				SET ".(implode(', ', $sqlParams))."
			");
        $orderId = mysql_insert_id();

        // �������� ������ �� �����
        $subject = "����� �������� ������ � ����� " . Config::get('site.web_addr') . " " . getOrderCode($orderId) ;
        $message = '<strong>���:</strong> ' . $data['name'] . '<br />
		<strong>���.:</strong> ' . $data['phone'] . '<br />
		<strong>E-mail:</strong> ' . $data['email']  . '<br />
		<strong>���������� :</strong> ' . $data['extra_information'] . '<br />' ;

        $mail = sendMailPHPMailer($subject, $message, Config::get('site.email_support'));

        return array(
            'orderId'   => $orderId,
            'mail'      => $mail,
        );
    } else {
        return array(
            'error' => array(
                'code' => 2,
                'message' => 'empty main client info (name or phone)'
            )
        );
    }

}



//������ � ����������� ���
//�������������� ��������� � ������ (����� ? (����� �������))
class queryString {
	protected $params = array();
	protected $hash;
	public function __construct($url = false) {
		if ($url == false) {
			$this->params = $_GET;
			unset($this->params['c'], $this->params['t']);
		} else {
			$this->params = $url;
		}
	}
	public function getParam($key) {
		if (isset($this->params[$key]))
			return $this->params[$key];
		else
			return null;
	}
	public function setParam($key, $value) {
		$obj = clone $this;
		$obj->params[$key] = $value;
		return $obj;
	}
	public function removeParam($key) {
		$obj = clone $this;
		if (isset($obj->params[$key])) unset($obj->params[$key]);
		return $obj;
	}
	public function setHash($string) {
		$obj = clone $this;
		$obj->hash = $string;
		return $obj;
	}
	public function __toString() {
		if (count($this->params) > 0 ) {
			$temp = array();
			foreach ($this->params as $key => $value) {
				$temp[] = $key . '=' . $value;
			}
			return '?' . implode('&', $temp).(!empty($this->hash) ? '#'.$this->hash : '');
		} else {
			return ''.(!empty($this->hash) ? '#'.$this->hash : '');
		}
	}
}

function getPage() {
	$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : 1;
	if ($page == 'all') {
		return 'all';
	} else if (is_numeric($page) && $page > 0) {
		return $page;
	} else {
		return 1;
	}
}
function generatePaginator($data, $qString = false, $onclick = false) {
	if ($data['pagesCount'] > 1) {
		if ($qString == false) $qString = new queryString();
		echo '<div class="paginator"><table border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="90" align="left"><em class="txt_sort">��������:</em></td>
			<td align="left">';
				for ($i = 1; $i <= $data['pagesCount']; $i++) { 
					if ($i ==  $data['currentPage']) {
						echo '<div class="divstr"><div><div class="divstrvns current">'.$i.'</div></div></div>';
					} else {
						if ($onclick != false) {
							echo '<div class="divstr"><a onclick="'.str_replace('[page]', $i, $onclick).'" href="#"><div class="divstrvn">'.$i.'</div></a></div>';
						} else {
							echo '<div class="divstr"><a href="'.$qString->setParam('page', $i).'"><div class="divstrvn">'.$i.'</div></a></div>';
						}
					}
				}
		echo '</td>';
		if ($onclick != false) {
			echo '<td width="70" align="right"><a onclick="'.str_replace('[page]', "'all'", $onclick).'" href="#"><em>��� �����</em></a></td>';
			} else {
			echo '<td width="70" align="right"><a href="'.$qString->setParam('page', 'all').'"><em>��� �����</em></a></td>';
			}
		 echo ' </tr>
		</table></div>';
	}
}
function showPaginator($data, $qString = false, $onclick = false) {
	if ($data['pagesCount'] > 1) {
		if ($qString == false) $qString = new queryString();
		echo '<div class="paginator">';
		if ($data['currentPage'] != 1) 
			echo '<a href="'.$qString->setParam('page', $data['currentPage'] - 1).'" title="" class="page-back"' . ($onclick ? 'onclick="'.str_replace('[page]', $data['currentPage'] - 1, $onclick).'"' : '') . '>�����</a>';
		if ($data['currentPage'] != $data['pagesCount'])
			echo '<a href="'.$qString->setParam('page', $data['currentPage'] + 1).'" title="" class="page-forward"' . ($onclick ? 'onclick="'.str_replace('[page]', $data['currentPage'] + 1, $onclick).'"' : '') . '>������</a>';

		echo '<div class="pages">';
			for ($i = 1; $i <= $data['pagesCount']; $i++) {
				if ($i == $data['currentPage']) {
					echo '<span>'.$i.'</span>';
				} else {
					echo '<a href="'.$qString->setParam('page', $i).'" title=""' . ($onclick ? 'onclick="'.str_replace('[page]', $i, $onclick).'"' : '') . '>' . $i . '</a>';
				}
			}
		echo '</div>';
		
		echo '</div>';
	}
	/*
	<div class="pager">
		<a href="#" title="" class="page-back">�����</a>
		<a href="#" title="" class="page-forward">������</a>
		<div class="pages">
			<a href="#" title="">1</a>
			<a href="#" title="">2</a>
			<a href="#" title="">3</a>
			<a href="#" title="">4</a>
			<a href="#" title="">5</a>
			<a href="#" title="">6</a>
			<span>. . . .</span>
			<a href="#" title="">25</a>
			<a href="#" title="">26</a>
		</div>
	</div>
	*/
}
function getSort() {
	$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
	if (in_array($sort, array('price_asc', 'price_desc', 'news_sale'))) {
		return $sort;
	} else {
		return false;
	}
}
//������� ���������� ��������� ������ � ���� � ��������� �������� ���� ����� ������
function setPrice ($id, $price) {
	if (is_numeric($id) && is_numeric($price)) {
		mysql_query("
					UPDATE
						`catalog`
					SET
						`price` = '{$price}'
					WHERE
						`id` = '{$id}'
					LIMIT 1
				");

				$sel = mysql_query("SELECT `price`, `discount_type`, `discount_value` FROM `catalog` WHERE `id` = '{$id}'");
		if ($res = mysql_fetch_assoc($sel)) {
			$priceAfter = getPriceAfterDiscount($res['price'], $res['discount_type'], $res['discount_value']);
			mysql_query("
				UPDATE
					`catalog`
				SET
					`price_after_discount` = '{$priceAfter}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
		}
		return true;
	} else {
		return false;
	}
}
//���������� ��������� ������ ��� ����� ������
function getPrice($id) {
	if (!is_numeric($id)) return false;
	$temp = mysql_fetch_assoc(mysql_query("SELECT `price` FROM `catalog` WHERE `id` = '{$id}'"));
	return $temp['price'];
}
//����������� ��������� � ������ ������ ������
function getPriceAfterDiscount($price, $discountType, $discountValue) {
	switch ($discountType) {
		case 'percent' : $price = round($price * ( 1 - $discountValue / 100)); break;
		case 'value' : $price = $price - $discountValue; break;
	}
	return $price;
}
//���������� "%" ��� "���" � ����������� �� ���� ������, ���� ������ ������ ���� ��� ������ �� ��������
function getDiscountTypeString($type) {
	switch($type) {
		case 'percent' : return '%';
		case 'value' : return '���';
		default : return '';
	}
}
//���������� ���������� ��������� �� �����. ���������� ������ �� ������
function getPercentForDiscount($summ, $price) {
	if ($price <= $summ)
		return round(($price * 100 / $summ));
	else 
		return 100;
}
//����������� ��������� � ������ ���������� ������
function getGlobalDiscount($count, $summ) {
	//������ �� ���������. �.�. ��� ������
	$discount = array('type' => 'value', 'value' => 0);
	
	$sel = mysql_query("SELECT * FROM `order_discount_rules` ORDER BY `order` DESC");
	if (mysql_num_rows($sel) > 0) {
		while ($cond = mysql_fetch_assoc($sel)) {
			//����� ����� ������� (�� ���������� �������)
			switch ($cond['products_condition']) {
				case '>' : $firstCond = ($count > $cond['products_count']); break;
				case '>=' : $firstCond = ($count >= $cond['products_count']); break;
				case '<' : $firstCond = ($count < $cond['products_count']); break;
				case '<=' : $firstCond = ($count <= $cond['products_count']); break;
				case '=' : $firstCond = ($count == $cond['products_count']); break;
				default  : $firstCond = true; break;
			}
			
			//������ ����� ������� (� ������ ������)
			switch ($cond['price_condition']) {
				case '>' : $secondCond = ($summ > $cond['price_summ']); break;
				case '>=' : $secondCond = ($summ >= $cond['price_summ']); break;
				case '<' : $secondCond = ($summ < $cond['price_summ']); break;
				case '<=' : $secondCond = ($summ <= $cond['price_summ']); break;
				case '=' : $secondCond = ($summ == $cond['price_summ']); break;
				default  : $secondCond = true; break;
			}
			
			if ($firstCond && $secondCond) {
				return array('type' => $cond['discount_type'], 'value' => $cond['discount_value'] );
			}
		}
	}
	return $discount;
}
//���������� �������� ���������
function addAvailableParam($params) {

	$paramId = $params['paramId'];
	$catId = $params['catId'];
	$value = $params['value'];
	$value_float = $params['valueFloat'];
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	//���������� ��������� � ������-Replace ���� �� ������� ��������� ��������� (����� � �������� ��� range)
	//�� ���������� �����

		mysql_query("
			INSERT INTO
				`params_available_values`
			SET
				`param_id` = '{$paramId}',
				`cat_id` = '{$catId}',
				`value` = '{$value}',
				`value_float` = '{$value_float}'
		");
	if (mysql_affected_rows() <= 0) {
		$findParamId = fetchOne("
			SELECT
				`id`
			FROM
				`params_available_values`
			WHERE
				`param_id` = '{$paramId}' AND
				`cat_id` = '{$catId}' AND
				`value` = '{$value}' AND
				`value_float` = '{$value_float}'
			LIMIT 1
		");
	} else {
		$findParamId = mysql_insert_id();
	}
	return $findParamId;
}
//������� �������� �� ���������
function deleteAvailableParam($paramId) {
	if (!is_numeric($paramId)) return false;
	mysql_query("
				DELETE FROM
					`params_available_values`
				WHERE
					`id` = '{$paramId}'
				LIMIT 1
			");
	return mysql_affected_rows > 0;
}
//������� �������� � ����������
function linkParamAndCat($paramId, $catId) {
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	mysql_query("
		INSERT INTO
			`params_cat_links`
		SET
			`param_id` = '{$paramId}',
			`cat_id` = '{$catId}'
		");
	return true;
}
//������� ��������� �� ���������
function unlinkParamAndCat($paramId, $catId) {
	if (!is_numeric($paramId) || !is_numeric($catId)) return false;
	mysql_query("
		DELETE FROM
			`params_cat_links`
		WHERE
			`param_id` = '{$paramId}'
		AND
			`cat_id` = '{$catId}'
		");
	return true;
}
//������� �������� �� ���������, ������ ��� ����� ������ � ���������, ��������� � ���� ����������
function deleteParamFromCat($catId, $paramIds) {
	if (!is_numeric($catId)) return false;
	
	$paramIds = (array) $paramIds;
	
	//������� ������ � ��������� �����
	
	//�������� id ���� ��������� �������� ���������
	$paramsAvailableToDel = array();
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_available_values`
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
		");
	while ($res = mysql_fetch_assoc($sel)) {
		$paramsAvailableToDel[] = $res['id'];
	}
	//������� ������ ���� �������� � ��������
	if (count($paramsAvailableToDel) > 0) {
		mysql_query("
		DELETE FROM `params_catalog_links` WHERE `param_id` IN ('" . implode("', '", $paramsAvailableToDel) . "')
		");
	}
	//������� ��������� �������� ����������
	mysql_query ("
		DELETE FROM `params_available_values` 
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
		");
	//������� ������ ��������� � ����������
	mysql_query ("
		DELETE FROM
			`params_cat_links`
		WHERE
			`cat_id` = '{$catId}'
			AND
			`param_id` IN ('" . implode("', '", $paramIds) . "')
	");
}

//
function getParamInfo($paramId) {
	if (!is_numeric($paramId)) return false;
	return sqlFetch("SELECT * FROM `params_available` WHERE `id` = '{$paramId}'");
}
//���������� ������ ��������� ���������� � ���������
//��������� � ��������� �������� ���� ����������
function getParamsInCat($id) {
	$info = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM 
			params_cat_links, params_available
		WHERE
			params_available.id = params_cat_links.param_id 
			AND
			cat_id = '{$id}'
	");
	
	while ($res = mysql_fetch_assoc($sel)) {
		$selParams = mysql_query("
			SELECT 
				*
			FROM
				params_available_values
			WHERE
				cat_id = '{$id}'
				AND
				param_id = '{$res['param_id']}'
		");
		$params = array();
		while ($param = mysql_fetch_assoc($selParams)) {
			$params[] = $param;
		}
		$info[] = array(
			'info' => $res,
			'params' => $params
		);
	}
	return $info;
}

//���������� ������ �� ��������� (������, �������������) ������� ��������� � ���������,
//� ������ �� �������� ��������� � ������ (45, 46, ������), ������� ��������� ���� �� � ������ ������
// $fc (in_filter, in_compare), ���� �������� "filter" - ������ �������� ��� �������, ���� "compare" - ������ ��� ���������, false - ��� ��������.
function getRealParamsAndValuesInCat($id,$fc = false, $tagId = false) {
	$info = array();
	
	switch ($fc) {
		case 'filter':
			$where = " AND `params_available`.`in_filter` = '1'";
			break;

		case 'compare':
			$where = " AND `params_available`.`in_compare` = '1'";
			break;
		
		default:
			$where ="";
			break;
	}

	// Select only existing params values if category  tag
	if($tagId) {
		$id_select ="SELECT DISTINCT `catalog_id`
                      FROM `tags_links`
						 LEFT JOIN `tags` ON `tags_links`.`tag_id` = `tags`.`id`
						 WHERE `tags`.`cat_id` = '{$id}'
						 AND `tags`.`id` = '{$tagId}'";
	} else {
		$id_select = "SELECT `id` FROM `catalog` WHERE `cat` = '{$id}'";
	}

	$sel = mysql_query("
		SELECT
			*
		FROM 
			`params_cat_links`, `params_available`
		WHERE
			`params_available`.`id` = `params_cat_links`.`param_id`
			AND
			`params_cat_links`.`cat_id` = '{$id}'
			AND
			`params_cat_links`.`enable` = '1'
			{$where}
	");
	
	while ($res = mysql_fetch_assoc($sel)) {
		$selParams = mysql_query("
			SELECT 
				`pav`.*
			FROM
				`params_available_values` AS `pav`
			WHERE
				`pav`.`param_id` = '{$res['param_id']}'
				AND
				 (`pav`.`cat_id` = '{$id}' OR `pav`.`cat_id` = '0')
				AND
				`pav`.`id` IN
					(SELECT `param_id` FROM `params_catalog_links` WHERE `catalog_id` IN 
						($id_select)
					)
		");
		$params = array();
		while ($param = mysql_fetch_assoc($selParams)) {
			$params[] = $param;
		}
		if (count($params) > 0) {
			$info[] = array(
				'info' => $res,
				'params' => $params
			);
		}
	}
	return $info;
}

function getRealParamsAndValuesInCats($ids) {
	$array = array();
	//����� ������� � ��������� ������
	//� �� ���� ��� ��� ��������, �� ��� �������� :)
	foreach ($ids as $id) {
		$temp = getRealParamsAndValuesInCat($id);
		foreach ($temp as $param) {
			$param_id = $param['info']['param_id'];
			$array[ $param_id ]['info'] = $param['info'];
			
			if ( isset($array[ $param_id ]['params']) ) {
				$array[ $param_id ]['params'] = array_merge($array[ $param_id ]['params'], $param['params']);
			} else {
				$array[ $param_id ]['params'] = $param['params'];
			}
		}
	}
	//�������� ������ ��������
	foreach ($array as &$param) {
		$temp = array();
		foreach ($param['params'] as $p) {
			$temp[$p['id']] = $p;
		}
		//array_merge - ��� ������ ������
		$param['params'] = array_merge($temp);
	}
	return $array;
}
//������ � ����������� ������
//���������� ��� ��������� ���������
function getAvailableParams($onlyGloblals = false) {
	$params = array();
	
	$whereString = $onlyGloblals ? "WHERE `global` = '1'" : '';
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_available`
		{$whereString}
		");
	while ($line = mysql_fetch_assoc($sel)) {
		$params[] = $line;
	}
	return $params;
}
//���������� ��������� ��������� ���������
//��� ��������� ��������
function getCatParams($id) {
	$params = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`params_cat_links` as links, `params_available` as av
		WHERE
				( links.cat_id = '{$id}' OR links.cat_id = '0')
			AND
				links.enable = '1'
			AND
				av.id = links.param_id
		");
	while ($line = mysql_fetch_assoc($sel)) {
		$params[] = $line;
	}
	return $params;
}
//���������� ��������� �������� ��������� � ���������
function getCatAvailableValues($paramId, $catId, $productId = false) {
	$values = array();
	$global = fetchOne("SELECT `global` FROM `params_available` WHERE `id` = '{$paramId}'") == '1';
	
	if ($productId != false && $productId > 0) {
		//����� ������, �������� ��� ��������� ��������� ��� ���������
		//(�������� ������� params_cat_links, �.�. �������� ������ ���� �������� � ����������)
		//���� ��������� ���� enabled = 1, � ������ ���� ����� ����� ��������� �������� ���������
		if ($global) {
			$sql = "
				SELECT
				params_available_values.*,
				CASE
					WHEN params_catalog_links.catalog_id > '0'
						THEN 1
					ELSE	
						0
					END
				as `enable`
				FROM
					params_available_values 
				LEFT JOIN params_catalog_links ON
					params_available_values.id = params_catalog_links.param_id
					AND
					params_catalog_links.catalog_id = '{$productId}'
				WHERE
				params_available_values.cat_id = '0'
					AND
				params_available_values.param_id = '{$paramId}'	
			";
		} else {
			$sql = "
				SELECT
					params_available_values.*,
					
					CASE
						WHEN params_catalog_links.catalog_id > '0'
							THEN 1
						ELSE	
							0
						END
					as `enable`
				FROM
					params_available_values 
				INNER JOIN params_cat_links ON
					params_available_values.cat_id = '{$catId}'
					AND
					params_available_values.param_id = '{$paramId}'
					AND
					params_available_values.cat_id = params_cat_links.cat_id
					AND
					params_available_values.param_id = params_cat_links.param_id
				LEFT JOIN params_catalog_links ON
					params_available_values.id = params_catalog_links.param_id
					AND
					params_catalog_links.catalog_id = '{$productId}'
			";
		}
	} else {
		//����� ������, �������� ��� ��������� ��������� ��� ���������
		//(�������� ������� params_cat_links, �.�. �������� ������ ���� �������� � ����������)
		//�����-�� �����, ��-�����, �������
		//��� ���� �������� ���������
		if (!$global)
			$sql = "
				SELECT
					params_available_values.*
				FROM
					 params_available_values
				INNER JOIN params_cat_links ON
					params_available_values.cat_id = '{$catId}'
					AND
					params_available_values.param_id = '{$paramId}'
					AND
					params_available_values.cat_id = params_cat_links.cat_id
					AND
					params_available_values.param_id = params_cat_links.param_id
				";
		//� ����� ����������
		else
			$sql = "
			SELECT
				params_available_values.*
			FROM
				params_available_values
			WHERE
				params_available_values.param_id = '{$paramId}'
				AND
				params_available_values.cat_id = '0'
		";
	}
	$sel = mysql_query($sql);
	while ($line = mysql_fetch_assoc($sel)) {
		$values[] = $line;
	}
	return $values;
}
//������� �������� ���������
function deleteParamValue($id) {
	if (!is_numeric($id)) return false;
	
	//������� �� ��������� ����������
	mysql_query("
		DELETE FROM
			`params_available_values`
		WHERE
			`id` = '{$id}'
	");
	//������� ������
	mysql_query("
		DELETE FROM
			`params_catalog_links`
		WHERE
			`param_id` = '{$id}'
	");
}
//������� ���������� ���������
//���������� enable = 0|1 ��� $catId > 0
function getGlobalParams($catId = 0) {
	$params = array();
	
	if ($catId > 0) {
		$sql = "
			SELECT
				`params_available`.*,
				CASE
					WHEN `params_cat_links`.`enable` = '1'
						THEN 1
						ELSE 0
					END
				as enable
			FROM
				`params_available`
			LEFT JOIN
				`params_cat_links`
			ON
				`params_available`.`id` = `params_cat_links`.`param_id`
				AND
				`params_cat_links`.`cat_id` = '{$catId}'
			WHERE
				`global` = '1'
			ORDER BY `enable` DESC
		";
	} else {
		$sql = "	
			SELECT
				*
			FROM
				`params_available`
			WHERE
				`global` = '1'
		";
	}
	$sel = mysql_query($sql);
	while ($row = mysql_fetch_assoc($sel)) {
			$params[] = $row;
	}
	return $params;
}
//������� ��������� ���������
//���������� enable = 0|1
function getLocalParams($catId) {
	if (!is_numeric($catId)) return false;
	
	$params = array();
	
	$sql = "
		SELECT
			`params_available`.*,
			CASE
				WHEN `params_cat_links`.`enable` = '1'
					THEN 1
					ELSE 0
				END
			as enable
		FROM
			`params_available`
		INNER JOIN
			`params_cat_links`
		ON
			`params_available`.`id` = `params_cat_links`.`param_id`
			AND
			`params_cat_links`.`cat_id` = '{$catId}'
		WHERE
			`global` = '0'
		ORDER BY `enable` DESC
	";
	
	$sel = mysql_query($sql);
	while ($row = mysql_fetch_assoc($sel)) {
		$params[] = $row;
	}
	return $params;
}
//���������� ��������� ������
function setProductsParams($productId, $paramsArray) {
	//��������������� � �������� ������ � ���������� ������
	mysql_query("DELETE FROM `params_catalog_links` WHERE `catalog_id` = '{$productId}'");
	
	//������ � ID ����������
	$paramsIds = (array) $paramsArray;
	
	//������ ��� ���������� sql �������
	$paramsSqlIds = array();

	//������� sql ������
	foreach ($paramsIds as $pid) {
		if (!is_numeric($pid)) continue;
		$paramsSqlIds[] = "('{$productId}', '{$pid}')";
	}
	//��������� ���������
	if (count($paramsSqlIds) > 0) {
		mysql_query("
			INSERT INTO
				`params_catalog_links` (`catalog_id`,`param_id`)
			VALUES 
			".implode(', ', $paramsSqlIds)
			);
	}
	return true;
}
//���������� ����� � ��� ��������� � ������ ���������
//$id = id ������, $newId = id ���������
function catalogChangeCat($id, $newId) {
	if (!is_numeric($id) || !is_numeric($newId)) return false;
	
	//// ���������� ����������� ���������
	$oldId = mysql_fetch_assoc(mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$id}'"));
	$oldId = $oldId['cat'];
	
	//���� ��������� ���������, do nothing
	if ($oldId == $newId) return true;
	
	//�������� ������ ���������
	$oldParams = array();
	$sel = mysql_query("SELECT * FROM `params_cat_links` WHERE `cat_id` = '{$oldId}'");
	while ($res = mysql_fetch_assoc($sel)) {
		$oldParams[] = $res['param_id'];
	}
	//�������� ����� ���������
	$newParams = array();
	$sel = mysql_query("SELECT * FROM `params_cat_links` WHERE `cat_id` = '{$newId}'");
	while ($res = mysql_fetch_assoc($sel)) {
		$newParams[] = $res['param_id'];
	}
	//������� ���������� ���������
	$paramsEqual = array_merge(array_intersect($oldParams, $newParams));

	//// ������� � params_catalog_links ����� � �������������� �����������
	if ( count($paramsEqual) > 0) {
		//���� ��������� �� ���������� ����, ������� ����� �������� ��� ������������� ����������
		mysql_query("
			DELETE FROM 
				`params_catalog_links`
			WHERE
				`catalog_id` = '{$id}'
				AND
				`param_id` IN 
				(SELECT id FROM `params_available_values` WHERE `param_id` NOT IN ('" . implode(",", $paramsEqual) . "'))
		");
	} else {
		//���� ������ �� ���� �������� �� ���������, ������� ��� ��������
		mysql_query("
			DELETE FROM 
				`params_catalog_links`
			WHERE
				`catalog_id` = '{$id}'
		");
	}
	
	//// �������� id ���������� �������� ����������, � � params_available_values � ��������� ��, �� �� ��������� ������ ���������
	
	$sel = mysql_query("
		SELECT
			`params_available_values`.*,
			`params_available_values`.`param_id` as `param_type_id`,
			`params_catalog_links`.`param_id` as `catalog_param_id`
		FROM
			`params_catalog_links`
		INNER JOIN `params_available_values`
		ON `params_available_values`.`id` = `params_catalog_links`.`param_id`
		WHERE
			`catalog_id` = '{$id}'
		");
	//echo mysql_error();
	//��� ������� �������� ���������
	while ($row = mysql_fetch_assoc($sel)) {
		
		//���������, ���� �� ��� �������� ���������, ������� �� ����� �������� `params_available_values`
		$ssel = mysql_query("
			SELECT
				*
			FROM
				`params_available_values`
			WHERE
				`param_id` = '{$row['param_type_id']}' AND
				`value` = '{$row['value']}' AND
				`cat_id` = '{$newId}'
		");

		if ($srow = mysql_fetch_assoc($ssel) ) {
			//���� ����� �������� ����, �� ������������ id � ������ �� ��� ��������
			$newParamAvailableId = $srow['id'];
		} else {
			//�� � ���� ���, �� ���������
			mysql_query("
				INSERT INTO
					`params_available_values`
				SET
					`param_id` = '{$row['param_type_id']}',
					`value` = '{$row['value']}',
					`cat_id` = '{$newId}'
			");
			$newParamAvailableId = mysql_insert_id();
		}
		//���������� ������������
		mysql_query("
				UPDATE
					`params_catalog_links`
				SET
					`param_id` = '{$newParamAvailableId}'
				WHERE
					`catalog_id` = '{$id}' AND
					`param_id` = '{$row['catalog_param_id']}'
			");
	}
	
	
	//�������� ��������� ������ 
	mysql_query("
		UPDATE
			`catalog`
		SET
			`cat` = '{$newId}'
		WHERE
			`id` = '{$id}'
	");

	//���������� ����� �� ����
	unlinkCatalogAndTags($id);

	return true;
}
function query_cat_info($_id) {

	global $id;
	global $checkis;

	$checkis = 'adada';

	$id = $_id;

	return $id;
}
//������� ����� �� ��������
//� �������� � ���� � �������������� ������, � ������ � �����������������
function deleteProduct($id) {
	if ($catalog = mysql_fetch_assoc(mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$id}' LIMIT 1"))) {

		//������� ��� ���� ���� ����
		$sel = mysql_query("SELECT * FROM `foto` WHERE `catalog_id` = '{$id}'");
		while ($line = mysql_fetch_assoc($sel)) {
			deleteExtraPhoto($line['id']);
		}

        // �������� ����������
        deleteInstruction($id);

		//������� �������� ����
		deleteMainPhoto($id);
		//������� ��� �������� ���������� ������
		mysql_query("DELETE FROM `params_catalog_links` WHERE `catalog_id` = '{$id}'");
		//������� ��������� �����\���������������
		unlinkCatalogAndTags($id);
		//������� �� ����
		mysql_query("DELETE FROM `catalog` WHERE `id` = '{$id}'");


		
		return true;
	} else {
		return false;
	}
}

// ���������� - ����������
function saveInstruction($file, $product_id){

    if ($file['error']) return false ;

    if (!isset($file['name']) || $file['tmp_name'] == '' ) return false ;

    $result = array();

    //$extension = substr($file['name'], strrpos($file['name'], '.') );

    $path = $_SERVER['DOCUMENT_ROOT'] . getInstructionPath($product_id) ;

    if (!is_dir($path)) {
        mkdir($path, 0777) ;
    }
    $filename = $file['name'] ;
    $upload_name = $path . $filename ;

    if (!move_uploaded_file($file['tmp_name'], $upload_name)){
        $result['error'] = true ;
        $result['msg'] = "�� ���� ��������� ���� " . $file['tmp_name'] . " � " . $upload_name ;
    } else {
        $result['error'] = false ;
    }

    if (!$result['error']) {
        $q = "UPDATE `catalog` SET `instruction` = '{$filename}' WHERE `id` = {$product_id} LIMIT 1 ; " ;
        mysql_query($q) ;
    }
    return $result ;
}

// ��������� ���� �� ����������
function getInstructionPath($product_id) {
    return  '/upload/files/' . $product_id . '/' ;
}

// �������� ����������
function deleteInstruction($id) {

    $instruction_name = mysql_fetch_assoc(mysql_query("SELECT id, instruction FROM `catalog` WHERE `id` = '{$id}'"));

    if ($instruction_name['instruction'] == '') {
        return false ;
    }

    $path = $_SERVER['DOCUMENT_ROOT'] . getInstructionPath($id) . $instruction_name['instruction'] ;
    if (file_exists($path)){
        @unlink($path) ;
    } else {
        return false ;
    }
    return true ;
}


//������� ��� ������ � �������������
//$newName ��� ����������
function saveMainImage($postFile, $newName) {
	
	$id = $newName;
	//��������
	$imgPath = getImagePath('product_original');
	$sFileDest = $id;
	$iWidthDest = null;
	$iHeightDest = null;
	$iResizeMode = 1;
	$originalFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
	
	//������ � �������� ������
	$imgPath = getImagePath('product_preview');
	$sFileDest = $id;
	$size = getConfigImageSize('product_preview');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_preview');
	$bigFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//Medium �����
	$imgPath = getImagePath('product_medium');
	$sFileDest = $id;
	$size = getConfigImageSize('product_medium');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_medium');
	$mediumFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//Small �����
	$imgPath = getImagePath('product_small');
	$sFileDest = $id;
	$size = getConfigImageSize('product_small');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_small');
	$smallFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);

	//Little �����
	/*
	$imgPath = getImagePath('product_little');
	$sFileDest = $id;
	$size = getConfigImageSize('product_little');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_little');
	$smallFile = uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	*/
	return !($bigFile == false || $mediumFile == false || $smallFile == false);

	/*
	$id = $newName;

	$settings = array(
			'product_preview',
			'product_small',
			'product_medium',
			'product_little'
			);
	$imgPath = getImagePath('product_original');

	// *** 1) Initialize / load image
	$resizeObj = new resize($postFile['tmp_name']);
	// *** 2) Save image
	$done[] = $resizeObj -> saveWithoutResize($imgPath.$id.'.jpg', 100);
	
	foreach ($settings as $option) {
		$imgPath = getImagePath($option);
		$size = getConfigImageSize($option);
		$iWidthDest = $size['width'];
		$iHeightDest = $size['height'];
		$quality = getConfigImageQuality($option);
		// *** 1) Initialize / load image
		//$resizeObj = new resize($postFile['tmp_name']);
		
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
		 
		// *** 3) Save image
		$done[] = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
	}
	in_array(false, $done) ? $a = false : $a = true;
	return $a; */
}
	
	
//���������� ��������������� ����
function saveExtraPhoto($postFile, $catalogId) {

	//��������� ������ � ���� � ������ id ������� ��������
	mysql_query ("
		INSERT INTO
			`foto`
		SET
			`catalog_id` = '{$catalogId}'
		");
	$imgId = mysql_insert_id();

	$sFileDest = $postFile['tmp_name'];
	/*
	if (!($aSize=getimagesize($sFileDest))) {
		return false;
	}
	$img_src=false;
	switch ($aSize[2]) {
		case 3:
			$img_src=imagecreatefrompng($sFileDest);
			$sFileDest.='.png';
			break;
		case 1:
			$img_src=imagecreatefromgif($sFileDest);
			$sFileDest.='.gif';
			break;
		case 2:
			$img_src=imagecreatefromjpeg($sFileDest);
			$sFileDest.='.jpg';
			break;
		default:
			return false;
			break;
	}
	if (!$img_src) {
		return false;
	}*/
	/*
	$settings = array(
			'product_extra_preview',
			'product_extra_small',
			'product_extra_medium',
			'product_extra_little'
			);
	$imgPath = getImagePath('product_extra_original');

	// *** 1) Initialize / load image
	$resizeObj = new resize($postFile['tmp_name']);
	// *** 2) Save image
	$done[] = $resizeObj -> saveWithoutResize($imgPath.$imgId.'.jpg', 100);

	foreach ($settings as $option) {
		$imgPath = getImagePath($option);
		$size = getConfigImageSize($option);
		$iWidthDest = $size['width'];
		$iHeightDest = $size['height'];
		$quality = getConfigImageQuality($option);
		// *** 1) Initialize / load image
		$resizeObj = new resize($postFile['tmp_name']);
		
		// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
		$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
		 
		// *** 3) Save image
		$done[] = $resizeObj -> saveImage($imgPath.$imgId.'.jpg', 100);
	}
	in_array(false, $done) ? $a = false : $a = true;
	return $a ? $imgId : $a;
	*/
	//������� ��������
	$imgPath = getImagePath('product_extra_original');
	$sFileDest = $imgId;
	$iWidthDest = null;
	$iHeightDest = null;
	$iResizeMode = 1;
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode);
	
	//������
	$imgPath = getImagePath('product_extra_preview');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_preview');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_preview');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	//�������
	$imgPath = getImagePath('product_extra_medium');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_medium');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_medium');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);

	//Small
	$imgPath = getImagePath('product_extra_small');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_small');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_small');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);

	//Little
	$imgPath = getImagePath('product_extra_little');
	$sFileDest = $imgId;
	$size = getConfigImageSize('product_extra_little');
	$iWidthDest = $size['width'];
	$iHeightDest = $size['height'];
	$iResizeMode = 1;
	$quality = getConfigImageQuality('product_extra_little');
	uploadAndResize($postFile, $imgPath, $sFileDest, $iWidthDest, $iHeightDest, $iResizeMode, $quality);
	
	return $imgId;
}
//�������� ��������������� ����
function deleteExtraPhoto($id) {
	
	mysql_query("DELETE FROM `foto` WHERE `id` = '{$id}'");
	@unlink(getImagePath('product_extra_original').$id.".jpg");
	@unlink(getImagePath('product_extra_preview').$id.".jpg");
	@unlink(getImagePath('product_extra_medium').$id.".jpg");
	@unlink(getImagePath('product_extra_small').$id.".jpg");
	@unlink(getImagePath('product_extra_little').$id.".jpg");
	return ;
}
function deleteMainPhoto($catalogId) {
	//��������
	@unlink(getImagePath('product_original').$catalogId.'.jpg');
	//������ � �������� ������
	@unlink(getImagePath('product_preview').$catalogId.'.jpg');
	//Medium �����
	@unlink(getImagePath('product_medium').$catalogId.'.jpg');
	//Small �����
	@unlink(getImagePath('product_small').$catalogId.'.jpg');
	//Little 
	@unlink(getImagePath('product_little').$catalogId.'.jpg');
}

function deleteCatImages($id) {
	@unlink(getImagePath('cats').$id.'.jpg');
	@unlink(getImagePath('cats_menu').$id.'.jpg');
}
/*
function deleteInstruction($file){
        $path = $_SERVER['DOCUMENT_ROOT'] . '/upload/files/' . $file; 
        @unlink($path);
}
*/
//�������� ������ ������ � ���������
function getCatArticles($id) {
	if (!is_numeric($id)) return false;
	$data = array();
	
	$sel = mysql_query("
		SELECT
			*
		FROM
			`articles`
		WHERE
			`cat_id` = '{$id}'
	");
	while ($row = mysql_fetch_assoc($sel)) {
		$data[] = $row;
	}
	return $data;
}
//�������� ������ � ������ (���� ������)
function getArticle($id, $forceChpu = false) {
	if (is_numeric($id) && !$forceChpu) {
		$sel = mysql_query("SELECT * FROM `articles` WHERE `id` = '{$id}'");
	} else {
		$chpu = mysql_real_escape_string($id);
		$sel = mysql_query("SELECT * FROM `articles` WHERE `chpu` = '{$id}'");
	}
	return mysql_fetch_assoc($sel);
}
//������� ������
function deleteArticle($id) {
	if (!is_numeric($id)) return false;
	mysql_query("
		DELETE FROM
			`articles`
		WHERE
			`id` = '{$id}'
	");
	return mysql_affected_rows() > 0;
}
//���������� ������ ������� � �������. � ���������� � ���������� ������
//������ �����������
//$forWeb ���� ���, ������ ���������� ������ � ������ (��� ������������� js �� ��������) � ��������� ������� � ���
function getProductsInBasket($imageType = 'medium', $forWeb = false) {
	$cart = $_COOKIE['cart_products'];
	
	//��������� ������ ������� � �������
	$productsInCart = array();
	
	$temp = explode('|', $cart);
	foreach ($temp as $t) {
		$productTemp = explode(':', $t);
		if (is_numeric($productTemp[0]) && ($productTemp[0] > 0) && ($productTemp[1] > 0) && is_numeric($productTemp[1])) {
			$productsInCart[ $productTemp[0] ] = array(
				'id' => $productTemp[0],
				'amount' => $productTemp[1]
				);
		}
	}
	
	//������� ������ ��� ������
	$productsIds = array();
	foreach ($productsInCart as $p) {
		$productsIds[] = $p['id'];
	}
	$products = getProducts(false, array('id' => $productsIds));
	$products = $products['products'];
	
	//����� ������ �� �������� ������� ������ ��� ���������� ������
	$productsCount = count($products);
	
	for ($i = 0; $i < $productsCount; $i++) {
		if ($forWeb) {
			$products[$i] = array(
				'id' => $products[$i]['id'],
				'title' => iconv('windows-1251', 'utf-8', $products[$i]['title']),
				'price' => $products[$i]['price_after_discount'] ? $products[$i]['price_after_discount'] : $products[$i]['price'],
				'link' => getTemplateLink($products[$i], 'catalog'),
				'img_src' => getImageWebPath('product_' . $imageType) . $products[$i]['id'] . '.jpg'
			);

		}
		$products[$i]['amount'] = $productsInCart[ $products[$i]['id'] ]['amount'];
	}
	
	return $products;
}

// -------------------------------------------------------------------------------- ������ ------------------------------------------------------------------------
function addFeedBack() {
	//	��������� �����
	if (
		isset($_POST['username']) &&
		!isLinksContain($_POST['username']) &&
		isset($_POST['usermail']) &&
		!isLinksContain($_POST['usermsg']) &&
		isset($_POST['usermsg']) &&
		preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i', $_POST['usermail'])
		) {
		if (preg_match('/([<>\[\]]+)|(http:?\/?\/?)/m', $_POST['usermsg'])) {
			header('Location: /page/spam-filtr.html');
		} else {
			$date = date('d') . '.' . date('m') . '.' . date('Y');
			$uname = htmlspecialchars($_POST['username']);
			$umail = htmlspecialchars($_POST['usermail']);
			$umsg = htmlspecialchars($_POST['usermsg']);

			$query = "INSERT INTO `shop_otzyv` (`id`, `date`, `name`, `mail` , `des`, `otvet`, `confirm`) VALUES (NULL, '" . $date . "' , '" . $uname . "', '" . $umail . "', '" . $umsg . "', '', '0')";
			$result = @mysql_query($query);
			if (mysql_affected_rows() > 0) {
				return "OK";
			} else {
				return NULL;
			}
		}
	} else {
		//	����� �� ���������
		return NULL;
	}
}

function getMainBanners($all = false) {
	$data = array();

	$sel = mysql_query("SELECT * FROM `banners_main` WHERE `menuleft_id` = '0'" . ($all ? '' : " AND `active` = '1'") . " ORDER BY `sort` ASC, `id` ASC");
	while ($res = mysql_fetch_assoc($sel)) {
		$res['image_src'] = getImageWebPath('banner_main') . $res['id'] . '.jpg';
		$res['image_src_preview'] = getImageWebPath('banner_main') . $res['id'] . '_s.jpg';
		$data[] = $res;
	}

	return $data;
}

function getMainBanner($id) {
	if (!is_numeric($id))
		return false;
	return sqlFetch("SELECT * FROM `banners_main` WHERE `id` = '{$id}'");
}

//-------------- ����������� UTF8 � 1251
function utf8_to_win($string) {
	for ($c = 0; $c < strlen($string); $c++) {
		$i = ord($string[$c]);
		if ($i <= 127)
			@$out .= $string[$c];
		if (@$byte2) {
			$new_c2 = ($c1 & 3) * 64 + ($i & 63);
			$new_c1 = ($c1 >> 2) & 5;
			$new_i = $new_c1 * 256 + $new_c2;
			if ($new_i == 1025) {
				$out_i = 168;
			} else {
				if ($new_i == 1105) {
					$out_i = 184;
				} else {
					$out_i = $new_i - 848;
				}
			}
			@$out .= chr($out_i);
			$byte2 = false;
		}
		if (($i >> 5) == 6) {
			$c1 = $i;
			$byte2 = true;
		}
	}
	return $out;
}

//----------------- Mail �� ������
function feedbackSendMail($username, $usermail, $usermsg) {
	if ($usermail != '') {
		$subject = '�������� ������� ���-������ - ��� ����� ���������.';
		$mailbody = ('
			<h2>���������, ' . $username . ', �� ���������� ��� �� �����.<br />
			�� ����� ����������� ����� �������� �����������.</h2>
			&nbsp;<br />
			�� ��������:<br />"' . $usermsg . '"<br />
			<a href="http://med-serdce.ru/feedback/">������� � ��������� ������� �������� �������� ���-������.��</a>.
		');
		mail($usermail, $subject, $mailbody, join("\r\n", array(
					"From: info@med-serdce.ru",
					"Reply-To: info@med-serdce.ru",
					"Content-type: text/html;\n\t charset=windows-1251",
					"X-Mailer: PHP/" . phpversion()))
		);
	}
}

// ------------------  ������ �������
function selectLastFeedback($limit = 2) {
	$query = "SELECT `name`, `date`, `des` FROM `shop_otzyv` WHERE `confirm`=1 ORDER BY `id` DESC LIMIT {$limit}";
	$result = @mysql_query($query);
	$lastFeed = array();
	$cnt = 0;
	while ($row = @mysql_fetch_assoc($result)) {
		$lastFeed[$cnt]['name'] = $row['name'];
		$lastFeed[$cnt]['date'] = $row['date'];
		$lastFeed[$cnt]['des'] = $row['des'];
		$cnt++;
	}
	return $lastFeed;
}

function menu_createArray($msql_result, $no_group = false) {

	$arr = Array();
	$cur_group = -1;
	while ($line = mysql_fetch_assoc($msql_result)) {
		if ($line['is_group']) {
			if ($cur_group != $line['id']) {
				if (!$no_group) {

				}
				$cur_group = $line['id'];
			}
		} else {
			$arr[] = Array();
			$arr[count($arr) - 1][0] = $line['id'];
			$arr[count($arr) - 1][1] = $line['cat_id'];
			$arr[count($arr) - 1][2] = $cur_group;
		}
	}
	return $arr;
}

function multi_array_search($needle, $haystack) {

	foreach ($haystack as $key => $value) {
		//print_r($value);
		foreach ($value as $nkey => $nvalue) {
			if ($nvalue == $needle) {
				return 1;
			}
		}
	}

	return 0;
}

function menu_getMenu($_cat_id, $menuar) {
	$s_result = multi_array_search($_cat_id, $menuar);
	//echo $s_result;
	if (!$s_result) {
		$query = "select pod from cat where id=$_cat_id";
		$msql_result = mysql_query($query) or die(mysql_error());
		if ($line = mysql_fetch_assoc($msql_result)) {
			//echo "\n".$line['pod'];
			return menu_getMenu($line['pod'], $menuar);
		} else {
			return null;
		}
	} else {
		return $_cat_id;
	}
}

function menu_getActiveMenu($_cat_id) {

	//echo 1;
	$mysql_result = zh_getMenuResource();


	$menuar = menu_createArray($mysql_result, true);

	//print_r($menuar);
	$rcat_id = menu_getMenu($_cat_id, $menuar);

	if (!empty($rcat_id)) {
		return $rcat_id;
	} else {
		return $_cat_ids;
	}
}

// --------------------------------------------------------------------------------  ��� ������ YANDEX-XML ------------------------------------------------------------------------
//----------------- ��������� �������� ����
function highlight_words($node) {
	$stripped = preg_replace('/<\/?(title|passage)[^>]*>/', '', $node->asXML());
	return str_replace('</hlword>', '</strong>', preg_replace('/<hlword[^>]*>/', '<strong>', $stripped));
}

//----------------------------------------------- ����� �������� �� ��� ������ ��� ���������
function getInfoByChPU($url) {
	//	���� �����
	$result = array();
	if (substr_count($url, 'catalog')) {
		$parts = explode('/', $url);
		$last = str_replace('.html', '', $parts[count($parts) - 1]);
		$mquery = "SELECT `id`, `price` FROM `catalog` WHERE `chpu`='" . $last . "' LIMIT 1";
		$mresult = @mysql_query($mquery);
		//	���� ����� ����� �����
		if (mysql_affected_rows() > 0) {
			$mrow = @mysql_fetch_assoc($mresult);
			$img_href = '/upload/hit/' . ($mrow['id']) . '.jpg';
			$result['img_href'] = $img_href;
			$result['price'] = $mrow['price'];
			return $result;
		} else {
			return NULL;
		}
		//	���� ���������
	} else {
		$parts = explode('/', $url);
		$chpu = str_replace('.html', '', $parts[4]);
		$mquery = "SELECT `id` FROM `cat` WHERE `chpu`='" . $chpu . "' LIMIT 1";
		$mresult = @mysql_query($mquery);
		if (mysql_affected_rows() > 0) {
			$mrow = @mysql_fetch_assoc($mresult);
			$img_href = '/upload/cat/' . $mrow['id'] . '.jpg';
			$result['img_href'] = $img_href;
			$mresult = @mysql_query('SELECT * FROM `catalog` WHERE `cat`=' . $mrow['id']);
			if (mysql_affected_rows() > 0) {
				$result['count'] = mysql_num_rows($mresult);
			}
			return $result;
		}
	}
	return NULL;
}


/* 	�������� ���������:
  $short_des = preg_replace('/([\.\:\-\,\;\!\?])([a-zA-Z�-��-�])/', '$1 $2', $short_des); - ������� ����� ������ ����������
  $short_des = preg_replace('</?(?!br)[^>]+\s?/?>', '', $short_des); - ������ ��� ���� ����� <br />
 */

//------------------------------------------------ ������� ����� �� N �������� �� �������� ����:
function crop_str($string, $limit) {
	$substring_limited = substr($string, 0, $limit);		//����� ������ �� 0 �� limit
	return substr($substring_limited, 0, strrpos($substring_limited, ' '));	//����� ����� ���������� ������ �� 0 �� ���������� �������
}



function partnerCounter($pid, $counterType = array('follows', 'orders')) {
	$query = "SELECT * FROM `partners` WHERE `id`=$pid";
	$result = mysql_query($query);
	if(mysql_affected_rows() > 0) {
		$partner = mysql_fetch_assoc($result);
		$partner[$counterType]++;
	}
	$query = "UPDATE `partners` SET `$counterType`={$partner[$counterType]} WHERE `id`=$pid";
	$result = mysql_query($query);
}

function setPartnerCookie() {
	$query = "SELECT * FROM `partners` WHERE `id`=".$_GET['p'];
	$result = mysql_query($query);
	if(mysql_affected_rows() >0) {
		setcookie('fpid', $_GET['p'], time()+2000000, '/');
		partnerCounter($_GET['p'], 'follows');
	}
}


//Just a functions
function slashes($string) {
	return (get_magic_quotes_gpc() ? stripslashes($string) : $string);
}
function schars($string) {
	return htmlspecialchars(slashes($string), ENT_QUOTES);
}
function iconv_deep($e1, $e2, $value) {
   if (is_array($value)) {
      $item = null;
      foreach ($value as &$item) {
         $item = iconv_deep($e1, $e2, $item);
      }
      unset($item);
   } else {
      if (is_string($value)) $value = mb_convert_encoding($value, $e2, $e1);
   }
   return $value;
}
function translitIt($str) 
{
    $tr = array(
        "�"=>"A","�"=>"B","�"=>"V","�"=>"G",
        "�"=>"D","�"=>"E","�"=>"J","�"=>"Z","�"=>"I",
        "�"=>"Y","�"=>"K","�"=>"L","�"=>"M","�"=>"N",
        "�"=>"O","�"=>"P","�"=>"R","�"=>"S","�"=>"T",
        "�"=>"U","�"=>"F","�"=>"H","�"=>"TS","�"=>"CH",
        "�"=>"SH","�"=>"SCH","�"=>"","�"=>"YI","�"=>"",
        "�"=>"E","�"=>"YU","�"=>"YA","�"=>"a","�"=>"b",
        "�"=>"v","�"=>"g","�"=>"d","�"=>"e","�"=>"j",
        "�"=>"z","�"=>"i","�"=>"y","�"=>"k","�"=>"l",
        "�"=>"m","�"=>"n","�"=>"o","�"=>"p","�"=>"r",
        "�"=>"s","�"=>"t","�"=>"u","�"=>"f","�"=>"h",
        "�"=>"ts","�"=>"ch","�"=>"sh","�"=>"sch","�"=>"y",
        "�"=>"yi","�"=>"","�"=>"e","�"=>"yu","�"=>"ya",
		' ' => '-', ',' => '-', '/' => '-',
		'\\' => '-', '+' => '-'
    );
    $str = strtr($str,$tr);
	return preg_replace("/(-)+/is", '-', $str);
}
function validateEmail($email) {
	return preg_match('/^([a-z0-9_\-]+\.)*[a-z0-9_\-]+@([a-z0-9][a-z0-9\-]*[a-z0-9]\.)+[a-z]{2,4}$/i',$email);
}
function isLinksContain($text) {
	return strpos($text, 'http://') !== false || preg_match('/(<a[^>]*)href=(\"?)([^\s\">]+?)(\"?)([^>]*>)/ism', $text) > 0; //|| preg_match('/([<>\[\]]+)|(http:?\/?\/?)/m', $text) > 0;
}
function toForm($data) {
	return htmlspecialchars(slashes($data), ENT_QUOTES);
}
function tf($name, $default = '') {
	if (isset($_POST[$name]))
		return toForm($_POST[$name]);
	else
		return $default;
}
function sqlFetch($query) {
	return mysql_fetch_assoc(mysql_query($query));
}
function fetchOne($query) {
	$var = mysql_fetch_array(mysql_query($query));
	return $var[0];
}
function redirect($url = '/', $code = false) {
	switch ($code) {
		case 301 : header("HTTP/1.0 301 Moved Permanently", true, 301);
			break;
		case 404 : header("HTTP/1.0 404 Not Found", true, 404);
			break;
	}
	header("Location: {$url}");
	die();
}
//������� �������� �� �����, �������� � ������ �����, ����� ����� ��������, ������ �� ��������
function trimDesc($str) {
	$str = strip_tags($str);
	$str = str_replace('&nbsp;','', $str);
	$str = trim($str);
	return $str;
}
//�������� html ����� �� ������ $symbolsLimit
function cropText($desc, $symbolsLimit = 200) {
	//	����� ��� ���� � �������� �����, ����� <br>, <br />
	$tmp_short = preg_replace('/(<\/?(?!br)[^>]+\s?>|\n)/', '', html_entity_decode($desc));

	//	�������� ��� <br>, <br /> �� ������� ������
	$tmp_short = preg_replace('/<\/?(?=br)[^>]+\s?>/', "\n", $tmp_short);

	//	���� ������ ����� � ����� ������
	$lastDot = @strpos($tmp_short, ".", $symbolsLimit - 50);

	//	���� ����� ������� � �� ����� $symbolsLimit �������� - ��������
	if (($lastDot) && ($lastDot < $symbolsLimit)) {
		$tmp_short = substr($tmp_short, 0, $lastDot) . ".";
		//	���� �� ������� - �������� ��� �����
	} else {
		$tmp_short = substr($tmp_short, 0, $symbolsLimit) . "...";
	}
	return $tmp_short;
}
//���������� �������� �������� ��� ������. (������������ � �������)
//�������� � ������� ������� ������ �� ������� � ������� ����� getProduct, � ����� ���������� ������� Array $config
function getShortDesc($productData, $cropType = SHORT_IF_EXISTS_ELSE_CROP, $symbolsLimit = 200) {
	switch($cropType) {
		//�������� �������� �������� ������ �� �������
		case CROP_FROM_FULL:
			return cropText($productData['des'], $symbolsLimit);
		break;
		//���� �������� ������� ����, ���������� ���. ���� ���, ���������� �������.
		case SHORT_IF_EXISTS_ELSE_NONE:
			if (isset($productData['short_des'])) {
				return $productData['short_des'];
			} else {
				return false;
			}
		break;
		//���� �������� �������� ���� � ����, ������� ���, ���� ��� - �������� �� ������� ��������
		case SHORT_IF_EXISTS_ELSE_CROP:
		default :
			if (isset($productData['short_des'])) {
				$tmp = trim(strip_tags($productData['short_des'])); //fix, when CEditor added empty tags (p, br or something)
				if (empty($tmp))
					return cropText($productData['des'], $symbolsLimit);
				else 
					return $productData['short_des'];
			} else {
				return false;
			}
	}
}
//����������� ������ ����� ��� ��������� ������
//Example: 1235 -> 1 235
function moneyFormat($number, $fractional=false) {
    if ($fractional) {
        $number = sprintf('%.2f', $number);
    }
    while (true) {
        $replaced = preg_replace('/(-?\d+)(\d\d\d)/', '$1 $2', $number);
        if ($replaced != $number) {
            $number = $replaced;
        } else {
            break;
        }
    }
    return $number;
}
function declOfNum($number, $titles) {
    $cases = array (2, 0, 1, 1, 1, 2);
    return $titles[ ($number%100>4 && $number%100<20)? 2 : $cases[min($number%10, 5)] ];
}
/**
 *	���������� ���������� ��� �������� ������
 */
function getSnippet($product) {
	if (is_array($product)) {
		$snippet = "";
		//HTML ���� ���� ��������
		$spec_chars = array('phone' => '&#9742;',
							'mobile_phone' => '&#128241;',	
							'tick' => '&#10004;',
							'plane' => '&#9992;',
							'star' => '&#10047;',
							'car' => '&#128663;',
							'money_bag' => '&#128176;',
							'money' => '&#128184;',
							'speech_balloon' => '&#128172;',
							'camera' => '&#128247;',
							);
		$title = trim($product['title']) . ', ';
		$credit = $spec_chars['tick'] . '������� � ������ ';
		$price =  '���� ' . trim($product['price_after_discount']) . ' ���., ';
		//$moscow_delivery = $spec_chars['tick'] . '�������� 250� �� ������ ';
		
		if ($product['deliveryPrice'] !== false) {
			$moscow_delivery = ($product['deliveryPrice'] > 0) ? 
				  '�������� �� ������ ' . $product['deliveryPrice'] . ' ���., ' : 
					'���������� �������� �� ������, ';
			} else {
			$moscow_delivery =  '�������� �� ������ �� 300 ���, ';
		}
		
		$global_delivery = $spec_chars['plane'] . '�������� �� ���� ������ ';
		$phone =trim(Config::get('site.phone')) . ' ';
		$mobile_phone = $spec_chars['phone']  . '8 (800) 555-62-50' . ' '; //������� � ������
		
		$snippet .= $title;
		//$snippet .= $credit;
		$snippet .= $price;
		
		// $snippet .=  '����������� ���� Beurer, ��������, ';
		
		if (!empty($product['like_desc'])) {
			$video =  '�����, ';		
			$snippet .= $video;
		}
		
		if (count($product['feedback']) > 0)
			$snippet .=  '������, ';
			
		if ($product['sale'] == 1) 
			$snippet .=  '����� ';
			
		$snippet .= $moscow_delivery;
		//$snippet .= $global_delivery;
		
		//if ($product['rests_main']['summ'] > 0 || $product['fake_in_stock'] == 1) 
		//	$snippet .= $spec_chars['tick'] . ' ' . '���� � ������� ';
		
		$snippet .= $phone;
		//$snippet .= $mobile_phone;
		
		//echo strlen($snippet) . "</br>";
		//echo $snippet;
		return $snippet;
	}
	return false;
}
/**
 *	���������� ���������� ����-������� 
 *      �� ��������� �����, ������, ������
 */
function getQiwiSize($x, $y, $z) {
    $size = array(intval($x), intval($y), intval($z));
    sort($size);
    if ($size[2] > 640) return false;
    elseif ($size[1] > 410) return false;  
    elseif ($size[1] > 380) {
        if ($size[0] > 380) 
            return false;
        else
            return 'C';
    }
    elseif ($size[0] > 190)
        return 'C';
    elseif ($size[0] > 80)
        return 'B';
    else 
        return 'A';
}

//���������� ���. ���������
function linkAdditionalCats($catalog_id, $cats){
	if (is_numeric($catalog_id)) {	
		$cats = (array)$cats;
		
		//�������� ������, ���������� ���� ������ �� ���� ����������.
		//���� ����� ����������� �������, ������ ������ ������.
		$spec_rang = getSpecRang($catalog_id);
		
		$query = mysql_query("SELECT * FROM `catalog` WHERE `id` = '{$catalog_id}'") or die(mysql_error());
		//������� ��� ��������� � ��� �����������
		$res = mysql_fetch_assoc($query);
		if (!empty($res)) {
			mysql_query("DELETE FROM `catalog_2_cat` WHERE `catalog_id` = {$catalog_id} AND `cat_id` != {$res['cat']}") or die(mysql_error());
		}
		//���� � ��� �����������	
		foreach ($cats as $cat_id) {			
			
			//��������� ����� � ���. ���������
			if (array_key_exists($cat_id, $spec_rang))
				$rang = $spec_rang[$cat_id];
			else
				$rang = 0;
			
			$query = "INSERT INTO `catalog_2_cat` (`catalog_id`, `cat_id`, `spec_rang`)
										   VALUES ({$catalog_id}, {$cat_id}, {$rang})";
			
			mysql_query($query) or die(mysql_error());
			
			if (mysql_error())
				return false;
						
		}
		return true;
	}
	return false;
}

//���������� ������, ���������� ���� ������ � ������ ����������
//����� ������� - ��������� ���������.
function getSpecRang($catalog_id) {
	if (is_numeric($catalog_id)) {
		
		$query = "SELECT * 
				  FROM `catalog_2_cat` 
				  WHERE `catalog_id` = {$catalog_id}";
		$res = mysql_query($query) or die(mysql_error());
		
		if (mysql_error())
			return false;
		$rang = array();
		if (mysql_num_rows($res) != 0) {
			while ($row = mysql_fetch_assoc($res)) {
				$rang[$row['cat_id']] = $row['spec_rang'];
			}
		}
		return $rang;
	}
	return false;
}

//���������� ������, ���������� id ���. ��������� ��� 
//������� ������
function getAdditionalCatsId($catalog_id) {
	if (is_numeric($catalog_id)) {
		$query = mysql_query("SELECT `cat` FROM `catalog` WHERE `id` = {$catalog_id}") or die(mysql_error());
		$res = mysql_fetch_assoc($query);
		//������ ���������
		$cat_id = $res['cat'];
		//����� ���� ���������, ����� ������
		$query = "SELECT `cat_id` 
				  FROM `catalog_2_cat` 
				  WHERE `catalog_id` = {$catalog_id}
				  AND `cat_id` != {$cat_id}";
		$res = mysql_query($query) or die(mysql_error());
		$catsId = array();

		while ($row = mysql_fetch_assoc($res)) {			
			$catsId[] = $row['cat_id'];
		}
		return $catsId; 
	}
	return false;
}

/**
 * ��������� ���� ��������� �� ��
 * @return type array
 */
function getQiwiPoint(){
	mysql_query("SET NAMES utf-8");
	$query = "SELECT * FROM QIWI_POINT GROUP BY `point`";
	$row = mysql_query($query) or die(mysql_error());
	$arr = array();
	while ($tow = mysql_fetch_assoc($row)) {
		$arr[] = $tow;
	}
	mysql_query("SET NAMES cp1251");
	return $arr;
}

/**
 * ������� ������, ���� ��� �� �������� ���������� ������, ����� ������.
 * @return type boolean 
 */
function getDebug(){
	$my_ip = $_SERVER['REMOTE_ADDR'];
	$debug = false;
	if($my_ip == "217.194.255.193"){
		$debug = true;
	}
	return $debug;
}

function getQiwiSizeName($arr){
	$qiwi = array(
		array(80, 380, 640), // A
		array(190, 380, 640), // B
		array(380, 410, 640), // C
	);
	
	if( $arr[0]==0 && $arr[1]==0 && $arr[2]==0 ){
		return 'none';
		break;
	}
	
	sort($arr);
	// var_dump($arr);
	if( $arr[0]<=$qiwi[0][0] && $arr[1]<=$qiwi[0][1] && $arr[2]<=$qiwi[0][2] ){
		return 'A';
	}elseif( $arr[0]<=$qiwi[1][0] && $arr[1]<=$qiwi[1][1] && $arr[2]<=$qiwi[1][2] ){
		return 'B';
	}elseif( $arr[0]<=$qiwi[2][0] && $arr[1]<=$qiwi[2][1] && $arr[2]<=$qiwi[2][2] ){
		return 'C';
	}else{
		return 'none';
	}
}

function getOrderById($orderId) {
  if (is_numeric($orderId)) {
    $query = "SELECT * FROM `orders` 
				  WHERE `id` = {$orderId} 
				  LIMIT 1";
    $row = mysql_fetch_assoc(mysql_query($query));

    if (!$row) return false;

    $data = array();
    foreach ($row as $key => $value) {
      $data[$key] = $value;
    }
    $orderInfo['details'] = $data;
    $price = unserialize($row['order_price']);
    $items = unserialize($row['products']);

    $productsInCart = array();
    foreach ($items as $item) {
      $productsInCart[$item['id']] = array('id' => $item['id'], 'amount' => $item['amount']);
    }

    $query = "
			SELECT
				*
			FROM
				`catalog`
			WHERE
				`id` IN ('" . implode("','", array_keys($productsInCart)) . "')
		";
    $res = mysql_query($query);
    while ($line = mysql_fetch_assoc($res)) {
      $productsToResponse[] = array('id' => $line['id'], 'amount' => $productsInCart[$line['id']]['amount'], 'price' => $line['price'], 'price_after_discount' => $line['price_after_discount'], //	'converted_title' =>  iconv("windows-1251", "utf-8", $line['title']),
        'converted_title' => iconv('cp1251','utf-8',$line['title']), 'title' => $line['title']);
    }
    $orderCode = getOrderCode($orderId);

    $orderInfo['items'] = array('orderId' => $orderId, 'orderCode' => $orderCode, 'price' => $price, 'productsToResponse' => $productsToResponse);
// var_dump($orderInfo);
    return $orderInfo;
  }
  return false;
}

//���������� ���������� � ������ ��� ����� ��������/�����������
function getBase64Order($orderInfo) {
  $k = 0;
  $order = array();

  foreach ($orderInfo['items']['productsToResponse'] as $product) {
    $order['items'][$k]['title'] = $product['converted_title'];
    $query = "SELECT `c2`.`title` FROM `catalog` as `c1` INNER JOIN `cat` as `c2` ON `c1`.`cat` = `c2`.`id` WHERE `c1`.`id` = {$product['id']} LIMIT 1";
    $res = mysql_fetch_assoc(mysql_query($query));
    $category =  iconv('cp1251','utf-8',trim($res['title']));
    $order['items'][$k]['category'] = $category;
    $order['items'][$k]['qty'] = $product['amount'];
    $order['items'][$k]['price'] = $product['price_after_discount'];
    $k++;
  }

  $fio = explode(' ', $orderInfo['details']['name']);
  $firstname = $middlename = $lastname = "";
  switch (count($fio))  {
    case 3:
      $firstname = $fio[0];
      $middlename = $fio[1];
      $lastname = $fio[2];
      break;
    case 2:
      $firstname = $fio[0];
      $middlename = ' ';
      $lastname = $fio[1];
      break;
    case 1:
      $firstname = $fio[0];
      $middlename = ' ';
      $lastname = ' ';
      break;
    default:
      //$name = $middlename = $lastname = ' ';
      break;
  }

  $order['details']['firstname'] = $firstname;
  $order['details']['middlename'] =  $middlename;
  $order['details']['lastname'] = $lastname;
  $order['details']['email'] = $orderInfo['details']['email'];
  $order['details']['cellphone'] = $orderInfo['details']['phone'];
  $order['partnerId'] = Config::get('credit.partner_id');
  $order['partnerName'] = 'usmedic.ru';
  $order['partnerOrderId'] = $orderInfo['items']['orderId'];
  $order['deliveryType'] = '';
  $order['discount'] =
    ($orderInfo['items']['price']['price_before_global_discount'] -
      $orderInfo['items']['price']['price_after_global_discount']);
  return $base64 = base64_encode(json_encode($order));
}

function signMessage($message, $salt, $iterationCount = 1102) {
  $message = $message . $salt;
  $result = md5($message) . sha1($message);
  for ($i = 0; $i < $iterationCount; $i++) $result = md5($result);
  return $result;
}

/**
 * ������� �������. ������������� ������ ��������� ������ �������� ����������
 * $value
 * 
 * @param variant $value ���������� ��� ������ �� �� ��������
 */
function d($value = null, $die = 1) {
    
    echo 'Debug: <br /><pre>';
    print_r($value);
    echo '</pre>';
    
    if($die) die;
}

/**
* ������� �������� ������ �� �����
*
* @param string $message ���� ���������
* @param string $mailTo Email ���������� ������
* @param string $subject ��������� ������
*/
function sendMailPHPMailer($subject,$message,$mailTo) {
	require_once('../edit/mail/PHPMailerAutoload.php');
	$mail = new PHPMailer();
	$mail->CharSet = 'cp1251';
	$mail->IsSMTP();
	$mail->SMTPDebug = 0;
	$mail->SMTPAuth = true;
	$mail->SMTPSecure = 'ssl';
	$mail->Host = 'smtp.yandex.ru';
	$mail->Port = 465;
	$mail->Username = 'doctorcenter@yandex.ru';
	$mail->Password = 'dfhvwh423gvi';
	$mail->SetFrom('doctorcenter@yandex.ru', 'robot');
	$mail->Subject = $subject;
	$mail->IsHTML(true);
	$mail->Body = $message;
	$mail->AddAddress($mailTo);
	if(!$mail->Send()) {
	  $error = 0;
	} else {
	  $error = 1;
	}
	return $error;
}
/*������� ��� ������ ���� ��������, ����������� �� ��������� ������.*/
function GetDeliveryPriceByGroup($cat,$pad) {
$sql =mysql_query("
SELECT delivery_price.`from`, delivery_price.`price`
FROM  `category_2_delivery_group` 
INNER JOIN  `delivery_group_2_delivery_price` ON ( category_2_delivery_group.`delivery_group_id` = delivery_group_2_delivery_price.`delivery_group_id` ) 
INNER JOIN  `delivery_price` ON ( delivery_group_2_delivery_price.`delivery_price_id` = delivery_price.`id`) 
WHERE  `category_id` = {$cat}");
$dev_price_1=-999; $max_from=-999;
while ($arr=mysql_fetch_assoc($sql)) { 
	if(($pad>$arr['from']) && ($arr['from']!=0) && ($arr['from']>$max_from)) {$dev_price_1=$arr['price']; $max_from=$arr['from'];}
	elseif($arr['from']==0) {$dev_price_0=$arr['price'];}
	}
	if($dev_price_1==-999) {echo '�������� �� ������ ��� ������� ������ �� '.$dev_price_0.' ���';}
	elseif($dev_price_1==0) { echo '�������� �� ������ ��� ������� ������ ���������';}
	else { echo '�������� �� ������ ��� ������� ������ �� '.$dev_price_1.' ���';}
}
?>