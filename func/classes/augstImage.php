<?php
//                        _   _____                            
//                       | | |_   _|                           
//   __ _ _   _  __ _ ___| |_  | |  _ __ ___   __ _  __ _  ___ 
//  / _` | | | |/ _` / __| __| | | | '_ ` _ \ / _` |/ _` |/ _ \
// | (_| | |_| | (_| \__ \ |_ _| |_| | | | | | (_| | (_| |  __/
//  \__,_|\__,_|\__, |___/\__|_____|_| |_| |_|\__,_|\__, |\___|
//               __/ |                               __/ |     
//              |___/                               |___/      
//
//	РљР»Р°СЃСЃ, РєРѕС‚РѕСЂС‹Р№ РїРѕРјРѕРіР°РµС‚ Р·Р°РіСЂСѓР¶Р°С‚СЊ, РѕС‚РєСЂС‹РІР°С‚СЊ, СЃРѕС…СЂР°РЅСЏС‚СЊ,
//	РјР°СЃС€С‚Р°Р±РёСЂРѕРІР°С‚СЊ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ Рё СЃС‚Р°РІРёС‚СЊ РЅР° РЅРёС… РІРѕС‚РµСЂРјР°СЂРєРё.
//

class augstImage {

	const IMG_RESIZE_FIT     = 989; // РІРїРёСЃС‹РІР°РЅРёРµ РІ РєРІР°РґСЂР°С‚ (СѓРјРµРЅСЊС€РµРЅРёРµ РїРѕ РјР°РєСЃРёРјР°Р»СЊРЅРѕР№ СЃС‚РѕСЂРѕРЅРµ)
	const IMG_RESIZE_MAX     = 988; // СѓРјРµРЅСЊС€РµРЅРёРµ РїРѕ РјР°РєСЃРёРјР°Р»СЊРЅРѕР№ СЃС‚РѕСЂРѕРЅРµ
	const IMG_RESIZE_MIN     = 987; // СѓРјРµРЅСЊС€РµРЅРёРµ РїРѕ РјРёРЅРёРјР°Р»СЊРЅРѕР№ СЃС‚РѕСЂРѕРЅРµ
	const IMG_RESIZE_CROP    = 986; // РѕР±СЂРµР·РєР° РёСЃС…РѕРґРЅРѕР№ РєР°СЂС‚РёРЅРєРё: РµСЃР»Рё W>H, С‚Рѕ РѕР±СЂРµР·РєР° РїРѕ Р±РѕРєР°Рј, РµСЃР»Рё W<H, С‚Рѕ СЃРЅРёР·Сѓ

	const IMG_WMPOS_TOP_LEFT      = 979; // Р»РµРІС‹Р№ РІРµСЂС…РЅРёР№ СѓРіРѕР»
	const IMG_WMPOS_TOP_CENTER    = 978; //	СЃРІРµСЂС…Сѓ РїРѕСЃРµСЂРµРґРёРЅРµ
	const IMG_WMPOS_TOP_RIGHT     = 977; //	РїСЂР°РІС‹Р№ РІРµСЂС…РЅРёР№ СѓРіРѕР»
	const IMG_WMPOS_MIDDLE_LEFT   = 976; //	РїРѕСЃРµСЂРµРґРёРЅРµ СЃР»РµРІР°
	const IMG_WMPOS_CENTER        = 975; //	РїРѕ С†РµРЅС‚СЂСѓ
	const IMG_WMPOS_MIDDLE_RIGHT  = 974; //	РїРѕСЃРµСЂРµРґРёРЅРµ СЃРїСЂР°РІР°
	const IMG_WMPOS_BOTTOM_LEFT   = 973; // Р»РµРІС‹Р№ РЅРёР¶РЅРёР№ СѓРіРѕР»
	const IMG_WMPOS_BOTTOM_CENTER = 972; // СЃРЅРёР·Сѓ РїРѕСЃРµСЂРµРґРёРЅРµ
	const IMG_WMPOS_BOTTOM_RIGHT  = 971; // РїСЂР°РІС‹Р№ РЅРёР¶РЅРёР№ СѓРіРѕР»

	private $_path        = null;			//	РїРѕР»РЅС‹Р№ РїСѓС‚СЊ Рє РёР·РѕР±СЂР°Р¶РµРЅРёСЋ
	private $_size        = array();		//	СЂР°Р·РјРµСЂС‹ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ, РєР»СЋС‡Рё - width, height
	private $_type        = IMAGETYPE_JPEG;	//	С‚РёРї РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РїРѕ СѓРјРѕР»С‡Р°РЅРёСЋ jpeg
	private $_handler     = null;			//	handler СѓСЃРїРµС€РЅРѕ РѕС‚РєСЂС‹С‚РѕРіРѕ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ

	private static $_watermark = null;		//	augstImage С…СЂР°РЅРёС‚ РґР°РЅРЅС‹Рµ Рѕ РІРѕС‚РµСЂРјР°СЂРєРµ

	//	РїРѕРґРґРµСЂР¶РёРІР°РµРјС‹Рµ С‚РёРїС‹ РёР·РѕР±СЂР°Р¶РµРЅРёР№: jpeg, gif, png
	public static $extenstions = array(
			IMAGETYPE_JPEG => '.jpg',
			IMAGETYPE_GIF  => '.gif',
			IMAGETYPE_PNG  => '.png',
		);

	/**
	 * Р—Р°РіСЂСѓР·РєР° РІС‹Р±СЂР°РЅРЅРѕРіРѕ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ.
	 * @return mixed|null Handle РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РёР»Рё null РІ СЃР»СѓС‡Р°Рµ РЅРµСѓРґР°С‡Рё.
	 */
	private function _open() {
		switch ($this->_type) {
			case IMAGETYPE_JPEG:
				return imagecreatefromjpeg($this->_path);

			case IMAGETYPE_GIF:
				return imagecreatefromgif($this->_path);
			
			case IMAGETYPE_PNG:
				return imagecreatefrompng($this->_path);

			default:
				return null;

		}
	}

	/**
	 * РЎРѕС…СЂР°РЅСЏРµС‚ РёР·РѕР±СЂР°Р¶РµРЅРёРµ РІ РІС‹Р±СЂР°РЅРЅРѕРј С„РѕСЂРјР°С‚Рµ Рё СЃС‚Р°РІРёС‚ РїСЂР°РІР° РґРѕСЃС‚СѓРїР°
	 * СЂР°РІРЅС‹РјРё 0666.
	 * @param  mixed $imageHandler Handle СЃРѕС…СЂР°РЅСЏРµРјРѕРіРѕ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ.
	 * @param  string $imagePath   РџРѕР»РЅС‹Р№ РїСѓС‚СЊ Рє РёР·РѕР±СЂР°Р¶РµРЅРёСЋ.
	 * @param  int $imageType      РўРёРї РёР·РѕР±СЂР°Р¶РµРЅРёСЏ - PHP-РєРѕРЅСЃС‚Р°РЅС‚Р° IMAGETYPE_XXX.
	 * @param  int $quality        РљР°С‡РµСЃС‚РІРѕ СЃРѕС…СЂР°РЅРµРЅРёСЏ.
	 * @return bool                TRUE, РµСЃР»Рё РёР·РѕР±СЂР°Р¶РµРЅРёРµ СЃРѕС…СЂР°РЅРµРЅРѕ.
	 */
	static private function _save($imageHandler, $imagePath, $imageType, $quality) {
		$flag = false;
		switch ($imageType) {
			case IMAGETYPE_JPEG:
				$flag = imagejpeg($imageHandler, $imagePath, $quality);
				break;
			
			case IMAGETYPE_GIF:
				$flag = imagegif($imageHandler, $imagePath, $quality);
				break;
			
			case IMAGETYPE_PNG:
				$flag = imagepng($imageHandler, $imagePath, $quality);
				break;

			default:
				break;
		}
		if ($flag) {
			chmod($imagePath, 0666);
			imagedestroy($imageHandle);
		}
		return $flag;
	}

	/**
	 * Р’РѕР·РІСЂР°С‰Р°РµС‚ СЂР°СЃС€РёСЂРµРЅРёРµ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ РЅР° Р¶РµСЃС‚РєРѕРј РґРёСЃРєРµ.
	 * @param  string $imagePath  РџРѕР»РЅС‹Р№ РїСѓС‚СЊ Рє РёР·РѕР±СЂР°Р¶РµРЅРёСЋ.
	 * @param  bool   $includeDot Р’РєР»СЋС‡Р°С‚СЊ Р»Рё РІ СЂРµР·СѓР»СЊС‚Р°С‚ С‚РѕС‡РєСѓ.
	 * @return string             Р Р°СЃС€РёСЂРµРЅРёРµ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ.
	 */
	public static function getExt($imagePath, $includeDot = true) {
		$size = getimagesize($imagePath);
		if ($includeDot) return self::$extenstions[$size[2]];
		return ltrim(self::$extenstions[$size[2]], '.');
	}

	/**
	 * РћР±СЂР°Р±Р°С‚С‹РІР°РµС‚ Р·Р°РіСЂСѓР¶РµРЅРЅС‹Рµ С‡РµСЂРµР· С„РѕСЂРјС‹ РёР·РѕР±СЂР°Р¶РµРЅРёСЏ, РїРѕРјРµС‰Р°СЏ РёС… РІ РїР°РїРєСѓ
	 * РЅР° СЃРµСЂРІРµСЂРµ, РєРѕС‚РѕСЂР°СЏ Р·Р°РґР°РµС‚СЃСЏ РІ РєРѕРЅС„РёРіСѓСЂР°С†РёРё СЃР°Р№С‚Р°.
	 * @param  string $filesKey РљР»СЋС‡ РјР°СЃСЃРёРІР° $_FILES[...]
	 * @param  string $savePath Р”РёСЂРµРєС‚РѕСЂРёСЏ РґР»СЏ СЃРѕС…СЂР°РЅРµРЅРёСЏ РёР·РѕР±СЂР°Р¶РµРЅРёР№.
	 * @param  array  $ids      РњР°СЃСЃРёРІ СЃ ID РґРѕР±Р°РІР»СЏРµРјС‹С… РёР·РѕР±СЂР°Р¶РµРЅРёР№.
	 * @return array|bool       False РІ СЃР»СѓС‡Р°Рµ РЅРµСѓРґР°С‡Рё. РњР°СЃСЃРёРІ СЃ РїРѕР»РЅС‹РјРё
	 *                                РїСѓС‚СЏРјРё Рє Р·Р°РіСЂСѓР¶РµРЅРЅС‹Рј РёР·РѕР±СЂР°Р¶РµРЅРёСЏРј РІ
	 *                                СЃР»СѓС‡Р°Рµ СѓСЃРїРµС…Р°.
	 */
	public static function upload($filesKey, $savePath, $ids) {
		//	Р•СЃР»Рё Сѓ РїРѕР»СЏ СЃ Р·Р°РіСЂСѓР·РєРѕР№ СѓСЃС‚Р°РЅРѕРІР»РµРЅ Р°С‚СЂРёР±СѓС‚ multiple
		if (is_array($_FILES[$filesKey]['tmp_name'])) {
			$result = array();
			foreach ($_FILES[$filesKey]['tmp_name'] as $i => $sourceFile) {
				if (!empty($_FILES[$filesKey]['error'][$i])) return false;
				$sourceFile = $_FILES[$filesKey]['tmp_name'][$i];
				$imagePath = $savePath . $ids[$i] . self::getExt($sourceFile);
				if (!move_uploaded_file($sourceFile, $imagePath)) return false;
				$result[] = $imagePath;
			}
			return $result;
		} else {
			if (!empty($_FILES[$filesKey]['error'])) return false;
			$sourceFile = $_FILES[$filesKey]['tmp_name'];
			$imagePath = $savePath . $ids[0] . self::getExt($sourceFile);
			if (!move_uploaded_file($sourceFile, $imagePath)) return false;
			return array($imagePath);
		}
		return false;
	}

	public function __construct($imagePath = false) {
		if ((!$imagePath) || (!file_exists($imagePath)))
			return null;
		$this->_path = $imagePath;
		$size = getimagesize($imagePath);
		$this->_type = $size[2];
		$this->_handler = $this->_open();
		if (!$this->_handler) return null;
		$this->_size = array(
				'width' => $size[0],
				'height' => $size[1],
			);
		return $this;
	}

	public function __destruct() {
		imagedestroy($this->_handler);
		unset($this->_handler);
	}

	public function __get($name) {
		switch ($name) {
			case 'width':
				return $this->_size['width'];
				break;
			
			case 'height':
				return $this->_size['height'];
				break;

			case 'ext':
				return self::$extenstions[$this->_type];
				break;
			
			default:
				$privateProperty = '_' . $name;
				if (property_exists($this, $privateProperty))
					return $this->$privateProperty;
				return false;
				break;
		}
	}

	/**
	 * РќР°РєР»Р°РґС‹РІР°РµС‚ РІР°С‚РµСЂРјР°СЂРєСѓ РЅР° РёР·РѕР±СЂР°Р¶РµРЅРёРµ.
	 * @param  string $destinationFile РџРѕР»РЅС‹Р№ РїСѓС‚СЊ Рє РёР·РѕР±СЂР°Р¶РµРЅРёСЋ СЃ РјР°СЂРєРѕР№.
	 * @param  string $watermarkPath   РџСѓС‚СЊ Рє РІР°С‚РµСЂРјР°СЂРєРµ РѕС‚ РєРѕСЂРЅСЏ (DOCUMENT_ROOT).
	 * @param  int $position           РџРѕР·РёС†РёСЏ РІР°С‚РµСЂРјР°СЂРєРё. (СЃРј. РєРѕРЅСЃС‚Р°РЅС‚С‹ augstImage).
	 * @param  float  $offset          РћС‚СЃС‚СѓРї РѕС‚ РєСЂР°СЏ РІ РґРѕР»СЏС….
	 * @return string|bool             False РІ СЃР»СѓС‡Р°Рµ РЅРµСѓРґР°С‡Рё, РёРЅР°С‡Рµ РїРѕР»РЅС‹Р№
	 *                                       РїСѓС‚СЊ Рє РёР·РѕР±СЂР°Р¶РµРЅРёСЋ СЃ РјР°СЂРєРѕР№.
	 */
	public function watermark($destinationFile, $watermarkPath, $position = self::IMG_WMPOS_BOTTOM_RIGHT, $offset = 0.15) {
		if (is_null(self::$_watermark))
			self::$_watermark = new augstImage($watermarkPath);

		$ratio = $this->width / 600;
		$watermarkResized = array(
				'width' => self::$_watermark->width * $ratio,
				'height' => self::$_watermark->height * $ratio,
			);

		$dW = $this->width - $watermarkResized['width'];
		$dH = $this->height - $watermarkResized['height'];

		switch ($position) {
			case self::IMG_WMPOS_TOP_LEFT:
				$transfer_x = $this->width * $offset;
				$transfer_y = $this->height * $offset;
				break;
			
			case self::IMG_WMPOS_TOP_CENTER:
				$transfer_x = $dW / 2;
				$transfer_y = $this->height * $offset;
				break;
			
			case self::IMG_WMPOS_TOP_RIGHT:
				$transfer_x = $this->width * (1 - $offset) - $watermarkResized['width'];
				$transfer_y = $this->height * $offset;
				break;
			
			case self::IMG_WMPOS_MIDDLE_LEFT:
				$transfer_x = $this->width * $offset;
				$transfer_y = $dH / 2;
				break;
			
			case self::IMG_WMPOS_CENTER:
				$transfer_x = $dW / 2;
				$transfer_y = $dH / 2;
				break;
			
			case self::IMG_WMPOS_MIDDLE_RIGHT:
				$transfer_x = $this->width * (1 - $offset) - $watermarkResized['width'];
				$transfer_y = $dH / 2;
				break;
			
			case self::IMG_WMPOS_BOTTOM_LEFT:
				$transfer_x = $this->width * $offset;
				$transfer_y = $this->height * (1 - $offset) - $watermarkResized['height'];
				break;
			
			case self::IMG_WMPOS_BOTTOM_CENTER:
				$transfer_x = $dW / 2;
				$transfer_y = $this->height * (1 - $offset) - $watermarkResized['height'];
				break;
			
			case self::IMG_WMPOS_BOTTOM_RIGHT:
			default:
				$transfer_x = $this->width * (1 - $offset) - $watermarkResized['width'];
				$transfer_y = $this->height * (1 - $offset) - $watermarkResized['height'];
				break;
		}

		// $offset_x = $watermarkResized['width'] / 2 + 55 * ratio;
		// $offset_y = $watermarkResized['height'] / 2;

		// $transfer_x = $this->width * 0.85 - $offset_x;
		// $transfer_y = $this->height * 0.85 - $offset_y;

		imagecopyresampled($this->_handler,
			self::$_watermark->handler,
			$transfer_x, $transfer_y,
			0, 0,
			$watermarkResized['width'], $watermarkResized['height'],
			self::$_watermark->width, self::$_watermark->height);

		if (self::_save($this->_handler, $destinationFile, $this->_type, 100))
			return $destinationFile;
		return false;
	}

	/**
	 * РњР°СЃС€С‚Р°Р±РёСЂСѓРµС‚ РёР·РѕР±СЂР°Р¶РµРЅРёРµ РІ СѓРєР°Р·Р°РЅРЅРѕРј СЂРµР¶РёРјРµ Рё РѕРїСЂРµРґРµР»РµРЅРЅРѕРј С„РѕСЂРјР°С‚Рµ.
	 * @param  array $format  РњР°СЃСЃРёРІ, СЃРѕРґРµСЂР¶Р°С‰РёР№ РёРЅС„РѕСЂРјР°С†РёСЋ Рѕ РЅРµРѕР±С…РѕРґРёРјРѕРј С„РѕСЂРјР°С‚Рµ.
	 * @param  int $imageId ID РёР·РѕР±СЂР°Р¶РµРЅРёСЏ, РґР»СЏ РєРѕС‚РѕСЂРѕРіРѕ РґРµР»Р°РµС‚СЃСЏ СЂРµСЃР°Р№Р·.
	 * @param  int $mode    Р РµР¶РёРј СЂРµСЃР°Р№Р·Р°. (СЃРј. РєРѕРЅСЃС‚Р°РЅС‚С‹ augstImage).
	 */
	public function resize($format, $imageId, $mode = self::IMG_RESIZE_MAX) {
		$ratioW = $format['width'] / $this->width;
		$ratioH = $format['height'] / $this->height;
		$destinationFile = $format['path'] . $imageId . $this->ext;

		switch ($mode) {
			case self::IMG_RESIZE_MAX:
				$relation = ($ratioW < $ratioH) ? $ratioW : $ratioH;
				break;
			
			case self::IMG_RESIZE_MIN:
				$relation = ($ratioW > $ratioH) ? $ratioW : $ratioH;
				break;
			
			case self::IMG_RESIZE_CROP:
				$relation = ($ratioW > $ratioH) ? $ratioW : $ratioH;
				break;
			
			case self::IMG_RESIZE_FIT:
			default:
				$relation = ($ratioW < $ratioH) ? $ratioW : $ratioH;
				break;
		}

		$newWidth = round($this->width * $relation);
		$newHeight = round($this->height * $relation);
		$destX = $destY = 0;
		$sizeW = $newWidth;
		$sizeH = $newHeight;

		switch ($mode) {
			case self::IMG_RESIZE_CROP:
				$sizeW = $sizeH = ($newWidth < $newHeight) ? $newWidth : $newHeight;
				$destX = round(($sizeW - $newWidth) / 2);
				break;
			
			case self::IMG_RESIZE_FIT:
				$sizeW = $sizeH = ($newWidth < $newHeight) ? $newWidth : $newHeight;
				$destX = round(($sizeW - $newWidth) / 2);
				$destY = round(($sizeH - $newHeight) / 2);
				break;

			default:
				break;
		}

		if (($format['width'] != $this->width) &&
			!($sizeW > $this->width && 
				$sizeH > $this->heigh && 
				$mode == self::IMG_RESIZE_MAX)) {
			$destination = imagecreatetruecolor($sizeW, $sizeH);
			imagesavealpha($destination, true);
			imagealphablending($destination, false);
			if (imagecopyresampled($destination,
				$this->_handler,
				$destX, $destY,
				0, 0,
				$sizeW, $sizeH,
				$this->width, $this->height)) {
				self::_save($destination, $destinationFile, $this->_type, $format['quality']);
			} 
		} else {
			copy($sourceFile, $destinationFile);
		}
		return;
	}

}
?>
