<?php
class discountPrice {
	static $default = 0;
	public static function getByGabaritAndSum($gabarit, $sum) {
		if ($gabarit > 0 && $sum > 0) {
			$sql = 
"SELECT dp.`price` AS price
FROM `cat_gabarits_discount_2_delivery_price` cgd2dp
INNER JOIN `delivery_price` dp
ON (dp.`id`=cgd2dp.`delivery_price_id`)
WHERE cgd2dp.`cat_gabarits_id` = $gabarit AND dp.`from` <= $sum
ORDER BY dp.`from` DESC LIMIT 1";
			if ($result = DB::query($sql)) {
				if ($line = $result->fetch_assoc()) {
					return $line['price'];
				}
			}
		}
		return self::$default;
	}
}
?>