<?
function api_10med($method, $params) {
	global $config;
	//� ������� � API 10med ��� ����� ����������� �������� api ���� 
	$data['api_key'] = $config['10med_apikey'];
	//� ������������� ��������, ������� ��������� ������ � ���
	$data['shop_id'] = $config['10med_shopid'];
	//� �� ��� �������� ������ ��������
	$data['method'] = $method;
	//� ��� � ��� ��������� ������ � ��� �����. �� �� �����
	$data['data'] = $params;
	
	//�������������� ������ ��� ��������
	$data = http_build_query($data);
	
	$context = stream_context_create(array(
         'http' => array(
             'method' => 'POST',
             'content' => $data,
         ),
    ));
	//���������� ������
	$response = file_get_contents($config['10med_apiurl'], false, $context);
	//var_dump($response);
	//die();
	//��������� �����, ���������� ��� � ������ � ����������
	return unserialize($response);
}

class api10med{
	private static $siteId = 5;
	private static $url = 'http://95.85.47.127/api/';
	private static $tokenParam = '?access-token=ZyaPryZ7UrrQ5S-8oVwphH2pR9xRhbmV';

	public static function search($title) {
		$action = 'search';
		$params = [
			'title' => $title,
			'siteId' => self::$siteId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	public static function link($localId, $remoteId) {
		$action = 'link';
		$params = [
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
			'remoteProductId' => $localId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}

	public static function product($remoteId) {
		$action = 'product';
		$params = [
			'siteId' => self::$siteId,
			'id10med' => $remoteId,
		];

		$myCurl = curl_init();
		curl_setopt_array($myCurl, array(
		    CURLOPT_URL => self::$url . $action . self::$tokenParam,
		    CURLOPT_RETURNTRANSFER => true,
		    CURLOPT_POST => true,
		    CURLOPT_POSTFIELDS => http_build_query($params)
		));
		$res = curl_exec($myCurl);
		curl_close($myCurl);
		return $res;
	}
}