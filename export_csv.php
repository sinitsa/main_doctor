<?php
/*
Простая выгрузка в CSV.
*/

ini_set('display_errors', 1);
error_reporting(0);
mb_internal_encoding("UTF-8");

include ("connect.php");

$csv_file = ''; // создаем переменную, в которую записываем строки

$query = "SELECT id, title,cat,art, chpu,price_after_discount FROM catalog";
$result = mysql_query($query);

   while ($row = mysql_fetch_assoc($result))
   {
      $rest = mysql_result(mysql_query("SELECT amount FROM storage_rests WHERE catalog_id = '{$row['id']}'"), 0);
      $cat_title = mysql_result(mysql_query("SELECT title FROM cat WHERE id = '{$row['cat']}'"), 0);
      $csv_file .= '"'.$row["title"].'","'.'http://www.doctor-center.ru/medtehnika/'.$row["chpu"].'","'.$rest.'","'.$row["price_after_discount"].'","'. $cat_title .'"'."\r\n";
   }


$file_name = 'export.csv'; // название файла
$file = fopen($file_name,"w"); // открываем файл для записи, если его нет, то создаем его в текущей папке, где расположен скрипт
fwrite($file,trim($csv_file)); // записываем в файл строки
fclose($file); // закрываем файл

// задаем заголовки. то есть задаем всплывающее окошко, которое позволяет нам сохранить файл.
header('Content-type: application/csv'); // указываем, что это csv документ
header("Content-Disposition: inline; filename=".$file_name); // указываем файл, с которым будем работать
readfile($file_name); // считываем файл
unlink($file_name); // удаляем файл. то есть когда вы сохраните файл на локальном компе, то после он удалится с сервера
