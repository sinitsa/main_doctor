<?php
chdir(dirname(__FILE__));
error_reporting(E_ALL);
ini_set('pcre.backtrack_limit', 100000000);
ini_set('memory_limit', '512M');
iconv_set_encoding("internal_encoding", "UTF-8");
set_time_limit(0);


define('DB_USER', 'root');
define('DB_PASS', '');
define('DB_BASE', 'work');
define('DB_HOST', 'localhost');

class SomeClassName extends Parser {
   public function Parse() {
      try {
         $this->db = new My_MysqlDB(DB_USER, DB_PASS, DB_BASE, DB_HOST);
      }
      catch (exception $e) {
         exit($e->getMessage());
      }
      
      is_dir('img') OR mkdir('img');
      is_dir('img/small') OR mkdir('img/small');
      is_dir('img/medium') OR mkdir('img/medium');
      is_dir('img/big') OR mkdir('img/big');
      is_dir('img/about') OR mkdir('img/about');   
      
      echo "\nmain page -> ";
      
      $this->loadUrl('03m.ru') OR die('woops');
      preg_match_all('#<h3>([^<]++)</h3>\s*+<ul>(.*?)</ul>#uis', $this->getBuffer(), $cats, PREG_SET_ORDER) OR die('main not found!');
      
      foreach($cats as $cat) {
         if(!preg_match_all('#<li><a href="/catalog/([^"]++)">([^<]++)</a></li>#uis', $cat[2], $subCats, PREG_SET_ORDER)) {
            continue;
         }
         
         foreach($subCats as $sub) {
            echo $this->parseCat($sub[1], array($sub[2]));
         }
      }
   }
   
   protected function parseCat($catId, $catsList) {
      echo "\n ", $this->translite($catsList[count($catsList) - 1]), " -> ";
      
      if(!$this->loadUrl('http://03m.ru/catalog/'.$catId)) {
         return "error";
      }
      
      if(preg_match('#<div id="term-subcats">(.*?)</div>#uis', $this->getBuffer(), $data)) {
         if(preg_match_all('#<a href="/catalog/([^"]++)">([^<]++)</a>#uis', $data[1], $subCats, PREG_SET_ORDER)) {
            echo "ok; sub: ", count($subCats);

            foreach($subCats as $sub) {
               $list = $catsList;
               $list[] = $sub[2];
               
               echo $this->parseCat($sub[1], $list);
            }
            
            echo "\n ", $this->translite($catsList[count($catsList) - 1]), " -> ";
            if(!$this->loadUrl('http://03m.ru/catalog/'.$catId)) {
               return "error";
            }
         }
      }
      
      $pages = 1;
      if(preg_match_all('#\?page=(\d++)#uis', $this->getBuffer(), $data)) {
         $pages = max(1, max($data[1]) + 1);
      }
      
      echo "ok; pages: {$pages}";
      
      for($page = 1; $page <= $pages; ++$page) {
         echo "\n  {$page} / {$pages} -> ";
         
         if($page > 1 && !$this->loadUrl('http://03m.ru/catalog/'.$catId.'?page='.($page-1))) {
            echo 'error';
            continue;
         }
         
         if(preg_match('#<ul class="catalog-list"></ul>#uis', $this->getBuffer())) {
            echo 'empty';
            continue;
         }
         
         if(!count($block = $this->getBlocks('<ul class="catalog-list">'))) {
            echo 'wtf?';
            continue;
         }
         
         if(!count($items = $this->getBlocks('<li', $block[0]))) {
            echo 'WTF?';
            continue;
         }
         
         echo 'ok; save: ', count($items), '; ';
         
         foreach($items as $item) {
            $info = array();
            
            $info['small'] = '';
            if(preg_match('#<a href="/product/[^"]++"[^>]*+><img src="(http://[^"]+jpg)"#uis', $item, $data)) {
               $info['small'] = $data[1];
            }
            
            if(!preg_match('#<strong class="price">(\d++)(?:[.,]\d++)? руб#uis', $item, $data)) {
               echo 'p';
               continue;
            }
            $info['price'] = $data[1];
            
            if(!preg_match('#<h2><a href="/product/([^"/]++)">([^<]++)</a></h2>#uis', $item, $data)) {
               echo 'i';
               continue;
            }
            $info['id'] = $data[1];
            $info['link'] = 'http://03m.ru/product/'.$info['id'];
            $info['name'] = $data[2];

            $info['desc'] = '';
            if(preg_match('#<div class="product-body">(.*?)</div>#uis', $item, $data)) {
               $info['desc'] = $data[1];
            }
            
            $info['cats'] = $catsList;
            echo $this->parseItem($info);
         }
      }
   }
   
   protected function parseItem($info) {
      if($this->check($info['id'])) {
         return 's';
      }
      
      if(!$this->loadUrl($info['link'])) {
         return 'e';
      }
      
      $info['big'] = '';
      if(preg_match('#<div class="field-item odd">\s*+<a href="(http://[^"]+jpg)"#uis', $this->getBuffer(), $data)) {
         $info['big'] = $data[1];
      }
      
      $info['medium'] = '';
      if(preg_match('#<div class="field-item odd">\s*+(?:<a[^>]++>)?<img src="(http://[^"]+jpg)"#uis', $this->getBuffer(), $data)) {
         $info['medium'] = $data[1];
      }
      
      $info['artucul'] = '';
      if(preg_match('#<td class="title">Артикул:</td>\s*+<td>(?!n/a)([^<]++)</td>#uis', $this->getBuffer(), $data)) {
         $info['artucul'] = $data[1];
      }
      
      $info['full'] = '';
      if(!preg_match('#<div class="decription-holder"></div>#uis', $this->getBuffer())) {
         if(count($block = $this->getBlocks('<div class="decription-holder">'))) {
            $info['full'] = trim(preg_replace('#<h3>Описание и характеристики товара</h3>#uis', '', $block[0]));
         }
      }
      
      $info['desc'] = trim(preg_replace('#<(?!br)[^>]++>#uis', '', htmlspecialchars_decode($info['desc'])));
      $info['desc'] = preg_replace('#&nbsp;#uis', ' ', $info['desc']);
      $info['desc'] = preg_replace('#\s++#uis', ' ', $info['desc']);
      
      // Создаём категорию
      $parrentId = 0;
      $catId = 0;
      
      foreach($info['cats'] as $cat) {
         $catId = $this->db->query('SELECT id FROM cat WHERE title LIKE ? AND pod = ?', $cat, $parrentId);
         
         if($catId->rows()) {
            $catId = $catId->result(); 
         } else {
            $this->db->query('INSERT INTO cat(pod,title) VALUES(?, ?)', $parrentId, $cat);
            $catId = $this->db->query('SELECT LAST_INSERT_ID();')->result();
         }
         
         $parrentId = $catId;
      }
      
      $info['cat'] = $catId;
      
      
      $full = $this->SaveAllImages($info['full'], 'img/about/', $info['link'], '/img/about/');
      
      if($full != $info['full']) {
         // $this->log($info['link']);
         $info['full'] = $full;
      }
      
      $this->db->query('
         INSERT INTO catalog (
            cat, title, short, des, price, art
         ) VALUES (
            ?, ?, ?, ?, ?, ?
         )',
         $info['cat'], $info['name'], $info['desc'], $info['full'], $info['price'], $info['artucul']
      );
      
      $itemId = $this->db->query('SELECT LAST_INSERT_ID();')->result();
      
      foreach(array('small', 'big', 'medium') as $picType) {
         if(!isset($info[$picType])) {
            continue;
         }
         
         if($info[$picType] == '') {
            continue;
         }
         
         if(file_exists('img/'.$picType.'/'.$itemId.'.jpg')) {
            continue;
         }
         
         $pic = $info[$picType];
         $name = $itemId.'.jpg';
         $file = 'img/'.$picType.'/'.$name;
         
         
         if(!$this->loadUrl($pic, $file)) {
            continue;
         }
            
         if(filesize($file) < 1024) {
            unlink($file);
            continue;
         }
      } while(false);
      
      $this->save($info['id']);
      return '+';
   }
};

$parser = new SomeClassName();
$parser->Parse();

abstract class Parser {
   protected 
      $serverResponse = array(), // Заголовки ответа сервера
      $config = array(), // Конфиг
      $headers = array(), // Заголовки
      $cookies = array(), // Массив, содержащий куки.
      $files = array(), // Файлы, которые необходимо отсылать.
      $post = array(), // Массив содержащий post
      $request = '', // Строка запроса
      $keyArray = array(), // Массив ключей
      $keysLoaded = false, // Флаг загрузки ключей из файла
      $keysAdded = false, // Флаг добавления новых ключей
      $pageBuffer = '', // Содержимое загруженной страницы
      $currentLink = '',
      $id_string = '',
      $id_init = false,
      $proxyList = array(),
      $currentProxy
   ;
   
   public function __construct($cookie_dir = 'cookie/', $charset = 'utf-8') {
      $this->config = array(
         'reloadingEnabled' => false, // Пытаться ли повторно загрузить страницу в случе fail`а?
         'reloadingMax' => 3, // Количество попыток повторно загрузить страницу
         'locationEnabled' => false, // Переходим ли по lodation`ам?
         'locationMax' => 5, // Сколько раз максимально переходить по location`ам?
         'max_page_size' => 4096 * 1024, // Максимальный размер загружаемой страницы.
         'default_charset' => $charset, // Кодировка по умолчанию
         'GMT+' => 6, // Часовой пояс
         'cookie_dir' => $cookie_dir, // Куда сохранять куки?
         'cookieIsEnabled' => true, // Передаем / сохраняем куки?
         'cacheLinks' => false,
         'useProxy' => true,
         'read_timeout' => 10,
      );
      
      $this->SetDefaultHeaders();
      
      // Грузим прокси
      $this->proxyList = array();
      $this->currentProxy = 0;
      foreach(array('proxy', 'sock4', 'sock5') as $type) {
         if(!file_exists('_'.$type.'.list')) {
            continue;
         }
         
         $file = fopen('_'.$type.'.list', 'rb') OR die('woops #1');
         while($line = fgets($file, 4096)) {
            if(!preg_match('#^(\d++(?:\.\d++){3}):(\d++)$#i', trim($line), $data)) {
               continue;
            }
               
            $this->proxyList[] = array(
               'addr' => $data[1],
               'port' => $data[2],
               'type' => $type,
            );
         }
      }
   }
   
   protected function encodeUrl($link) {
      $link = rawurldecode($link);
      
      $good = array(
         'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
         'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
         '1','2','3','4','5','6','7','8','9','0',
         '-','_','/',':','.','','?','&', '='
      );
      $count = count($good);
      
      $newLink = '';
      for($i = 0, $len = iconv_strlen($link); $i < $len; ++$i) {
         $c = iconv_substr($link, $i, 1);
         
         for($j = 0; $j < $count; ++$j) {
            if($good[$j] == $c) {
               $newLink .= $c;
               continue 2;
            }
         }
         
         $newLink .= rawurlencode($c);
      }
      
      return $newLink;
   }
   
   protected function saveImages($images) {
      $socks = array();
      $buffers = array();
      $end = count($images);
      $infos = array();
      
      for($i = 0; $i < $end; ++$i) {
         $socks[$i] = false;
         $buffers[$i] = false;
         $headers[$i] = true;
         
         $key = $i;
         
         $port = 80;
         if(!preg_match('#^(?:http://)?((?:www\.)?(?:[a-z0-9_-]++\.)++[a-z0-9]{2,4})(:\d++)?/?(.++)?$#', $images[$i]['link'], $data)) {
            $images[$key]['status'] = false;
            continue;
         }
         
         if(isset($data[2]) && $data[2] != '') {
            $port = substr($data[2], 1);
         }
         
         $uri = '';
         if(isset($data[3])) {
            $uri = $data[3];
         }
         
         $host = $data[1];
         $uri = '/'.$uri;
         
         if($this->config['useProxy'] && count($this->proxyList)) {
            ++$this->currentProxy;
      
            if($this->currentProxy >= count($this->proxyList)) {
               $this->currentProxy = 0;
            }
         
            if($this->proxyList[$this->currentProxy]['type'] == 'sock4' || $this->proxyList[$this->currentProxy]['type'] == 'sock5') {
               $socks[$i] = $this->sock_connect(
                  $this->proxyList[$this->currentProxy]['addr'],
                  $this->proxyList[$this->currentProxy]['port'],
                  $host,
                  $port
               );
            } else if($this->proxyList[$this->currentProxy]['type'] == 'proxy') {
               $socks[$i] = @fsockopen(
                  $this->proxyList[$this->currentProxy]['addr'],
                  $this->proxyList[$this->currentProxy]['port'],
                  $errno,
                  $errstr,
                  2.0
               );
               $uri = 'http://'.$host.$uri;
            }
         } else {
            $socks[$i] = @fsockopen($host, $port, $errno, $errstr, 2.0);
         }
         
         if(!$socks[$i]) {
            continue;
         }
         
         $request  = "GET $uri HTTP/1.1\r\n";
         $request .= "Host: $host\r\n";
      
         // Добавляем стандартные заголовки
         foreach($this->headers as $key => $val) {
            $request .= $key . ': ' . $val . "\r\n";
         }
         
         $request .= "\r\n";
         fwrite($socks[$i], $request);
         $buffers[$i] = '';
         
         stream_set_blocking($socks[$i], false);
         stream_set_timeout($socks[$i], $this->config['read_timeout']);
         $infos[$i] = stream_get_meta_data($socks[$i]);
         $real_headers[$i] = '';
      }
      
      // Считываем
      $start = time();
      $loop = true;
      do {
         $loop = false;
         
         $time = time();
         
         for($i = 0; $i < $end; ++$i) {
            if(!$socks[$i]) {
               continue;
            }
            
            if(($time - $start) >= 30) {
               fclose($socks[$i]);
               $socks[$i] = false;
               continue;
            }
            
            if(feof($socks[$i])) {
               fclose($socks[$i]);
               $socks[$i] = false;
               continue;
            }
            
            if($infos[$i]['timed_out']) {
               fclose($socks[$i]);
               $socks[$i] = false;
               continue;
            }
            
            $loop = true;
            
            $buffers[$i] .= fread($socks[$i], 256);
         }
      } while($loop);
      
      for($i = 0; $i < $end; ++$i) {
         $pos1 = strpos($buffers[$i], "\n\n");
         $pos2 = strpos($buffers[$i], "\r\n\r\n");
      
         $headersEnd = 0;
         if($pos1 !== false && $pos2 !== false) {
            if($pos1 < $pos2) {
               $headersEnd = $pos1 + strlen("\n\n");
            } else {
               $headersEnd = $pos2 + strlen("\r\n\r\n");
            }
         }
         else if($pos1 !== false) {
            $headersEnd = $pos1 + strlen("\n\n");
         }
         else if($pos2 !== false) {
            $headersEnd = $pos2 + strlen("\r\n\r\n");
         } else {
            continue;
         }
      
         $headers = substr($buffers[$i], 0, $headersEnd);
         $buffers[$i] = substr($buffers[$i], $headersEnd);
         
         $response = array();
         
         foreach(explode("\n", $headers) as $header) {
            if(!preg_match('#^\s*+([^:]++):(.++)$#is', $header, $data)) {
               continue;
            }
            
            $response[strtolower($data[1])] = trim($data[2]);
         }
      
         if(isset($response['transfer-encoding'])) {
            if(strtolower($response['transfer-encoding']) != 'chunked') {
               $file = fopen( '_errors', 'wb' ) OR die('fuck');
               fputs($file, print_r($thread['response'], true)."\n");
               fclose($file);
               
               continue;
            }
         
            $buffer = '';
            $read = 0;
            $len = strlen($buffers[$i]);
         
            // Читаем строку, отлавливая "пакеты"
            while ($read <= $len) {
               // Размер следующего пакета
               $packSize = ''; 
               
               if($buffers[$i][$read] == "\r") { ++$read; }
               if($buffers[$i][$read] == "\n") { ++$read; }
               
               // Получаем размер пакета
               while ($buffers[$i][$read] != "\n" && $read <= $len) {
                  $packSize .= $buffers[$i][$read];
                  ++$read;
               }
               ++$read;            
               
               // Размер пакета 16 -> 10
               $packSize = base_convert(trim($packSize), 16, 10);
               if(!intval($packSize)) { 
                  break; 
               }
               
               $buffer .= substr($buffers[$i], $read, $packSize);
               $read = $read + $packSize;
            }
            
            $buffers[$i] = $buffer;
            $buffer = '';
         }
      
         if(isset($response['content-encoding'])) {
            $buffers[$i] = gzinflate(substr($buffers[$i], 10));
            
            if($response['content-encoding'] != 'gzip' || strlen($buffers[$i]) == 0) {
               $file = fopen( '_errors', 'wb' ) OR die('fuck');
               fputs($file, print_r($thread['response'], true)."\n");
               fclose($file);
               continue;
            }
         }
      
         $file = fopen($images[$i]['file'], 'wb');
         fputs($file, $buffers[$i]);
         fclose($file);
      }
   }
   
   
   protected function getLocation() {
      foreach($this->getServerResponse() as $name => $val) {
         if(!preg_match('#location#i', $name)) {
            continue;
         }
         
         return $val;
      }
      
      return false;
   }
   
   
   public function __destruct() {
      // Перед уничтожением сохраняем все добавленные ключи
      $this->SaveKeyList();
   }
   
   
   /**
    * Создаем файл с индексами
    */
   public function createIdIndex($num) {
      if(file_exists('_browser.id') && filesize('_browser.id') == $num) {
         return;
      }
      
      $file = fopen('_browser.id', 'wb');
      fputs($file, str_repeat('0', $num));
      fclose($file);
   }
   
   
   /**
    * Проверяем - существует ли такой индекс?
    */
   protected function checkId($id) {
      if($this->id_init == false) {
         $this->loadId();
      }
      
      if(strlen($this->id_string) < $id) {
         exit('Woops. Id string is too smal '.strlen($this->id_string).' vs. '.$id);
      }
      
      return ($this->id_string[$id] == 1);
   }
   
   
   /**
    * Сохраняем индекс
    */
   protected function saveId($id) {
      if($this->id_init == false) {
         $this->loadId();
      }
      
      static $file = false;
      if($file == false) {
         $file = fopen('_browser.id', 'r+b') OR die('can`t open _browser.id in r+b mode');
      }
      
      if(strlen($this->id_string) < $id) {
         exit('Woops. Id string is too smal '.strlen($this->id_string).' vs. '.$id);
      }
      
      if($this->id_string[$id] == 1) {
         return;
      }
      
      $this->id_string[$id] = 1;
      fseek($file, $id, SEEK_SET);
      fwrite($file, '1', 1);
   }
   
   
   /**
    * Грузим индексы
    */
   protected function loadId() {
      $this->id_init = true;
      
      if(!file_exists('_browser.id')) {
         $this->createIdIndex(300000);
      }
      
      $this->id_string = file_get_contents('_browser.id');
   }
   
   
   public abstract function Parse();
   
   
   protected function preg_getBlocks($regexp, $tag, $buffer = NULL) {
      if($buffer === NULL) {
         $buffer = $this->GetBuffer();
      }
      
      $len = strlen($buffer);

      $tagLen = strlen($tag);
      
      $lastOffset = 0;
      $blocks = array();
      
      do {
         if(!preg_match($regexp, $buffer, $data, PREG_OFFSET_CAPTURE, $lastOffset)) {
            break;
         }
         $pos = $data[0][1] + strlen($data[0][0]);
         $blockBuff = '';
         
         $tagCounter = 1;
         while($pos < $len && $tagCounter > 0) {
            $blockBuff .= $buffer[$pos++];
            
            if($buffer[$pos] != '<') {
               continue;
            }
            
            if(($pos + 1) < $len && $buffer[$pos + 1] == '/') {
               $isTag = true;
               
               for($i = 0; $i < $tagLen; ++$i) {
                  if(($p = $pos + 2 + $i) > $len) {
                     $isTag = false;
                     break;
                  }
                  
                  if($buffer[$p] != $tag[$i]) {
                     $isTag = false;
                     break;
                  }
               }
               
               if(!$isTag) { continue; }
               
               // if(preg_match('#[a-zA-Z]#', $buffer[$pos + 2 + $tagLen])) { continue; }
               
               --$tagCounter;
               continue;
            }
            
            $isTag = true;
            for($i = 0; $i < $tagLen; ++$i) {
               if(($p = $pos + 1 + $i) > $len) {
                  $isTag = false;
                  break;
               }
                  
               if($buffer[$p] != $tag[$i]) {
                  $isTag = false;
                  break;
               }
            }
            
            if(!$isTag) { continue; }
            // if(preg_match('#[a-zA-Z]#', $buffer[$pos + 1 + $tagLen])) { continue; }
            
            ++$tagCounter;
            continue;
         }
         
         $lastOffset = $pos;
         $blocks[] = trim($blockBuff);
         $blockBuff = '';
      } while(true);
   
      return $blocks;
   }
   
   
   /**
    * Вырезаем все блоки с $block_start
    */
   protected function getBlocks($block_start, $buffer = NULL) {
      if($buffer === NULL) {
         $buffer = $this->GetBuffer();
      }
      $len = strlen($buffer);
      
      if(!preg_match('#^</?([^\s]++)#is', $block_start, $data)) {
         return array();
      }
      $tag = $data[1];
      $tag_len = strlen($tag);
      
      $lastOffset = 0;
      $blocks = array();
      
      do {
         if(($pos = strpos($buffer, $block_start, $lastOffset)) === false) {
            break;
         }
         $pos += strlen($block_start);
         
         $block_buff = '';
         
         $tagCounter = 1;
         while($pos < $len && $tagCounter > 0) {
            $block_buff .= $buffer[$pos++];
            
            if($buffer[$pos] != '<') {
               continue;
            }
            
            if($buffer[$pos + 1] == '/') {
               $isTag = true;
               
               for($i = 0; $i < $tag_len; ++$i) {
                  if($buffer[$pos + 2 + $i] != $tag[$i]) {
                     $isTag = false;
                  }
               }
               if(!$isTag) { continue; }
               
               if(preg_match('#[a-zA-Z]#', $buffer[$pos + 2 + $tag_len])) {
                  continue;
               }
               
               --$tagCounter;
               continue;
            }
            
            $isTag = true;
            for($i = 0; $i < $tag_len; ++$i) {
               if($buffer[$pos + 1 + $i] != $tag[$i]) {
                  $isTag = false;
               }
            }
            if(!$isTag) { continue; }
            
            if(preg_match('#[a-zA-Z]#', $buffer[$pos + 1 + $tag_len])) {
               continue;
            }
            
            ++$tagCounter;
            continue;
         }
         
         $lastOffset = $pos;
         $blocks[] = trim($block_buff);
         unset($block_buff);
      } while(true);
   
      return $blocks;
   }
   
   
   /**
    * Переводим название в транслит, с учетом уже сохраненных названий
    */
   public function Translite($text, $site = '') {
      $newText = '';
      
      $matrix_from = array(
         'А','Б','В','Г','Д','Е','Ё','Ж','З','И','Й','К','Л','М','Н','О','П','Р','С','Т','У','Ф','Х','Ц','Ч','Ш','Щ','Ъ','Ы','Ь','Э','Ю','Я',
         'A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
         'а','б','в','г','д','е','ё','ж','з','и','й','к','л','м','н','о','п','р','с','т','у','ф','х','ц','ч','ш','щ','ъ','ы','ь','э','ю','я',
         'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
         '1','2','3','4','5','6','7','8','9','0',
         ' ','-','_'
      );
   
      $matrix_to = array(
         'a','b','v','g','d','e','e','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sh','','i','','je','ju','ja',
         'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
         'a','b','v','g','d','e','e','j','z','i','i','k','l','m','n','o','p','r','s','t','u','f','h','c','ch','sh','sh','','i','','je','ju','ja',
         'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z',
         '1','2','3','4','5','6','7','8','9','0',
         '-','-','-'
      );
      
      $mat_len = count($matrix_from);
      
      for($i = 0, $end = iconv_strlen($text); $i < $end; ++$i) {
         $replace = false;
         $c = iconv_substr($text, $i, 1);
         
         for($j = 0; $j < $mat_len; ++$j) {
            if($c == $matrix_from[$j]) {
               $newText .= $matrix_to[$j];
               $replace = true;
               break;
            }
         }
         
         if($replace) {
            continue;
         }
         
         $newText .= '-';
      }
      
      $newText = preg_replace('#^[-]++#', '', $newText);
      $newText = preg_replace('#[-]++$#', '', $newText);
      $newText = preg_replace('#[-]++#', '-', $newText);
      
      // А теперь делаем уникальный
      if($site != '') {
         if($this->Check($site . $newText)) {
            $index = 2;
            
            while($this->Check($site . $newText . '-' . $index)) {
               ++$index;
               
               if($index > 20000) {
                  exit('Woops. Translit failed');
               }
            }
            
            $newText = $newText . '-' . $index;
         }
         
         $this->Save($site . $newText);
      }
      
      return $newText;
   }
   
   
   /**
    * Создаем полную ссылку на файл.
    */
   protected function createFullLink($site, $addr) {
      // Проверяем корректность переданного сайта
      if(!preg_match('#^((?:[a-z0-9]{1,10}://)?(?:[0-9a-z_-]++\.)++[0-9a-z]{2,5})(/(?:[^/]+/)*+)?[^/]*+$#i', $site, $data)) {
         return 'BAD_SITE';
      }
      if(!isset($data[2])) {
         $data[2] = '/';
      }
      $addr = str_replace(array('&amp;', '&amp'), '&', $addr);
      
      // Является законченной ссылкой?
      if(preg_match('#^[0-9a-z]{1,10}://#is', $addr)) {
         return $this->cleanLink($addr);
      }
      
      // Ссылается на туже дирректорию?
      if(strpos($addr, '/') !== 0) {
         return $this->cleanLink($data[1] . $data[2] . $addr);
      }
      
      // Ссылается на корень
      return $this->cleanLink($data[1] . $addr);
   }
   
   
   /**
    * Приводим ссылку к корректному виду
    */
   protected function cleanLink($link) {
      if(strpos($link, '../') === false) {
         return $link;
      }
   
      // Вырезаем
      if(!preg_match('#^((?:[0-9a-z]{1,10}://)?(?:[0-9a-z_-]++\.)++[0-9a-z]{2,5})(/(?:[^/]+/)*+[^/]*+)?$#i', $link, $data)) {
         return 'BAD_SITE';
      }
      if(!isset($data[2])) {
         $data[2] = '/';
      }
      
      $addr  = $data[1];
      $link = explode('/', $data[2]);
      $new  = array();
      $c = 0;
      
      for($i = 0, $end = count($link); $i < $end; ++$i) {
         if($link[$i] == '..') {
            if($c > 0) { --$c; }
            continue;
         }
      
         $new[$c++] = $link[$i];
      }
      $link = '';
      for($i = 0; $i < $c; ++$i) {
         if($link == '' && $new[$i] == '') {
            continue;
         }
         $link .= '/' . $new[$i];
      }
      
      if($link == '' || $link[0] != '/') {
         $link = '/' . $link;
      }
      
      return $addr . $link;
   }
   
   
   /**
    * Возвращяем ext файла
    */
   protected function GetFileExt($fileName) {
      if(!preg_match('#\.([a-z0-9]{2,4})$#i', $fileName, $data)) {
         return false;
      }
      
      return strtolower($data[1]);
   }
   
   
   /**
    * Создаем случайное название для файла
    */
   protected function CreateRandName($dir, $type) {
      if($dir[strlen($dir) - 1] != '/') {
         $dir .= '/';
      }
      
      if(!is_dir($dir)) {
         mkdir($dir) OR die('Can`t create dir ' . $dir);
      }
      
      $c = 0;
      $fileName = substr(md5($type . (time() - rand(1, 4000000) + (rand(100, 1000) * rand(200, 2000)) + ++$c) . $type), 0, 20) . '.' . $type;
      while(file_exists($dir . $fileName)) {
         $fileName = substr(md5($type . (time() - rand(1, 4000000) + (rand(100, 1000) * rand(200, 2000)) + ++$c) . $type), 0, 20) . '.' . $type;
         
         if($c > 100000) {
            die('Woops. Can`t create file name');
         }
      }
      
      return $fileName;
   }
   
   
   /**
    * Сохраняем все изображения в тексте
    */
   protected function SaveAllImages($text, $saveTo, $sitePath, $siteLink) {
      $maxImageWidth = 700;
      $maxParamWidth = 500;
      
      $text = preg_replace('#<img\s+([^>]*)src=([^\'">\s]+)([^>]*)>#is', '<img src="$2">', $text);
      $text = preg_replace('#<img\s+[^>]*src\s*=\s*(\'|")([^\'">]+)\\1[^>]*>#is', '<img src="$2">', $text);
      
      $tmpDir = 'tmp/';
      if(!is_dir($tmpDir)) {
         mkdir($tmpDir) OR die('FUUUUUUUUUUUUUUUCCCCCCCCCCCCCCKKKKKKKKKKKKKKK');
      }
      
      preg_match_all('#<img src="([^\'">]+)">#is', $text, $data, PREG_SET_ORDER);
      foreach($data as $pic) {
         $link = $this->CreateFullLink($sitePath, $pic[1]);
         
         $fileName = substr(md5($link), 0, 6).'.jpg';
         if(file_exists($saveTo.$fileName)) {
            $text = str_replace($pic[1], $siteLink . $fileName, $text);
            continue;
         }
         
         // Сохраняем картинку во временный файл
         if(!$this->LoadUrl($link, $tmpDir . 'image.tmp')) {
            $text = str_replace($pic[0], '', $text);
            continue;
         }
      
         if(!file_exists($tmpDir . 'image.tmp')) {
            $text = str_replace($pic[0], '', $text);
            continue;
         }
      
         if(filesize($tmpDir . 'image.tmp') < 10) {
            // Картинка меньше 10 байт? О_О
            $text = str_replace($pic[0], '', $text);
            continue;
         }
      
         // Пытаемся определить тип изображения
         $type = '';
         $res = @getimagesize($tmpDir . 'image.tmp');
      
         if(!$res) {
            continue;
         }
         
         $oldImage = false;
         
         switch($res[2]) {
            case 1: {
               $oldImage = @imagecreatefromgif($tmpDir . 'image.tmp');
               $type = 'gif';
               break;
            }
            case 2: {
               $oldImage = @imagecreatefromjpeg($tmpDir . 'image.tmp');
               $type = 'jpg';
               break;
            }
            case 3: {
               $oldImage = @imagecreatefrompng($tmpDir . 'image.tmp');
               $type = 'png';
               break;
            }
         }
         
         if($oldImage == false) {
            $text = str_replace($pic[0], '', $text);
            continue;
         }
         
         if($type == '') {
            $text = str_replace($pic[0], '', $text);
            continue;
         }
         
         $oldWidth  = $res[0];
         $oldHeight = $res[1];
         
         $newWidth  = $oldWidth;
         $newHeight = $oldHeight;
         
         if($oldWidth > $maxImageWidth) {
            $newWidth = $maxImageWidth;
            $newHeight = round(($oldHeight / ($oldWidth / $newWidth)), 0);
         }
         
         $type = '.' . $type;
         
         $newImage = @imagecreatetruecolor($newWidth, $newHeight);
         @imagecopyresampled($newImage, $oldImage, 0, 0, 0, 0, $newWidth, $newHeight, $oldWidth, $oldHeight);
         
         @imagejpeg($newImage, $saveTo . $fileName);
         
         if(!file_exists($saveTo.$fileName)) {
            $text = str_replace($pic[0], '', $text);
            continue;
         }
         
         // Если надо - меняем 
         if($newWidth > $maxParamWidth) {
            $paramWidth  = $maxParamWidth;
            $paramHeight =  round(($newHeight / ($newWidth / $paramWidth)), 0);
            
            $text = str_replace($pic[0], '<img src="' . $pic[1] . '" width="'.$paramWidth.'" height="'.$paramHeight.'">', $text);
         }
         
         // Делаем замену в тексте
         $text = str_replace($pic[1], $siteLink . $fileName, $text);
      }
      
      return $text;
   }
   
   
   /**
    * Устанавливаем стандартные заголовки
    *
    * @browserName Имя браузера, под который маскироваться будем. По умолчанию - Opera
    */
   public function SetDefaultHeaders($browserName = 'Opera') {
      $browsers = array(
         'chrome'  => 'Mozilla/5.0 (Windows; U; Windows NT 6.1; en-US) AppleWebKit/525.19 (KHTML, like Gecko) Chrome/1.0.154.65 Safari/525.19',
         'firefox' => 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.1) Gecko/20090624 Firefox/3.5',
         'ie6'     => 'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1)',
         'ie7'     => 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; YPC 3.0.1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
         'ie8'     => 'Mozilla/4.0 (compatible; MSIE 8.0; Windows NT 6.1; Trident/4.0)',
         'opera'   => 'Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.2.15 Version/10.00',
      );
      
      if(in_array(strtolower($browserName), $browsers)) {
         $browser = $browsers[$browserName];
      } else {
         $browser = $browsers['opera'];
      }
      
      $this->headers = array(
         'Accept'          => 'text/html, application/xml;q=0.9, application/xhtml+xml, image/png, image/jpeg, image/gif, image/x-xbitmap, */*;q=0.1',
         'Accept-Language' => 'ru',
         'Accept-Charset'  => 'windows-1251, utf-8;q=0.6, *;q=0.1',
         'Accept-Encoding' => 'gzip',
         'Expires'         => 'no-store, no-cache, must-revalidate',
         'Cache-Control'   => 'no-cache',
         'User-Agent'      => $browser,
         'Keep-Alive'      => '300',
         'Connection'      => 'close',
      );
   }
   
   
   /**
    * Сохраняем запись в лог
    */
   protected function Log($message) {
      $path = str_replace('\\', '/', dirname(__FILE__)) . '/';
      $path .= '_' . preg_replace('@\.[a-z]{1,4}$@is', '', basename(__FILE__)) . '.log';
      
      $file = fopen($path, 'ab') OR die("Can`t open $path");
      fputs($file, '[' . date('d.m.y H:i:s') . "][{$this->currentLink}]" . $message . "\n");
      fclose($file);
   }
   
   
   /**
    * Проверяем - сохранен ли данный ключ
    */
   protected function check($key) {
      if($this->keysLoaded == false) {
         $this->LoadKeyList();
      }
      
      return isset($this->keyArray[$key]);
   }
   
   
   /**
    * Добавляем данный ключ к списку сохраненных
    */
   protected function save($key) {
      if($this->keysLoaded == false) {
         $this->LoadKeyList();
      }
      
      if($this->check($key)) {
         return;
      }
      
      $this->keyArray[$key] = '';
      
      // $this->keysAdded = true;
      
      // Добавляем ключ в файл
      $fileName = $this->GetKeyFileName();
      $file = fopen($fileName, 'ab') OR die("Cat`n open file $fileName");
      
      fputs($file, $key . "\n");
      
      fclose($file);
   }
   
   
   /**
    * Загружаем список ключей
    */
   protected function loadKeyList() {
      $fileName = $this->GetKeyFileName();
      
      // Если файл с ключами не существует - пытаемся создать
      if(!file_exists($fileName)) {
         $file = fopen($fileName, 'wb') OR die("Can`t create $fileName");
         fclose($file);
      }
      
      $file = fopen($fileName, 'rb') OR die("Can`t open $fileName");
      
      while($line = fgets($file, 1000)) {
         $line = trim($line);
         
         // Пустые строки пропускаем
         if(strlen($line) < 1) {
            continue;
         }
         
         $this->keyArray[$line] = '';
      }
      
      fclose($file);
      $this->keysLoaded = true;
   }
   
   
   /**
    * Сохраняем список ключей
    */
   protected function SaveKeyList() {
      // Если ключи не добавлялись - то и сохранять не надо
      if($this->keysAdded == false) {
         return;
      }
      
      // Сбрасываем все ключи в файл
      $fileName = $this->GetKeyFileName();
      $file = fopen($fileName, 'wb') OR die("Cat`n open file $fileName");
      
      for($i = 0, $end = count($this->keyArray); $i < $end; ++$i) {
         fputs($file, $this->keyArray[$i] . "\n");
      }
      
      fclose($file);
   }
   
   
   /**
    * Возвращяем имя файла с ключами для данного скрипта
    */
   protected function GetKeyFileName() {
      $path = str_replace('\\', '/', dirname(__FILE__)) . '/';
      $path .= '_' . preg_replace('@\.[a-z]{1,4}$@is', '', basename(__FILE__)) . '.key';
      
      return $path;
   }
   
   
   /**
    * Номер месяца по его названию
    */
   protected function MounthToInt($m) {
      switch(strtolower($m)) {
         case 'jan': { return 1; }
         case 'feb': { return 2; }
         case 'mar': { return 3; }
         case 'apr': { return 4; }
         case 'may': { return 5; }
         case 'jun': { return 6; }
         case 'jul': { return 7; }
         case 'aug': { return 8; }
         case 'sep': { return 9; }
         case 'oct': { return 10; }
         case 'nov': { return 11; }
         case 'dec': { return 12; }
      }
      
      return 12;
   }
   
   
   /**
    * Добавляем файл к списку загружаемых.
    
    * @fileName имя файла
    * @filePath путь к файлу
    */
   public function AddFile($fileName, $filePath) {
      $this->files[$fileName] = $filePath;
   }
   
   
   /**
    * Отчищяем файлы, пост - данные, куки.
    */
   public function CleadAllData() {
      $this->files = array();
      $this->post = array();
      $this->cookies = array();
   }
   
   
   /**
    * Устанавливаем куку
    *
    * @name Имя куки
    * @val Значение куки
    */
   public function SetCookie($name, $val = NULL) {
      if($val == NULL && preg_match('#^(\S+?)=(\S+)$#is', $name, $pData)) {
         list(, $name, $val) = $pData;
      }
      
      $this->cookies[$name] = $val;
   }
   
   
   /*
    * Устанавливаем POST - переменную
    *
    * @name Имя переменной
    * @val Значение переменной
    */
   public function SetPost($name, $val = NULL) {
      if($val == NULL && preg_match('#^([^\s=]++)=(\S*+)$#is', $name, $pData)) {
         list(, $name, $val) = $pData;
      }
      $this->post[$name] = $val;
   }
   
   
   /**
    * Возвращяем куку по её имени. Если имя не передано - возвращяем все куки
    *
    * @host Имя хоста, для которого ищем кукисы
    * @name Имя куки.
    */
   public function GetCookie($host, $name = NULL) {
      
      if($this->config['cookie_dir'] != false && !file_exists($this->config['cookie_dir'] . $host . '.cookie')) {
         return false;
      }
      
      $file = fopen($this->config['cookie_dir'] . $host . '.cookie', 'rb');
      $cookies = fread($file, 10000);
      fclose($file);
         
      $cookies = unserialize($cookies);
      
      if($name == NULL) {
         return $cookies;
      }
      
      if(!isset($cookies[$name])) {
         return false;
      }
      
      return $cookies[$name];
   }
   
   
   /**
    * Добавляем заголовок
    */
   public function SetHeader($name, $val = NULL) {
      if($val == NULL && preg_match('#^(\S+?): (\S+)$#is', $name, $pData)) {
         list(, $name, $val) = $pData;
      }
      
      $this->headers[$name] = $val;
   }
   
   
   /**
    * Пытаемся загрузить страницу и возвращяем её содержимое
    */
   public function GetUrl($url) {
      if(!$this->LoadUrl($url)) {
         return false;
      }
      
      return $this->pageBuffer;
   }
   
   
   /**
    * Возвращяем загруженную ранее страницу
    */
   public function GetBuffer() {
      return $this->pageBuffer;
   }
   
   
   /**
    * Возвращяем ответ сервера
    */
   public function GetServerResponse($name = NULL) {
      if($name == NULL) {
         return $this->serverResponse;
      }
      
      $name = strtolower($name);
      return isset($this->serverResponse[$name]) ? $this->serverResponse[$name] : false;
   }
   
   
   /**
    * Возвращяем заголовки запроса серверу
    */
   public function GetRequest() {
      return $this->request;
   }
   
   
   /**
    * Загружаем список куков для текущего сайта
    */
   protected function LoadCookies($siteName) {
      if($this->config['cookieIsEnabled'] == false) {
         return;
      }
      
      if(!is_dir($this->config['cookie_dir'])) {
         @mkdir($this->config['cookie_dir']);
         
         if(!is_dir($this->config['cookie_dir'])) {
            die('Can`t create dir ' . $this->config['cookie_dir']);
         }
      }
      
      $fileName = $this->config['cookie_dir'] . $siteName . '.cookie';
      if(!file_exists($fileName)) {
         return;
      }
      
      $file = fopen($fileName, 'rb') OR die("Can`t open cookie file $fileName");
      $cookies = fread($file, 10000);
      fclose($file);
         
      $cookies = unserialize($cookies);
      
      foreach($cookies as $key => $val) {
         if(!isset($this->cookies[$key])) {
            $this->cookies[$key] = $val;
         }
      }
   }
   
   
   /**
    * Сохраняем кукисы
    */
   protected function SaveCookie($siteName) {
      if($this->config['cookieIsEnabled'] == false) {
         return;
      }
      
      if(!is_dir($this->config['cookie_dir'])) {
         @mkdir($this->config['cookie_dir']);
         
         if(!is_dir($this->config['cookie_dir'])) {
            die('Can`t create dir ' . $this->config['cookie_dir']);
         }
      }
      
      $fileName = $this->config['cookie_dir'] . $siteName . '.cookie';
      
      $file = fopen($fileName, 'w') OR die("Can`t create cookie file $fileName");
      
      fputs($file, serialize($this->cookies));
      fclose($file);
   }
   
   /**
    * Загружаем страницу
    */
   public function LoadUrl($url, $save_to = NULL) {
      if($this->config['cacheLinks'] == true && $save_to == NULL) {
         $hash = md5($url);
         
         $dir = substr($hash, 0, 2);
         $name = substr($hash, 2);
         
         is_dir('_cache/') OR mkdir('_cache/');
         is_dir('_cache/'.$dir) OR mkdir('_cache/'.$dir);
         
         $hash = $dir.'/'.$name;
         
         if(file_exists('_cache/'.$hash)) {
            $this->pageBuffer = file_get_contents('_cache/'.$hash);
            return true;
         }
      }
      
      $reloads = $this->config['reloadingMax'];
      if($this->config['reloadingEnabled'] == false) {
         $reloads = 1;
      }
      
      $locations = $this->config['locationMax'];
      if($this->config['locationEnabled'] == false) {
         $locations = 0;
      }
      
      $result = false;
      while($reloads) {
         $result = $this->Load($url, $save_to);
         
         // Если загрузка не удачна
         if($result == false) {
            --$reloads;
            $this->log('Не могу загрузить '.$url);
            continue;
         }
         
         // Если удачно - смотрим, можем ли еще перейти по location`у
         if(!$locations) {
            // ... не можем. Выходи из цикла
            break;
         }
         
         // Смотрим - есть ли location
         $location = $this->getServerResponse('location');
         if($location == false) {
            // ... не найден. Выходим
            break;
         }
         
         // Найден. Переходим
         --$locations;
         $url = $location;
      }
      
      if($result && $this->config['cacheLinks'] == true && $save_to == NULL) {
         is_dir('_cache') OR mkdir('_cache');
         $hash = md5($url);
         
         $dir = substr($hash, 0, 2);
         $name = substr($hash, 2);
         
         $hash = $dir.'/'.$name;
         
         file_put_contents('_cache/'.$hash, $this->getBuffer());
      }
      
      $this->cleadAllData();
      return $result;
   }
   
   
   /**
    * Грузим страницу по переданной ссылке
    */
   protected function Load($url, $save_to) {
      // Проверяем корректность переданного адреса
      if(!preg_match('#^(?:(https?)://)?((?:www\.)?(?:[0-9a-z_-]++\.)++[a-z0-9_-]{1,4})(:\d++)?/?(.++)?$#', $url, $pData)) {
         return 0;
      }
      
      $this->currentLink = $url;
      
      // Разбираем адрес
      $protocol = $pData[1] == '' ? 'http' : $pData[1];
      $host = $pData[2];
      $uri = isset($pData[4]) ? $pData[4] : '';
      
      $ports = array('http' => 80, 'https' => 8080);
      $port = $ports[$protocol];
      
      if(isset($pData[3]) && $pData[3] !='') {
         $port = str_replace(':', '', $pData[3]);
      }
      
      // Кодировка страницы
      $charset = $this->config['default_charset'];
      
      // Необходимо ли перекодировать страницу?
      $iconvIsNeed = strtolower($charset) != strtolower($this->config['default_charset']);
      
      // Отчищаем заголовки ответа сервера
      $this->serverResponse = array();
      
      // Грузим кукисы
      $this->LoadCookies($host);
      
      // Сбрасываем буффер страницы
      $this->pageBuffer = '';
      
      $uri = '/'.$uri;
      
      if($this->config['useProxy'] && count($this->proxyList)) {
         ++$this->currentProxy;
      
         if($this->currentProxy >= count($this->proxyList)) {
            $this->currentProxy = 0;
         }
         
         if($this->proxyList[$this->currentProxy]['type'] == 'sock4' || $this->proxyList[$this->currentProxy]['type'] == 'sock5') {
            $fs = $this->sock_connect(
               $this->proxyList[$this->currentProxy]['addr'],
               $this->proxyList[$this->currentProxy]['port'],
               $host,
               $port
            );
         } else if($this->proxyList[$this->currentProxy]['type'] == 'proxy') {
            $fs = @fsockopen(
               $this->proxyList[$this->currentProxy]['addr'],
               $this->proxyList[$this->currentProxy]['port'],
               $errno,
               $errstr,
               5
            );
            $uri = 'http://'.$host.$uri;
         }
      } else {
         $fs = fsockopen($host, $port, $errno, $errstr, 5);
      }
      
      if(!$fs) { return false; }
      
      // Начинаем формировать запрос серверу
      $request  = ((count($this->post) || count($this->files)) ? 'POST' : 'GET') . " $uri HTTP/1.1\r\n";
      $request .= "Host: $host\r\n";
      
      // Добавляем стандартные заголовки
      foreach($this->headers as $key => $val) {
         $request .= $key . ': ' . $val . "\r\n";
      }
      
      // Добавляем кукисы
      if(count($this->cookies)) {
         $request .= 'Cookie: ';
         foreach($this->cookies as $key => $val) {
            $request .= $key  . '=' . $val . '; ';
         }
         $request .= "\r\n";
      }
      
      // Если есть файлы
      if(count($this->files)) {
         // ...то отправляем как multipart
         $this->request = $request;
         
         if(!fwrite ($fs, $request)) {
            return false;
         }
         
         $un = '---------------------------'.strtoupper(substr(uniqid(time()), 0, 11));
         $dop = '';
         
         // Добавляем post
         if(count($this->post)) {
            $first = true;
            foreach($this->post as $name => $val) {
               if($first) {
                  $dop .= "--{$un}\r\n";
                  $first = false;
               }
               $dop .= 'Content-Disposition: form-data; name="' . $name . "\"\r\n\r\n";
               $dop .= $val;
               $dop .= "\r\n--{$un}\r\n";
            }
         }
         
         // Добавляем файлы
         foreach($this->files as $name => $file) {
            if(!file_exists($file)) {
               continue;
            }
            
            // $dop .= "--{$un}\r\n";
            
            $dop .= 'Content-Disposition: form-data; name="'.$name.'"; filename="' . basename($file) . "\"\r\n";
            $dop .= "Content-Type: text/plain\r\n";
            $dop .= "\r\n"; 
            $dop .= fread(fopen($file, 'rb'), filesize($file))."\r\n";
            $dop .= "--{$un}--\r\n";
         }
         
         fwrite ($fs, "Content-Type: multipart/form-data; boundary=".$un."\r\n");
         fwrite ($fs, "Content-Length: ".strlen($dop)."\r\n");
         fwrite ($fs, "\r\n");
         fwrite ($fs, $dop);
         fwrite ($fs, "\r\n");
         
      } else {
         // Добавляем post
         if(count($this->post)) {
            $post_data = '';
            foreach($this->post as $name => $val) {
               if(isset($post_data[1])) { $post_data .= '&'; }
               $post_data .= $name . '=' . $val;
            }
            $request .= "Content-Type: application/x-www-form-urlencoded\r\n";
            $request .= 'Content-Length: ' . strlen($post_data) . "\r\n\r\n";
            $request .= $post_data . "\r\n";
         }
         
         $request .= "\r\n";
         
         $this->request = $request;
         
         if(!fwrite ($fs, $request)) {
            continue;
         }
      }
      
      // Получаем страницу
      $server_response = 1;
      $headers = 1;
      $headersIsEnded = false;
      
      stream_set_timeout($fs, $this->config['read_timeout']);
      $info = stream_get_meta_data($fs);
      
      $timeStart = time();
      $page_size = 0;
      
      while(!feof($fs) && !$info['timed_out']) {
         $line = fgets($fs, 1024);
         
         if((time() - $timeStart) > $this->config['read_timeout']) {
            $info['timed_out'] = true;
            break;
         }
         
         $info = stream_get_meta_data($fs);
         
         // Проверяем - не закончили ли читать ответ?
         if($line == "\n" || $line == "\r\n") { 
            break;
         }
         
         if(!preg_match('#^(\S+?): (.+)$#', trim($line), $pData)) {
            continue;
         }
         
         // Надо ставить куки
         if($pData[1] == 'Set-Cookie' && preg_match('#^(\S+?)=(\S+); (expires=(.+?);)?#', $pData[2], $cookie)) {
            $set = 1;
            
            // Проверяем - устаревшая ли кука
            if(isset($cookie[4]) && preg_match('#([0-9]{2})-([a-z]{3})-([0-9]{2,4}) ([0-9]{2}):([0-9]{2}):([0-9]{2}) (GMT)?#is', $cookie[4], $date)) {
               $time = mktime($date[4], $date[5], $date[6], $this->MounthToInt($date[2]), $date[1], $date[3]);
               if(isset($date[7]) && $date[7] == 'GMT') {
                  $time += 3600 * $this->config['GMT+'];
               }
               if($time < time()) {
                  // Кука устарела
                  $set = 0;
               }
            }
            
            if($set) {
               $this->cookies[$cookie[1]] = $cookie[2];
            } else {
               if(isset($this->cookies[$cookie[1]])) {
                  unset($this->cookies[$cookie[1]]);
               }
            }
         }
         
         $this->serverResponse[strtolower($pData[1])] = $pData[2];
      }
      
      $page_size = 0;
      $this->pageBuffer = '';
      
      while(!feof($fs) && !$info['timed_out']) {
         $line = fread($fs, 256);
         
         if((time() - $timeStart) > $this->config['read_timeout']) {
            $info['timed_out'] = true;
            break;
         }
         
         $page_size += strlen($line);
         $info = stream_get_meta_data($fs);
         
         if($page_size > $this->config['max_page_size']) {
            fclose($fs);
            return false;
         }
         
         $this->pageBuffer .= $line;
      }
      
      if($info['timed_out']) {
         return false;
      }
      
      if(isset($this->serverResponse['transfer-encoding'])) {
         if(strtolower($this->serverResponse['transfer-encoding']) != 'chunked') {
            is_dir('_te') OR mkdir('_te');
         
            $file = fopen( '_te/'.substr( md5($url.$this->currentProxy), 0, 10 ).'', 'wb' ) OR die('fuck');
            fputs($file, $url."\n");
            fputs($file, $this->request."\n");
            fputs($file, print_r($this->serverResponse, true)."\n");
            fputs($file, $this->pageBuffer."\n");
            fclose($file);
            
            return false;
         }
         
         $buffer = '';
         $read = 0;
         $len = strlen($this->pageBuffer);
         
         // Читаем строку, отлавливая "пакеты"
         while ($read <= $len) {
            // Размер следующего пакета
            $packSize = ''; 
            
            if($this->pageBuffer[$read] == "\r") { ++$read; }
            if($this->pageBuffer[$read] == "\n") { ++$read; }
            
            // Получаем размер пакета
            while ($this->pageBuffer[$read] != "\n" && $read <= $len) {
               $packSize .= $this->pageBuffer[$read];
               ++$read;
            }
            ++$read;            
            
            // Размер пакета 16 -> 10
            $packSize = base_convert(trim($packSize), 16, 10);
            if(!intval($packSize)) { 
               break; 
            }
            
            $buffer .= substr($this->pageBuffer, $read, $packSize);
            $read = $read + $packSize;
         }
         
         $this->pageBuffer = $buffer;
      }
      
      if(!count($this->serverResponse)) {
         fclose($fs);
         return false;
      }
      
      if(isset($this->serverResponse['content-encoding'])) {
         $this->pageBuffer = gzinflate(substr($this->pageBuffer, 10));
         
         if($this->serverResponse['content-encoding'] != 'gzip' || false === $this->pageBuffer) {
            is_dir('_errors') OR mkdir('_errors');
         
            $file = fopen( '_errors/'.substr( md5($url.$this->currentProxy), 0, 10 ).'', 'wb' ) OR die('fuck');
            fputs($file, $url."\n");
            fputs($file, $this->request."\n");
            fputs($file, print_r($this->serverResponse, true)."\n");
            fputs($file, $this->pageBuffer."\n");
            fclose($file);
            
            return false;
         }
      }
      
      if($save_to !== NULL) { 
         if(!($file = fopen($save_to, 'w'))) {
            $this->pageBuffer = '';
            echo "Can`t open {$save_to}\n";
            return false;
         }
         
         fputs($file, $this->pageBuffer);
         fclose($file); 
         
         $this->pageBuffer = '';
      }
      
      if($page_size < (1024 * 1024) && $save_to === NULL) {
         
         // Проверяем - надо ли изменять кодировку
         do {
            if(!preg_match('#<meta\s++[^>]*charset=[\'"]?([^"\']++)#is', $this->pageBuffer, $data)) {
               break;
            }
            
            if(strtolower($data[1]) == strtolower($charset)) {
               break;
            }
            
            $charset = $data[1];
            
            $this->pageBuffer = @iconv($charset, $this->config['default_charset'] . '//TRANSLIT', $this->pageBuffer);
         } while(false);
      }
      
      // Сохраняем кукисы
      $this->SaveCookie($host);
      return true;
   }
   
   
   protected function sock_connect($host, $port, $dh, $dp) {
      $result=true;
      $f = @fsockopen($host, $port, $errno, $errstr, 5) or $result=false;
      
      if(!$result)  {
         return false;
      }
      
      $h = gethostbyname($dh);
      preg_match("#(\d+)\.(\d+)\.(\d+)\.(\d+)#", $h, $m);
      fwrite($f, "\x05\x01\x00");
      $r = fread($f, 2);
      
      if($r == '') {
         return false;
      }
      
      if(!(ord($r[0])==5 and ord($r[1])==0)) $result=false;
      
      if(!$result) {
         return false;
      }
      
      fwrite($f, "\x05\x01\x00\x01" . chr($m[1]).chr($m[2]).chr($m[3]).chr($m[4]).chr($dp/256).chr($dp%256));
      $r = fread($f, 10);
      
      if($r == '') {
         return false;
      }
      
      if(!(ord($r[0])==5 and ord($r[1])==0)) {
         return false;
      }
      
      return $f;
   }
};


class My_MysqlDB {

   private 
      $charset, // Кодировка
      $querys, // Количество запросов
      $time, // Время выполнения запросов
      $errors, // Количество ошибок
      $res, // res-id
      $connect, // Флаг успешного подключения к БД
      $extendedLog, // Вести детализированный лог?
      $extendedLines = array(), // Массив, в котором будем хранить детализированный лог
      $logFile, // Файл, в который будет идти запись лога ошибок
      $errmsg // Текст ошибки
   ;
   
   public function __construct($user, $pass, $base, $host = 'localhost', $charset = 'utf8', $logFile = 'mysql.error') {
      $this->charset = $charset;
      $this->time = $this->errors = $this->querys = $this->res = 0;
      $this->logFile = $logFile;
      
      $this->extendedLog = 0;
      
      $this->connect = 0;
      
      $this->charset = $charset;
      $this->time = $this->errors = $this->querys = $this->res = 0;
      $this->logFile = $logFile;
      
      $this->extendedLog = 0;
      
      $this->connect = 0;
      
      // Пытаемся подключиться к БД
      if(!($this->c = @mysql_connect($host, $user, $pass))) {
         $this->errmsg = mysql_error(); 
         throw new Exception('DB error: Can`t connect to mysql server');
      }
   
      
      // Выбираем базу
      if(!@mysql_select_db($base)) {
         $this->errmsg = mysql_error(); 
         throw new Exception('DB error: Can`t select DB');
      }
      
      
      // Устанавливаем кодировку соединения
      if(!@mysql_query('SET NAMES '.$this->charset)) {
         $this->errmsg = mysql_error(); 
         throw new Exception('DB error: Can`t set charset');
      }

      $this->connect = 1;
   }
   
   
   public function __destruct() {
      return $this->connect ? mysql_close() : true;
   }
   
   
   public function __toString() {
      return 'MysqlDB class v0.1; querys: ' . $this->querys . ', errors: ' . $this->errors . ', time: ' . round($this->time, 5);
   }
   
   
   // Экранируем строку
   public function escape($string) {
      return $this->connect ? @mysql_real_escape_string($string) : @mysql_escape_string($string);
   }
   
   
   // Возвращяем детализированный лог выполнения запросов
   public function getExtendedLog() { return $this->extendedLines; }
   
   
   // Текущее время в милисекундах
   // Необходима для подсчета времени выполнения запроса
   private function timeMeasure() {
      list($msec, $sec) = explode(chr(32), microtime());
      return ($sec + $msec);
   }
   
   
   // mysql_error();
   public function error() { return $this->errmsg; }
   
   
   // Возвращяет общее время выполнения запросов
   public function getTime() { return round($this->time, 5); }
   
   
   // Возвращяем количество совершенных запросов
   public function getQuerys() { return $this->querys; }
   
   
   //Выполняем запрос
   public function query() {
      if(!$this->connect) { return false; }
      if(!is_resource($this->c)) { return false; }
      
      // 1. Получаем все аргументы.
      $args = func_get_args();
      
      // Количество аргументов
      $argsCount = count($args);
      
      // Аргументов нет
      if($argsCount == 0) {
         return false;
      }
      
      // Передано более одного аргумента - обрабатываем шаблон + переменные
      else if($argsCount > 1) {
         
         // 1.1 Получаем количество маркеров
         $markers = 0;
         for($i = strlen($args[0]) - 1; $i >= 0; $i--) {
            if($args[0][$i] == '?') {
               $markers++;
            }
         }
         
         // 1.2 Делаем замену в шаблоне
         $args[0] = str_replace(array('%', '?'), array('%%', '%s'), $args[0]);
         
         // 1.3 Экранируем аргументы
         for($i = 1; $i < $argsCount; $i++) {
            // Аргумент - целое число, экранировать не надо
            if(is_int($args[$i])) { continue; }
            
            $args[$i] = '\'' . $this->escape($args[$i]) . '\'';
         }
         
         // 1.4 Проверяем - не превышает ли количество маркеров количества аргументов
         if($markers >= $argsCount) {
            for($i = $argsCount; $i <= $markers; $i++) {
               $args[$i] = 'UNCNOWN_MARKER_' . $i;
            }
         }
         
         // 1.5 Формируем sql запрос.
         $args[0] = call_user_func_array('sprintf', $args);
      }
      
      
      // 2. Выполняем запрос.
      $this->querys++;
      $this->errmsg = '';
      
      $start = $this->timeMeasure();
      $res = @mysql_query($args[0]);
      $this->time += $this->timeMeasure() - $start;
      
      // 3. При необходимости сохраняем информацию о запросе
      if($this->extendedLog) {
         $stack = debug_backtrace();
         $this->extendedLines[] = array(
            'query'  => $args[0],
            'time'   => round($this->timeMeasure() - $start, 6),
            'status' => $res ? 1 : 0,
            'rows'   => $res ? @mysql_num_rows($res) : 0,
            'file'   => $stack[0]['file'],
            'line'   => $stack[0]['line'],
         );
      }
      
      if($res) {
         return new My_MysqlDB_Result($res);
      }
      
      // Запрос выполнился с ошибкой.
      $this->errmsg = mysql_error();
      $this->errors++;

      $stack = debug_backtrace();
      echo "\nMysql Error!<br>";
      echo "\n<textarea style='width: 800px; height: 150px;'>";
      echo "\n-- File: {$stack[0]['file']}";
      echo "\n-- Query: ".preg_replace('#\s++#is', ' ', $args[0]);
      echo "\n-- Line: {$stack[0]['line']}";
      echo "\n-- Error: ", mysql_error();
      echo "\n</textarea>\n";
      
      // 4. Пишем в лог
      if($this->logFile == '') { return false; }
      
      $file = fopen($this->logFile, 'a+') OR die('Can`t open file '.$this->logFile);
      if(!$file) { return false; }

      $error  = "--------- error ---------\n";
      $error .= '-- Date: ' . date('d.m.y H:i:s')."\n";
      $stack = debug_backtrace();
      $error .= '-- File: ' . $stack[0]['file'] . "\n";
      $error .= '-- Line: ' . $stack[0]['line'] . "\n";
      if(isset($_SERVER['REMOTE_ADDR'])) {
         $error .= '-- Ip  : ' . (isset($_SERVER['HTTP_X_FORWARDED_FOR']) ? $_SERVER['REMOTE_ADDR'].'/'.$_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR']) . "\n";
      }
      if(isset($_SERVER['REQUEST_URI'])) {
         $error .= '-- Uri : ' . $_SERVER['REQUEST_URI'] . "\n";
      }
      $error .= "-- Query\n" . $args[0] . "\n";
      $error .= "-- Error\n" . mysql_error() . "\n\n";

      fputs($file, $error);
      fclose($file);
      
      exit();
   }

}

class My_MysqlDB_Result {

   private $res, $rows;
   
   function __construct($res) {
      $this->rows = is_resource($res) ? mysql_num_rows($res) : 0;
      $this->current_line = 0;
      $this->res = $res;
   }
   
   public function __destruct() {
      unset($this->res);
   }
   
   // mysql_data_seek();
   public function data_seek($row) {
      if($this->rows) {
         return mysql_data_seek($this->res, $row);
      }
   }
   
   // mysql_fetch_assoc()
   public function fetch_assoc() {
      return mysql_fetch_assoc($this->res);
   }
   
   // mysql_result()
   public function result($row = 0, $col = 0) {
      return mysql_result($this->res, $row, $col);
   }
   
   // mysql_num_rows()
   public function rows() {
      return $this->rows;
   }
}
?>