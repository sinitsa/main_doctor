<?php
include('../../connect.php');
include('../../func/core.php');
include("../../func/classes/resize-class.php");

switch ($_REQUEST['method']) {
	//������������� �������� ������ (������, �������, ����������)
	case 'toggle' :
		$availableAttrs = array('best','novinka','sale');
		$id = $_REQUEST['id'];
		$attr = $_REQUEST['attr'];
		$toggle = $_REQUEST['toggle'];
		if (is_numeric($id) && in_array($attr, $availableAttrs) && ($toggle == '1' || $toggle == '0')) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$attr}` = '{$toggle}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//��������� ��������� ������
	case 'setprice' :
		$id = $_REQUEST['id'];
		$price = $_REQUEST['price'];
		
		if (is_numeric($id) && is_numeric($price)) {
			setPrice($id, $price);
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//��������� ������� ������
	case 'setart' :
		$id = $_REQUEST['id'];
		$art = mysql_real_escape_string(iconv("utf-8", "windows-1251", urldecode($_REQUEST['art'])));
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`art` = '{$art}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'set_field' :
		$id = $_REQUEST['id'];
		$value =$_REQUEST['value'];
		$field = mysql_real_escape_string($_REQUEST['field']);
		
		if ($field == 'delivery_cost') {
			$value = empty($value) && $value != '0' ? 'NULL' : "'".mysql_real_escape_string(iconv("utf-8", "windows-1251", $value))."'";
		} else {
			$value = mysql_real_escape_string(iconv("utf-8", "windows-1251", $value));
			$value = "'{$value}'";
		}
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`{$field}` = {$value}
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => (mysql_affected_rows() < 0)));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//��������� ������ ������� ������
	case 'setbid' :
		$id = $_REQUEST['id'];
		$bid = mysql_real_escape_string($_REQUEST['bid']);
		
		if (is_numeric($id)) {
			mysql_query("
				UPDATE
					`catalog`
				SET
					`market_cost_per_click` = '{$bid}'
				WHERE
					`id` = '{$id}'
				LIMIT 1
			");
			echo json_encode(array('error' => false));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
		//�������� ������
	case 'deleteproduct' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			deleteProduct($id);
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//�������� ��������� �������� ��� ������
	case 'get_delivery_cost' :
		$id = $_REQUEST['id'];
		if (is_numeric($id)) {
			echo json_encode(array(
				'error' => false,
				'delivery_price' => deliveryPrice::getByCatAndPrice(0, 0, $id)
				));
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	//�������� �������� ���� � ������
	case 'uploadmainimage' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$id = $_REQUEST['id'];
			
			$a = saveMainImage($_FILES['main_image'], $id);
			
			if ($a) {
				echo json_encode(array('error' => true));
			} else {
				echo json_encode(array('error' => false));
			}
			
		} else {
			echo json_encode(array('error' => true));
		}
	break;
	case 'uploadextraimages' :
		if (isset($_REQUEST['id']) && is_numeric($_REQUEST['id'])) {
			$catalogId = $_REQUEST['id'];
						
			$idAdded = array();
		
			for ($i = 0; $i < count($_FILES['extra_photos']['name']); $i++) {
				$photo = array(
					'name' => $_FILES['extra_photos']['name'][$i],
					'type' => $_FILES['extra_photos']['type'][$i],
					'tmp_name' => $_FILES['extra_photos']['tmp_name'][$i],
					'error' => $_FILES['extra_photos']['error'][$i],
					'size' => $_FILES['extra_photos']['size'][$i]
				);
				
				$idAdded[] = saveExtraPhoto($photo, $catalogId) ;
			}
			echo json_encode(array('error' => false, 'images' => $idAdded));
		} else {
			echo json_encode(array('error' => true, 'code' => 1));
		}
	break;
	
	case 'deleteextraimages' :
		$ids = array();
		
		foreach ($_POST['extra_photo'] as $id => $v) {
			$ids[] = $id;
			deleteExtraPhoto($id);
		}
		echo json_encode(array('error' => false, 'images' => $ids));
	break;
	
	case 'changecat' :
		$ids = $_REQUEST['products'];
		$catId = $_REQUEST['cat_id'];
		
		foreach($ids as $id) {
			if (catalogChangeCat($id, $catId))
				$success[] = $id;
		}
		
		echo json_encode(array('error' => false, 'products' => $success));
	break;
	//�������� ��� �� ����������
	case 'checkchpu' : 
		$chpu = mysql_real_escape_string($_REQUEST['chpu']);
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `chpu`='{$chpu}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	//�������� �������� �� ����������
	case 'checktitle' : 
		$title = mysql_real_escape_string(iconv('utf-8', 'cp1251', $_REQUEST['title']));
		$id = is_numeric($_REQUEST['product_id']) ? $_REQUEST['product_id'] : 0;
		$count = fetchOne("SELECT COUNT(*) FROM `catalog` WHERE `title`='{$title}' AND `id` <> '{$id}'");
		
		echo json_encode(array('is_double' => $count > 0));
	break;
	
	//���������� ���������� �������
	case 'setsort' :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);

		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`catalog`
					SET
						`spec_rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	//��������� ������ � �����\�������������\����������������
	case 'linkwithtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			linkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	//����������� ������ � ����\������������\���������������
	case 'unlinkfromtag' :
		$tagId = $_REQUEST['tag_id'];
		if (!is_numeric($tagId)) {
			echo json_encode(array('error' => true, 'message' => 'No tag ID given'));
			break;
		}
		foreach ($_REQUEST['products'] as $pid) {
			unlinkCatalogAndTags($pid ,$tagId);
		}
		echo json_encode(array('error' => false, "products" => $_REQUEST['products']));
	break;
	case "sorttags" :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`tags`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//���������� ������ � ����������� ��� ������ (� �������� �������)
	case 'getparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			
			$product = getProduct($id);
			include('params_tpl.php');
			?>
			<table border="0" cellspacing="0" cellpadding="0" style="margin-top:5px;">
			  <tr>
				<td width="75" align="left" class="txtskidksm">������</td>
				<td align="left" class="txtskidksm" width="65">
					<input type="text" name="discount_percent" id="discount_percent" class="txtpricesmf" value="<?php echo $product['discount_value'];?>">%
				</td>
				<td align="left" >
					<input type="text" name="discount_value" id="discount_value" class="txtpricesmfr" value="<?php echo ($product['price']-$product['price_after_discount']);?>">
					<span class="txtrub">���.</span>
				</td>
			  </tr>
			</table>
			<input type="hidden" id="price" value="<?php echo $product['price']; ?>" />
			<?php
		}
	break;
	case 'setparams' :
		if (isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id'])) {
			$id = $_REQUEST['product_id'];
			$product = getProduct($id);
			//������ � ID ����������
			$paramsIds = array();
			
			//�������� id ���������� � ��������� � ��������
			if (count($_REQUEST['param_checkbox']) > 0 ) {
				foreach	($_REQUEST['param_checkbox'] as $key => $value) {
					$paramsIds[] = $key;
				}
			}
			if (count($_REQUEST['param_select']) > 0) {
				foreach	($_REQUEST['param_select'] as $key => $value) {
					if ($value > 0) 
						$paramsIds[] = $value;
				}
			}
             
			if (count($_REQUEST['param_field']) > 0) {
				foreach	($_REQUEST['param_field'] as $key => $value) {
					if ($value['value'] != '') { 
						$loloId2 = addAvailableParam(array(
							'paramId' => $key,
							'catId' => $value['global'] ? 0 : $value['cat'],
							'value' => $value['value'],
							'valueFloat' => null
						));
						if ($loloId2) $paramsIds[] = $loloId2;
					}
				}
			}
			
			if (count($_REQUEST['param_range']) > 0) {
				foreach	($_REQUEST['param_range'] as $key => $value) {
					if ($value['value'] != '') {
						//$value['value'] = iconv("utf-8", "windows-1251", urldecode($value['value']));
						
						$loloId = addAvailableParam(array(
							'paramId' => $key,
							'catId' => $value['global'] ? 0 : $value['cat'],
							'value' => $value['value'],
							'valueFloat' => null
						));
						if ($loloId) $paramsIds[] = $loloId;
					}
				}
			}
			
			setProductsParams($id, $paramsIds);
			
			//��������� ������.
			$data = array();
			$data['price'] = $product['price'];
			
			$dp = $_REQUEST['discount_percent'];
			$dv = $_REQUEST['discount_value'];
			if (!empty($dp) && $dp > 0) {
				$data['discount_type'] = 'percent';
				$data['discount_value'] = $dp;
			} elseif (!empty($dv) && $dv > 0 ) {
				$data['discount_type'] = 'value';
				$data['discount_value'] = $dv;
			} else {
				$data['discount_value'] = 0;
			}
			$data['price_after_discount'] = getPriceAfterDiscount($data['price'], $data['discount_type'] , $data['discount_value']);
			
			mysql_query("
				UPDATE
					`catalog`
				SET	".getSetString($data)."
				WHERE
					`id` = '{$id}'
			");
		}
	break;
	
	//���������� �������������� ���� � ������
	case 'sortextraphoto' :
		$sort = $_REQUEST['sort_list'];
		$count = count($sort);
		
		for ($i = 0; $i < $count; $i++) {

			if (is_numeric($sort[$i]['id']) && is_numeric($sort[$i]['sort'])) {
				$id = $sort[$i]['id'];
				$rang = $sort[$i]['sort'];
				mysql_query ("
					UPDATE
						`foto`
					SET
						`rang` = '{$rang}'
					WHERE
						`id` = '{$id}'
				");
			}
		}
		echo json_encode(array('error' => false));
	break;
	
	//��������� ������� �� ������ ��������� ������ � 10���
	case 'want_to_link' :
		$id = $_REQUEST['id'];
		if (!is_numeric($id)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		//�������� ������ � ������
		$product = getProduct($id);
		
		//���������� api ������
		// $response = api_10med($method = 'want_to_link', $params);
		$response = api10med::search($product['title']);

		//��������� �����
		echo iconv('utf-8', 'cp1251', $response);
	break;
	
	case 'get_linked_product':
		//ID ������ � ��������
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => "Bad ID given"));
			break;
		}
		$remoteId = fetchOne("SELECT `linked_with_10med` FROM `catalog` WHERE `id` = '{$localId}'");
		if ($remoteId) {
			//���������� ������ �� ���������
			echo iconv('utf-8', 'cp1251', api10med::product($remoteId));
		} else {
			echo json_encode(array('error' => true, 'error_message' => "Product not linked"));
			break;
		}
	break;
	
	case 'link':
		//ID ������ � ��������
		$localId = $_REQUEST['local_id'];
		//ID ������ �� 10���
		$remoteId = $_REQUEST['remote_id'];
		
		if (!is_numeric($localId) || !is_numeric($remoteId)) {
			echo json_encode(array('error' => true, 'error_code' => 0));
			break;
		}
		
		$params = array(
			'local_id' => $localId ,
			'remote_id' => $remoteId
		);
		
		//���������� ������ �� ���������
		$response = json_decode(api10med::link($localId, $remoteId), true);
		if ($response['code'] == 200) {
			mysql_query("UPDATE `catalog` SET `linked_with_10med` = '{$response['linkedWith']}' WHERE `id` = '{$localId}' LIMIT 1");
		}
		echo json_encode(array('error' => $response['error'], 'error_code' => 0));
		break;
	break;
	
	case 'unlink' :
		//ID ������ � ��������
		$localId = $_REQUEST['local_id'];
		
		if (!is_numeric($localId)) {
			echo json_encode(array('error' => true, 'error_message' => 'Bad ID given'));
			break;
		}
		
		$params = array(
			'local_id' => $localId
		);
		$id10med = mysql_result(mysql_query('select `linked_with_10med` from `catalog` where `id` = '.$localId.' limit 1'), 0);
		
		//���������� ������ �� ���������
		$response = json_decode(api10med::link(0, $id10med),true);
		if ($response['code'] == 200) {
			mysql_query("UPDATE `catalog` SET `linked_with_10med` = '0' WHERE `id` = '{$localId}' LIMIT 1");
		}
		
		echo json_encode(array('error' => $response['error'], 'error_message' => $response['error_message']));
	break;

    //�������� ������ � ��� ���������
    case 'link_with_add_cats' :

        if (is_array($_REQUEST['products']) && is_array($_REQUEST['categories']) &&
            !empty($_REQUEST['products']) && !empty($_REQUEST['categories'])) {
            $products = $_REQUEST['products'];
            $categories = $_REQUEST['categories'];

            $product_string = implode(', ', $products);
            $cat_string = implode(', ', $categories);


            //�������� ��� ������������ ���� �����-��������� � ��������� �� � ������
            $query = "SELECT `cc`.`cat_id`, `cc`.`catalog_id` FROM `catalog_2_cat` as `cc`
					  WHERE `cc`.`catalog_id` IN ({$product_string})
					  AND `cc`.`cat_id` IN ({$cat_string})";

            $res = mysql_query($query);
            while ($row = mysql_fetch_assoc($res)) {
                $products_in_cat[] = $row;
            }
            //file_put_contents('prod_in_cat.txt', var_export($products_in_cat, true));

            foreach($categories as $cat) {
                foreach($products as $product) {
                    foreach($products_in_cat as $pr_cat) {
                        //���� ���� �����-��������� ��� ����������,
                        //�� ��������� � � ��
                        if (in_array($product, $pr_cat) && in_array($cat, $pr_cat)) {
                            continue 2;
                        }
                    }
                    $values[] = "({$cat}, {$product})";
                }
            }
            //��������� ����� � ���. ���������
            $values_string = implode(', ', $values);
            $query = "INSERT INTO `catalog_2_cat`
						(`cat_id`, `catalog_id`)
					  VALUES
						{$values_string}";
            mysql_query($query);
            //file_put_contents('values_string.txt', $query);
            echo json_encode(array(
                'error' => false,
                'suggestion' => $response['suggestion']
            ));
        }

        break;
	
	case 'manual_search' :
		$params = array(
			'search_text' => $_REQUEST['search_text']
		);
		
		//���������� ������ �� �����
		$response = api_10med('manual_search', $params);
		
		echo json_encode(array(
				'error' => false,
				'suggestion' => $response['suggestion']
				));
	break;
	case 'fake_in_stock' :
		
		if (isset($_REQUEST['id']) && 
			is_numeric($_REQUEST['id']) &&
			isset($_REQUEST['check'])) {
		
			$fake_in_stock = ($_REQUEST['check'] === 'true') ? 1 : 0;
			$id = $_REQUEST['id'];
			
			$query = "UPDATE catalog 
					  SET fake_in_stock = {$fake_in_stock}
					  WHERE id = {$id}";			
			mysql_query($query);
			
			if (mysql_error()) 
				echo json_encode(array("error" => true, 
										"msg" => "Database error"));
		}
		else 
			echo json_encode(array("error" => true, 
									"msg" => "Wrong data")); 
	break;	
}