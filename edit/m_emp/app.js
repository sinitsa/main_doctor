$(function(){
	$(".datepicker").datepicker({
		format: "dd/mm/yyyy"
	});
	$("#t1").on('click', function(){
		var all = $("#ch1").prop("checked");
		var time1 = $("#d1").val() ? $("#d1").val(): "";
		var time2 = $("#d2").val() ? $("#d2").val(): "";
		$.ajax({
			url: 'ajax.php?action=getAll',
			type: 'POST',
			data: {time1:time1, time2:time2, all: all},
			success: function(data){
				$("tbody #d4").empty().append(data);
			}
		})
	});
	$(".kro").on('click', function(){
		var id = $(this).data('id');
		$.ajax({
			url: 'ajax.php?action=one',
			type: 'POST',
			data: {id: id},
			success: function(data){
				$(".modal-body").empty().append(data);
				$(".modal").modal();
			}
		})
	});
});