<?php 
header('Content-Type: text/html; charset=utf-8');

// ini_set('error_reporting', E_ALL);
// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);

include ("header.php");
require_once('functions.php');

$config_file = 'conf.ini';
$myConfig = parse_ini_file('conf.ini');

// передапись конфига данными из формы
if (count($_POST['config'])) {

	$string = "; настройки модуля работы c 10med \n[site_config] \n";

	$fp = fopen($config_file, 'w');
	ftruncate($fp, 0); // очищаем файл
	fwrite($fp, $string);
	foreach ($_POST['config'] as $key => $value) {
		echo "{$key} = '{$value}' \n";
		fwrite($fp, "{$key} = '{$value}' \n");
	}
	fclose($fp); // Закрытие файла
	$myConfig = parse_ini_file('conf.ini');
} 

 ?>


<header>
 	<div class="container">
 		<h3>Модуль работы с 10med (настройка конфигурации)</h3>
 		<nav class="col-md-12" >
 			<ul class="nav nav-pills">
 			  <li role="presentation"><a href="">Home</a></li>
 			  <li role="presentation"><a href="index.php">Назад к модулю</a></li>
 			  <li role="presentation"><a href="/edit/">В админку</a></li>
 			</ul>
 		</nav>
 	</div>
</header>
<section>
	<div class="container">
	<div class="col-md-8">
		<form action="" method="POST">
 		<?php foreach ($myConfig as $key => $value): ?>
 			
			<div class="input-group param_row">
  				<span class="input-group-addon key_area" id="sizing-addon2"> <?=$key; ?> </span>
  				<input type="text" class="form-control" name="config[<?=$key; ?>]" placeholder="Username" aria-describedby="sizing-addon2" value="<?=$value; ?>">
			</div>

 		<?php endforeach ?>
 		<button type="submit" class="btn btn-default">Сохранить</button>
 		</form>
	</div>
	<div class="col-md-4">
		
	</div>
	
	</div>
</section>