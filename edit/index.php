<?php
include ('connect.php');
include ('../func/core.php');

//������� ���� �� �������
$days['Mon'] = '�����������';
$days['Tue'] = '�������';
$days['Wed'] = '�����';
$days['Thu'] = '�������';
$days['Fri'] = '�������';
$days['Sat'] = '�������';
$days['Sun'] = '�����������';

$months = array('������', '�������', '����', '������', '���', '����', '����', '������', '��������', '�������', '������', '�������');
//=========

session_start();

$_SESSION['edit_hash'] = true;
$_SESSION['auth'] = 'edit';

include ('up.php');

query_all('pages');

$fd = isset($global_massiv[0]['title']) 
        ? $global_massiv[0]['title']
        : '';
query_email();
?>
<script>
	$(function(){
		$(".oplata").on("click", function(){
			var delivery = $(this).parent().find("input[type=checkbox]").prop("checked");
			$.ajax({
				url : '/edit/sendletter.php',
				data : 'oid='+$(this).data('id')+'&omail='+$(this).data('mail')+'&delivery='+ delivery,
				type : "POST", 
				success : function (data, textStatus, jqXHR) {
					alert(data);
				}
			});
		});
		$(".icon-ok.price").on("click", function(){
			var el = $(this);
			var newPrice = el.parent().find("input").val();
			var idOrder = el.data('id');
			if(newPrice === "" || newPrice === {} || newPrice === NaN || newPrice === undefined){
				alert('������� ����� ����.');
				return false;
			}
			var $this = this;
			$.ajax({
				url: '/edit/editOrder.php', 
				data: {method: "editPrice", newPrice: newPrice, idOrder: idOrder}, 
				type: "POST", 
				success: function(data){
					$($this).parent().parent().find(".all_price_to_pay").text("����� �����: " + newPrice);
					$($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>����� ����� ��������<div>');
				}	
			})
		});
		$(".icon-ok.delivery").on("click", function(){
			var el = $(this);
			var newDel = el.parent().find("input").val();
			if(newDel === 0){
				newDel = "0";
			}
			var idOrder = el.data('id');
			if(newDel === "" || newDel === {} || newDel === NaN || newDel === undefined){
				alert('������� ����� ��������� ��������.');
				return false;
			}
			var $this = this;
			$.ajax({
				url: '/edit/editOrder.php', 
				data: {method: "editDel", newDel: newDel, idOrder: idOrder}, 
				type: "POST", 
				success: function(data){
					$($this).parent().parent().find(".dellcPr").text(newDel);
					$($this).parent().parent().find(".all_price_to_pay").append('<div class="alert alert-warning fade in" style="font-size: 12px; margin: 0px;"><button type="button" class="mini close" data-dismiss="alert" aria-hidden="true" style="font-size: 11px;">x</button>����� �������� ��������<div>');
				}	
			})
		});
	});

</script>
<table width="90%" border="0" align="center" cellpadding="0" cellspacing="0">
	<?php if ($_SESSION['auth_type'] != 'manager') { ?>
	<tr>
		<td width="33%" valign="top">

			<table width="100%" border="0" cellspacing="0" cellpadding="0">
				<tr>
					<td width="70" align="left" valign="top"><img src="icons/page.jpg" width="60" height="60" alt="" /></td>
					<td align="left" valign="top">
						<table width="100%" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td align="left" valign="bottom" class="big_menu">��������</td>
							</tr>
							<tr>
								<td align="left"><i class="icon-plus"></i> <a href="m_pages/add.php" class="txt_spisok">�������� ��������</a></td>
							</tr>
							<tr>
								<td align="left"></td>
							</tr>

						</table></td>
				</tr>
				<tr>
					<td colspan="2" align="left" valign="top">
						<table width="80%" class="table-condensed" border="0" cellpadding="1" cellspacing="0">
							<tr>
								<td align="left"><i class="icon-pencil"></i> <em>��������:</em></td>
							</tr>
<?php
// ������ �������
$pages = getStaticPages();
foreach ($pages as $page) {
	echo ('
								<tr>
									<td align="left"><a href="/edit/m_pages/edit.php?id=' . $page['id'] . '" class="txt_spisok">' . $page['title'] . '</a></td>
								</tr>
							');
}
?>
							
							
						</table>
					</td>
				</tr>
			</table>

		</td>
		
		<link rel="stylesheet" href="m_edit_menu/edit_menu.css">
		<link rel="stylesheet" href="css/nprogress/nprogress.css">
		<script src='css/nprogress/nprogress.js'></script>
		<link rel="stylesheet" href="/css/animate.css">
		<script src='/js/jquery-ui-1.8.13.js'></script>
		<script src='m_edit_menu/edit_menu.js'></script>
		
		<td width="67%" colspan="2" valign="top">
			<div class="menuHolder">
			<?
				$edMenuQ = mysql_query('select * from `edit_menu` order by `rang` asc');
				$menuNum = 0;
				while($edR = mysql_fetch_array($edMenuQ)){
					if($edR['title']=='separator'){
						if($edR['url']==1){
							echo '<div class="allSepar separ1"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}elseif($edR['url']==2){
							echo '<div class="allSepar separ2"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}elseif($edR['url']==3){
							echo '<div class="allSepar separ3"><img src="/edit/m_edit_menu/menuDelBtn.jpg" data-id="',$edR['id'],'" alt="" class="menuDelBtn"></div>';
						}
					}else{
						$menuNum++;
			?>
				<div class="menuBtn">
					<img src="/edit/m_edit_menu/menuDelBtn.jpg" alt="" data-id='<?=$edR['id']?>' class='menuDelBtn'>
					<div class='menuBtnContent'>
						<div class='menuBtnImg'>
							<a href='<?=$edR['url']?>' class='menuLink'><img src="/edit/m_edit_menu/img/<?=$edR['id']?>.jpg" alt="" width="60px" height="60px" class='menuImg'></a>
						</div>
						<div class='menuBtnText'>
							<a href='<?=$edR['url']?>' class='menuLink'><?=$edR['title']?></a>
						</div>
					</div>
				</div>
			<?}}?>
				<div class="newMenuBtn" <?if($menuNum==0){echo 'style="display:block;"';}?>>
					<div class='newMenuBtnContent'>
						<div class='newMenuBtnImg'>
							<img src="/edit/m_edit_menu/menuAddBtn.jpg" alt="" class='newMenuImg'>
						</div>
						<div class='newMenuBtnText'>
							����� �������
						</div>
					</div>
				</div>
			</div>
		</td>
	</tr>
	<tr>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
		<td valign="top">&nbsp;</td>
	</tr>
	<tr>
		<td colspan="2" valign="top">

		</td>
		<td valign="top">
	<?php } ?>

		</td>
	</tr>
	<tr>
		<td colspan="3" valign="top">

			<table width="100%" border="0" cellpadding="1" cellspacing="0">
				<tr>
					<td align="left" valign="middle">           
					</td>
				</tr>
				<tr>
					<td align="left" valign="middle">		  
<?php include ('../tpl/admin/nav_zakaz.tpl.php'); ?>
					</td>
				</tr>
            </table>

<?php
	// �������
	$k = (isset($_GET['k']) && !empty($_GET['k'])) 
                ? $_GET['k']
                : 0;
	$s = (isset($_GET['status'])) ? $_GET['status'] : 0;
	//����� ������ ��������
	switch ($s) {
	 case '0' :
	 case '1' :
	 case '4' :
		$status = $s;
		break;
	default:
		$status = 0;
	}
	// ��������������
	$listing = 40;
	$result = mysql_query("SELECT COUNT(*) as c FROM `orders` WHERE `status` = '{$status}' ");
	$all_pay = mysql_fetch_assoc($result);
	$all_pay = $all_pay['c'];
	$num_page = $all_pay / $listing;
	$num_page = round($num_page);
	
	$previusOrderDay = 0;
	
	//�������� �� ������������ ������
	
	if (isset($_POST['order'])) {
		$search_str = mysql_real_escape_string($_POST['order']);
		$query = "
			SELECT
				`orders`.*,
				`order_delivery_types`.`name` as `delivery`
			FROM
				`orders`
			LEFT JOIN `order_delivery_types`
				ON `orders`.`delivery_type` = `order_delivery_types`.`id`
			WHERE
				`orders`.`id` LIKE '%{$search_str}%'
			LIMIT 1";	
	}
	else {
		$query = "
		SELECT
			`orders`.*,
			`order_delivery_types`.`name` as `delivery`
		FROM
			`orders`
		LEFT JOIN `order_delivery_types`
			ON `orders`.`delivery_type` = `order_delivery_types`.`id`
		WHERE
			`status` = '{$status}'
		ORDER BY
			`orders`.`id` DESC
		LIMIT {$k},{$listing}";
		include ('../tpl/admin/list_zakaz.tpl.php'); // �������������� ������� 
	}
	$sel = mysql_query($query);
	
	while ($order = mysql_fetch_assoc($sel)) {
		// if(($order['name']=='test' && !isset($_GET['sec'])) || ( $order['name']!='test' && isset($_GET['sec']) )){continue;}else{
			//�������������� ������ ��� ������
		$order['products'] = unserialize($order['products']);
		fillProductsWithInfo($order['products']);
		$order['order_price'] = unserialize($order['order_price']);
		
		//print_r($order);
		$day = date('d', $order['date']);
		if ($previusOrderDay != $day) echo ('<div align="center" class="big_menu">' . date('d.m.Y', $order['date']) . '</div><hr> ');
		$previusOrderDay = $day;
		?>
		<div class="order" data-id="<?=$order['id'];?>">
		<span  class="txt_zag_zakaz"><strong><?php echo getOrderCode($order['id']); ?> </strong></span> <span class="txt_zakaz">(<?php echo date('d.m.Y H:i', $order['date']); ?>)</span>
		<table width="900" border="0" cellspacing="0" cellpadding="0">
		  <tr>
			<td width="500" align="left" valign="top" class="txt_zakaz">
				<div style="padding-top:10px">
					<?php switch ($order['status']) {
						case '0' : echo '<span class="label label-info">����� </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break; 
						case '1' : echo '<span class="label label-success">��������� </span>&nbsp;&nbsp;&nbsp;&nbsp;';
							break;
						case '4' : echo '<span class="label label-important">����� </span>&nbsp;&nbsp;&nbsp;&nbsp;';
					}
					?>
			<? if ($order['status'] != '1') { ?>
				<img src="/tpl/icons/ok.gif" align="absbottom" >
				<a href="#" class="processed" style="color:#333;">���������</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			<? if ($order['status'] != '4') { ?>
				<img src="/tpl/icons/del.gif" align="absbottom" >
				<a href="#" class="refusal" style="color:#333;">�����</a>&nbsp;&nbsp;&nbsp;&nbsp;
			<? } ?>
			</div>
			
			<table>
				<tr>
					<td>
						<?php 
						$sizeArr = array(0, 0, 0);
						foreach ($order['products'] as $p) { 
                                                    $info = isset($p['info']) 
                                                            ? $p['info']
                                                            : false;
                                                    if (!$info) continue;
                                                ?>
							<table width="100%" border="0" cellpadding="7" cellspacing="0" style="border-bottom-width: 1px;border-bottom-style: dashed;border-bottom-color: #d8d8d8;">
							  <tr>
								<td width="80" align="left" valign="top">
                                                                    <a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>"><img src="<?=getImageWebPath('product_small').$info['id']?>.jpg"  border="0" align="left" /></a>
                                                                </td>
								<td align="left" valign="top"><a target="_blank" href="<?php echo getTemplateLink($info, 'catalog'); ?>" style="color:#333;"><?=$info['title']?></a> <span style="font-size: 1.3em;">(<?=$p['amount']?> ��.)</span><br />
								  <br />
								  <span style="font-size: 17px;color: grey;font-family: Verdana, Geneva, sans-serif;"><?=$p['price_after_discount']?></span> <span style="font-size: 12px;color: #A8A8A8;font-weight: bold;font-family: Verdana, Geneva, sans-serif;">���.</span>
                                                                 <? if ( isset($p['info']['height']) && !empty($p['info']['height']) &&
                                                                         isset($p['info']['width']) && !empty($p['info']['width']) &&
                                                                         isset($p['info']['length']) && !empty($p['info']['length'])
                                                                        ) {
																		$pSize = array($p['info']['height'], $p['info']['width'], $p['info']['length']);
																		sort($pSize);
																		for($i=0; $i<$p['amount']; $i++ ){
																			$sizeArr[0] += $pSize[0];
																			$sizeArr[1] += $pSize[1];
																			$sizeArr[2] += $pSize[2];
																		}
																		$qiwiSize = getQiwiSizeName($pSize);
                                                                  ?>
                                                                  <br/>
                                                                  <span>
																	<?
																	if($qiwiSize != 'none'){
																	?>
                                                                        ������ <?=$qiwiSize.' ('.$p['info']['length'].'x'.$p['info']['width'].'x'.$p['info']['height'].')';?>
                                                                  <?}else{?>
																		������ <span style="color:red;">��������</span><?=' ('.$p['info']['length'].'x'.$p['info']['width'].'x'.$p['info']['height'].')';?>
																  <?}?>
                                                                  </span>
                                                                  <? }else{
																		$sizeArr[0] += 5000;
																		$sizeArr[1] += 5000;
																		$sizeArr[2] += 5000;
																  ?>
																	  <br/>
																	  <span>
																		������ <span style="color:red;">��������</span>
																	  </span>
																  <?}?>
                                                                </td>
								</tr>
							</table>
						<?php } 
						$qiwiAllSize = getQiwiSizeName($sizeArr);
						?>
						<span style="font-size: 1.2em;" class="all_price_to_pay">����� �����: <?=$order['order_price']['price_after_global_discount']?> (������ <?=$order['order_price']['discount_value']?> <?=getDiscountTypeString($order['order_price']['discount_type'])?>)</span><br />
						����� ������: <?if($qiwiAllSize == 'none'){echo '<span style="color:red;">��������</span>';}else{echo $qiwiAllSize;}?><br />
						<br />

						<!-- ������ ��������� ����� � �������� -->
						<?php if($order['status'] == 0){?>
							<span>�������� �����
							<br/>
								<!-- <span style="color: red;"> [�� ��������]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['price_after_global_discount']?>"/>
								<i class="icon-ok price" data-id= "<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
							<br/>
							<span>�������� ��������� ��������
							<br/>
								<!-- <span style="color: red;"> [�� ��������]</span>:  -->
								<input type="text" placeholder="<?=$order['order_price']['delivery_price']?>"/>
								<i class="icon-ok delivery" data-id="<?=$order['id']?>" style="cursor: pointer; position: relative; top: -4px;-webkit-touch-callout: none;-webkit-user-select: none;-khtml-user-select: none;-moz-user-select: none;-ms-user-select: none;user-select: none;"></i>
							</span>
							<br />
						<?php }?>
						<?php if(((int)$order['payment_type']) === 2){ ?>
								<span style="text-decoreation: underline;">��������� ��������� ��������(�������������):<span>
								<input type="checkbox" class="i55" value="yes" name="plus_dev" <?php if($order["total_price"] == 1){echo 'checked';}?> >
							<br/>
						<?php }?>
						<!-- ����� ��������� ����� � �������� -->

						<span class="delPrice">
							<?php if ($order['delivery_type'] == 1) { ?>
							<strong>��������� ��������:</strong> 
							<?php if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
									echo '<span class="dellcPr">���������� �� �������</span>';
								} elseif ($order['order_price']['delivery_price'] == 0) {
									echo '<span class="dellcPr">���������</span>';
								} else {
									echo '<span class="dellcPr">'.$order['order_price']['delivery_price'] . ' ���.</span>';
								}
							} elseif ($order['delivery_type']  == 2) {
								echo '<strong>������� ������ ��������:</strong> ';
								if (!isset($order['order_price']['delivery_price']) || $order['order_price']['delivery_price'] === false) {
									echo '<span class="dellcPr"><span class="dellcPr">���������� �� �������</span>';
								} elseif ($order['order_price']['delivery_price'] == 0) {
									echo '<span class="dellcPr">���������</span>';
								} else {
									echo '<span class="dellcPr">'.$order['order_price']['delivery_price'] . ' ���.</span>';
								}
							} elseif ($order['delivery_type']  == 7 && $order['status'] == 0) {
								// ������ ���������
								
								$qiwiName = $order['qiwi_terminal'];
								// $qiwiSize = getQiwiSizeName(array($p['info']['length'], $p['info']['width'], $p['info']['height']));
								if($qiwiAllSize != 'none'){
									$qiwiPrice = file_get_contents( "http://wt.qiwipost.ru/calc?key=".Config::get('qiwi_post.key')."&id=".$qiwiName."&size=".$qiwiSize ).' ���.';
								}else{
									$qiwiPrice = ' <span style="color:red;">�������� ����� Qiwi Post ����������</span>';
								}
								
								echo '<strong>��������� �������� QIWI: </strong>', $qiwiPrice, '<br>';
								echo '<strong>����� ���������: </strong>', $qiwiName, '<br>';
	                            
	                            // ����� ��������� 

							} ?>
						</span>
						<br />
						<strong>���:</strong> <?=$order['name']?><br />
						<strong>���.:</strong> <?=$order['phone']?><br />
						<strong>��������:</strong> <?=$order['delivery']?><br />
						<strong>�����:</strong> <?=$order['adress']?><br />
						<?php
							if ($order['email'] != '') echo ('<i class="icon-envelope"></i> ' . $order['email'] . '<br />');
						?>
						<strong>�������������:</strong> <?=$order['extra_information']?>
						<br />
						
						<?php 
							$__payment = $order['payment_type'];
							if(((int)$order['payment_type']) === 2){
								$__payment = "���������� ������";
							}
						?>

                       	<strong>������ ������:</strong> <?=($order['payment_type'] == 1)? "���������": $__payment;?>

						<?if ($order['payment_type'] == 2){?>
							<button class="oplata" data-id="<?=$order['id']?>" data-mail="<?=$order['email']?>">������� ������.</button>
						<?}?>

						<br />
                                                <?php if ($order['delivery_type'] == 7) { ?>
                                                <strong><a href="<?=Config::get('qiwi_post.url');?>">������� � �������� �������</a></strong>
                                                <?php } ?>
					</td>
				</tr>
			</table>

			</td>
			<td align="left" valign="top" width="30">&nbsp;</td>
			<td align="left" valign="top" width="375">



			<div class="txt_zakaz" style="padding-left:20px; padding-right:10px; padding-bottom:10px;  padding-top:10px;  background-color:#fdf6f0">
			<table class="table table-condensed order-log">
			<?php
			$s = mysql_query("SELECT * FROM `orders_log` WHERE `order_id`='{$order['id']}' AND `action` <> '0' ORDER BY `date` ASC");
			$lastActionTime = $order['date'];
			$actionsCount = mysql_num_rows($s);

			while ($row = mysql_fetch_assoc($s)) {
				$actionTime = strtotime($row['date']);
				
				$dateDiff = $actionTime - $lastActionTime;
				$dH = floor($dateDiff / 3600);
				$dM = ($dateDiff - $dH * 3600) / 60;
				$delta = $dH . ' �. ' . round($dM) . ' �.';
				
				$lastActionTime = $actionTime;

				$month = $months[date('n', $actionTime) - 1];
				$day_of_week = $days[date('D', $actionTime)];
				$day = date('d', $actionTime);
				$year = date('Y', $actionTime);
				$time = date('H:i', $actionTime);
				?>
					<tr>
						<td width='100'><b><?=$time?></b>, <?=$day?> <?=$month?><br /><?=$day_of_week?> </td>
						<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>
						<td><?=$row['descr']?></td>
						<td width="75px"><?=$delta?></td>
					</tr>
				<?php
			}
			$keyw = '';
			if ($actionsCount > 1) {
					$dd = $lastActionTime - $order['date'];
					$dh = floor($dd / 3600);
					$dm = ($dd - $dh * 3600) / 60;
					$delta = $dh . ' �. ' . round($dm) . ' �.';
					echo "<tr><td colspan='3'>&nbsp;</td><td><strong>" . $delta . "</strong></td></tr>";
				}
				//������ ������
				$hrefer = explode("/", $order['h']);
				$site = "<img src=http://favicon.yandex.net/favicon/" . $hrefer[2] . " align=absmiddle  height=15 width=15>";
				if ($hrefer[2] == "yandex.ru" or $hrefer[2] == "www.yandex.ru") {
					$site = "<img src=/tpl/icons/ya_mini.jpg align=absmiddle height=15 width=15 >";
					$hrefer2 = explode("text=", $hrefer[3]);
					if (isset($hrefer2[1])) {
                                            $keyw = $hrefer2[1];
                                            $keyw = urldecode($keyw);
                                            $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
                                            $hrefer3 = explode("&", $keyw);
                                            $keyw = $hrefer3[0];
                                        }
					//echo "<pre>"; print_r($hrefer2); echo "</pre>";
				}
				if ($hrefer[2] == "www.google.ru" or $hrefer[2] == "google.ru" or $hrefer[2] == "www.google.by" or $hrefer[2] == "www.google.com") {
					$site = "<img src=/tpl/icons/goog_mini.jpg align=absmiddle  height=15 width=15 >";
					$hrefer2 = explode("q=", $hrefer[3]);
                                        if (isset($hrefer2[1])) {
                                            $keyw = $hrefer2[1];
                                            $keyw = urldecode($keyw);
                                            $keyw = iconv('UTF-8', 'Windows-1251', $keyw);
                                            $hrefer3 = explode("&", $keyw);
                                            $keyw = $hrefer3[0];
                                        }       
					//$keyw = str_replace("&hl", "", $keyw );
				}
				//echo "<pre>"; print_r($hrefer); echo "</pre>";
			?>
			</table>
				<img src="/tpl/icons/comment.jpg" border="0" align="absbottom" >
				<a href="javascript:void(0)" class="comment-button" style="color:#333;">��������������</a>
				<div style="display: none;" class="comment-form">
					<textarea rows="3" cols="20" class="comment-field" style="width: 300px;"></textarea><br />
					<input type="button" name="s" class="send-button btn"  value="��������" />
				</div>
				<br /><br />
			</div>

			<br>
				<table width="270" border="0" cellspacing="10" cellpadding="0">
				<?php
					//���� �� ��������
					if ($order['status'] == 1 AND false) {
						if (($order['remind'] != 1) && ($order['email'] != '')) {
							echo('<tr><td><a class="btn btn-warning reminder-link" pay_id="' . $order['id'] . '"><i class="icon-warning-sign"></i> ��������� �����</a></td></tr>');
						} elseif ($order['email'] == '') {
							//	���� ���, ��� � ������
						} else {
							echo('<tr><td><a class="btn btn-success disabled"><i class="icon-ok"></i> ����� ��������</a></td></tr>');
						}
					}
				?>
					<tr>
					  <td class="txt_zakaz_small" ><?=$site?> <?=$keyw?> <a href="<?=$order['h']?>" style="color:#333;" target="_blank">>>></a></td>
					</tr>
				<?php if($order['partner_id'] != 0) {
					echo('<tr>
						<td class="txt_zakaz_small">����� �� ����������� ������.</td>
					</tr>');
				}
				?>
				</table>
			</td>
		  </tr>
		</table>
		<hr style="margin: 50px 0;"/>
		</div>
	<?
	// }
	}
?> 
		</td>
	</tr>
	<tr>
		<td colspan="3" valign="top" >
			<?php include ('../tpl/admin/list_zakaz.tpl.php'); // �������������� �������   ?>
		</td>
	</tr>
	<tr>
		<td colspan="3" valign="top" class="big_menu">&nbsp;</td>
	</tr>
</table>
</td>
</tr>
<tr>
    <td><?php
include ('down.php');
?></td>
</tr>
</table>
<script>
	$(document).ready( function() {
		//���� �� ����������� ������� ���������
		$('.processed').bind('click', function () {
			var order = $(this).closest('.order');
			
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				data : {"method" : "setstatus", "order_id" : order.data('id'), status : "<?=STATUS_PROCESSED;?>"},
				type : "POST", 
				//dataType : 
				beforeSend : function () {
					order.css('opacity', .5);
				},
				success : function (data, textStatus, jqXHR) {
					order.fadeOut('slow', function () {
						$(this).remove();
					});
				}
			});
			return false;
		});
		//���� �� ����������� ������� �����
		$('.refusal').bind('click', function () {
			var order = $(this).closest('.order');
			
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				data : {"method" : "setstatus", "order_id" : order.data('id'), status : "<?=STATUS_REFUSAL;?>"},
				type : "POST", 
				//dataType : 
				beforeSend : function () {
					order.css('opacity', .5);
				},
				success : function (data, textStatus, jqXHR) {
					order.fadeOut('slow', function () {
						$(this).remove();
					});
				}
			});
			return false;
		});
		
		//�������� ���������
		$('.comment-button').bind('click', function() {
			$(this).next('.comment-form').slideToggle();
		});
		
		$('.send-button').bind('click', function () {
			var _this = this;
			var order = $(this).closest('.order');
			var orderId = order.data('id');
			
			var field = $(this).closest('.comment-form').find('.comment-field');
			var button = $(this);
			
			var comment = field.val();
			$.ajax({
				url : '/edit/m_zakaz/ajax.php',
				type : "POST",
				data : {"method" : "add_comment", "order_id" : orderId, "comment" : comment},
				dataType : 'json',
				beforeSend : function () {
					field.attr('disabled', 'disabled');
					button.attr('disabled', 'disabled');
				},
				success : function (data, textStatus, jqXHR) {
					field.removeAttr('disabled');
					button.removeAttr('disabled');
					
					$(_this).closest('.order').find('.order-log').append($('<tr></tr>').html(
					"<td width='100'>" + data.date + " </td>" +
					"<td><img src='/tpl/icons/comment.jpg' border='0' align='absbottom' ></td>" +
					"<td>" + comment + "</td>" +
					"<td>" + data.delta + "</td>"
					));
					field.val('');
					$(_this).closest('.comment-form').slideToggle();
				}
			});
		});
	})
</script>

<div class="menuCreateBack"></div>
<div class="menuCreateHolder">
	<div class="selectBtn" data-type='1'>
		�����������
	</div>
	<div class="selectBtn" data-type='2'>
		����� ������
	</div>
	<div class='createArea'>
		<div class='createSeparator'>
			<div class='separator' data-type='1'>
				<div class='separType1'></div>
			</div>
			<div class='separator' data-type='2'>
				<div class='separType2'></div>
			</div>
			<div class='separator' data-type='3'>
				<div class='separType3'></div>
			</div>
		</div>
		<div class='createBtn'>
			<div><input type='text' name='btnTitle' class='skip' placeholder='��������' /></div>
			<div><input type='text' name='btnUrl' class='skip' placeholder='url' /></div>
			<div><input type='file' name='btnFile' class='skip' id='newFile' /></div>
			<div> <input type='button' id='saveNewBtn' class='skip' value='��������' /> </div>
		</div>
	</div>
</div>

<div class="modal hide fade" id="tt4">
  <div class="modal-header">
    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
    <h3>�������� ������ ���������� ��� ������ ����� �������������.</h3>
  </div>
  <div class="modal-body">
    <p></p>
  </div>
</div>

</body>
</html>
