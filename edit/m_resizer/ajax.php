<?php
include('../../connect.php');
include('../../func/core.php');
include("../../func/classes/resize-class.php");

switch ($_REQUEST['method']) {
	case 'resizeproducts' :
		$id = is_numeric($_REQUEST['id']) ? $_REQUEST['id'] : 0;
		$oneByOne = isset($_REQUEST['one_by_one']);
		
		//�������� ���������� �������
		set_time_limit(0);
		
		//��������� �������
		$settings = array(
			'product_preview' => isset($_REQUEST['preview']),
			'product_extra_preview' => isset($_REQUEST['preview_extra']),
			'product_small' => isset($_REQUEST['small']),
			'product_extra_small' => isset($_REQUEST['small_extra']),
			'product_medium' => isset($_REQUEST['medium']),
			'product_extra_medium' => isset($_REQUEST['medium_extra']),
			'product_little' => isset($_REQUEST['little']),
			'product_extra_little' => isset($_REQUEST['little_extra'])
		);

		
		//��� ������ ����� ���� �� ���� true � ����������
		
		$start = false;
		foreach ($settings as $key => $value) {
			if ($value) {
				$start = true;
				break;
			}
		}
		if (!$start) {
			echo json_encode(array('resized' => 0));
			break;
		}
		//��. �������
		$resizedCounter = 0;
		$lastId = 0;
		
		//���� �������� ��� ����������� ������
		if ($id > 0 || $oneByOne) {
			if ($oneByOne) {
				$sel = mysql_query("SELECT `id` FROM `catalog` WHERE `id` > '{$id}' ORDER BY `id` ASC LIMIT 1");
			} else {
				$sel = mysql_query("SELECT ('{$id}') as `id`;");
			}
		} else {
			$sel = mysql_query("SELECT `id` FROM `catalog`");
		}

		while ($row = mysql_fetch_assoc($sel)) {
			$filename = $row['id'];
			$lastId = $row['id'];
			$sourceFile = array(
				'name' => 'unnamed',
				'tmp_name' => getImagePath('product_original').$filename.'.jpg',
				'error' => 0
			);
			
			if (file_exists($sourceFile['tmp_name'])) { 
				//�������� ������ ������
				if ($settings['product_preview']) {
					try {
					$imgPath = getImagePath('product_preview');
					$size = getConfigImageSize('product_preview');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					
					$quality = getConfigImageQuality('product_preview');
					
					// *** 1) Initialize / load image
					$resizeObj = new resize($sourceFile['tmp_name']);
					
					// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
					$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
					 
					// *** 3) Save image
					$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
					
					if ($done !== false) $resizedCounter++;
					} catch (Exception $e) {}
				}
				//�������� ��������� ����������� ������
				if ($settings['product_small']) {
					try {
					$imgPath = getImagePath('product_small');
					$size = getConfigImageSize('product_small');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					
					$quality = getConfigImageQuality('product_small');

					// *** 1) Initialize / load image
					$resizeObj = new resize($sourceFile['tmp_name']);
					
					// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
					$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
					 
					// *** 3) Save image
					$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
					
					if ($done !== false) $resizedCounter++;
					} catch (Exception $e) {}
				}
				//�������� ������� ����������� ������
				if ($settings['product_medium']) {
					$imgPath = getImagePath('product_medium');
					$size = getConfigImageSize('product_medium');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					
					$quality = getConfigImageQuality('product_medium');

					// *** 1) Initialize / load image
					$resizeObj = new resize($sourceFile['tmp_name']);
					
					// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
					$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
					 
					// *** 3) Save image
					$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
					
					if ($done !== false) $resizedCounter++;
				} 
				//�������� ����� ��������� ����������� ������
				if ($settings['product_little']) {
					$imgPath = getImagePath('product_little');
					$size = getConfigImageSize('product_little');
					$iWidthDest = $size['width'];
					$iHeightDest = $size['height'];
					
					$quality = getConfigImageQuality('product_little');
					
					// *** 1) Initialize / load image
					$resizeObj = new resize($sourceFile['tmp_name']);
					
					// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
					$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
					 
					// *** 3) Save image
					$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
					
					if ($done !== false) $resizedCounter++;
				}
			}
			//�������� ������ ���. ����
			if ($settings['product_extra_preview'] || $settings['product_extra_medium'] || $settings['product_extra_small'] || $settings['product_extra_little'] ) {
				$sel2 = mysql_query("SELECT `id` FROM `foto` WHERE `catalog_id` = '{$filename}'");
				while ($row2 = mysql_fetch_assoc($sel2)) {
					$filename = $row2['id'];
					$sourceFile = array(
						'name' => 'unnamed',
						'tmp_name' => getImagePath('product_extra_original').$filename.'.jpg',
						'error' => 0
					);
					if (file_exists($sourceFile['tmp_name']) && is_file($sourceFile['tmp_name'])) {

						if ($settings['product_extra_preview']) {
							$imgPath = getImagePath('product_extra_preview');
							$size = getConfigImageSize('product_extra_preview');
							$iWidthDest = $size['width'];
							$iHeightDest = $size['height'];
							
							$quality = getConfigImageQuality('product_extra_preview');
							// *** 1) Initialize / load image
							$resizeObj = new resize($sourceFile['tmp_name']);
							
							// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
							$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
							 
							// *** 3) Save image
							$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
							if ($done !== false) $resizedCounter++;
						}

						if ($settings['product_extra_medium']) {
							$imgPath = getImagePath('product_extra_medium');
							$size = getConfigImageSize('product_extra_medium');
							$iWidthDest = $size['width'];
							$iHeightDest = $size['height'];
							
							$quality = getConfigImageQuality('product_extra_medium');
							// *** 1) Initialize / load image
							$resizeObj = new resize($sourceFile['tmp_name']);
							
							// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
							$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
							 
							// *** 3) Save image
							$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
							if ($done !== false) $resizedCounter++;
						}

						if ($settings['product_extra_small']) {
							$imgPath = getImagePath('product_extra_small');
							$size = getConfigImageSize('product_extra_small');
							$iWidthDest = $size['width'];
							$iHeightDest = $size['height'];
							
							$quality = getConfigImageQuality('product_extra_small');
							// *** 1) Initialize / load image
							$resizeObj = new resize($sourceFile['tmp_name']);
							
							// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
							$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
							 
							// *** 3) Save image
							$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
							if ($done !== false) $resizedCounter++;
						}

						if ($settings['product_extra_little']) {
							$imgPath = getImagePath('product_extra_little');
							$size = getConfigImageSize('product_extra_little');
							$iWidthDest = $size['width'];
							$iHeightDest = $size['height'];
							
							$quality = getConfigImageQuality('product_extra_little');
							// *** 1) Initialize / load image
							$resizeObj = new resize($sourceFile['tmp_name']);
							
							// *** 2) Resize image (options: exact, portrait, landscape, auto, crop)
							$resizeObj -> resizeImage($iWidthDest, $iHeightDest, 'auto');
							 
							// *** 3) Save image
							$done = $resizeObj -> saveImage($imgPath.$id.'.jpg', 100);
							if ($done !== false) $resizedCounter++;
						}
					}
				}
			} 
		}
		echo json_encode(array('resized' => $resizedCounter, 'last_id' => $last_id)); 
	break;
}
