function isInt(n) {
   return typeof n === 'number' && parseFloat(n) == parseInt(n, 10) && !isNaN(n);
} 
//������ ��� jquery. ��������� ������� �� ������ ������
jQuery.fn.center = function (dx, dy) {
	if (dx == undefined) dx = 0;
	if (dy == undefined) dy = 0;
    this.css("position","absolute");
    this.css("top", (Math.max(0, (($(window).height() - this.height()) / 2) + $(window).scrollTop()) + dy) + "px");
    this.css("left", (Math.max(0, (($(window).width() - this.width()) / 2) + $(window).scrollLeft()) + dx) + "px");
    return this;
}
//��������� ��������
function stylizeCheckbox() {
	//Checkbox
	$('.hidden-checkbox').css('opacity', 0);
	$('.hidden-checkbox').click(function() {
		if ($(this).is(':checked')) {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-on');
		} else {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-off');
		}
	});
	$('.hidden-checkbox').each(function(index, element) {
		if ($(this).is(':checked')) {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-on');
		} else {
			$(this).parent('.filter-checkbox').removeClass('filter-checkbox-off filter-checkbox-on').addClass('filter-checkbox-off');
		}
	});
}
//������ �������� �������� ������ (�������, ������, ...)
function toggleProductAttr(_this, id, attr) {
	$.ajax({
		url: 'ajax.php?method=toggle',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id, "attr" : attr, "toggle" : ($(_this).data('enabled') == 1 ? '0' : '1') },
		beforeSend : function () {
			
		},
		success : function (data, textStatus, jqXHR) {
			if (!data.error) {
				var enabled = ($(_this).data('enabled') == 1 ? 0 : 1);
				var src = '';
				switch (attr) {
					case 'best' :
						if (enabled == 1) {
							//���������� ������� ������
							src = '/img/best.png';
							$(_this).data('enabled', 1);
						} else {
							//������ ������� ������
							src = '/img/bests.png';
							$(_this).data('enabled', 0);
						}
					break;
					case 'sale' :
						if (enabled == 1) {
							//���������� ������� ������
							src = '/img/sale.png';
							$(_this).data('enabled', 1);
						} else {
							//������ ������� ������
							src = '/img/sales.png';
							$(_this).data('enabled', 0);
						}
					break;
					case 'novinka' :
						if (enabled == 1) {
							//���������� ������� ������
							src = '/img/new.png';
							$(_this).data('enabled', 1);
						} else {
							//������ ������� ������
							src = '/img/news.png';
							$(_this).data('enabled', 0);
						}
					break;
				}
				_this.src = src;
			}
		},
		error : function () {
			
		}
	});
}
//������������� ����� �������� ��������� ������
function updatePrice(id, price) {
	$.ajax({
		url: 'ajax.php?method=setprice',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id, "price" : price},
		beforeSend : function () {},
		success : function (data, textStatus, jqXHR) {},
		error : function (jqXHR, textStatus, errorThrown) {}
	});
}
//�������������� ����� �������� �������� ������
function updateArt(id, art) {
	$.ajax({
		url: 'ajax.php?method=setart',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id, "art" : encodeURIComponent(art)},
		beforeSend : function () {},
		success : function (data, textStatus, jqXHR) {},
		error : function (jqXHR, textStatus, errorThrown) {}
	});
}
//�������������� ����� �������� bid\market_cost_per_click ������
function updateBid(id, bid) {
	$.ajax({
		url: 'ajax.php?method=setbid',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id, "bid" : bid},
		beforeSend : function () {},
		success : function (data, textStatus, jqXHR) {},
		error : function (jqXHR, textStatus, errorThrown) {}
	});
}
//������������� ����� �������� � ��������� ����
function updateField(id, field, value, onSuccess, onError) {
	if (onSuccess == undefined) onSuccess = function(){};
	if (onError == undefined) onError = function(){};
	
	$.ajax({
		url: 'ajax.php?method=set_field',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id, "field" : field, "value" : value},
		beforeSend : function () {},
		success : onSuccess,
		error : onError
	});
}
//������� �����
function deleteProduct(id, onsuccess) {
	if (onsuccess == undefined) onsuccess = function () {};
	
	$.ajax({
		url: 'ajax.php?method=deleteproduct',
		type : 'POST',
		dataType : 'json',
		data : {"id" : id},
		//beforeSend : function () {},
		success : onsuccess
		//error : function (jqXHR, textStatus, errorThrown) {}
	});
}
//��������� �������� ���������
function addParamValue(paramId, catId, paramValue, onSuccess, onError) {
	if (onSuccess == undefined) onSuccess = function(id) {};
	if (onError == undefined) onError = function() {};
	
	$.ajax({
		url: 'ajax.php?method=addparam',
		type : 'POST',
		dataType : 'json',
		data : {"param_id" : paramId, "cat_id" : catId, "param_value" : encodeURIComponent(paramValue)},
		beforeSend : function () {},
		success : function (data, textStatus, jqXHR) {
			if (data.error) {
				onError();
			} else {
				onSuccess(data.id);
			}
		},
		error : function (jqXHR, textStatus, errorThrown) {
			onError();
		}
	});
	
}
//������� �������� ���������
function deleteParamValue(paramId, onSuccess, onError) {
	if (onSuccess == undefined) onSuccess = function(id) {};
	if (onError == undefined) onError = function() {};
	
	$.ajax({
		url: 'ajax.php?method=deleteparam',
		type : 'POST',
		dataType : 'json',
		data : {"param_id" : paramId},
		beforeSend : function () {},
		success : function (data, textStatus, jqXHR) {
			if (data.error) {
				onError();
			} else {
				onSuccess();
			}
		},
		error : function (jqXHR, textStatus, errorThrown) {
			onError();
		}
	});
	
}

String.prototype.translit = (function(){
    var L = {
'�':'A','�':'a','�':'B','�':'b','�':'V','�':'v','�':'G','�':'g',
'�':'D','�':'d','�':'E','�':'e','�':'Yo','�':'yo','�':'Zh','�':'zh',
'�':'Z','�':'z','�':'I','�':'i','�':'Y','�':'y','�':'K','�':'k',
'�':'L','�':'l','�':'M','�':'m','�':'N','�':'n','�':'O','�':'o',
'�':'P','�':'p','�':'R','�':'r','�':'S','�':'s','�':'T','�':'t',
'�':'U','�':'u','�':'F','�':'f','�':'Kh','�':'kh','�':'Ts','�':'ts',
'�':'Ch','�':'ch','�':'Sh','�':'sh','�':'Sch','�':'sch','�':'','�':'',
'�':'Y','�':'y','�':"",'�':"",'�':'E','�':'e','�':'Yu','�':'yu',
'�':'Ya','�':'ya', ' ':'-',
'/':'-', '\\\\' : '-', '%': '-', ':' : '-',
';' : '-', '#' : '-', '?' : '-'
        },
        r = '',
        k;
    for (k in L) r += k;
    r = new RegExp('[' + r + ']', 'g');
    k = function(a){
        return a in L ? L[a] : '';
    };
    return function(){
        return this.replace(r, k);
    };
})();