<?php
include ("../connect.php");
include ("../../func/core.php");

if ($_GET['del'] || $_POST['menu_add']) {
	include('sql.php');
}


$cssOl = true;
include ("../up.php");

?>
<script type="text/javascript" src="/js/jquery-ui-1.8.13.js"></script>
<script type="text/javascript">
	$(function() {
		$('#menu-items').sortable({
			connectWith: "div.menu-item .header",
			cancel: ".cl",
			tolerance : "pointer",
			update : function (event, ui) {
				saveMenuOrder();
			}
		}).disableSelection();
		
		var sortableIn = 0;
		$('.cat-items').sortable({
			//connectWith: ".cat-items",
			items : ".cat-item",
			tolerance : "pointer",
			cancel: ".cl",
			update : function (event, ui) {
				var catId = ui.item.data('cat_id');
				var count = 0;
				if (catId != '0') {
					ui.item.closest('.cat-items').
						children('.cat-item').
						each(function (index, element){
							if ($(element).data('cat_id') == catId) {
								count++;
							}
							if (count > 1) {
								ui.item.remove();
								return false;
							}
						});
				}
				if (count <= 1)	saveMenuItemsOrder(ui.item.closest('.cat-items'));
			},
			receive: function(e, ui) {
				sortableIn = 1;
			},
			over: function(e, ui) {
				sortableIn = 1;
				ui.item.css('opacity', 1);
			},
			out: function(e, ui) {
				sortableIn = 0;
				ui.item.css('opacity', .5);
			},
			stop : function(e, ui) {
				ui.item.css('opacity', 1);
			},
			beforeStop: function(e, ui) {
			   if (sortableIn == 0) { 
					var id = ui.item.data('id');
					ui.item.remove(); 
					removeMenuItem(id);
			   }
			}
		}).disableSelection();
		
		$( "#all-cats-list .cat-item" ).draggable({
			connectToSortable: ".cat-items",
			helper: "clone",
			revert: "invalid",
			stop : function (event, ui) {
				//alert(ui.helper);
			}
		});
		
		refreashCatsCounter();
	});
	//������ ������� ���������� ����
	function saveMenuOrder() {
		var orderList = new Array();
		
		$('#menu-items .menu-item').each(function (index, element) {
			orderList.push($(element).data('id'));
		});
		
		$.ajax({
			url: 'ajax.php?method=setmenuorder',
			type : 'POST',
			dataType : 'json',
			data : {"order_list" : orderList},
			//beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {}
			//error : function (jqXHR, textStatus, errorThrown) {}
		});
	}
	//������ ������� ���������� ���������
	function saveMenuItemsOrder(elements) {
		var items = new Array();
		var menuId = elements.closest('.menu-item').data('id');
		
		elements.children('.cat-item').each(function (index, element) {
			items.push({
				"id" : $(element).data('id'),
				"cat_id" : $(element).data('cat_id')
			});
		});
		
		$.ajax({
			url: 'ajax.php?method=setmenuitemsorder',
			type : 'POST',
			dataType : 'json',
			data : {"menu_id": menuId, "order_list" : items},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {
				if (!data.error) {
					//�������� "new" �� ��������
					elements.children('.cat-item').each(function (index, element){
						var id = $(element).data('id');
						var catId = $(element).data('cat_id');
						if (id == 'new') {
							//����� ������� ����������� ��������� �������?
							for (var i in data.update) {
								if (data.update[i].cat_id == catId) {
									$(element).data('id', data.update[i].id);
									//hack. � ������� ���� �� �������� ������-��
									$(element).attr('data-id', data.update[i].id);
									return false;
									//break;
								}
							}
						}
					});
				}
			},
			error : function (jqXHR, textStatus, errorThrown) {
				
			}
		});
		refreashCatsCounter();
	}
	//������� ��������� � ������ ����
	function removeMenuItem(itemId) {
		$.ajax({
			url: 'ajax.php?method=deletemenuitem',
			type : 'POST',
			dataType : 'json',
			data : {"cat_item_id": itemId},
			beforeSend : function () {},
			success : function (data, textStatus, jqXHR) {

			},
			error : function (jqXHR, textStatus, errorThrown) {
				
			}
		});
		refreashCatsCounter();
	}
	function areYouSure() {
		return confirm('������� ��� ������ ������� ���� ����?');
	}
	
	//�������� ������� ������������� ��������� � ����
	function refreashCatsCounter() {
		$('#all-cats-list .cat-item').each(function(i, element) {
			var counter = 0;
			var catId = $(element).data('cat_id');
			//alert(catId);
			$('#menu-items .cat-item').each(function(i, el) {
				if ($(el).data('cat_id') == catId) counter++;
			})
			
			if (counter > 0) {
				$(element).find('.counter').text(counter).addClass('counter-style');
			} else {
				$(element).find('.counter').text('').removeClass('counter-style');
			}
		});
	}
</script>
<table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td class="menu-editor">
			<div class="ol">
				<h2>�������� ����� ����� ����</h2>
				<form action="#menu-items" method="post">
					<input type="text" name="menu_name" />
					<input type="submit" name="menu_add" value="��������" />
				</form>
			</div>
			<div id="all-cats"> 
				<div class="header">
					<div style="margin-bottom:20px; margin-top:30px;" align="left">
						<table border="0" cellspacing="0" cellpadding="0" >
							<tr>
								<td><img src="/img/oglleft.jpg" width="14" height="35"></td>
								<td class="oglrast"><a href="#"><div style="margin-left:10px; margin-right:10px;">��� ���������</div></a></td>
								<td><img src="/img/oglright.jpg" width="14" height="35"></td>
							</tr>
						</table>
						<img src="/img/ogln.jpg" width="59" height="7">
					</div>
				</div>
				<div <?php /* class="cat-items" */?> id="all-cats-list" >
					<?php
						foreach (getCats() as $cat) { ?>
							<div class="cat-item" data-cat_id="<?=$cat['id']?>" data-id="new">
								<div class="counter"></div>
								<div><img src="<?=getImageWebPath('cats').$cat['id']?>.jpg" style="height: 80px;" /></div>
								<div><?=$cat['title']?></div>
							</div>
							<?php if (isset($cat['subcats'])) {
									foreach ($cat['subcats'] as $subcat) { ?>
										<div class="cat-item" data-cat_id="<?=$subcat['id']?>" data-id="new">
											<div class="counter"></div>
											<div><img src="<?=getImageWebPath('cats').$subcat['id']?>.jpg" style="height: 80px;" /></div>
											<div><?=$subcat['title']?></div>
										</div>
							<?php }
							}?>
					<?php
						}
					?>
					<div class="cat-item" data-cat_id="0" data-id="new">
						<div class="counter"></div>
						<div class="separator"></div>
						<div>�����������</div>
					</div>
				</div>
			</div>
			<div class="cl">&nbsp;</div>
			<div id="menu-items">
				<?php foreach (getMenu(true) as $item) { ?>
						<div class="menu-item" data-id="<?=$item['id'];?>">
							<div class="header">
								<div style="margin-bottom:20px; margin-top:10px;" align="left">
									<table border="0" cellspacing="0" cellpadding="0" >
										<tr>
											<td><img src="/img/oglleft.jpg" width="14" height="35"></td>
											<td class="oglrast">
												<div style="margin-left:10px; margin-right:10px;">
												<a href="/edit/m_menuleft/edit.php?id=<?php echo $item['id']; ?>">
														<?php echo $item['title']?>
												</a>
												&nbsp;
												<a href="?del=<?=$item['id']?>#menu-items" onclick="return areYouSure();">
													<img style="vertical-align: middle;" src="/img/dopfotodel.png" />
												</a>
												</div>	
											</td>
											<td><img src="/img/oglright.jpg" width="14" height="35"></td>
										</tr>
									</table>
									<img src="/img/ogln.jpg" width="59" height="7">
								</div>
							</div>
						
							<div class="cat-items">
								<?php if (count($item['submenu']) > 0) { ?>
									<?php foreach ($item['submenu'] as $submenu) { ?>
										<? if ($submenu['separator']) { ?>
											<div class="cat-item" data-id="<?php echo $submenu['id']; ?>" data-cat_id="0">
												<div class="separator"></div>
												<div>�����������</div>
											</div>
										<? 
											continue;
											}
										?>
										<div class="cat-item" data-id="<?php echo $submenu['id']; ?>" data-cat_id="<?php echo $submenu['cat_id']; ?>">
											<div><img src="<?php echo getImageWebPath('cats').$submenu['cat_id']; ?>.jpg" style="height: 80px;" /></div>
											<div><?php echo $submenu['title']; ?></div>
										</div>
									<?php } ?>
								<?php } ?>
							</div>
							<div class="cl"></div>
						</div>
				<?php } ?>
			</div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>