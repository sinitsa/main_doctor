/*
 * CKFinder
 * ========
 * http://ckfinder.com
 * Copyright (C) 2007-2012, CKSource - Frederico Knabben. All rights reserved.
 *
 * The software, this file, and its contents are subject to the CKFinder
 * License. Please read the license.txt file before using, installing, copying,
 * modifying, or distributing this file or part of its contents. The contents of
 * this file is part of the Source Code of CKFinder.
 *
 */

/**
 * @fileOverview Defines the {@link CKFinder.lang} object for the Russian
 *		language.
 */

/**
 * Contains the dictionary of language entries.
 * @namespace
 */
CKFinder.lang['ru'] =
{
	appTitle : 'CKFinder',

	// Common messages and labels.
	common :
	{
		// Put the voice-only part of the label in the span.
		unavailable		: '%1<span class="cke_accessibility">, ����������</span>',
		confirmCancel	: '��������� ���� ��������� ����� �������. �� �������?',
		ok				: 'OK',
		cancel			: '������',
		confirmationTitle	: '�������������',
		messageTitle	: '����������',
		inputTitle		: '������',
		undo			: '��������',
		redo			: '���������',
		skip			: '����������',
		skipAll			: '���������� ���',
		makeDecision	: '��� ������� �������?',
		rememberDecision: '��������� ��� �����'
	},


	// Language direction, 'ltr' or 'rtl'.
	dir : 'ltr',
	HelpLang : 'en',
	LangCode : 'ru',

	// Date Format
	//		d    : Day
	//		dd   : Day (padding zero)
	//		m    : Month
	//		mm   : Month (padding zero)
	//		yy   : Year (two digits)
	//		yyyy : Year (four digits)
	//		h    : Hour (12 hour clock)
	//		hh   : Hour (12 hour clock, padding zero)
	//		H    : Hour (24 hour clock)
	//		HH   : Hour (24 hour clock, padding zero)
	//		M    : Minute
	//		MM   : Minute (padding zero)
	//		a    : Firt char of AM/PM
	//		aa   : AM/PM
	DateTime : 'dd.mm.yyyy H:MM',
	DateAmPm : ['AM', 'PM'],

	// Folders
	FoldersTitle	: '�����',
	FolderLoading	: '��������...',
	FolderNew		: '����������, ������� ����� ��� �����: ',
	FolderRename	: '����������, ������� ����� ��� �����: ',
	FolderDelete	: '�� �������, ��� ������ ������� ����� "%1"?',
	FolderRenaming	: ' (��������������...)',
	FolderDeleting	: ' (������...)',

	// Files
	FileRename		: '����������, ������� ����� ��� �����: ',
	FileRenameExt	: '�� �������, ��� ������ �������� ���������� �����? ���� ����� ����� �����������.',
	FileRenaming	: '��������������...',
	FileDelete		: '�� �������, ��� ������ ������� ���� "%1"?',
	FilesLoading	: '��������...',
	FilesEmpty		: '������ �����',
	FilesMoved		: '���� %1 ��������� � %2:%3.',
	FilesCopied		: '���� %1 ���������� � %2:%3.',

	// Basket
	BasketFolder		: '�������',
	BasketClear			: '�������� �������',
	BasketRemove		: '������ �� �������',
	BasketOpenFolder	: '������� � ����� ����� �����',
	BasketTruncateConfirm : '�� ����� ������ �������� �������?',
	BasketRemoveConfirm	: '�� ����� ������ ������ ���� "%1" �� �������?',
	BasketEmpty			: '� ������� ���� ��� ������, �������� ����� � ������� ����-�-����� (���������� ���� � �������).',
	BasketCopyFilesHere	: '����������� ���� �� �������',
	BasketMoveFilesHere	: '����������� ���� �� �������',

	BasketPasteErrorOther	: '��������� ������ ��� ��������� ����� %s: %e',
	BasketPasteMoveSuccess	: '����� ����������: %s',
	BasketPasteCopySuccess	: '����� �����������: %s',

	// Toolbar Buttons (some used elsewhere)
	Upload		: '��������� ����',
	UploadTip	: '��������� ����� ����',
	Refresh		: '�������� ������',
	Settings	: '���������',
	Help		: '������',
	HelpTip		: '������',

	// Context Menus
	Select			: '�������',
	SelectThumbnail : '������� ���������',
	View			: '����������',
	Download		: '���������',

	NewSubFolder	: '����� �����',
	Rename			: '�������������',
	Delete			: '�������',

	CopyDragDrop	: '����������',
	MoveDragDrop	: '�����������',

	// Dialogs
	RenameDlgTitle		: '�������������',
	NewNameDlgTitle		: '����� ���',
	FileExistsDlgTitle	: '���� ��� ����������',
	SysErrorDlgTitle : '��������� ������',

	FileOverwrite	: '�������� ����',
	FileAutorename	: '������������� ���������������',

	// Generic
	OkBtn		: '��',
	CancelBtn	: '������',
	CloseBtn	: '�������',

	// Upload Panel
	UploadTitle			: '��������� ����� ����',
	UploadSelectLbl		: '������� ���� ��� ��������',
	UploadProgressLbl	: '(�������� � ��������, ���������� ���������...)',
	UploadBtn			: '��������� ��������� ����',
	UploadBtnCancel		: '������',

	UploadNoFileMsg		: '����������, �������� ���� �� ����� ����������.',
	UploadNoFolder		: '����������, �������� �����, � ������� �� ������ ��������� ����.',
	UploadNoPerms		: '�������� ������ ���������.',
	UploadUnknError		: '������ ��� �������� �����.',
	UploadExtIncorrect	: '� ��� ����� ������ ��������� ����� � ����� �����������.',

	// Flash Uploads
	UploadLabel			: '����� ��� ��������',
	UploadTotalFiles	: '����� ������:',
	UploadTotalSize		: '����� ������:',
	UploadSend			: '��������� ����',
	UploadAddFiles		: '�������� �����',
	UploadClearFiles	: '��������',
	UploadCancel		: '�������� ��������',
	UploadRemove		: '������',
	UploadRemoveTip		: '������ !f',
	UploadUploaded		: '��������� !n%',
	UploadProcessing	: '��������...',

	// Settings Panel
	SetTitle		: '���������',
	SetView			: '������� ���:',
	SetViewThumb	: '���������',
	SetViewList		: '������',
	SetDisplay		: '����������:',
	SetDisplayName	: '��� �����',
	SetDisplayDate	: '����',
	SetDisplaySize	: '������ �����',
	SetSort			: '����������:',
	SetSortName		: '�� ����� �����',
	SetSortDate		: '�� ����',
	SetSortSize		: '�� �������',
	SetSortExtension		: '�� ����������',

	// Status Bar
	FilesCountEmpty : '<������ �����>',
	FilesCountOne	: '1 ����',
	FilesCountMany	: '%1 ������',

	// Size and Speed
	Kb				: '%1 K�',
	Mb				: '%1 MB', // MISSING
	Gb				: '%1 GB', // MISSING
	SizePerSecond	: '%1/s', // MISSING

	// Connector Error Messages.
	ErrorUnknown	: '���������� ��������� ������. (������ %1)',
	Errors :
	{
	 10 : '�������� �������.',
	 11 : '��� ������� �� ������ � �������.',
	 12 : '�������� ����������� ��� �������.',
	102 : '�������� ��� ����� ��� �����.',
	103 : '���������� ��������� ������ ��-�� ����������� �����������.',
	104 : '���������� ��������� ������ ��-�� ����������� ���������� �������� �������.',
	105 : '�������� ���������� �����.',
	109 : '�������� ������.',
	110 : '����������� ������.',
	115 : '���� ��� ����� � ����� ������ ��� ����������.',
	116 : '����� �� �������. ����������, �������� ��� ����� � ���������� ��� ���.',
	117 : '���� �� ������. ����������, �������� ������ ������ � ���������� ��� ���.',
	118 : '�������� ������������ ����� ��������� � ���������.',
	201 : '���� � ����� ������ ��� ����������. ����������� ���� ��� ������������ � "%1".',
	202 : '�������� ����.',
	203 : '�������� ����. ������ ����� ������� �������.',
	204 : '����������� ���� ���������.',
	205 : '���������� ��������� ����� ��� �������� ������ �� ������.',
	206 : '�������� �������� ��-�� ����������� ������������. ���� �������� ������� �� HTML ������.',
	207 : '����������� ���� ��� ������������ � "%1".',
	300 : '��������� ������ ��� ����������� �����(��).',
	301 : '��������� ������ ��� ����������� �����(��).',
	500 : '������� ������ �������� ��-�� ����������� ������������. ����������, �������� ������ ���������� ������������� � ��������� ���������������� ���� CKFinder.',
	501 : '��������� �������� ���������.'
	},

	// Other Error Messages.
	ErrorMsg :
	{
		FileEmpty		: '��� ����� �� ����� ���� ������.',
		FileExists		: '���� %s ��� ����������.',
		FolderEmpty		: '��� ����� �� ����� ���� ������.',

		FileInvChar		: '��� ����� �� ����� ��������� ����� �� ������������� ��������: \n\\ / : * ? " < > |',
		FolderInvChar	: '��� ����� �� ����� ��������� ����� �� ������������� ��������: \n\\ / : * ? " < > |',

		PopupBlockView	: '���������� ������� ���� � ����� ����. ����������, ��������� ��������� �������� � ��������� ���������� ����������� ���� ��� ����� �����.',
		XmlError		: '������ ��� ������� XML-������ �������.',
		XmlEmpty		: '���������� ��������� XML-����� �������, �������� ������ ������.',
		XmlRawResponse	: '�������������� ����� �������: %s'
	},

	// Imageresize plugin
	Imageresize :
	{
		dialogTitle		: '�������� ������� %s',
		sizeTooBig		: '������ ��������� ������� ������, ��� � ������������� ����� (%size).',
		resizeSuccess	: '������� ������� ��������.',
		thumbnailNew	: '������� ���������(�)',
		thumbnailSmall	: '��������� (%s)',
		thumbnailMedium	: '������� (%s)',
		thumbnailLarge	: '������� (%s)',
		newSize			: '���������� ����� �������',
		width			: '������',
		height			: '������',
		invalidHeight	: '������ ������ ���� ������ ������ ����.',
		invalidWidth	: '������ ������ ���� ������ ������ ����.',
		invalidName		: '�������� ��� �����.',
		newImage		: '��������� ��� ����� ����',
		noExtensionChange : '�� ������� �������� ���������� �����.',
		imageSmall		: '�������� �������� ������� ���������.',
		contextMenuName	: '�������� ������',
		lockRatio		: '��������� ���������',
		resetSize		: '������� ������� �������'
	},

	// Fileeditor plugin
	Fileeditor :
	{
		save			: '���������',
		fileOpenError	: '�� ������� ������� ����.',
		fileSaveSuccess	: '���� ������� ��������.',
		contextMenuName	: '�������������',
		loadingFile		: '���� �����������, ���������� ���������...'
	},

	Maximize :
	{
		maximize : '����������',
		minimize : '��������'
	},

	Gallery :
	{
		current : 'Image {current} of {total}' // MISSING
	}
};
