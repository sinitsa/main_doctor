/*
Copyright (c) 2003-2010, CKSource - Frederico Knabben. All rights reserved.
For licensing, see LICENSE.html or http://ckeditor.com/license
*/

CKEDITOR.editorConfig = function( config )
{
	// Define changes to default configuration here. For example:
	 config.language = 'ru';
	// config.uiColor = '#AADC6E';
	
	config.toolbar = 'Basic'; //функциональность редактора, Basic-минимум, Full-максимум
	config.toolbar_Basic = //индивидуальная настройка режима Basic
	[
['Source','-', 'Bold', 'Italic','Underline', '-', 'Link', 'Unlink','-','NumberedList','BulletedList','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','Image','Table','HorizontalRule','SpecialChar', 'Format']

	];

};