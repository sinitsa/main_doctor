<?php
extract($_GET);
include ("connect_mysqli.php");
$table_name = 'delivery';

if (isset($newRow)) { // INSERT
	$columns = '`'.implode('`,`', array_keys($fields)).'`'; // `field_name`,
	$values = array_fill(0, count($fields)+1, '?');
	$values = implode(',', $values);
	$sql = "INSERT INTO `$table_name` ($columns, `id`) VALUES($values)";
} else { // UPDATE
	$sql = "UPDATE `$table_name` SET ";
	$set = array();
	foreach ($fields as $name => $value) $set[] = "`$name`=?";
	$sql .= implode(',', $set);
	$sql .= ' WHERE `id`=?';
}

if ($stmt = $mysqli->prepare($sql)) {
	$fields[] = $id;
	$types = str_repeat('i', count($fields));
	$refArr[] = &$types;
	foreach ($fields as &$value) $refArr[] = $value;
	$ref    = new ReflectionClass('mysqli_stmt'); 
	$method = $ref->getMethod("bind_param"); 
	$method->invokeArgs($stmt, $refArr); 
	$result = $stmt->execute();

	$response = array('result'=>$result);
	if ($newRow) $response['removeNewRow'] = $id;
	echo json_encode($response);
}


?>