<?
include ("../connect.php");
include ("../../func/core.php");
include ("../up.php");


?>
<script src='/edit/css/nprogress/nprogress.js'></script>
<link rel='stylesheet' href='/edit/css/nprogress/nprogress.css'/>
<style>
	.leftArea{
		float: left;
		display: inline-block;
		width: 300px;
		height: 300px;
		overflow: auto;
		box-shadow: 7px 0px 9px -8px black;
	}
	.leftArea input{
		padding: 0;
		margin: 0;
		background: rgba(0,0,0,0);
		border: solid 1px rgba(0,0,0,0);
		box-shadow: none;
		padding: 0 3px;
	}
	.leftArea .page{
		padding: 5px 20px;
		cursor: pointer;
		position: relative;
	}
	.leftArea .page .icon-remove{
		position: absolute;
		top: 8px;
		right: 7px;
	}
	.leftArea .page.remove{
		background: orange;
	}
	.leftArea .page:hover{
		background: rgb(219, 118, 118);
	}
	.leftArea .page:hover input{
		color: #fff;
	}
	.leftArea .page input:hover{
		border: solid 1px rgba(255,255,255,0.5);
	}
	.leftArea .page.active{
		background: rgb(162, 90, 90);
	}
	.leftArea .page.active input{
		color: #fff;
	}
	.rightArea{
		float: left;
		display: inline-block;
		width: calc(100% - 300px);
		height: 300px;
		overflow: auto;
	}
	.rightArea .lookThis{
		position: absolute;
		right: 100px;
		top: 10px;
	}
	.addPage{
		width: 298px;
		position: fixed;
		bottom: 0;
		left: 0;
		display: inline-block;
		height: 38px;
		color: #fff;
		background: rgb(35, 121, 35);
		cursor: pointer;
		text-align: center;
		line-height: 38px;
		vertical-align: middle;
		border: solid 1px #ebebeb;
		
		-webkit-transition: all 0.4s ease-out;
		-moz-transition: all 0.4s ease-out;
		-ms-transition: all 0.4s ease-out;
		-o-transition: all 0.4s ease-out;
		transition: all 0.4s ease-out;
	}
	.addPage:hover{
		background: rgb(87, 206, 87);
		color: #000;
	}
	
	.rightArea .pageInfo{
		display: none;
		padding: 10px 20px;
	}
</style>
<script>
	$(document).ready(function(){
		resizeLeft();
		
		$('.addPage').on('click',function(){
			$('.leftArea').append('<div class="page" data-id="new"><input type="text" data-val="" value="����� ��������" /><i class="icon-remove"></i></div>');
			$('.leftArea .page').removeClass('active');
			$('.leftArea .page').last().addClass('active').find('input').focus().select();
		});
		$('.leftArea').on('blur','.page input',function(){
			var text = $(this).val();
			console.log(text);
			if( text!= $(this).data('val') ){
				while( text.indexOf(' ') != -1 ){
					text = text.replace(' ','');
				}
				if( text!='' ){
					text = $(this).val();
					// ��������� �������� ��� ���� id=new ��������� �����
					var id = $(this).closest('.page').data('id');
					var elem = $(this);
					$.ajax({
						url : "ajax.php?method=pageAction",
						type : "POST",
						data: 'id='+id+'&title='+text,
						beforeSend: function() {
							NProgress.start();
						},
						success : function (data) {
							if( data=='chpu' ){
								alert('����� ��� ��� ����');
							}else{
								data = JSON.parse(data);
								elem.data('val',text);
								if(id=='new'){
									elem.closest('.page').data('id',data.id);
									var str = '<div id="p'+data.id+'" data-id="'+data.id+'" class="pageInfo">\
													<div>���</div>\
													<div style="position:relative;"><select><option value="0">������</option><option value="1">�������</option><option value="2">��������</option></select><div class="lookThis"><a href="/page/'+data.chpu+'.html" target="_black">���������� �� �����</a></div></div>\
													<div>seo title:</div>\
													<div><input type="text" data-field="seo_title" data-val="" value="" /></div>\
													<div>seo description:</div>\
													<div><input type="text" data-field="seo_des" data-val="" value="" /></div>\
													<div>seo keywords:</div>\
													<div><input type="text" data-field="seo_key" data-val="" value="" /></div>\
													<div>chpu:</div>\
													<div><input type="text" data-field="chpu" class="chpuField" data-val="'+data.chpu+'" value="'+data.chpu+'" /></div>\
													<div>id �������:</div>\
													<div><input type="text" data-field="chpu" class="prodId" data-val="" value="" /> <input type="text" data-field="chpu" class="prodId" data-val="" value="" /> <input type="text" data-field="chpu" class="prodId" data-val="" value="" /></div>\
													<div>����</div>\
													<div><input type="text" data-field="price" data-val="" value="" /></div>\
													<div>������ 1 (��������� �����)</div>\
													<div><input class="links linksLink" type="text" placeholder="������" data-val="" value="" /></div>\
													<div>��������� �����������</div>\
													<div><input class="links linksLink" type="text" placeholder="������" data-val="" value="" /></div>\
													<div>������ 3</div>\
													<div><input class="links linksText" type="text" placeholder="�����" data-val="" value="" /> <input class="links linksLink" type="text" placeholder="������" data-val="" value="" /></div>\
													<div>���������� �����������</div>\
													<div><input class="links linksLink" type="text" placeholder="������" data-val="" value="" /></div>\
												</div>';
									$('.rightArea').append(str);
									elem.closest('.page').click();
									$('.rightArea').scrollTop(0);
								}
							}
						},
						complete : function() {
							NProgress.done();
						},
						error : function () {
						}
					});
				}else{
					if( $(this).data('val')=='' ){
						$(this).closest('.page').remove();
					}else{
						$(this).val( $(this).data('val') );
					}
				}
			}
		});
		$('.leftArea').on('click','.page',function(){
			$('.leftArea .page').removeClass('active');
			$(this).addClass('active');
			$('.rightArea .pageInfo').hide().removeClass('visible');
			$('#p'+$(this).data('id')).show().addClass('visible');
		});
		$('.leftArea .page input').on('focus',function(){
			$('.leftArea .page').removeClass('active');
			$(this).closest('.page').addClass('active');
		});
		
		$('.rightArea').on('blur','input',function(){
			if( $(this).val()!=$(this).data('val') ){
				var elem = $(this);
				var id = elem.closest('.pageInfo').data('id');
				if( elem.hasClass('prodId') ){
					var field = 'products';
					var val = $('.pageInfo.visible .prodId').eq(0).val()+','+$('.pageInfo.visible .prodId').eq(1).val()+','+$('.pageInfo.visible .prodId').eq(2).val();
				}else if( elem.hasClass('links') ){
					var field = 'links';
					var val = 'back|'+$('.pageInfo.visible .linksLink').eq(0).val()+',nextOff|'+$('.pageInfo.visible .linksLink').eq(1).val()+','+$('.pageInfo.visible .linksText').eq(0).val()+'|'+$('.pageInfo.visible .linksLink').eq(2).val()+',prevOff|'+$('.pageInfo.visible .linksLink').eq(3).val();
				}else{
					var field = elem.data('field');
					var val = elem.val();
				}
				$.ajax({
					url : "ajax.php?method=pageEdit",
					type : "POST",
					data: 'id='+id+'&field='+field+'&val='+val,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						if( data=='chpu' ){
							alert('����� ��� ��� ����');
							elem.val( elem.data('val') );
						}else{
							if(field=='chpu'){
								elem.closest('.pageInfo').find('.lookThis a').attr('href','/page/'+val+'.html');
							}else if(field=='seo_title' && val!=''){
								elem.closest('.pageInfo').find('.chpuField').val(data).data('val',data);
								elem.closest('.pageInfo').find('.lookThis a').attr('href','/page/'+val+'.html');
							}
							elem.data('val',val);
						}
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('.rightArea').on('change','select',function(){
				var elem = $(this);
				var id = elem.closest('.pageInfo').data('id');
				var field = 'type';
				var val = $(this).val();
				$.ajax({
					url : "ajax.php?method=pageEdit",
					type : "POST",
					data: 'id='+id+'&field='+field+'&val='+val,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
						
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
		});
		$('.leftArea').on('click','.icon-remove',function(){
			if(confirm('������� ��������?')){
				var id = $(this).closest('.page').data('id');
				var elem = $(this);
				$.ajax({
					url : "ajax.php?method=pageDelete",
					type : "POST",
					data: 'id='+id,
					beforeSend: function() {
						NProgress.start();
					},
					success : function (data) {
					console.log('#p'+elem.closest('.page').data('id'));
						$('#p'+elem.closest('.page').data('id')).remove();
						elem.closest('.page').remove();
						$('.leftArea .page').click();
					},
					complete : function() {
						NProgress.done();
					},
					error : function () {
					}
				});
			}
		});
		$('.leftArea').on('mouseover','.icon-remove',function(){
			$('.leftArea .icon-remove').closest('.page').removeAttr('style');
			$(this).closest('.page').css('background','rgb(204, 167, 99)');
		});
		$('.leftArea').on('mouseout','.icon-remove',function(){
			$('.leftArea .page').removeAttr('style')
		});
		
		$('.leftArea .page').eq(0).click();
	});
	window.onresize = function(){ resizeLeft(); };
	function resizeLeft(){
		$('.leftArea').height( $(window).height() - $('.addPage').height() - 138 );
		$('.rightArea').height( $(window).height() - 138 );
	}
</script>

<div class='leftArea'>
	<?php
		$arr = array();
		$q = mysql_query('select * from `pages` where `banner`=1 order by `id`');
		while($r = mysql_fetch_array($q)){
			$price = explode(',', $r['products']);
			$links = unserialize($r['links']);
			echo '<div class="page" data-id="',$r['id'],'"><input type="text" data-val="',$r['title'],'" value="',$r['title'],'" /><i class="icon-remove"></i></div>';
			$str = '<div id="p'.$r['id'].'" data-id="'.$r['id'].'" class="pageInfo">
				<div>���</div>
				<div style="position:relative;"><select><option value="0"';
			if($r['type']==0){
				$str .= ' selected="selected"';
			}
			$str .= '>������</option><option value="1" ';
			if($r['type']==1){
				$str .= ' selected="selected"';
			}
			$str .= '>�������</option><option value="2" ';
			if($r['type']==2){
				$str .= ' selected="selected"';
			}
			$str .= '>��������</option></select><div class="lookThis"><a href="/page/'.$r['chpu'].'.html" target="_black">���������� �� �����</a></div></div>
				
				<div>seo title:</div>
				<div><input type="text" data-field="seo_title" data-val="'.$r['seo_title'].'" value="'.$r['seo_title'].'" /></div>
				
				<div>seo description:</div>
				<div><input type="text" data-field="seo_des" data-val="'.$r['seo_des'].'" value="'.$r['seo_des'].'" /></div>
				
				<div>seo keywords:</div>
				<div><input type="text" data-field="seo_key" data-val="'.$r['seo_key'].'" value="'.$r['seo_key'].'" /></div>
				
				<div>chpu:</div>
				<div><input type="text" data-field="chpu" class="chpuField" data-val="'.$r['chpu'].'" value="'.$r['chpu'].'" /></div>
				
				<div>id �������:</div>
				<div><input type="text" data-field="chpu" class="prodId" data-val="'.$price[0].'" value="'.$price[0].'" /> <input type="text" data-field="chpu" class="prodId" data-val="'.$price[1].'" value="'.$price[1].'" /> <input type="text" data-field="chpu" class="prodId" data-val="'.$price[2].'" value="'.$price[2].'" /></div>
				
				<div>����</div>
				<div><input type="text" data-field="price" data-val="'.$r['price'].'" value="'.$r['price'].'" /></div>
				
				<div>������ 1 (��������� �����)</div>
				<div><input class="links linksLink" type="text" placeholder="������" data-val="'.$links[0]['link'].'" value="'.$links[0]['link'].'" /></div>
				
				<div>��������� �����������</div>
				<div><input class="links linksLink" type="text" placeholder="������" data-val="'.$links[1]['link'].'" value="'.$links[1]['link'].'" /></div>
				
				<div>������ 3</div>
				<div><input class="links linksText" type="text" placeholder="�����" data-val="'.$links[2]['text'].'" value="'.$links[2]['text'].'" /> <input class="links linksLink" type="text" placeholder="������" data-val="'.$links[2]['link'].'" value="'.$links[2]['link'].'" /></div>
				
				<div>���������� �����������</div>
				<div><input class="links linksLink" type="text" placeholder="������" data-val="'.$links[3]['link'].'" value="'.$links[3]['link'].'" /></div>
			</div>';
			$arr[] = $str;
		}
	?>
</div>
<div class='rightArea'>
	<?php
		foreach($arr as $a){
			echo $a;
		}
	?>
</div>

<div class="addPage">�������� ��������</div>

</body>
</html>