<?php 
include ("../connect.php");
include ("../../func/core.php");

	$catId = (isset($_GET['cat_id']) && is_numeric($_GET['cat_id'])) ? $_GET['cat_id'] : 0;

	if ($_POST['param_name'] && strlen($_POST['param_name']) > 1 ) {
		$data = array(
			'type' => mysql_real_escape_string($_POST['param_type']),
			'title' => mysql_real_escape_string($_POST['param_name']),
			'unit' => mysql_real_escape_string($_POST['param_unit']),
			'step' => mysql_real_escape_string($_POST['param_step']),
			'show_title' => isset($_POST['param_show_title']) ? 1 : 0,
			'show_input' => isset($_POST['param_show_input']) ? 1 : 0,
			'global' => $catId ? 0 : 1,
			'in_filter' => mysql_real_escape_string($_POST['in_filter']) ? 1 : 0,
			'in_compare' => mysql_real_escape_string($_POST['in_compare']) ? 1 : 0
		);
		
		$setString = getSetString($data);
		//d($setString);
		//��������� ��� ��������
		mysql_query("
			INSERT INTO
				`params_available`
			SET
				{$setString}
			");
		$pid = mysql_insert_id();
		
		//���� �������� ����������� � ��������� (��������� ��������)
		//������� ��� � ����������
		if ($catId > 0)
			linkParamAndCat($pid, $catId);
		
		//��������� ��� ��������� ��������
		foreach ($_POST['av_params'] as $av_name) {
			if (trim($av_name) == '') continue;
			if ($data['type'] == PARAM_RANGE) {
				
				// Was set value into `value_float`
				mysql_query ("
					INSERT INTO
						`params_available_values`
					SET
						`param_id` = '{$pid}',
						`{$value_float}` = '{$av_name}',
						`cat_id` = '{$catId}'
				");
			} else {
				mysql_query ("
					INSERT INTO
						`params_available_values`
					SET
						`param_id` = '{$pid}',
						`value` = '{$av_name}',
						`cat_id` = '{$catId}'
				");
			}
		}
		if ($catId > 0)
			redirect("/edit/m_catalog/list.php?id={$catId}");
		else
			redirect("/edit/m_mcat/");
		die();
	}
	$catInfo = getCatInfo($catId);
include ("../up.php");
?>
<script>
	$(function () {
		$('.av-param:last').live('focus' , function(){
			//������� ������ �������
			//������: �� ��������� ������ ����, ���� �� ��� 2 ������ � ����� �����.
			var emptyInputsCount = 0;
			$('.av-param').each(function (index, element){
				if ( $(element).find('input').val() == '') emptyInputsCount++;
			});
			if (emptyInputsCount < 2) {
				var div = $('<div></div>').addClass('av-param');
				var input = $('<input />').attr('name', 'av_params[]');
				var img = $('<img />').attr('src','/img/zndelete.gif');
				div.append(input).append(img).appendTo('#available-params');
			}
		});
		$('.av-param img').live('click', function () {
			if ( $('.av-param').length > 1) {
				$(this).closest('.av-param').remove();
			}
		});
	});
</script>
<style>
	.av-param img {
		vertical-align: top;
		cursor: pointer;
		margin-left: 3px;
	}
	.super-table td {
		padding: 2px 5px;
	}
	.super-table thead td {
		vertical-align: top;
		border-bottom: 1px solid lightGray;
	}
</style>
 <table width="90%" border="0" align="center" class="txt">
	<tr>
		<td width="10">&nbsp;</td>
		<td>
			<?php if ($catId) { ?>
			<div>���������� ��������� � ��������� <strong>"<?=$catInfo['title'];?>"</strong></div>
			<?php } ?>
			<form action="" method="post">
				<div>
					<table class="super-table">
						<thead>
							<tr>	
								<td><strong>�������� ���������</strong></td>
								<td><strong>��� ���������</strong></td>
								<td><strong>�������<br />���������</strong></td>
								<td><strong>���</strong></td>
								<td><strong>����������<br />���������</strong></td>
								<td><strong>���������� input'�<br />(��� ����������)</strong></td>
								<td><strong>����������<br />� �������</strong></td>
								<td><strong>����������<br />� ���������</strong></td>
							</tr>
						</thead>
						<tr>	
							<td><input type="text" size="20" name="param_name" class="" value="<?=slashes($_POST['param_name'])?>" /></td>
							<td>
								<select name="param_type">
									<option value="set">��������� � ���</option>
									<option value="value">���������� ������</option>
									<option value="field">���� ��� �����</option>
									<option value="range">�������� ��������</option>
								</select>
							</td>
							<td>
								<input type="text" size="15" name="param_unit" class="span1" value="<?=slashes($_POST['param_unit'])?>" /></td>
							</td>
							<td>
								<input type="text" size="15" name="param_step" class="span1" value="<?=slashes($_POST['param_step'])?>" /></td>
							</td>
							<td>
								<input style="margin: -6px 0 0 15px;" type="checkbox" name="param_show_title" checked="checked" />
							</td>
							<td>
								<input style="margin: -6px 0 0 15px;" type="checkbox" name="param_show_input" checked="checked" />
							</td>
							<td>
								<input style="margin: -6px 0 0 15px;" type="checkbox" name="in_filter" checked="checked" />
							</td>
							<td>
								<input style="margin: -6px 0 0 15px;" type="checkbox" name="in_compare" checked="checked" />
							</td>
						</tr>
					</table>
				</div>
				<div><strong>��������� ��������</strong></div>
				<div id="available-params">
					<?php
						if (isset($_POST['av_params'])) { 
							foreach ($_POST['av_params'] as $av_name) {
					?>
						<div class="av-param">
							<input name="av_params[]" value="<?=slashes($av_name)?>" /><img src="/img/zndelete.gif" alt="" />
						</div>
					<?php }
					} else { ?>
					<div class="av-param">
						<input name="av_params[]" value="" /><img src="/img/zndelete.gif" alt="" />
					</div>
					<?php } ?>
				</div>
				<div><input type="submit" class="btn" value="�������� ��������"/></div>
			</form>
			<div>
				<?php $url = $catId ? '/edit/m_catalog/list.php?id=' . $catId : '/edit/m_mcat/' ; ?>
				<a href="<?=$url;?>" class="btn btn-small"><i class="icon-chevron-left"></i> �����</a>
			<div>
		</td>
	</tr>
</table>
<?php include ("../down.php");	?>