<?php
/**
* ������� �������� ����� �������������� �������� ������.
*
* @param int $id ID ������
* @return array $res ������ ���� �������������� ����������
*/
function getProductExtraPhoto($id) {
	//�������������� ����
	$res = array();
	
	$sel = mysql_query("SELECT * FROM `foto` WHERE `catalog_id` = '{$id}' ORDER BY `rang`");
	while ($row = @mysql_fetch_assoc($sel)) {
		$res[] = $row['id'];
	}
	return $res;
}

/**
* ������� �������� ������������� �� ���� ������ � ������� � ��������.
*
* @param int $limit ����������� ���������� �������.
* @param int $offset ���������� ������������ �� ������ �������.
* @param boolean $need_count ���� �������� true �� � ������������ ������ ����� �������� ����� ���������� ������� � ������� � � ��������.
*
* @return array $feedbacks ������ � ��������.
*/
function getShopAndProductFeedbacks($limit = 0, $offset = false, $need_count = false) {
				$limitString = $offset ? 'LIMIT '.$offset.','.$limit : 'LIMIT '.$limit;
				$feedbacks = array();

				$query = "(SELECT `o`.`catalog_id`, `o`.`date`, `o`.`name`, `o`.`comment`, `o`.`answer`, `c`.`title`, `c`.`chpu` 
							FROM `otzyv` AS `o`
							LEFT JOIN `catalog` AS `c` ON `o`.`catalog_id` = `c`.`id`
							WHERE `confirm` = 1
							)
							UNION ALL
							(SELECT null AS `catalog_id`, `s`.`date`, `s`.`name`, `s`.`des` AS `comment`, `s`.`otvet` AS `answer`, null AS `title`, null AS `chpu`  
							FROM `shop_otzyv` AS `s` 
							WHERE `confirm` = 1)
							ORDER BY `date` DESC {$limitString}";
				$sel = mysql_query($query);
				$feebacksArr = array();
				while ($res = mysql_fetch_assoc($sel)) {
					$feedbacksArr[] = array(
						'id' => isset($res['id']) ? $res['id'] : '',
						'catalog_id' => $res['catalog_id'],
						'title' => $res['title'],
						'chpu'	=> $res['chpu'],
						'name' => $res['name'],
						'comment' => $res['comment'],
						'answer' => $res['answer'],
						'date' => date('d.m.Y', $res['date'])
					);
				}
				if ($need_count) {
					$feedbacks['feedbacks'] = $feedbacksArr;
					// ������ � ��������
					$feedbacks['sf_count'] = fetchOne("SELECT COUNT(*) FROM `shop_otzyv` WHERE `confirm` = '1'");
					
					// ������� � �������
					$feedbacks['pf_count'] = fetchOne("SELECT COUNT(*) FROM `otzyv` WHERE `confirm` = '1'");
				} else {
					$feedbacks = $feedbacksArr;
				}
	return $feedbacks; 
}

/**
* ������� �������� ������ �� �������� ����� �������� �� �����
* @param $string - ���������� ������
* @param $length - �� ������� �������� �������� ������
* @return srtin - ��������� ������
*/
function trimming_line($string, $length = 70)
{
	++$length;
	$string = strip_tags($string);
	if ($length && strlen($string) > $length)
	{
		$str = substr($string, 0, $length);
		$pos = strrpos($string, ' ');
		return substr($str, 0, $pos).' �';
	}
    return $string;
}		
?>