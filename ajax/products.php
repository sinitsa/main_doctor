<?php
header('Content-Type: text/html; charset=cp1251');
/*
���� ������� �� ��������� ������ ������� �������� ����.
�� ����� � ��� ������� ����
*/
include('../connect.php');
include('../func/core.php');
include('../hooks/index.php');

// ���� $_GET['action']

switch ($_GET['method']) {
	case 'mcat' :
		$filter = array();
		
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']['cat']) {
			foreach($_REQUEST['filter']['cat'] as $key => $value) {
				if (is_numeric($key)) $filter['cat'][] = $key;
			}
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		if (!isset($filter['cat']) || count($filter['cat']) <= 0) {
			$tempCats = getCatsInMenuItem($_REQUEST['menu_item']);
			foreach ($tempCats as $cat) {
					$filter['cat'][] = $cat['cat_id'];
				}
		}
		$page = array(
			'page' => getPage(),
			'onPage' => Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		//$temp = getProducts(false, $filter, $sort, $page);
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		$p_count = count($temp['products']);
		
		if ($p_count > 0) {
			foreach ($temp['products'] as $product) {
				include('../tpl/element_product_list.tpl.php');
			}
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					$temp['page']['p_count'] = $p_count;
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}
		} else {
			echo '<div style="margin-left: 20px;">��� �������, ��������������� ��������� ��������.</div>';
		}
		break;
	case 'cat' :
		$tpl_path = "../tpl/";
		//������ ��� ������ �������
		$filter = array();
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if (isset($_REQUEST['filter'])) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if (isset($_REQUEST['filter_range'])) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}

		if (isset($_REQUEST['tag'])) {
			foreach($_REQUEST['tag'] as $key => $value) {
				if (is_numeric($key)) $filter['tag'][] = $key;
			}
		}
		$catInfo = getCatInfo($_REQUEST['cat']);
		if ($catInfo['id']) {
			$filter['cat'] = $catInfo['id'];
		}

		$page = array(
			'page' => getPage(),
			'onPage' => isset($_REQUEST['onPage']) ? $_REQUEST['onPage'] : Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		$p_count = count($temp['products']);
		$temp['page']['p_count'] = $p_count;
		$temp['page']['onPage'] = $page['onPage'];
		if ($p_count > 0) {
			$data['products'] = $temp['products'];
			
			include($tpl_path.'cat_element_tile.tpl.php');
			include('../tpl/cat_element_list.tpl.php');
			
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			} 
		} else {
			echo '<div style="margin-left: 20px;">��� �������, ��������������� ��������� ��������.</div>';
			//echo iconv('windows-1251', 'UTF-8', "<div style=\"margin-left: 20px;\">��� �������, ��������������� ��������� ��������.</div>");
		}

		break;
	case 'tag' :
		$tpl_path = "../tpl/";
		$filter = array();
		if (is_numeric($_REQUEST['min_price'])) {
			$filter['>=price_after_discount'] = $_REQUEST['min_price']; 
		}
		if (is_numeric($_REQUEST['max_price'])) {
			$filter['<=price_after_discount'] = $_REQUEST['max_price']; 
		}
		if ($_REQUEST['filter']) {
			foreach($_REQUEST['filter'] as $key => $value) {
				if (is_numeric($key) && $value != '') $filter['params'][] = array('type' => PARAM_VALUE, 'data' => $key);
			}
		}
		if ($_REQUEST['filter_range']) {
			foreach($_REQUEST['filter_range'] as $key => $value) {
				if (is_numeric($key) && $value != '')
					$filter['params'][] = array(
						'type' => PARAM_RANGE,
						'data' => array(
							'id' => $key,
							'min' => $value['min'],
							'max' => $value['max']
						)
					);
			}
		}
		
		$filter['tag'] = is_numeric($_REQUEST['tag']) ? $_REQUEST['tag'] : 0;

		$page = array(
			'page' => getPage(),
			'onPage' => isset($_REQUEST['onPage']) ? $_REQUEST['onPage'] : Config::get('catalog.products_on_page')
		);
		$sort = isset($_REQUEST['sort']) ? $_REQUEST['sort'] : false;
		
		if (isset($filter['params']) && count($filter['params']) > 0) {
			$temp = getProductsWithExtraFilter(false, $filter, $sort, $page);
		} else {
			$temp = getProducts(false, $filter, $sort, $page);
		}
		if (count($temp['products']) > 0) {
			$data['products'] = $temp['products'];
			
			include($tpl_path.'cat_element_tile.tpl.php');
			include('../tpl/cat_element_list.tpl.php');
			
			switch ($_REQUEST['paginator_type']) {
				case '2':
					showPaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				case 'uniq':
					UniqGeneratePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
				default:
					generatePaginator(
						$temp['page'],
						new queryString(array('action' => 'cat')),
						'reloadProducts([page])'
					);
				break;
			}
		} else {
			echo '<div style="margin-left: 20px;">��� �������, ��������������� ��������� ��������.</div>';
		}
		break;
	case 'quick-view':
		$pId = isset($_REQUEST['product_id']) ? intval($_REQUEST['product_id']) : null;
		$product = getProduct($pId);
		//d($product,0);
		include('../tpl/product_quick_view.tpl.php');
		
		
		break;

	case 'autocomplete':
			// Search autocomplete
			
			$string = $_POST['keyword'] ;

			$string = iconv('utf-8', 'windows-1251', $string);
			$keyword = '%'.$string.'%';

			
			$sql = "SELECT `title`,`id`,`chpu` FROM `catalog` WHERE `title` LIKE '%{$string}%' ORDER BY `spec_rang` ASC LIMIT 0, 10";
			$res = mysql_query($sql);
			if($res) {
				$result = '';
				while ($rs = mysql_fetch_assoc($res)) {

					$sugest = str_replace($string, '<b>'.$string.'</b>', $rs['title']);
					$result .='<li  onclick="set_item(\''.str_replace("'", "\'", $rs['title']).'\', \''.getTemplateLink($rs, 'catalog').'\')">'.$sugest.'</li>';
				}
			}
		    
		    echo $result;


		break;
}