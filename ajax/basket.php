<?php
/*
Ажакс для работы с корзиной. Доставочки, скидочки
*/

error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

include('../connect.php');
include('../func/core.php');

switch ($_GET['method']) {
		case 'get_delivery_price_by_terminal' :
			if (isset($_POST['terminal_id']) && !empty($_POST['terminal_id'])) {
				$terminal_id = $_POST['terminal_id'];
				$qiwi_key = Config::get('qiwi_post.key');
				$delivery = file_get_contents( "http://wt.qiwipost.ru/calc?key={$qiwi_key}&id={$terminal_id}" );
				echo json_encode (array('error' => false, 'delivery' => $delivery));
			}
			else 
				echo json_encode (array('error' => true));
		break;
		case 'getPriceByTerminal' :
			if (isset($_POST['terminal_id']) && !empty($_POST['terminal_id'])) {
				if($_POST['size']=='A' || $_POST['size']=='B' || $_POST['size']=='C'){
					$qiwiSize = $_POST['size'];
					$terminal_id = $_POST['terminal_id'];
					$qiwi_key = Config::get('qiwi_post.key');
					$delivery = file_get_contents( "http://wt.qiwipost.ru/calc?key={$qiwi_key}&id={$terminal_id}&size={$qiwiSize}" );
				}else{
					$delivery = 'none';
				}
				
				echo json_encode (array('error' => false, 'delivery' => $delivery));
			}
			else 
				echo json_encode (array('error' => true));
		break;
	case 'get_delivery_and_discount' :
		$products = $_REQUEST['products'];
		
		$idArr = array();
		foreach($products as $p){
			$idArr[] = $p['id'];
		}
		$idStr = '('.implode(', ',$idArr).')';
		$q = mysql_query('select `id`,`height`,`width`,`length` from `catalog` where `id` in '.$idStr);
		$prodArr = array();
		while($r = mysql_fetch_array($q)){
			$prodArr[$r['id']] = $r;
		}
		
		$sizeArr = array(0,0,0);
		foreach($products as $p){
			$tempArr = array($prodArr[$p['id']]['height']*$p['amount'],$prodArr[$p['id']]['width']*$p['amount'],$prodArr[$p['id']]['length']*$p['amount']);
			sort($tempArr);
			$sizeArr[0] += $tempArr[0];
			$sizeArr[1] += $tempArr[1];
			$sizeArr[2] += $tempArr[2];
		}
		$qiwiSize = getQiwiSizeName($sizeArr);
		
		$res = cart::getDeliveryAndDiscount($products);
		$res['qiwiSize'] = $qiwiSize;
		if(isset($_REQUEST['qiwiTerminal']) && $_REQUEST['qiwiTerminal']!=''){
			if($qiwiSize!='none'){
				$res['qiwiDelivery'] = file_get_contents( "http://wt.qiwipost.ru/calc?key=". Config::get('qiwi_post.key')."&id=".$_REQUEST['qiwiTerminal']."&size=".$qiwiSize );
			}else{
				$res['qiwiDelivery'] = 'none';
			}
		}
		
		echo json_encode($res);
		
		/*echo json_encode(array(
				'deliveryPrice' => $deliveryPrice,
				'discount' => $discount['value']
			));
		*/
		
	break;
	
	case 'make_order' :
	//iconv("utf-8", "windows-1251", urldecode($_REQUEST['art'])));
		$cart = isset($_COOKIE['cart_products']) ? $_COOKIE['cart_products'] : array();
		
		//Быстрый заказ?
		$fastOrder = isset($_REQUEST['fast_order']);
		$fastProductId = ( ( isset($_REQUEST['product_id']) && is_numeric($_REQUEST['product_id']) ) ? $_REQUEST['product_id'] : 0 );
		$fastProductArr = false;
		if( $fastProductId == 0 && preg_match("#^[0-9,]+$#",$_REQUEST['product_id']) ){
			$fastProductArr = explode(',', $_REQUEST['product_id']);
		}
		$fastProductAdded = false;

		// Покупка в кредит?
        $isCredit = (isset($_REQUEST['fast_order']) && $_REQUEST['fast_order'] == 1 ) ? true : false ;
		
		// В случае заказа комплекта со скидкой
		$discSetId = ( ( isset($_REQUEST['set_id']) && is_numeric($_REQUEST['set_id']) ) ? $_REQUEST['set_id'] : 0 );
		$discSetName = ( ( isset($_REQUEST['set_name'])) ? schars($_REQUEST['set_name']) : 0 );
		
		// Если это не заказ комплекта добавляем в корзину 
		
			//Распарсим список товаров в корзине
			$productsInCart = array();
			
			$temp = explode('|', $cart);
			foreach ($temp as $t) {
				$productTemp = explode(':', $t);
				if (is_numeric($productTemp[0]) && ($productTemp[0] > 0) && ($productTemp[1] > 0) && is_numeric($productTemp[1])) {
					if ($fastOrder && ($productTemp[0] == $fastProductId) ) {
						//Коли это быстрый заказ. То товар , с карточки которого была нажата кнопка, должен остаться в корзине один (по количеству)
						$productsInCart[ $productTemp[0] ] = array(
							'id' => $productTemp[0],
							'amount' => 1
							);
						$fastProductAdded = true;
					} else {
						$productsInCart[ $productTemp[0] ] = array(
							'id' => $productTemp[0],
							'amount' => $productTemp[1]
							);
					}
				}
			}
			//Коли не было в корзине товара по быстрому заказу, то его туда добавить
			if ($fastOrder && !$fastProductAdded) {
				if( $fastProductArr == false ){
					$productsInCart[ $fastProductId ] = array(
								'id' => $fastProductId,
								'amount' => 1
								);
				}else{
					foreach ($fastProductArr as $p) {
						$productsInCart[ $p ] = array(
								'id' => $p,
								'amount' => 1
								);
					}
				}
			}

			// Заказ комплекта со скидкой
			if ($discSetId){
				// Обнуляем карзину, чтобы скидка преминилась только к товарам из комплекта
				$productsInCart = array();
				$rs = mysql_query("SELECT `product_ids`,`discount_percent` FROM `discounted_set` WHERE `id` = '{$discSetId}' LIMIT 1");
				$p = mysql_fetch_assoc($rs);
				$ids = explode(',',$p['product_ids'] );
				$discount_value = $p['discount_percent'];
				foreach ($ids as $id) {
					$productsInCart[$id] = array (
							'id' => $id,
							'amount' => 1
						);
				}

			}
			
			
		$data = array();

/*
		$data['name'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['name'])) );
		$data['phone'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['phone'])) );
		$data['email'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['email'])) );
		$data['post_index'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['post_index'])) );
		$data['adress'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['adress'])) );
		$data['extra_information'] = schars( iconv("utf-8", "windows-1251", urldecode($_REQUEST['extra_information'])) );
*/
		
		$data['name'] = schars( $_REQUEST['name'] );
		$data['phone'] = schars( $_REQUEST['phone'] );
		$data['email'] = schars( urldecode($_REQUEST['email']) );
		$data['post_index'] = schars( urldecode($_REQUEST['post_index']) );
		$data['adress'] = schars( $_REQUEST['adress'] );
		$data['extra_information'] = schars( $_REQUEST['extra_information'] );		
		$data['delivery_type'] = schars( $_REQUEST['delivery_type'] );
		$data['payment_type'] = schars( $_REQUEST['oplata_type'] );
		$data['backcall'] = isset($_REQUEST['backcall']);
		
		
		//file_put_contents("./basket_debug.txt", var_export($data, TRUE));
		
		if ($fastOrder) {
			$data['extra_information'] = 'Быстрый заказ';
		}

        if ($isCredit) {
            $data['extra_information'] = 'Покупка в кредит';
        }
		
		if ($discSetId) {
			$data['extra_information'] = "Заказ комлекта со скидкой {$discSetName} ID={$discSetId}";
			$data['discounted_set'] =1;
		}

		// Вытаскиваем куку "Откуда пришел пользователь"	
		$data['ref'] = $_COOKIE["ref"];
		
		$orderStatus = makeOrder($productsInCart ,$data, $discount_value);
		$orderInfo = serialize($orderStatus['orderId']);
		setcookie('order_id', $orderInfo, time() + 7200, '/');
		

		ob_start();
		?>
		       <script type="text/javascript">
      rrApiOnReady.push(function() {
   try {
       rrApi.order({
           transaction: 42131,
           items: [
               <?php 
               $leng = count($productsInCart)-1;
               $i = 0;
               foreach ($productsInCart as $pic) {
                $comma = ($i < $leng) ? ',' : '' ;
                $price = mysql_result(mysql_query("SELECT price_after_discount FROM catalog WHERE id = '{$pic['id']}'"), 0);
                echo '{id: ' . $pic['id'] . ', qnt: ' . $pic['amount'] . ', price: ' . $price . '}' . $comma;
                $i++;  
               } 
               ?>
           ]
       });
   } catch(e) {}
    })
    </script> 
		<?php
		$testik = ob_get_contents();
		ob_end_clean();

		if (!isset($orderStatus['error'])) {
			// Обнуляем корзину
			setcookie("total_amount", "", time() + 7200, "/");
			setcookie("cart_products", "", time() + 7200, "/");
			
			if(isset($_REQUEST['qiwi_terminal']) && $_REQUEST['qiwi_terminal']){
				$term = mysql_real_escape_string(htmlspecialchars(trim($_REQUEST['qiwi_terminal'])));
				$query = "UPDATE `orders` SET qiwi_terminal = '{$term}' WHERE id = {$orderStatus['orderId']}";
				mysql_query($query) or die(mysql_error());
			}
			echo json_encode(array(
				'error' => false,
				'order_id' => $orderStatus['orderId'],
				'order_code' => $orderStatus['orderCode'],
				'price' => $orderStatus['price']['price_after_global_discount'],
				'products' => $orderStatus['productsToResponse'],
				'payment' => $data['payment_type'],
				'test' => $testik
			));
		} else {
			echo json_encode(array('error' => true));
		}	
	break;

    case 'wholesaler':

        $data = array();
        $data['name'] =  $_REQUEST['name'] ;
        $data['phone'] = $_REQUEST['phone'];
        $data['email'] = $_REQUEST['email'] ;
        $data['extra_information'] = $_REQUEST['extra_information'];

        $result = makeWholeSalerOrder($data);

        echo json_encode($result);

        break;

	case 'get_products_in_basket':
		$imageType = preg_match('/[a-z]+/i', $_REQUEST['image_type']) ? $_REQUEST['image_type'] : 'medium';
		echo json_encode(getProductsInBasket($imageType, true));
	break;
}