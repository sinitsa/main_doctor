<div class="content cart">
<?php //d($products); ?>
        <table align="center" class="cart-list" cellspacing="0">
            <tbody>
                <tr class="cart-title">
                    <td></td>
                    <td>�����</td>
                    <td>���������</td>
                    <td>����������</td>
                    <td>�����</td>
                </tr>
<?php
    foreach ($products as $product) {
        $price = $product['price_after_discount'] ? $product['price_after_discount'] : $product['price'];
?>
                <tr class="cart-item" data-product_id="<?=$product['id']?>" data-product_price="<?=$price?>">
                    <td class="cp-slide"><img src="<?=getImageWebPath('product_small').$product['id']?>.jpg" width="140" alt="Product name"></td>
                    <td>
                        <div><a class="cart-product-name" data-id="<?=$product['id']?>" href="<?=getTemplateLink($product, 'catalog')?>"><?=$product['title']?></a></div>
                    </td>
                    <td>
                        <span class="cart-product-price" data-price="<?=$price?>"><?=moneyFormat($price)?> ���.</span>
                    </td>
                    <td>
                        <input class="cart-product-amount amount-input" type="text" name="amount" value="<?=$product['amount']?>">
                        <div class="amount-change">
                            <div  data-value="1" class="amount-up"></div>
                            <div  data-value="-1" class="amount-down"></div>
                        </div>
                    </td>
                    <td>
                        <span class="cart-product-price total"><?=moneyFormat($price * $product['amount'])?> ���.</span>
                        <div class="cart-delete"></div>
                    </td>
                </tr>
<?php
    }
?>
            </tbody>
        </table>
        <div class="cart-summary">
            <div class="cart-total">�������� ����� �������:<span><?=moneyFormat($priceSumm)?> ���.</span></div>
            <button class="cart-empty">�������� �������</button>
            <a href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart')?>"><button class="cart-continue">�������� �����</button></a>
        </div>

        <div class="content-suitable">
 <?php/*
            <div class="content-h1">���������� ��� ������</div>
            <div class="content-carousel cc_1">
                <div class="carousel-element">
                    <div class="carousel-product">
                        <a class="cc_4" href="javascript:void(0);">
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                        </a>
                        <a class="quick-look-a" href="javascript:void(0);">
                            <div class="quick-look-button">
                                <span>������� ��������</span>
                            </div>
                        </a>
                        <div class="carousel-product-name">
                            <a href="javascript:void(0);">��������� ������� Beurer MG254</a>
                        </div>
                        <div class="carousel-element-price cart">3000 ���.</div>
                        <div class="element-hover cart">
                            <div class="carousel-other cart">
                                <div class="active" data-tab="tab1"></div>
                                        <div data-tab="tab2"></div>
                                        <div data-tab="tab3">
                                            <span class="htl-amount">17</span>
                                        </div>
                                        <div data-tab="tab4"></div>
                            </div>
                            <div class="eh-fast-order">
                                <a class="de-last-incart" href="javascript:void(0)"></a>
                                <a href="javascript:void(0);" class="header-button-top">������� �����</a>
                            </div>

                            <div class="eh-compare">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                            <div class="eh-like">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-element">
                    <div class="carousel-product">
                        <a class="cc_4" href="javascript:void(0);">
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                        </a>
                        <a class="quick-look-a" href="javascript:void(0);">
                            <div class="quick-look-button">
                                <span>������� ��������</span>
                            </div>
                        </a>
                        <div class="carousel-product-name">
                            <a href="javascript:void(0);">��������� ������� Beurer MG254</a>
                        </div>
                        <div class="carousel-element-price cart">3000 ���.</div>
                        <div class="element-hover cart">
                            <div class="carousel-other cart">
                                <div class="active" data-tab="tab1"></div>
                                        <div data-tab="tab2"></div>
                                        <div data-tab="tab3">
                                            <span class="htl-amount">17</span>
                                        </div>
                                        <div data-tab="tab4"></div>
                            </div>
                            <div class="eh-fast-order">
                                <a class="de-last-incart" href="javascript:void(0)"></a>
                                <a href="javascript:void(0);" class="header-button-top">������� �����</a>
                            </div>

                            <div class="eh-compare">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                            <div class="eh-like">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-element">
                    <div class="carousel-product">
                        <a class="cc_4" href="javascript:void(0);">
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                        </a>
                        <a class="quick-look-a" href="javascript:void(0);">
                            <div class="quick-look-button">
                                <span>������� ��������</span>
                            </div>
                        </a>
                        <div class="carousel-product-name">
                            <a href="javascript:void(0);">��������� ������� Beurer MG254</a>
                        </div>
                        <div class="carousel-element-price cart">3000 ���.</div>
                        <div class="element-hover cart">
                            <div class="carousel-other cart">
                                <div class="active" data-tab="tab1"></div>
                                        <div data-tab="tab2"></div>
                                        <div data-tab="tab3">
                                            <span class="htl-amount">17</span>
                                        </div>
                                        <div data-tab="tab4"></div>
                            </div>
                            <div class="eh-fast-order">
                                <a class="de-last-incart" href="javascript:void(0)"></a>
                                <a href="javascript:void(0);" class="header-button-top">������� �����</a>
                            </div>

                            <div class="eh-compare">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                            <div class="eh-like">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-element">
                    <div class="carousel-product">
                        <a class="cc_4" href="javascript:void(0);">
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                        </a>
                        <a class="quick-look-a" href="javascript:void(0);">
                            <div class="quick-look-button">
                                <span>������� ��������</span>
                            </div>
                        </a>
                        <div class="carousel-product-name">
                            <a href="javascript:void(0);">��������� ������� Beurer MG254</a>
                        </div>
                        <div class="carousel-element-price cart">3000 ���.</div>
                        <div class="element-hover cart">
                            <div class="carousel-other cart">
                                <div class="active" data-tab="tab1"></div>
                                        <div data-tab="tab2"></div>
                                        <div data-tab="tab3">
                                            <span class="htl-amount">17</span>
                                        </div>
                                        <div data-tab="tab4"></div>
                            </div>
                            <div class="eh-fast-order">
                                <a class="de-last-incart" href="javascript:void(0)"></a>
                                <a href="javascript:void(0);" class="header-button-top">������� �����</a>
                            </div>

                            <div class="eh-compare">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                            <div class="eh-like">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="carousel-element">
                    <div class="carousel-product">
                        <a class="cc_4" href="javascript:void(0);">
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                            <div class="cp-slide"><img class="carousel-product-img" src="img/content/product_image.jpg" alt="Product name"></div>
                        </a>
                        <a class="quick-look-a" href="javascript:void(0);">
                            <div class="quick-look-button">
                                <span>������� ��������</span>
                            </div>
                        </a>
                        <div class="carousel-product-name">
                            <a href="javascript:void(0);">��������� ������� Beurer MG254</a>
                        </div>
                        <div class="carousel-element-price cart">3000 ���.</div>
                        <div class="element-hover cart">
                            <div class="carousel-other cart">
                                <div class="active" data-tab="tab1"></div>
                                        <div data-tab="tab2"></div>
                                        <div data-tab="tab3">
                                            <span class="htl-amount">17</span>
                                        </div>
                                        <div data-tab="tab4"></div>
                            </div>
                            <div class="eh-fast-order">
                                <a class="de-last-incart" href="javascript:void(0)"></a>
                                <a href="javascript:void(0);" class="header-button-top">������� �����</a>
                            </div>

                            <div class="eh-compare">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                            <div class="eh-like">
                                <a href="javascript:void(0);">� ���������</a>
                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
*/ ?>
        </div>
        <!--.content-suitable -->

    </div>