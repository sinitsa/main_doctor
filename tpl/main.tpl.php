<?php 
    include('main_slider.tpl.php');
?>
    <?=$staticPage['benefits']['des']?>
    <div class="content main">
        
<?php
    include('popular_products.tpl.php');
?>      
        
<?php
    //include('discounted_set.tpl.php');
?>
          
        
        <div class="content-reviews">
            <span class="content-h1">������ ����� ��������</span>
            <div class="reviews-icon">
                <div>
                    <img src="/theme/img/content/review_green_icon.png" alt="icon">
                    <span class="htl-amount"><?=$feedbacks['sf_count']?></span>
                    <a href="<?=getTemplateLink(array('chpu'=>'otzyvy'), 'feedback');?>">������ � ��������</a>
                </div>
                <div style="position:relative">
                    <img src="/theme/img/content/review_green_icon.png" alt="icon">
                    <span class="htl-amount"><?=$feedbacks['pf_count']?></span>
                    <a href="<?=getTemplateLink(array('chpu'=>'otzyvy'), 'feedback');?>">������ � �������</a>
                </div>
            </div>
            <div class="content-carousel">
<?php
    foreach ($feedbacks['feedbacks'] as $feedback) {
?>
                <div class="reviews-element">
                    <div class="reviews-top">�</div>
                    <div class="review-text"><?=$feedback['comment']?></div>
                    <div class="review-gradient-wrapper">
                        <div class="review-gradient"></div>
                        <div class="review-product">
<?php
    unset($p_link); 
    if($feedback['title']) {
        $p_link = getTemplateLink(array('id' => $feedback['id'], 'chpu' => $feedback['chpu']), 'catalog'); // wrong link use catalog_id from feedback instead
                                 echo "� ������:"; 
?>
                            <a href="<?=$p_link ? $p_link : 'javascript:void(0)'?>"><?=$feedback['title']?></a>
<?php
    }
?>
                        </div>
                        <div class="review-signature">
                            <span><?=$feedback['name']?></span> <?=$feedback['date']?>
                        </div>
                    </div>
                </div>
<?php
    }
?>
            </div>
        </div>
        <div class="content-brands">
            <div class="content-h1"><?=$staticPage['brands']['seo_title']?></div>
            <?=$staticPage['brands']['des']?>
        </div>
    </div>
    <!--.content -->
    <div class="howto">
        <div class="content-howto">
            <span class="content-h1"><?=$staticPage['howto']['seo_title']?></span>
            <?=$staticPage['howto']['des']?>
        </div>
    </div>
    <!--.howto -->
    <div class="aboutUs">
        <div class="aboutUs-content">
            <h1 class="content-h2"><?=$staticPage['aboutUs']['seo_title']?></h1>
            <?=$staticPage['aboutUs']['des']?>
        </div>
    </div>
    <!-- aboutUs -->