<? $isMain = $tab == 'main';

$extraTitle = '';

if ($tab == 'catalog') {
	$extraTitleElements = array();
	if (!empty($product['like_desc']) && strpos($product['like_desc'], 'youtube') !== false )
		$extraTitleElements[] = '�����';
	if (count($data['feedback'])) 
		$extraTitleElements[] = '������';
		
		
	if (count($extraTitleElements) > 0)
		$extraTitle = ' (' . implode(', ', $extraTitleElements) . ')';
}

?>
<!DOCTYPE html>
<!--[if IE 7]><html lang="ru" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru" class="no-js"><!--<![endif]-->
	<head>
                <?php if (!isset($seo_title)) $seo_title = '';
                      if (!isset($seo_key)) $seo_key = '';
                      if (!isset($seo_des)) $seo_des = '';
                      if (!isset($extraTitle)) $extraTitle = '';
                      if (isset($data['seo_title']) && $data['seo_title'] == '404'){$seo_title = '404'; $title = '404';}
                ?>
        <link rel="canonical" type="text/css" href="<?=Config::get('site.web_addr').substr($_SERVER['REQUEST_URI'],1)?>"> 
		<title><?=$seo_title.$extraTitle;?></title>
		
		<meta name="google-site-verification" content="z3otVwLQ7yj_VprtwCCBRlaMXuH5S7wQk3Gfaljq4WM" />
		<meta charset="windows-1251">
		<meta name="keywords" content="<?php echo $seo_key; ?>" />
		<meta name="description" content="<?php echo $seo_des; ?>" />
        <meta name="viewport" content="width=device-width" />
        <link rel="stylesheet" href="/theme/production/main.min2.css">
        <?php /*
		<link rel="stylesheet" href="/theme/css/jquery-ui.css">
	    <!-- Magnific Popup core CSS file -->
	    <link rel="stylesheet" href="/theme/css/magnific-popup.css">
	    <link rel="stylesheet" type="text/css" href="/theme/css/slick.css" />
	    <link rel="stylesheet" href="/theme/css/style.css?ver=20150122" type="text/css">
	    <link rel="stylesheet" type="text/css" href="/theme/css/media-queries.css" />
	    */?>

	    <!--[if lt IE 9]>
		       <script src="js/html5shiv.js"></script>
	        <![endif]-->
	    <script src="/theme/js/include_script.js?ver=251101"></script>
	    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
<?php
if ($content == 'akcii') {
?>
		<style type="text/css">
        .top-image {
            background: none;
            height: 0;
        }
        .content-title {
            z-index: 1;
        }
        .content {
            margin-top: 42px;
        }
        .content-h1 {
            margin-top: 16px;
        }
        .benefits-container {
            margin-top:72px;
        }
    </style>
<?php
}
?>
	</head>
	<body class="<?=($isMain ? 'index' : 'inside')?>">
	<!--��������>
	<!-- �������� ��� retailrocket -->
	<script>
       var rrPartnerId = "54eecb391e994715a4747ddf";
       var rrApi = {}; 
       var rrApiOnReady = rrApiOnReady || [];
       rrApi.addToBasket = rrApi.order = rrApi.categoryView = rrApi.view = 
           rrApi.recomMouseDown = rrApi.recomAddToCart = function() {};
       (function(d) {
           var ref = d.getElementsByTagName('script')[0];
           var apiJs, apiJsId = 'rrApi-jssdk';
           if (d.getElementById(apiJsId)) return;
           apiJs = d.createElement('script');
           apiJs.id = apiJsId;
           apiJs.async = true;
           apiJs.src = "//cdn.retailrocket.ru/content/javascript/api.js";
           ref.parentNode.insertBefore(apiJs, ref);
       }(document));
</script>
<!-- /�������� ��� retailrocket -->
		<div class="wrapper">
			<header>
		        <div class="header-top-line">
		            <div class="htl-container">
		                <div class="htl-like">
		                    <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>">���������</a>
		                    <span class="htl-amount"><?=$like_count?></span>
		                </div>
		                <div class="htl-compare">
		                    <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">���������</a>
		                    <span class="htl-amount"><?=$compare_count?></span>
		                </div>
		                <div class="htl-basket">
<?php
	$summPrice = isset($_COOKIE['total_amount']) && !empty($_COOKIE['total_amount']) ? htmlspecialchars($_COOKIE['total_amount']) : 0;
	$summCount = 0;
        if (isset($_COOKIE['cart_products'])) {
            foreach (explode('|', $_COOKIE['cart_products']) as $productData) {
                    $temp = explode(':', $productData);
                    if (isset($temp[1])) {
                    	$summCount += $temp[1];
                    }    
            }
        }
        	$productsInCart = getProductsInBasket('preview',false);
        
?>
		                    <a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">
		                    <span class="basket-summ-products"><?=$summCount?> </span>
		                    <span class="basket-summ-products-text"><?=declOfNum($summCount, array('�����','������','�������'))?></span></a>
		                    <div class="htl-basket-text">
		                        �� �����
		                        <span class="htl-basket-amount basket-summ-price"><?=$summPrice?></span>
		                         ���.
		                    </div>
		                    <div class="htl-basket-content">
		                    	<div class="htl-basket-scroll">
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
<?php
		if ($productsInCart) {
			foreach (array_reverse($productsInCart) as $productIC) {
?>
								
			                        <div class="htl-basket-product" data-product_id="<?=$productIC['id'];?>" data-product_price="<?=$productIC['price_after_discount'] ? $productIC['price_after_discount'] : $productIC['price']?>">
			                            <img class="bp-img" src="<?=getImageWebPath('product_preview',false).$productIC['id']?>.jpg" alt="name">
			                            <a href="<?=getTemplateLink($productIC, 'catalog');?>"><?=$productIC['title']?></a>
			                            
			                            <span>���-��: <?=$productIC['amount']?></span>
			                            <span><?=$productIC['price_after_discount'] ? $productIC['price_after_discount'] : $productIC['price']?> ���.</span>
			                            <a href="javascript:void(0)">
			                                <img src="/theme/img/content/delete_img.png" alt="delete">
			                            </a>
			                        </div>
<?php
			}
?>
		                        
<?php
		} //else { echo '<div class="htl-basket-empty">� ������� ��� �������.</div>';}
?>
									<div class="htl-order">
		                            	<a class="header-button-top" href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">�������� �����</a>
		                        	</div>
		                    </div>
		                    </div>
		                </div>
		                <div class="htl-basket-add">��� ����� �������� � �������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="javascript:void(0)">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>" class="header-button-top">�������� �����</a>
		                        </div>
		                    </div>
		                    <div class="htl-compare-add">��� ����� �������� � ���������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="javascript:void(0)" class="header-button-top">� ���������</a>
		                        </div>
		                    </div>
		                    <div class="htl-like-add">��� ����� �������� � ���������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="javascript:void(0)">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>" class="header-button-top">� ���������</a>
		                        </div>
		                    </div>
		                <div class="htl-order">
		                    <a class="header-button-top" href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">�������� �����</a>
		                </div>
		            </div>
		        </div>
		        <!--.header-top-line -->
	            <div class="header-container">
	                <a href="/"><div class="header-logo"></div></a>
	                <div class="header-info">
	                    <div class="header-info-hours">�� ��������:
	                        <br><?=Config::get('site.work_mode');?> ��� ��������</div>
	                    <div class="header-info-phones">
	                        <span><?php echo(trim(Config::get('site.phone'))); ?></span>
	                        <br>
	                        <!-- <span><?php echo(trim(Config::get('site.phone_total'))); ?></span> -->
	                    </div>
	                    <div class="header-info-backcall">
	                        <a class="header-button" href="javascript:void(0)" onclick="yaCounter14113453.reachGoal('CALLBACK_STEP1');">��������     ������</a>
	                    </div>
	                </div>
	                <div class="header-menu">
	                    <a href="<?=getTemplateLink(array('chpu'=>'oplata'), 'page');?>">������</a>
	                    <a href="<?=getTemplateLink(array('chpu'=>'dostavka'), 'page');?>">��������</a>
	                    <a href="<?=getTemplateLink(array('chpu'=>'garantii'), 'page');?>">�������� � �������</a>
	                    <a href="<?=getTemplateLink(array('chpu'=>'kontakty'), 'page');?>">��������</a>
	                    <a href="<?=getTemplateLink(array('chpu'=>'otzyvy'), 'feedback');?>">������ ����� ��������</a>
	                    <!-- <a href="<?=getTemplateLink(array('chpu'=>'akcii'), 'deals');?>">���� �����</a> -->
	                </div>
	            </div>
	            <!--.header-container -->
	            <div class="search-container <?=($isMain ? '' : 'wide')?>">
	                <div class="search-wrapper">
	                    <div class="search-categories-menu">
<?php 						
		$menu = getMenu(false);
    	$i = 1;                
    	foreach ($menu as $k => $category) {
            $category['cat_id'] = isset($category['cat_id']) ? $category['cat_id'] : 0 ; ?>
    		<div class="menu-item  mi_<?=$i?>" data-ul="ch<?=$i?>">
            	<a href="<?=getTemplateLink(array('id' => $category['cat_id'], 'chpu' => $category['chpu']), 'mcat')?>"><?=$category['title']?></a>
				<div  id="ch<?=$i?>" class="categories-hover <?=$i > 3 ? 'menu_left' : ''?> hidden">
					<div class="level_1 <?=$i > 3 ? 'm_left' : 'dropdown-menu '?>">
						<?php foreach ($category['submenu'] as $key => $subcat) {			
							if($subcat['cat_id'] != -1) {
							$tags = getTagsInCat($subcat['cat_id']);
							if (isset($data) && isset($data['catInfo']) && $subcat['cat_id'] == $data['catInfo']['id']) {
							$m_pod = array('id' => $category['id'], 'chpu' => $category['chpu'], 'title' => $category['title']); 
							}?>
							<li data-submenu-id="sub_<?=$subcat['id']?>" class="level_1"> <?php ?>
								<a class="level_1" href="<?=getTemplateLink(array('id' => $subcat['cat_id'], 'chpu' => $subcat['chpu']), 'cat')?>"><?=$subcat['title']?></a>
							<?php $level_3 = array(); ?>
							</li>
							<?php }      	
						} ?>
					</div>
				</div>
			</div>
		<?php
		$i++;
    	} ?>						
	                    </div>
	                    <div class="search input_container">
	                    	<form name="search" action="/search" method="get">
		                        <div class="search-zoom"></div>
		                        <input name="search_text" class="search-input" type="search" placeholder="��� ����� ������?" id="product_search" onkeyup="autocomplet($(this),1)" autocomplete="off">
		                        <button id="search_button" class="search-button">�����</button>
	                         </form>
		                        <ul id="search_sugestion_1"></ul>
	                    </div>
<?php 

?>
	                    <? if (!$isMain) { ?>
	                    <div itemscope itemtype="https://schema.org/BreadcrumbList" class="page-path">
	                        <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <a itemprop="item" href="/" >
                                    <span itemprop="name" >�������</span>
                                </a>
                            </span>&nbsp;/
<?php
	if (isset($m_pod)){
?>
                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <a itemprop="item" href="<?=getTemplateLink($m_pod, 'mcat')?>">
                                    <span itemprop="name" ><?=$m_pod['title']?></span></a>
                            </span>&nbsp;/


<?php
	}
	
	if(isset($alv_brands) || $data['catInfo']["is_brand"] == 1){ ?>
	
					<span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
							<a itemprop="item" href="/brands">
                                <span itemprop="item" >������</span>
							</a>	
                           </span> &nbsp; /
	
	<? }

	if (isset($data) && isset($data['catInfo'])){
	    ?>


                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <a itemprop="item" href="<?=getTemplateLink($data['catInfo'], 'cat')?>">
                                    <span itemprop="name" ><?=$data['catInfo']['title']?></span>
                                </a>
                            </span>&nbsp; /

        <?php
    } 
	



                            if (isset($product) && isset($product['cat_info'])) {
?>
        <?php /*
        <span itemprop="itemListElement" itemscope itemtype="http://schema.org/ListItem">
                                <a itemprop="item" href="<?=getTemplateLink($product['cat_info'], 'cat')?>">
                                    <span itemprop="name" ><?=$product['cat_info']['title']?></span>
                                </a>
                            </span>&nbsp;/
 */ ?>

                            <span itemprop="itemListElement" itemscope itemtype="https://schema.org/ListItem">
                                <span itemprop="item" ><?=$product['title'] ? $product['title'] : $title?></span>
                            </span> &nbsp;/
        <?php
    } 
	
?>

					  

	
	                    </div>
	                    <? } ?>
	                </div>
	            </div>
	            <!--.search-container -->
	            <? if (!$isMain ) { 

	            	


            	?>

<style type="text/css">
	.top-image{
		<?=$background; ?>
        background-size:cover;
	}
</style>
<?php
if(isset($_COOKIE['top_title_slideUp']) && $tab == 'cat'){
    $slide_up = $_COOKIE['top_title_slideUp'] ? 1 : 0;
    //d($_COOKIE['top_title_slideUp'],0);
} else {
    $slide_up = 1;
}
$style = '' ;
if ( $content == 'akcii' || $tab == 'cat' ) {
	$style .= ' brown ' ;
}
if ($tab == 'cat' ) {
	$style .= ' category ' ;
} elseif($tab == 'catalog') {
	$style .= ' product_title ' ;
}

?>
	            <div class="top-image <?=$style?>" <?= $slide_up && $tab != 'catalog' && $tab != 'mcat' ? '' : ''?>>
	                <h1 class="content-title"><?=(isset($product) && $product['title']) ? $product['title'] : $title?></h1>
<?php
	if($tab == 'cat') 
	{
?>					
					<div <?= $slide_up ? 'style="display:none;"' : ''?> class="content-title <?=strlen($seo_title_text) < 5 ? 'hidden bg_none' : ''?>"><?=rtrim(substr(strip_tags(preg_replace('|<style\b[^>]*>(.*?)</style>|s', '', $seo_title_text)), 0, 55), '.,?!:')?><?=strlen($seo_title_text) < 55 ? '' : '...'?></div>
		            <div <?= $slide_up ? 'style="display:none;"' : ''?>>
<?php
		$s_t = strip_tags(preg_replace('|<style\b[^>]*>(.*?)</style>|s', '', $seo_text));
		if(strlen($s_t) > 10) {
?>
		                <span><?=rtrim(substr($s_t, 0, 140), '.,?!:')?><?=strlen($s_t) < 140 ? '' : '...'?></span>
		                <br>
		                <a id="more-about" href="javascript:void(0)">���������</a>
<?php
		}
?>	
		            </div>
<?php
	}
?>
	            </div>
	            <? } ?>
            </header>
            
            <?php
            
			define('TPL_DIR', dirname(__FILE__).'/');

			switch ($tab) {
				case 'main':
				default:
					$path = TPL_DIR.$tab.'.tpl.php';
					if (file_exists($path)) {
						include($path);
					} else {
						include('main.tpl.php');
					}
			}
?>

		    <footer class="footer">
		        <div class="footer-content">
		            <div class="footer-feedback">
                        <?php /*
		                <form class="footer-form" method="post" id="mail-feedback-form">
		                    <h3 class="footer-h3">�������� �����</h3>
		                    <div>
		                        <input type="text" placeholder="���" name="name">
		                        <br>
		                        <input type="email" placeholder="Email" name="email">
		                    </div>
		                    <textarea placeholder="����� ���������" name="message_body"></textarea>
	                    	<input type="hidden" name="method" value="sendmail">
		                    <input class="submit-button" type="submit" value="���������" id="send-mail">
		                </form>
                        */ ?>
                        <div class="footer-menu">
                            <div>


                            <div class="footer-h3">����:</div>
                            <ul>
                                <li><a href="/maincat/tovary-dlya-reabilitacii">������ ��� ������������</a></li>
                                <li><a href="/maincat/massazhnoe-oborudovanie">��������� ������������</a></li>
                                <li><a href="/maincat/meditsinskim-uchrezhdeniyam">����������� �����������</a></li>
                                <li><a href="/maincat/medtekhnika-dlya-doma">���������� ��� ����</a></li>
                                <li><a href="/maincat/zdorovye-i-krasota">�������� � �������</a></li>
                            </ul>
                            </div>
                            <ul>
                                <li><a href="/content/oplata">������</a></li>
                                <li><a href="/content/dostavka">��������</a></li>
                                <li><a href="/content/garantii">�������� � �������</a></li>
                                <li><a href="/content/kontakty">��������</a></li>
                                <li><a href="/feedback/otzyvy">������</a></li>
								<li><a href="/brands">���� ������</a></li>
                            </ul>
                        </div>
		                <div class="footer-contact">
		                    <span class="footer-h3">��������� � ����:</span>
		                    <span class="footer-h3">
		                    	<?php echo(trim(Config::get('site.phone'))); ?>
		                    	<br>
		                    	<?php //echo(trim(Config::get('site.phone_total'))); ?>
		                    </span>
		                    <span class="footer-h3">������, ������, ������-���� "���������", �������� �., 1, ���. ���������</span>
		                </div>
		                <div style="clear:both;"></div>
		                <div class="footer-rights">
		                    <div>��������-������� ����������
		                        <br>�������� ������</div>
		                    <div>��� ����� �������� � <?=date('Y')?>.</div>
                            <div class="payment-icons">
                                <div class="cl-icon"></div>
                                <div class="visa-icon"></div>
                                <div class="mc-icon"></div>
                            </div>
		                </div>
		            </div>
                    <?php /*
		            <a class="footer-map" id="mapOffline">
		                <img src="/theme/img/footer-map-yandex.jpg" alt="�������� ������ �����">
		            </a>
		            <div class="footer-map" id="mapLive">
		                <div id="map"></div>
		            </div>
                    */ ?>
		        </div>
		    </footer>

<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter14113453 = new Ya.Metrika({id:14113453, enableAll: true, webvisor:true});
        } catch(e) {}
    });
    
    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/14113453" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<!-- ��� ���� ������������ Google -->
<!--------------------------------------------------
� ������� ���� ������������ ����������� �������� ����������, �� ������� ����� ���������������� �������� ������������. ����� ����������� ��������� ��� �� ��������� � ��������� ����������� ���������. ��������� �� ���� ����������� � � ��������� ���� ������� �� �������� http://google.com/ads/remarketingsetup.
--------------------------------------------------->
<script type="text/javascript">
/* <![CDATA[ */
var google_conversion_id = 952982158;
var google_custom_params = window.google_tag_params;
var google_remarketing_only = true;
/* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
<noscript>
<div style="display:inline;">
<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/952982158/?value=0&amp;guid=ON&amp;script=0"/>
</div>
</noscript>

<script type="text/javascript">

/*  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-32266290-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();*/

(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-62593297-1', 'auto');
ga('send', 'pageview');

</script>
		    <div class="header-fix">
		        <div class="header-top-line">
		            <div class="htl-container">
		                <div class="htl-like">
		                    <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>">���������</a>
		                    <span class="htl-amount"><?=$like_count?></span>
		                </div>
		                <div class="htl-compare">
		                    <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">���������</a>
		                    <span class="htl-amount"><?=$compare_count?></span>
		                </div>
		                <div class="htl-basket">
		                    <a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">
		                    <span class="basket-summ-products"><?=$summCount?> </span>
		                    <span class="basket-summ-products-text"><?=declOfNum($summCount, array('�����','������','�������'))?></span></a>
		                    <div class="htl-basket-text">
		                        �� �����
		                        <span class="htl-basket-amount basket-summ-price"><?=$summPrice?></span>
		                         ���.
		                    </div>
		                    <div class="htl-basket-content">
		                    	<div class="htl-basket-scroll">
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
<?php
		if ($productsInCart) {
			foreach (array_reverse($productsInCart) as $productIC) {
?>
								
			                        <div class="htl-basket-product" data-product_id="<?=$productIC['id'];?>" data-product_price="<?=$productIC['price_after_discount'] ? $productIC['price_after_discount'] : $productIC['price']?>">
			                            <img class="bp-img" src="<?=getImageWebPath('product_preview',false).$productIC['id']?>.jpg" alt="name">
			                            <a href="<?=getTemplateLink($productIC, 'catalog');?>"><?=$productIC['title']?></a>
			                           
			                            <span>���-��: <?=$productIC['amount']?></span>
			                            <span><?=$productIC['price_after_discount'] ? $productIC['price_after_discount'] : $productIC['price']?> ���.</span>
			                            <a href="javascript:void(0)">
			                                <img src="/theme/img/content/delete_img.png" alt="delete">
			                            </a>
			                        </div>
<?php
			}
?>
		                        
<?php
		} //else { echo '<div class="htl-basket-empty">� ������� ��� �������.</div>';}
?>
							<div class="htl-order">
		                            <a class="header-button-top" href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">�������� �����</a>
		                        </div>
	                        </div>
		                    </div>
		                </div>
		                <div class="htl-basket-add">��� ����� �������� � �������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="javascript:void(0)">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>" class="header-button-top">�������� �����</a>
		                        </div>
		                    </div>
		                    <div class="htl-compare-add">��� ����� �������� � ���������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="javascript:void(0)">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>" class="header-button-top">� ���������</a>
		                        </div>
		                    </div>
		                    <div class="htl-like-add">��� ����� �������� � ���������
		                        <img class="basket-arrow" src="/theme/img/header/htl_basket_content.png" alt="basket">
		                            <a class="ba-close" href="javascript:void(0)">
		                                <img src="/theme/img/content/delete_img.png" alt="">
		                            </a>
		                        <div class="htl-order">
		                            <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>" class="header-button-top">� ���������</a>
		                        </div>
		                    </div>
		                <div class="htl-order">
		                    <a class="header-button-top" href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>">�������� �����</a>
		                </div>
		            </div>
		        </div>
		        <!--.header-top-line -->

		        <div class="search-wrapper">
		            <div class="search">
		                <div class="search-zoom"></div>
		                <div class="input_container">
                            <form name="search" action="/search" method="get">
                                <div class="search-zoom"></div>
                                <input name="search_text" class="search-input" type="search" placeholder="��� ����� ������?" id="product_search_2" onkeyup="autocomplet($(this),2)" autocomplete="off">
                                <button id="search_button_2" class="search-button">�����</button>
                            </form>
                            <ul id="search_sugestion_2"></ul>
		                </div>
		                <div class="header-info-phones">
		                    <span><?php echo(trim(Config::get('site.phone'))); ?></span>
		                </div>
		            </div>
		        </div>
		        <!--.search-wrapper -->
		    </div>
		    <!-- /header fix -->
		    <div class="quick-view-div">
<?php // include('product_quick_view.tpl.php'); ?>
			</div>
		    <div id="product-quick-order" class="zoom-anim-dialog mfp-hide">
		        <img src="/theme/img/content/delete_img.png" alt="">
		        <div>������� �����</div>
		        <input class="request" type="text" placeholder="���" name="name">
		        <input class="request" type="text" placeholder="�������" name="phone">
		        <button class="request" id="fast-order-send">��������</button>
		        <input id="fast-order-pid" type="hidden" name="product_id">
		        <input id="fast-order-sid" type="hidden" name="set_id">
		        <input id="fast-order-sname" type="hidden" name="set_name" >
                <input type="hidden" name="isCredit" value="0">
		        <span class="response" >����� �<span id="order-num"></span> ������ � ���������, � ��������� ����� � ���� �������� ���� �� ����� ����������.</span>
		        
		    </div>
		    <!-- /quick-order -->
		    <div id="back-call" class="backcall-form zoom-anim-dialog mfp-hide" style="text-align:center;">
		        <img src="/theme/img/content/delete_img.png" alt="">
		        <div onclick="yaCounter14113453.reachGoal('CALLBACK_STEP1');">�������� ������</div>
		        <input class="request" type="text" placeholder="���" name="name">
		        <input class="request" type="text" placeholder="�������" name="phone">
		        <button class="backcall-button request" id="backcall" onclick="yaCounter14113453.reachGoal('CALLBACK_STEP2');">- ��������� ���</button>
		        <span class="response" >������� �� ������, � ��������� ����� � ���� �������� ���� �� ����� ����������.</span>
		    </div>
		    <!-- /back-call -->
		    <div id="delete-basket" class="delete-basket zoom-anim-dialog mfp-hide">
		        <img src="/theme/img/content/delete_img.png" alt="">
		        <div>������� ����� �� �������?</div>
		        <button>��, �������</button>
		        <button>���, � ���������</button>
		    </div>
		    <!-- /delete basket -->
		    <div id="delete-compare" class="delete-basket zoom-anim-dialog mfp-hide">
		        <img src="/theme/img/content/delete_img.png" alt="">
		        <div>������� ����� �� ���������?</div>
		        <button>��, �������</button>
		        <button>���, � ���������</button>
		    </div>
		    <!-- /delete compare -->
		    <div id="delete-like" class="delete-basket zoom-anim-dialog mfp-hide">
		        <img src="/theme/img/content/delete_img.png" alt="">
		        <div>������� ����� �� ����������?</div>
		        <button>��, �������</button>
		        <button>���, � ���������</button>
		    </div>
		    <!-- /delete like -->
		    <div class="btn-top">
		            <div class="bt-container">
		                    <a href="javascript:void(0)"><div class="icon-bt-arrow"></div></a>
		            </div>
		    </div>
		    <!--/btn-top -->
		</div>
	</body>
</html>