<div class="list-block2 not-visible">
<?php 
    foreach ($list_products as $product) {
        
?>
                <div class="carousel-element list">
                        <div class="carousel-product">
                            <a class="cc_4 list-block" href="javascript:void(0);" data-href="<?=getTemplateLink($product, 'catalog');?>">
                                                            <div class="cp-slide">
                                <img class="carousel-product-img" data-lazy="<?=getImageWebPath('product_small',false).$product['id']?>.jpg" alt="<?=$product['title']?>">
                            </div>
<?php 
            $extra_photo = getProductExtraPhoto($product['id']);
            
            foreach ($extra_photo as $p_id) {
?>
                           <div class="cp-slide">
                                <img class="carousel-product-img" data-lazy="<?=getImageWebPath('product_extra_small',false).$p_id?>.jpg" alt="<?=$product['title']?>">
                           </div> 
<?php
            }
?>
                            </a>
                            <div class="promo">
<?php
	$t=false;
	if($product['novinka']) {$t='new';}
	elseif($product['best']){$t='hit';}
    if ($t) {
?>
            <img src="/theme/img/content/<?=$t?>_triangle.png" alt="$t">
<?php
    }
?>
                            </div>
                            <div class="list-block">
                                <div class="cat-product-name">
                                    <a href="<?=getTemplateLink($product, 'catalog');?>"><?=$product['title']?></a>
                                </div>
                                <div class="l-text hidden tab1 active"><?=$product['short_des']//trimming_line($product['des'],200)?>
                                    <br>
                                    <a  href="<?=getTemplateLink($product, 'catalog');?>">���������</a>
                                </div>
                                <div class="l-text hidden tab2 ">
                                    <ul class="list_product_desc">
<?php
	$i = 0;

	foreach ($product['params'] as $title => $value) {
		if ($title != '������ �������������' && $title != '��������') {
?>
										<li><?=$title?>: <?=$value['value'] ? $value['value'] : $value['value_float']?> <?=$value['unit']?></li>
<?php
			$i++;
			if ($i>4) {break;}
		}
	}// d($product['params']);
	if(trim($product['params']['������ �������������']['value']) == '��������') { $c = 'germany';}
	elseif(trim($product['params']['������ �������������']['value']) == '�����') { $c = 'china';}
	elseif(trim($product['params']['������ �������������']['value']) == '�������') { $c = 'taivan';}
	elseif(trim($product['params']['������ �������������']['value']) == '���') { $c = 'usa';}
	elseif(trim($product['params']['������ �������������']['value']) == '������') { $c = 'russia';}
    else{ $c = 0; }
?>
                                    </ul>
                                    <ul class="pod-right">
										<? 
											if ($product['params']['������ �������������']['value'] != null) echo ("<li class='manufacturerLi'><span class='flag ".$c."'></span>������ �������������: ".$product['params']['������ �������������']['value']."</li>");
											if ($product['params']['��������']['value'] != null) echo ("<li class='warrantyLi'>��������: ".$product['params']['��������']['value']."</li>");
										?>
                                    </ul><br>
                                    <div class="lt-a">
                                    <a  href="<?=getTemplateLink($product, 'catalog');?>">���������</a>
                                    </div>
                                </div>
                                <div class="l-text hidden tab3">
                                    <?php
    $feedbacks = getProductFeedback($product['id'],false);
    $last_feedback = end($feedbacks);
    $short_f = rtrim(substr($last_feedback['comment'], 0, 170), "!,.-");
    //d($feedback);
    if($feedbacks) {
        echo $short_f.' ...<br><span><a href="javascript:void(0)">'.$last_feedback['name']."</a> ".date('d.m.Y', $last_feedback['date'])."</span><br>";
    } else {
        echo "���� ����� �� ������� ����� � ����� ������.<br>";
    }
?>
                                </div>
                                <div class="l-text hidden tab4">
                                    <div class="po-video">
                                        <a class="<?=!empty($product['like_desc']) ? 'popup-youtube' : ''?>" href="<?=!empty($product['like_desc']) ? strip_tags($product['like_desc']) : 'javascript:void(0)'?>"></a>
                                    </div>
                                </div>
                                <div class="carousel-other">
                                    <div class="active" data-tab="tab1"></div>
                                    <div data-tab="tab2"></div>
                                    <div data-tab="tab3">
                                        <span class="htl-amount"><?=count($feedbacks)?></span>
                                    </div>
                                    <div data-tab="tab4"></div>
                                </div>
                            </div>
                            <div class="list-block">
                                <div class="carousel-element-price"><?=$product['price_after_discount']?> ���.</div>
                                <div class="eh-fast-order">
                                    <a class="de-last-incart buyLink" data-product_id="<?=$product['id'];?>" data-product_price="<?=$product['price_after_discount'] ? $product['price_after_discount'] : $product['price']?>" href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart');?>"></a>
                                <a  data-product_id="<?=$product['id'];?>" href="javascript:void(0);" class="one-click-order header-button-top">������� �����</a>
                                </div>  
<?php
    if ( !in_array($product['id'], $compare_products)) {
?>
                        <div class="eh-compare compare-<?=$product['id']?>">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-compare done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">���������</a>
<?php
    }
?>
                        </div>        
<?php
    if ( !in_array($product['id'], $like_products)) {
?>
                        <div class="eh-like">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-like done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>">���������</a>
<?php
    }
?>
                                </div>
                            </div>
                        </div>
                </div>
<?php
    }
?>
</div>