<? $isMain = $tab == 'main';

$extraTitle = '';

if ($tab == 'catalog') {
	$extraTitleElements = array();
	if (!empty($product['like_desc']) && strpos($product['like_desc'], 'youtube') !== false )
		$extraTitleElements[] = '�����';
	if (count($data['feedback'])) 
		$extraTitleElements[] = '������';
		
		
	if (count($extraTitleElements) > 0)
		$extraTitle = ' (' . implode(', ', $extraTitleElements) . ')';
}

?>
<!DOCTYPE html>
<!--[if IE 7]><html lang="ru" class="no-js lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="no-js lt-ie9"><![endif]-->
<!--[if gt IE 8]><!--><html lang="ru" class="no-js"><!--<![endif]-->
	<head>
                <?php if (!isset($seo_title)) $seo_title = '';
                      if (!isset($seo_key)) $seo_key = '';
                      if (!isset($seo_des)) $seo_des = '';
                      if (!isset($extraTitle)) $extraTitle = '';
                ?>
		<title><?=$seo_title.$extraTitle;?></title>
		<meta charset="utf-8">
		<meta name="keywords" content="<?php echo $seo_key; ?>" />
		<meta name="description" content="<?php echo $seo_des; ?>" />
        <meta name="viewport" content="width=device-width" />
		<link rel="stylesheet" href="/theme/css/fonts.css">
		<link rel="stylesheet" href="/theme/css/main.css?version=20141027">
		<link rel="stylesheet" href="/theme/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
		<script src="/theme/js/jquery-1.8.3.min.js"></script>
		<script src="/theme/js/modernizr-2.6.2.min.js"></script>
		<script src="/theme/js/jquery.carouFredSel-6.2.0-packed.js"></script>
		<script src="/theme/js/jquery.selectbox-0.2.min.js"></script>
		<script src="/theme/js/jquery-ui-1.10.2.custom.min.js"></script>
		<script src="/theme/fancybox/jquery.fancybox.pack.js"></script>
		<script src="/theme/js/jquery.form.js"></script>
		<script src="/theme/js/filters.js"></script>
		<script src="/theme/js/basket.js"></script>
		<script src="/theme/js/main.js"></script>
		<script src="http://api-maps.yandex.ru/2.0-stable/?load=package.standard&lang=ru_RU" type="text/javascript"></script>
		<script src="http://twitter.github.io/typeahead.js/releases/latest/typeahead.bundle.js"></script>
		<!--[if lt IE 9]><script src="/theme/js/html5.js"></script><![endif]-->
		<script async="true" type="text/javascript" src="http://servant.indexisto.com/files/searchbox/searchbox.nocache.js?type=edit"></script>
	</head>
	<body class="<?=($isMain ? 'index' : 'inside')?>">
		<div class="wrapper">
			<header class="header">
				<a href="/"><h1>Beurer</h1></a>
				<nav>
					<ul>
						<li><a href="#">�������</a>
							<?
							$menu = getMenu(true);
							$lastItem = count($menu) - 1;
							
							$data['catInfo'] = array();
							if (isset($product['cat_info'])) {
								$data['catInfo'] = $product['cat_info'];
							}
							
							//���������� ������� ������ � ���������
							foreach ($menu as $i) {
								foreach ($i['submenu'] as $si) {
									if (isset($data['catInfo']['id']) && ($si['cat_id'] == $data['catInfo']['id'])) {
										$mainItemId = $si['pod'];
										$subItemId = $si['id'];
									}
								}
							}
							
							$i = 0;
							?>
							<ul class="catalogueSubmenu">
								<? foreach ($menu as $item) { ?>
								<li>
									<a href="#">
										<div class="icon"><img src="<?=getImageWebPath('menu').$item['id'];?>.jpg" alt="<?=$item['title']?>"></div>
										<div class="caption"><?=$item['title']?></div>
									</a>
									<ul>
										<? foreach ($item['submenu'] as $subitem) { ?>
										<li><a href="<?=getTemplateLink($subitem, 'cat')?>"><?=$subitem['title']?></a></li>
										<? } ?>
									</ul>
								</li>
								<? } ?>
							</ul>
						</li>
						<li><a href="<?=getTemplateLink(array('chpu'=>'optom'), 'page');?>">���������</a></li>
						<li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'), 'page');?>">�������� � ������</a></li>
						<li><a href="<?=getTemplateLink(array('chpu'=>'garantiya'), 'page');?>">��������</a></li>
						<li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'), 'page');?>">���������</a></li>
						<!--li><a href="<?=getTemplateLink(array('chpu'=>'o-magazine'), 'page');?>">� ��������</a></li-->
						<li><a href="<?=getTemplateLink(array('chpu'=>'kontakty'), 'page');?>">��������</a></li>
					</ul>
				</nav>
				<ul class="tels">
					<li>
						<div class="tel">
							<span id="yad-phone"><?php echo(trim(Config::get('site.phone'))); ?></span> 
							
						</div>
						<div class="tel">
							<span id="yad-phone">8 (812) 243-16-09</span> 
						</div>						
					</li>
					<li>
						<div class="tel"><span>8 (800) 555-78-65</span> </div>
						���������� ������ �� ������ <br />
						����� ������ <?=Config::get('site.work_mode');?>
					</li>
				</ul>
				<form class="search" method="GET" action="search">
					<input name="search_text" type="text" placeholder="<?php if($_GET['search_text'] == NULL) { echo '������� ��������� ������...';} else {echo $_GET['search_text'];}  ?> ">
					<input type="submit" name="sub_ok" value="sub_ok">
				</form>
				<? include('block_basket_info.tpl.php'); ?>
			</header>
			<?php 
				define('TPL_DIR', dirname(__FILE__).'/');
				switch ($tab) {
					case 'main':
					default:
						$path = TPL_DIR.$tab.'.tpl.php';
						if (file_exists($path)) {
							include($path);
						} else {
							include('main.tpl.php');
						}
				}
			?>
		</div>
		<footer class="footer">
			<div class="footerIn">
				<div class="copy">
					<p>� <?=date('Y')?>. ����������� ������������� �Beurer�.</p>
					<p>��� ����� ��������.</p>
				</div>
				<div class="counters" style="top: 32px;">
					
					
					<!-- begin of Top100 code -->

					<script id="top100Counter" type="text/javascript" src="http://counter.rambler.ru/top100.jcn?2962606"></script>
					<noscript>
					<a href="http://top100.rambler.ru/navi/2962606/">
					<img src="http://counter.rambler.ru/top100.cnt?2962606" alt="Rambler's Top100" border="0" />
					</a>
					
					</noscript>
					<!-- end of Top100 code -->
					<div class="our_payment" style="width: 200px; left: -198px;position: absolute;font-size: 11px;font-weight: bold; top: 0px;"> 
						<div>
							<img src="/img/mas.gif" alt="����� ����" title="����� ����" />
						</div>
					</div>

				</div>
				<ul class="fLinks" style="margin-left: -214px;">
					<!--<li><a href="#">�������</a></li>-->
					<li><a href="<?=getTemplateLink(array('chpu'=>'aktsii'), 'page');?>">�����</a></li>
					<li><a href="<?=getTemplateLink(array('chpu'=>'dostavka-i-oplata'), 'page');?>">�������� � ������</a></li>
					<li><a href="<?=getTemplateLink(array('chpu'=>'garantiya'), 'page');?>">��������</a></li>
					<li><a href="<?=getTemplateLink(array('chpu'=>'o-magazine'), 'page');?>">� ��������</a></li>
					<li><a href="<?=getTemplateLink(array('chpu'=>'punkty-samovyvoza'), 'page');?>">���������</a></li>
					<li><a href="<?=getTemplateLink(array('chpu'=>'kontakty'), 'page');?>">��������</a></li>
				</ul>
			</div>
		</footer>
		
<!-- Yandex.Metrika counter -->
<script type="text/javascript">
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter21819568 = new Ya.Metrika({id:21819568,
                    webvisor:true,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true});
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/21819568" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-46033833-1', 'beurer.tv');
  ga('send', 'pageview');

</script>

		
		<div class="overlay" onclick='/*$("#toCatalogue")[0].click()*/'>&nbsp;</div>
		
		<noindex>
		<div class="confirmation popup">
			<span class="close" title="�������">�������</span>
			<div class="caption">��� ����� ������!</div>
			<p>������������, <strong class="name">�������</strong>.</p>
			<p>������ ������ �������� ����� <strong class="order-code">B-??</strong>.</p>
			<p>��� �������� ���������� ��� �� ����� � ������� ���������� ����� (� ������� �����) ��� ������������ ������� �������� � ��������� ������� ������������ ������.</p>
			<p><strong>����������, �� ���������� �������.</strong></p>
			<a href="/" class="catalogueLink">������� � �������</a>
		</div>
		
		<div class="quickBuy popup">
			<span class="close" title="�������">�������</span>
			<div class="caption">����� � ���� ����!</div>
			<p><a href="#" class="name">���������� ���� Beurer PM58</a> <span class="price"><span>4 500</span> ���.</span></p>
			<form id="fastorder-form" action="" method="post">
				<p><input type="text" name="name" placeholder="���� ���:"></p>
				<p><input type="tel" name="phone" placeholder="��� �������:"></p>
				<p><input type="email" name="email" placeholder="��� E-mail:"></p>
				<input type="hidden" name="product_id" value="" />
				<div class="loading" style="display: none;"><img src="/theme/i/ajax-loader-2.gif" alt="���������..." /></div>
				<button type="submit"  class="oneClickBuy" onclick="yaCounter21819568.reachGoal('FAST_OK'); return true;">��������</button>
			</form>
		</div>

		<div class="Map popup">
			<span class="close" title="�������">�������</span>
			<div class="caption">������� ����� ��������</div>
			<div class="all"></div>
		</div>
		
		<div class="confirmation-delete popup">
			<span class="close" title="�������">�������</span>
			<div class="caption">������� ����� �� �������?</div>
			<p><a class="name" href="#">���������� ���� Beurer PM58</a> <span class="price"><span>4 500</span> ���.</span></p>
			<a href="#" class="catalogueLink">�������</a>
		</div>
		
		<div class="feed-back-popup popup">
			<span class="close" title="�������">�������</span>
			<div class="caption">�������� �����</div>
			
			<div class="error error-code-1">����������, ��������� ��� ����</div>
			<div class="error error-code-2">������ ������������ E-mail �����</div>
			<div class="error error-code-3">��� ����� ����� �� ����. ����������, �� ����������� ������ � ���������.</div>
			
			<div class="success">��� ����� ������ � ����� ����������� ����� �������� ���������������.</div>
			
			<form id="feed-back-form" action="" method="post">
				<p><input type="text" name="name" placeholder="���� ���:"></p>
				<p><input type="email" name="email" placeholder="��� E-mail:"></p>
				<textarea name="comment" placeholder="��� �����������:"></textarea>
				<input type="hidden" name="product_id" value="" />
				<input type="hidden" name="method" value="addfeedback" />
				<div class="loading" style="display: none;"><img src="/theme/i/ajax-loader-2.gif" alt="���������..." /></div>
				
			</form>
			<button type="submit" class="send-button" onclick="yaCounter21819568.reachGoal('FEEDBACK'); return true;">��������</button>
		</div>
		
		</noindex>
		
		<!-- MH  !-->
		
		<!-- ��� ���� ������������ Google -->
		<?php if($ecomm_prodid) {?>	
		<script type="text/javascript">
		var prodid = <?=$ecomm_prodid?>;
		var google_tag_params = {};
		if (typeof prodid == 'object') 
			google_tag_params = {
				ecomm_prodid: <?=$ecomm_prodid?>,
				ecomm_pagetype: '<?=$ecomm_pagetype?>',
				ecomm_totalvalue: <?=$ecomm_totalvalue?>
			};
		else
			google_tag_params = {
				ecomm_prodid: '<?=$ecomm_prodid?>',
				ecomm_pagetype: '<?=$ecomm_pagetype?>',
				ecomm_totalvalue: '<?=$ecomm_totalvalue?>'
	        }; 
		//console.log(google_tag_params);
		</script>
		<?php } ?>
		<script type="text/javascript">
		/* <![CDATA[ */
		var google_conversion_id = 983608035;
		var google_custom_params = window.google_tag_params;
		var google_remarketing_only = true;
		/* ]]> */
		</script>
		<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
		</script>
		<noscript>
		<div style="display:inline;">
		<img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/983608035/?value=0&amp;guid=ON&amp;script=0"/>
		</div>
		</noscript>
		<?
	//	<script id="rhlpscrtg" type="text/javascript" charset="utf-8" async="async" src="https://web.redhelper.ru/service/main.js?c=beurertv"></script> 
?>
	</body>
</html>