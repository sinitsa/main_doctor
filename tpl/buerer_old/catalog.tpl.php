<?php
if("������" === $GRM_UserRealData['user_city']){
	$link_to_ankor = "http://beurer.tv/page/punkty-samovyvoza.html#mo";
}elseif("�����-���������" === $GRM_UserRealData['user_city']){
	$link_to_ankor = "http://beurer.tv/page/punkty-samovyvoza.html#sp";
}else{
	$link_to_ankor = "http://beurer.tv/page/punkty-samovyvoza.html";
}

$pointQiwi = getQiwiPoint();
$tempArr = []; 
foreach ($pointQiwi as $key => $value) {
	$st = iconv("UTF-8", "cp1251", unserialize($value['point'])['town']);
	$tempArr[] = $st;
}

$showMap = in_array($GRM_UserRealData['user_city'], $tempArr);

if ($showMap) {
	$json_point = [];
	$all = [];
	$arr = [];
	$i = 0;
	foreach ($pointQiwi as $key => $value) {
		$st = iconv("UTF-8", "cp1251", unserialize($value['point'])['town']);
		if($GRM_UserRealData['user_city'] === $st){
			$elem = unserialize($value['point']);
			if(!in_array($elem['town'], $town)){
				$town[] = $elem['town'];
			}
			$shir = $elem['latitude'];
			$dolg = $elem['longitude'];
			$arr[] = [$shir, $dolg];
			$all[$i]['point'] = $elem;
			$i++;			
		}
	}
	$json_arr = json_encode($arr);
	$json_all = json_encode($all);
	?>
	<script>
	var json_point = <?=$json_arr?>;
	var json_all = <?=$json_all?>;
		ymaps.ready(function(){
			var moscow_map = new ymaps.Map("first_map", {
				center: [55.76, 37.64],
	            zoom: 12
			});
			var myGeocoder = ymaps.geocode("<?=$GRM_UserRealData['user_city']?>");
			myGeocoder.then(function(res){
				var cord =  res.geoObjects.get(0).geometry.getCoordinates();
				moscow_map.setCenter(cord);
			});
	       	moscow_map.controls.add('smallZoomControl');
	        moscow_map.controls.add('zoomControl', {top: "131px"});
	        moscow_map.behaviors.enable('scrollZoom');

	        for (var i = 0; i < json_point.length; i++) {
	        	var el = json_point[i];
	        	var pt = json_all[i]['point'];
	        	var rasp = "<div style='font-size:12px;'><b>������������:</b> " + pt.locationdescription + "</div>";
	        	var timel = "<div class='font-size: 12px'><b>����� ������:</b> " + pt.operatinghours + "</div>";
	        	var p = new ymaps.Placemark(el,{
	        		preset: 'twirl#redIcon',
	        		balloonContent: '<div class="pcpc"><div style="font-size: 12px;">�������� <b>' + pt.name + '</b></div><br/>' + '<div style="font-size: 12px;">' + '<div> ' + pt.street + " " + pt.buildingnumber +"<br/>"+ pt.postcode + " " + pt.town +'</div><br/>' 
	        		+ rasp + timel
	        	});
				moscow_map.geoObjects.add(p);
	        };

	    });
		
		$(document).ready(function(){
			if( location.search == '?utm=feedback' ){
				$('.add-feed-button a').click();
				window.scrollTo(0,0);
			}
		});
	</script>
	<?php 
}

?>			

			<section class="content">
				<div class="back"><a href="<?=getTemplateLink($data['catInfo'], 'cat');?>"><?=$data['catInfo']['title'];?></a></div>
				<div class="product">
					<div class="photos"><div class="photosIn">
						<div class="mPhoto"><a rel="group" class="fgallery" href="<?=getImageWebPath('product_original').$product['id'];?>.jpg"><img src="<?=getImageWebPath('product_preview').$product['id'];?>.jpg" alt="<?=$product['title'];?>"></a></div>
						<div class="mPhotoThumbs">
							<ul>
									<li><a data-original="<?=getImageWebPath('product_original').$product['id'];?>.jpg" href="<?=getImageWebPath('product_preview').$product['id'];?>.jpg"><img src="<?=getImageWebPath('product_small').$product['id'];?>.jpg" alt=""></a></li>
								<? foreach ($product['extra_photo'] as $id)  { ?>
									<li><a data-original="<?=getImageWebPath('product_extra_original').$id;?>.jpg" href="<?=getImageWebPath('product_extra_medium').$id;?>.jpg"><img src="<?=getImageWebPath('product_extra_preview').$id;?>.jpg" alt=""></a></li>
								<? } ?>
							</ul>
						</div>
					</div></div>
					<div class="info">
						<h1><?=$product['title']?></h1>
						<? if ($product['discount_value'] > 0) { ?>
						<dl class="oldprice">
							<dt>������ ����:</dt>
							<dd><span><?=moneyFormat($product['price'])?></span> ���.</dd>
						</dl>
						<dl class="savings">
							<dt>��������:</dt>
							<dd><span><?=$product['discount_summ_value']?></span> ���.</dd>
						</dl>
						<? } ?>
						<dl class="price">
							<dt>����:</dt>
							<dd><span><?=moneyFormat($product['price_after_discount']);?></span> ���.</dd>
						</dl>
						<div class="buyLinkWrap">
							<a data-product_id="<?=$product['id'];?>" data-product_price="<?=$product['price_after_discount']?>" href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart');?>" class="buyLink">� �������</a>
						</div>
						<ul class="notes">
							<? if ((isset($product['fake_in_stock']) && ($product['fake_in_stock'] == 1)) || 								   (isset($product['rests_main']['summ']) && $product['rests_main']['summ'] > 0)) { ?>
								<li class="instock">������ ����� ���� � �������</li>
							<? } else { ?>
								<li class="onrequest">����� ��� �����</li>
							<? } ?>
							<?
							if ( false !== $product['deliveryPrice'] && "������" !== $GRM_UserRealData['user_city'] && "�����-���������" !== $GRM_UserRealData['user_city']) {
								$deliveryText = $product['deliveryPrice'] > 0 ? $product['deliveryPrice'] . ' ���.' : '���������';
							}elseif("������" == $GRM_UserRealData['user_city'] || "�����-���������" === $GRM_UserRealData['user_city']){
								if($product['price'] > 1000){
									$deliveryText = "���������";
								}else{
									$deliveryText = "300 ���.";
								}
							}else {
								$deliveryText = '�� 300 ���';
							}
							?>

							<?if($GRM_UserRealData['user_region']=='������' || $GRM_UserRealData['user_region']=='���������� �������'){?>
								<li class="delivery">�������� �� ������: <span><?=$deliveryText?></span></li>
								<?if($product['price_after_discount']>=1000){?><li class="delivery">�������� �� ������ ����������: <span><?if ($product['price_after_discount']>=3000){echo '���������';}elseif($product['price_after_discount']>=1000){echo '<br>','�� 150 ���.';}?> (<a href="<?=$link_to_ankor;?>">������</a>)</span></li><?}?>
							<?}elseif($GRM_UserRealData['user_region']=='�����-���������' || $GRM_UserRealData['user_region']=='������������� �������'){?>
								<li class="delivery">�������� �� �����-����������: <span><?=$deliveryText?></span></li>
								<?if($product['price_after_discount']>=1000){?><li class="delivery">�������� �� ������ ����������: <span><?if ($product['price_after_discount']>=3000){echo '���������';}elseif($product['price_after_discount']>=1000){echo '<br>','100 ���.';}?> (<a href="<?=$link_to_ankor;?>">������</a>)</span></li><?}?>
								
							<?}?>
							<?php if("������" == $GRM_UserRealData['user_city'] || "�����-���������" === $GRM_UserRealData['user_city']){?>
								<li class="delivery">
									C��� ��������: 1 - 2 ���
								</li>
							<?php } ?>
                            <?php if (isset($qiwi_price) && ($qiwi_price > 0) && ($product['cat'] != 1) && ($product['cat'] != 5)) { ?>
                            <?php //if (isset($qiwi_price) && ($qiwi_price > 0)) { ?>
                            	<!-- <li class="qiwipost">�������� QIWI Post � <strong style="color:#D92C61;"><?=$GRM_UserRealData['user_city']?></strong>: <span><?=$qiwi_price . ' ���.';?></span></li>  -->
                            <?php } ?> 
                            <li class="delivery" style="background: url(/img/visa.gif) no-repeat 6px 17px; padding-left: 50px;">�������� ������ ���������� ������ <span>Visa</span> ��� <span>MasterCard</span></li>

						</ul>
						<!--
						<div class="timer">
							<div class="caption">�� ����� �����:</div>
							<ul>
								<li>
									<span class="num">5</span> ����
								</li>
								<li>
									<span class="num">18</span> �����
								</li>
								<li>
									<span class="num">58</span> �����
								</li>
								<li>
									<span class="num">02</span> ������
								</li>
							</ul>
						</div>
						-->
						<div class="oneClickBuyWrap">
							<a href="#" data-product_id="<?=$product['id'];?>" data-name="<?=schars($product['title']);?>" data-price="<?=$product['price_after_discount'];?>" class="oneClickBuy" onclick="yaCounter21819568.reachGoal('FAST_ORDER'); return true;">������ � ���� ����</a>
						</div>
					</div>
					<div class="productNav">
					<?php $linkpage = getTemplateLink($product['around']['next'], 'catalog'); ?>
							<?php if ($linkpage != '/catalog/.html') { ?>
						<div class="simpleNav">
							<div class="prevProduct"><div class="prevProductIn">
								<a href="<?=getTemplateLink($product['around']['prev'], 'catalog');?>" class="prevProductLink">�������� ���������� �����</a>
									<div class="productPreview">
											<div class="pic"><img src="<?=getImageWebPath('product_small').$product['around']['prev']['id'];?>.jpg" alt="<?=$product['around']['prev']['title']?>"></div>
											<div class="caption"><?=$product['around']['prev']['title']?></div>
									</div>
								</a>
							</div></div>
							<div class="nextProduct"><div class="nextProductIn">
								<a href="<?=getTemplateLink($product['around']['next'], 'catalog');?>" class="nextProductLink">�������� ��������� �����</a>
									<div class="productPreview">
											<div class="pic"><img src="<?=getImageWebPath('product_small').$product['around']['next']['id'];?>.jpg" alt="<?=$product['around']['next']['title']?>"></div>
											<div class="caption"><?=$product['around']['next']['title']?></div>
									</div>
								</a>
							</div></div>
						</div>
						<? } ?>
						<? if (count($data['tags'])) { ?>
						<div class="rubrics">
							<div class="caption"><a href="<?=getTemplateLink($data['catInfo'], 'cat')?>"><?=$data['catInfo']['title']?></a></div>
							<ul>
								<? foreach ($data['tags'] as $tag) { ?>
									<li><a href="<?=getTemplateLink($tag, 'tags');?>"><?=$tag['title']?></a></li>
								<? } ?>
							</ul>
						</div>
						<? } ?>
					</div>
					<? if ($product['sale'] == 1) { ?>
						<div class="action">�����!</div>
					<? } elseif ($product['best'] == 1) { ?>
						<div class="hit">��� ������!</div>
					<? } ?>
				</div>
				<div id="tabs">
					<ul class="tabs">
						<li class="ui-tabs-active"><a href="#productDescription">��������</a></li>
						<? if (!empty($product['like_desc'])) { ?>
							<li><a href="#productVideo"><img src="/theme/i/video-tab.png" alt="����������">����������</a></li>
						<?}?>
						<li><a href="#productFeedback">������ (<?=count($data['feedback'])?>)</a></li>
						<?php if (!empty($product['instruction'])) {?>
                            <li><a href="#productInstruction">����������</a></li>
                        <?php } ?>
                        <? /* <li><a href="#qiwiMAP">��������� QIWI Post</a></li> */ ?>
					</ul>
					<div id="productDescription">
						<h2>��������:</h2>
						<?=$product['des'];?>
                                                <? if ( isset($product['height']) && !empty($product['height']) &&
                                                        isset($product['width']) && !empty($product['width']) &&
                                                        isset($product['length']) && !empty($product['length'])
                                                        ) {
                                                ?>
                                                <span>���: <?=getQiwiSize($product['height'], $product['length'], $product['width'])?></span>
                                                <? } ?>
					</div>
					<div id="productVideo" class="ui-tabs-hide">
						<?=$product['like_desc'];?>
					</div>
					<div id="productFeedback" class="ui-tabs-hide">
						<div>
							<div class="add-feed-button" data-product_id="<?=$product['id']?>"><a href="javascript:void(0);">�������� �����</a></div>
							<?php if (count($data['feedback']) > 0) { ?>
								<?php foreach ($data['feedback'] as $feed) { ?>
									<div class="feed">
										<div class="caption"><span class="name"><?=$feed['name'];?></span>&nbsp;&nbsp;<span class="date"><?=date('d.m.Y H:i', $feed['date'])?></span></div>
										<div class="comment"><?=$feed['comment']?></div>
										<?php if (!empty($feed['answer'])) { ?>
											<div class="answer">
												<div class="name">�����:</div>
												<div><?=$feed['answer']?></div>
											</div>
										<?php } ?>
									</div>
								<?php } ?>
							<?php } else { ?>
								<div>������� ���� ���, ������� ������.</div>
							<?php } ?>
						</div>
					</div>
                    <?php if (!empty($product['instruction'])) {?>
                    <div id="productInstruction" class="ui-tabs-hide">
                        <a href="http://beurer.tv/upload/files/<?=$product['instruction']?>" download><?=$product['instruction']?></a>
                    </div>
                    <?php } ?>

                    <?php if ($showMap) { /* ?>
                    <center>
                    	<div id="qiwiMAP">
                    		<div id="first_map" style="width:600px; height:500px;">
                    			
                    		</div>
                    	</div>
                    </center>
                    <? php */ }?>
				</div>
				
				<?$currProd = $product;?>
				<noindex>
					<h2>������������ ������������:</h2>
					<ul class="catalogue">
						<? foreach ($currProd['recomm'] as $product) {
							include('element_product_list.tpl.php');
						} ?>
					</ul>
				</noindex>
				
				<?php if (!$currProd['near'] == 0) { ?> <!--���� ��� ������� �������, �� �� �������� -->
				<h2>������� ������:</h2>
				<ul class="catalogue">
					<? foreach ($currProd['near'] as $product) {
						include('element_product_list.tpl.php');
					} ?>
				</ul>
				<?php } ?>
			</section>

                        <script> 
                            $(document).ready(function(){
                                var itemTitle = $('.info h1').html();
                                $("a.fgallery img").attr("alt", itemTitle);
                            });    
                        </script>