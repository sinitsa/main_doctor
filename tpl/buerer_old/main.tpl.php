<noindex>

	<div class="intro">
		<div class="introIn">
			<div class="introIn">
				<?
				$slideProducts = getProducts(false, array('novinka' => 1), false, array('page' => 1, 'onPage' => 'all'));
				$slideProducts = $slideProducts['products'];
				$slidesCount = count($slideProducts);
				?>
				<ul class="productsSlider">
					<? foreach ($slideProducts as $p) { ?>
					<li>
						<div class="left">
							<div class="photo"><a href="<?=getTemplateLink($p, 'catalog');?>"><img src="<?=getImageWebPath('product_preview').$p['id']?>.jpg" alt="<?=$p['title'];?>"></a></div>
							<ul class="tips">
								<li>
									<? if ($p['rests_main']['summ'] > 1) { ?>
										<img src="/theme/i/instock.png" alt="���� � �������">
									<? } else { ?>
										<img src="/theme/i/onrequest.png" alt="�� �����">
									<? } ?>
								</li>
								<? if(!empty($p['like_desc'])) {?>
								<li>
									<img src="/theme/i/video.png" alt="����������">
								</li>
								<? } ?>
								<li>
                                                                <? if (isset($p['deliveryPrice']) && ($p['deliveryPrice'] === 0)) { ?>
                                                                        <img src="/theme/i/delivery.png" alt="���������� �������� �� ������" />
                                                                <? } ?>
								</li>
							</ul>
						</div>
						<div class="right">
							<div class="description">
								<div class="caption"><?=$p['title']?></div>
								<noindex>
								<div>
								<?=preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", $p['des']);?>
								</div>
								</noindex>
							</div>
							<div class="meta">
								<div class="descriptionOverlay">&nbsp;</div>
								<div class="buyLinkWrap">
									<a data-product_id="<?=$p['id']?>" data-product_price="<?=$p['price_after_discount']?>"  href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart');?>" class="buyLink">� �������</a>
								</div>
								<? if ($p['discount_value'] > 0 ) { ?>
								<ul class="priceHistory">
									<li>
										<p>������ ����:</p>
										<div class="oldPrice"><span><?=moneyFormat($p['price'])?></span> ���.</div>
									</li>
									<li>
										<p>��������:</p>
										<div class="savings"><span><?=moneyFormat($p['discount_summ_value'])?></span> ���.</div>
									</li>
								</ul>
								<? } ?>
								<div class="price">����: <span><?=moneyFormat($p['price_after_discount'])?></span> ���.</div>
								<!--
								<div class="timer">
									<div class="caption">�� ����� �����:</div>
									<ul>
										<li>
											<span class="num">5</span> ����
										</li>
										<li>
											<span class="num">18</span> �����
										</li>
										<li>
											<span class="num">58</span> �����
										</li>
										<li>
											<span class="num">02</span> ������
										</li>
									</ul>
								</div>
								-->
							</div>
						</div>
						<? if ($p['sale']) { ?>
							<div class="action">�����!</div>
						<? } elseif ($p['best']) { ?>
							<div class="hit">��� ������!</div>
						<? } ?>
					</li>
					<? } ?>
				</ul>
				<div class="thumbs">
					<? foreach ($slideProducts as $p) { ?>
					<span>
						<table width="100%" height="100%"><td valign="center">
						<img src="<?=getImageWebPath('product_small').$p['id']?>.jpg" alt="<?=$p['title'];?>">
						</td></table>
					</span>
					<? } ?>
				</div>
			</div>
		</div>
	</div>
	</noindex>
			<nav class="catalogueNav">
				<ul>
					<? foreach ($menu as $item) { ?>
					<li>
						<a href="javascript:void(0);">
							<div class="icon"><img src="<?=getImageWebPath('menu').$item['id'];?>.jpg" alt="<?=$item['title'];?>"></div>
							<div class="caption"><?=$item['title'];?></div>
						</a>
						<ul>
							<? foreach ($item['submenu'] as $subitem) { ?>
								<li><a href="<?=getTemplateLink($subitem, 'cat')?>"><?=$subitem['title']?></a></li>
							<? } ?>
						</ul>
					</li>
					<? } ?>
				</ul>
			</nav>
			<section class="about">
				<h2><?php echo $staticPage['title']; ?></h2>
				<?php echo $staticPage['des']; ?>
			</section>