<section class="content">
  <?php
  echo $data['des'];
  if ("optom" === $data['chpu'] || (37 === (int)$data['id'] && "��������� " === $data['title'])) {
    ?>
    <style>
      input[type=text], textarea {
        border: 1px solid #d6d6d6;
        background: #fff;
        border-radius: 10px;
        padding: 6px;
        color: #929292;
        font: 12px Arial, sans-serif;
        margin: 0;
        outline: none;
        width: 100%;
      }
      .cl{clear: both; overflow: hidden; height: 0;}
      .send-button{
        margin-top: 10px;
        width: 154px;
        height: 39px;
        border: 0;
        font: bold 14px/39px Arial, sans-serif;
        color: #fff;
        cursor: pointer;
        border-radius: 15px;
        overflow: visible;
        background: url("/theme/i/popup-button.png") repeat-x 0 0;
        text-transform: uppercase;
        outline: none;
        box-shadow: 0 0 15px rgba(0,0,0,.3);
      }
    </style>
    <div class="last"></div>
    <form action="" method="post" class="forma">
      <div class="form-tpl-urname" style="float: left; width: 260px;">
        <div class="input-tpl mid" style="float: none;">
          <input placeholder="���� ���:" type="text" name="name" style="float: none;">
        </div>
      </div>

      <div class="form-tpl-urname" style="margin-left: 40px;width: 260px;float: left;">
        <div class="input-tpl mid" style="float: none;">
          <input name="email" type="text" placeholder="��� e-mail:" style="float: none;">
        </div>
      </div>

      <div class="form-tpl-urname" style="margin-left: 40px;width: 260px;float: left;">
        <div class="input-tpl mid" style="float: none;">
          <input class="tel-1" placeholder="��� �������:" name="phone" type="text" value="" style="float: none;">
        </div>
      </div>
      <div class="cl"></div>

      <div class="form-tpl-urname" style="clear: both;">
        <div class="input-tpl mid" style="margin-top: 10px;float: none;">
          <input  placeholder="���� �����������:" name="company" type="text"
                 style="float: none;width: 100%;">
        </div>
      </div>

      <div class="form-tpl-urname" style="clear: both;">
        <div class="input-tpl mid" style="margin-top: 10px;float: none;">
          <input name="company" type="text" placeholder="��������� ������:" style="float: none;width: 100%;">
        </div>
      </div>
      <div style="margin-top: 15px; margin-bottom: 5px; fort-size: 14px;">
        <div>������: </div>
        <label><input type="checkbox" name="nal" value="1"/>���������</label>
        <label><input type="checkbox" name="beznal" value="1"/>������������</label>
      </div>
      <div class="form-tpl-msg" style="clear:both;">
        <div class="textarea-tpl">
          <textarea placeholder="���� ���������:" name="comment" cols="30" rows="10" value=""></textarea>
        </div>
      </div>
      <button type="submit" class="send-button" onclick="yaCounter21819568.reachGoal('OPTOM'); return true;">��������</button>
    </form>
    <script>
      (function (e) {
        function t() {
          var e = document.createElement("input"), t = "onpaste";
          return e.setAttribute(t, ""), "function" == typeof e[t] ? "paste" : "input"
        }

        var n, a = t() + ".mask", r = navigator.userAgent, i = /iphone/i.test(r), o = /android/i.test(r);
        e.mask = {
          definitions: {9: "[0-9]", a: "[A-Za-z]", "*": "[A-Za-z0-9]"},
          dataName: "rawMaskFn",
          placeholder: "_"
        }, e.fn.extend({
          caret: function (e, t) {
            var n;
            if (0 !== this.length && !this.is(":hidden"))return"number" == typeof e ? (t = "number" == typeof t ? t : e, this.each(function () {
              this.setSelectionRange ? this.setSelectionRange(e, t) : this.createTextRange && (n = this.createTextRange(), n.collapse(!0), n.moveEnd("character", t), n.moveStart("character", e), n.select())
            })) : (this[0].setSelectionRange ? (e = this[0].selectionStart, t = this[0].selectionEnd) : document.selection && document.selection.createRange && (n = document.selection.createRange(), e = 0 - n.duplicate().moveStart("character", -1e5), t = e + n.text.length), {
              begin: e,
              end: t
            })
          }, unmask: function () {
            return this.trigger("unmask")
          }, mask: function (t, r) {
            var c, l, s, u, f, h;
            return!t && this.length > 0 ? (c = e(this[0]), c.data(e.mask.dataName)()) : (r = e.extend({
              placeholder: e.mask.placeholder,
              completed: null
            }, r), l = e.mask.definitions, s = [], u = h = t.length, f = null, e.each(t.split(""), function (e, t) {
              "?" == t ? (h--, u = e) : l[t] ? (s.push(RegExp(l[t])), null === f && (f = s.length - 1)) : s.push(null)
            }), this.trigger("unmask").each(function () {
              function c(e) {
                for (; h > ++e && !s[e];);
                return e
              }

              function d(e) {
                for (; --e >= 0 && !s[e];);
                return e
              }

              function m(e, t) {
                var n, a;
                if (!(0 > e)) {
                  for (n = e, a = c(t); h > n; n++)if (s[n]) {
                    if (!(h > a && s[n].test(R[a])))break;
                    R[n] = R[a], R[a] = r.placeholder, a = c(a)
                  }
                  b(), x.caret(Math.max(f, e))
                }
              }

              function p(e) {
                var t, n, a, i;
                for (t = e, n = r.placeholder; h > t; t++)if (s[t]) {
                  if (a = c(t), i = R[t], R[t] = n, !(h > a && s[a].test(i)))break;
                  n = i
                }
              }

              function g(e) {
                var t, n, a, r = e.which;
                8 === r || 46 === r || i && 127 === r ? (t = x.caret(), n = t.begin, a = t.end, 0 === a - n && (n = 46 !== r ? d(n) : a = c(n - 1), a = 46 === r ? c(a) : a), k(n, a), m(n, a - 1), e.preventDefault()) : 27 == r && (x.val(S), x.caret(0, y()), e.preventDefault())
              }

              function v(t) {
                var n, a, i, l = t.which, u = x.caret();
                t.ctrlKey || t.altKey || t.metaKey || 32 > l || l && (0 !== u.end - u.begin && (k(u.begin, u.end), m(u.begin, u.end - 1)), n = c(u.begin - 1), h > n && (a = String.fromCharCode(l), s[n].test(a) && (p(n), R[n] = a, b(), i = c(n), o ? setTimeout(e.proxy(e.fn.caret, x, i), 0) : x.caret(i), r.completed && i >= h && r.completed.call(x))), t.preventDefault())
              }

              function k(e, t) {
                var n;
                for (n = e; t > n && h > n; n++)s[n] && (R[n] = r.placeholder)
              }

              function b() {
                x.val(R.join(""))
              }

              function y(e) {
                var t, n, a = x.val(), i = -1;
                for (t = 0, pos = 0; h > t; t++)if (s[t]) {
                  for (R[t] = r.placeholder; pos++ < a.length;)if (n = a.charAt(pos - 1), s[t].test(n)) {
                    R[t] = n, i = t;
                    break
                  }
                  if (pos > a.length)break
                } else R[t] === a.charAt(pos) && t !== u && (pos++, i = t);
                return e ? b() : u > i + 1 ? (x.val(""), k(0, h)) : (b(), x.val(x.val().substring(0, i + 1))), u ? t : f
              }

              var x = e(this), R = e.map(t.split(""), function (e) {
                return"?" != e ? l[e] ? r.placeholder : e : void 0
              }), S = x.val();
              x.data(e.mask.dataName, function () {
                return e.map(R, function (e, t) {
                  return s[t] && e != r.placeholder ? e : null
                }).join("")
              }), x.attr("readonly") || x.one("unmask", function () {
                x.unbind(".mask").removeData(e.mask.dataName)
              }).bind("focus.mask", function () {
                clearTimeout(n);
                var e;
                S = x.val(), e = y(), n = setTimeout(function () {
                  b(), e == t.length ? x.caret(0, e) : x.caret(e)
                }, 10)
              }).bind("blur.mask", function () {
                y(), x.val() != S && x.change()
              }).bind("keydown.mask", g).bind("keypress.mask", v).bind(a, function () {
                setTimeout(function () {
                  var e = y(!0);
                  x.caret(e), r.completed && e == x.val().length && r.completed.call(x)
                }, 0)
              }), y()
            }))
          }
        })
      })(jQuery);
      $(function () {
        $(".tel-1").mask("9(999)999 9999", {placeholder: " "});
        $(".send-button").on("click", function(e){
          var form = $(".forma").serialize();
          var f = $(".forma");
          var text = '';
          f.find("input[type=text]").each(function(key, value){
            if($(value).val() === "" || !$(value).val()){
              text += "��������� ���� - " + $(value).attr("placeholder") + "\n";
            }
          });
          if(!f.find("textarea").val() || f.find("textarea").val() === ""){
            text += "��������� ���� - ���� ���������: ";
          }
          if(text){
            alert(text);
            return false;
            e.preventDefault();
          }
          $.ajax({
            url: "/ajax/opt.php",
            type: "POST",
            data: form,
            success: function(data){
              $(".forma").remove();
              $(".last").html("���� ��������� ����������. <br /> <a href=\"/\">�� �������</a>");
            }
          });
          return false;
        })
      });
    </script>
  <?php
  }
  ?>
</section>