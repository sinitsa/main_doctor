<?php 
$discount = '';
if ($product['discount_value'] > 0) {
	if ($product['discount_type'] == 'percent') {
		$discount = '-' . $product['discount_value'] . '%';
	} elseif ($product['discount_type'] == 'value') {
		$discount = '-' . getPercentForDiscount($product['price'], $product['discount_value']) . '%';
	}
}
$hadOldPrice = $product['discount_value'] > 0;
$product['deliveryPrice'] = deliveryPrice::getByCatAndPrice($product['cat'], $product['price_after_discount'], $product['id']);
?>					<li> <!-- <? //print_r($product); ?> -->
						<div class="item">
							<div class="itemIn">
								<!--
								<div class="timer">
									<ul>
										<li>
											<span class="num">5</span> ����
										</li>
										<li>
											<span class="num">18</span> �����
										</li>
										<li>
											<span class="num">58</span> �����
										</li>
										<li>
											<span class="num">02</span> ������
										</li>
									</ul>
								</div>
								-->
								<a href="<?=getTemplateLink($product, 'catalog');?>">
									<div class="pic">
										<table style="width: 100%; height:<?=($hadOldPrice?183:233)?>px;"><tr><td valign="center">
										<img src="<?=getImageWebPath('product_medium').$product['id']; ?>.jpg" alt="<?=$product['title'];?>"></a>
										</td></tr></table>
									</div>
									<div class="caption"><?=$product['title'];?></div>
								</a>
								<div class="pricebox">
									<? if ($product['discount_value'] > 0 ) { ?>
									<dl class="oldprice">
										<dt>������ ����:</dt>
										<dd><span><?=moneyFormat($product['price'])?></span> ���.</dd>
									</dl>
									<dl class="savings">
										<dt>��������:</dt>
										<dd><span><?=moneyFormat($product['discount_summ_value'])?></span> ���.</dd>
									</dl>
									<? } ?>
									<dl class="price">
										<dt>����:</dt>
										<dd><span><?=(moneyFormat($product['price_after_discount']))?></span> ���.</dd>
									</dl>
								</div>
								<div class="buyLinkWrap">
									<a data-product_id="<?=$product['id']?>" data-product_price="<?=$product['price_after_discount']?>" href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart');?>" class="buyLink">� �������</a>
								</div>
								<ul class="tips">
									<li>
										<? if ((isset($product['fake_in_stock']) && ($product['fake_in_stock'] == 1)) || ( $product['rest_main'] > 0) || ( $product['rest_extra'] > 0)) { ?>
											<img src="/theme/i/instock-small.png" alt="���� � �������" />
										<? } else { ?>
											<img src="/theme/i/onrequest-small.png" alt="�� �����" />
										<? } ?>
									</li>
									 <!--    �������� �����   !--> 
									<? if (!empty($product['like_desc'])) { ?>
									<li>
										<img src="/theme/i/video-small.png" alt="����������">
									</li>
									<? } ?>
									<? if ($product['deliveryPrice'] == 0) { ?>
									<li>
										<img src="/theme/i/delivery-small.png" alt="���������� �������� �� ������">
									</li>
									<? } ?>
								</ul>
								<? if ($product['sale'] == 1) { ?>
									<div class="action">�����!</div>
								<? } elseif ($product['best'] == 1) { ?>
									<div class="hit">��� ������!</div>
								<? } ?>
							</div>
							<div class="description">
								<?=preg_replace('/<a href=\"(.*?)\">(.*?)<\/a>/', "\\2", getShortDesc($product, SHORT_IF_EXISTS_ELSE_CROP, 300))?>
							</div>
						</div>
					</li>