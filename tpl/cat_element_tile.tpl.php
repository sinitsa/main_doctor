<div class="tile-block">
<?php
    $n = $h = array();
    $n = null; // new
     $h = null; // hit
    $i = 0;
    $t = false;
    $list_products = array();

    foreach ($data['products'] as $key => $product) {
       
        if ( $product['best'] && $i < 2 ) {
            $t = 'hit';
            $i++;
            $h = $product['id'];
            include($tpl_path.'cat_element_wide.tpl.php');
            $list_products[] = $product;
            continue;
        }
        if ( $product['novinka'] && $i < 2 ) {
            $t = 'new';
            $i++;
            $n = $product['id'];
            include($tpl_path.'cat_element_wide.tpl.php');
            $list_products[] = $product;
            continue;
        }
        if ($i > 1) {break;}
    }

    $z = 1;
    foreach ($data['products'] as $product) {
        if ( $product['id'] != $n && $product['id'] != $h ) {
        if ($i < 2) {
                $i++;
                $t = false;
                include($tpl_path.'cat_element_wide.tpl.php');
                $list_products[] = $product;
                continue;
        }
        
?>              
                <div class="cc-el tile-view <?=(($z) % 3 == 0) ? 'cc-mrzero' : '';?>">
                    <div class="carousel-element">
                        <div class="carousel-product">
                            <a class="cc_4" href="javascript:void(0);" data-href="<?=getTemplateLink($product, 'catalog');?>">
                                                            <div class="cp-slide">
                                <img class="carousel-product-img" data-lazy="<?=getImageWebPath('product_medium',false).$product['id']?>.jpg" alt="<?=$product['title']?>">
                            </div>
<?php 

            $extra_photo = getProductExtraPhoto($product['id']);
            foreach ($extra_photo as $p_id) {
?>
                           <div class="cp-slide">
                                <img class="carousel-product-img" data-lazy="<?=getImageWebPath('product_extra_medium',false).$p_id?>.jpg" alt="<?=$product['title']?>">
                           </div> 
<?php
            }
?>
                            </a>
                            <div class="promo">
                            </div>
                            <a class="quick-look-a" href="javascript:void(0);">
                                <div class="quick-look-button">
                                    <span>������� ��������</span>
                                    <form id="quick-view_<?=$product['id']?>" action="/ajax/products.php?method=quick-view" method="post">
                                        <input type="hidden" value="<?=$product['id']?>" name="product_id">
                                    </form>
                                </div>
                            </a>
                            <div class="carousel-product-name">
                                <a href="<?=getTemplateLink($product, 'catalog');?>"><?=$product['title']?></a>
                            </div>
                            <div class="carousel-element-price"><?=$product['price_after_discount'] ? $product['price_after_discount'] : $product['price']?> ���.</div>
                            <div class="carousel-other">
                                <div class="active" data-tab="tab1"></div>
                                            <div data-tab="tab2"></div>
                                            <div data-tab="tab3">
                                                <span class="htl-amount"><?=count(getProductFeedback($product['id'], true))?></span>
                                            </div>
                                            <div data-tab="tab4"></div>
                            </div>
                            <div class="element-hover">
                                <div class="eh-fast-order">
                                    <a class="de-last-incart buyLink" data-product_id="<?=$product['id'];?>" data-product_price="<?=$product['price_after_discount'] ? $product['price_after_discount'] : $product['price']?>" href="<?=getTemplateLink(array('chpu'=>'korzina'), 'cart');?>"></a>
                                    <a  data-product_id="<?=$product['id'];?>" href="javascript:void(0);" class="one-click-order header-button-top">������� �����</a>
                                </div>

<?php
    if ( !in_array($product['id'], $compare_products)) {
?>
                        <div class="eh-compare compare-<?=$product['id']?>">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-compare done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">���������</a>
<?php
    }
?>
                        </div> 
<?php
    if ( !in_array($product['id'], $like_products)) {
?>
                        <div class="eh-like like-<?=$product['id']?>">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-like done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>">���������</a>
<?php
    }
?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
<?php
            $list_products[] = $product;
            $z++;
        }
    }
?>
</div>