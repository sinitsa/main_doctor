<div id="product-quick-view" class="zoom-anim-dialog ">
    <div id="pqv-prev" data-product_id="<?=$product['around']['prev']['id']?>"></div>
    <div id="pqv-next" data-product_id="<?=$product['around']['next']['id']?>"></div>
    <div style="overflow:hidden;">
        <div>
            <img style="position:absolute;right:10px;top:10px;" src="/theme/img/content/delete_img.png" alt="delete">
            <div class="pqv-left">
                <div class="po-main-slider2">
                        <div class="cp-slide">
                            <img class="carousel-product-img" src="<?=getImageWebPath('product_preview',false).$product['id']?>.jpg" alt="<?=$product['title']?>">
                        </div>
<?php 
        foreach ($product['extra_photo'] as $p_id) {
?>
                       <div class="cp-slide">
                            <img class="carousel-product-img" src="<?=getImageWebPath('product_extra_preview',false).$p_id?>.jpg" alt="<?=$product['title']?>">
                       </div> 
<?php
        }
?>
                </div>
                <div class="po-slider-for-main2">
                        <div class="cp-slide">
                            <img class="carousel-product-img" src="<?=getImageWebPath('product_little',false).$product['id']?>.jpg" alt="<?=$product['title']?>">
                        </div>
<?php 
       // $extra_photo = getProductExtraPhoto($product['id']);
        
       // foreach ($extra_photo as $p_id) {
          foreach ($product['extra_photo'] as $p_id) {
?>
                       <div class="cp-slide">
                            <img class="carousel-product-img" src="<?=getImageWebPath('product_extra_little',false).$p_id?>.jpg" alt="<?=$product['title']?>">
                       </div> 
<?php
        }
?>
                </div>
            </div>
            <div class="pqv-right"> 
                <div class="product_title"><?=$product['title']?></div>
<?php
     if ((isset($product['fake_in_stock']) && ($product['fake_in_stock'] == 1)) || 
    (isset($product['rests_main']['summ']) && $product['rests_main']['summ'] > 0)) { 
?>
                    <div class="po-instock pqv-instock">� �������</div>
<?php
    } else {
?>
                    <div class="po-instock pqv-instock">�� �����</div>
<?php
    }
?>
                <div class="pqv-delivery-pay">
                    <div><a href="<?=getTemplateLink(array('chpu'=>'dostavka'), 'page');?>">��������</a>
                    </div>
                    <div><a href="<?=getTemplateLink(array('chpu'=>'oplata'), 'page');?>">������</a>
                    </div>
                </div>
                <div class="pqv-order">
                    <div class="carousel-element-price"><?=$product['price_after_discount']?> ���.</div>
                    <div class="eh-fast-order">
                        <a class="de-last-incart buyLink" data-product_id="<?=$product['id'];?>" data-product_price="<?=$product['price_after_discount'] ? $product['price_after_discount'] : $product['price']?>" href="<?=getTemplateLink(array('chpu'=>'finish'), 'cart');?>"></a>
                                <a  data-product_id="<?=$product['id'];?>" href="javascript:void(0);" class="one-click-order header-button-top">������� �����</a>
                    </div>
                </div>
                <div class="pqv-like-compare">
<?php
    if ( !in_array($product['id'], $compare_products)) {
?>
                        <div class="eh-compare">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-compare done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'sravnenie'), 'compare');?>">���������</a>
<?php
    }
?>
                        </div>  
<?php
    if ( !in_array($product['id'], $like_products)) {
?>
                        <div class="eh-like">
                        <a data-href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>" href="javascript:void(0);">� ���������</a>
<?php
    } else {
?>
                        <div class="eh-like done">
                        ����� � <a href="<?=getTemplateLink(array('chpu'=>'izbrannoe'), 'favorites');?>">���������</a>
<?php
    }
    $feedbacks = getProductFeedback($product['id'],false);
?>
                    </div>
                </div>
                <div class="carousel-other">
                    <div data-tab="tab1"></div>
                    <div class="active" data-tab="tab2"></div>
                    <div data-tab="tab3">
                        <span class="htl-amount"><?=count($feedbacks)?></span>
                    </div>
                    <div data-tab="tab4"></div>
                </div>
                <div class="pqv-text pqv-description hidden tab1">
                    <?=$product['short_des']?>
                </div>
                <div class="pqv-text hidden tab2 active">
                    <ul>
<?php
$i=0;
$tech_des = array();
foreach ($product['params'] as $param) {
    if ($param['title'] != '��������' && $param['title'] != '������ �������������') {
        
    if ($i < 4) {                              
?>
                            <li><?=$param['title'].": ".$param['values'][0]['value']?></li>                
<?php
        $tech_des[] = array('title' => $param['title'], 'value' => $param['values'][0]['value']);
    }
       $i++;
    } else { 
        if($param['title'] == '������ �������������'){
            $c = 'hidden';
            if($param['values'][0]['value'] == "��������") { $c = 'germany';}
            elseif($param['values'][0]['value'] == "�����") { $c = 'china';}
                elseif($param['values'][0]['value'] == "�������") { $c = 'taivan';}
                elseif($param['values'][0]['value'] == "���") { $c = 'usa';}
                elseif($param['values'][0]['value'] == "������") { $c = 'russia';}
                $descr['manufacturer'] = $param['values'][0]['value'];
        }else{
            $descr['warranty'] = $param['values'][0]['value'];
        }
    }   
}
?>
                    </ul>
                    <ul class="pod-right">
						<?
							if ($descr['manufacturer'] != null) echo ("<li class='manufacturerLi'><span class='flag ".$c."'></span>������ �������������:</br>".$descr['manufacturer']."</li>");
							if ($descr['warranty'] != null) echo ("<li class='warrantyLi'>��������: ".$descr['warranty']."</li>");
						?>
                    </ul>
                </div>
                <div class="pqv-text hidden tab3">
<?php
    $last_feedback = end($feedbacks);
    $short_f = rtrim(substr($last_feedback['comment'], 0, 170), "!,.-");
   
    if($feedbacks) {
        echo $short_f." ...<br><span>".$last_feedback['name']." ".date('d.m.Y', $last_feedback['date'])."</span>";
    } else {
        echo "���� ����� �� ������� ����� � ����� ������.<br>";
    }
?>
                </div>
                <div class="pqv-text po-video hidden tab4"> 
<?php
    // Video from like_desc field
    $youtube = strpos($product['like_desc'], '//www.youtube') ? trim(strip_tags($product['like_desc'])) : false;
   
    if(!$youtube) {
     $youtube = 'javascript:void(0)';
     $y_text = 'none';
     $y_id = false;
?>
                    � ����� ������ ��� �����.
<?php
    } else {
     $y_id = explode('v=', $youtube);
     $y_id = $y_id[1];
     $y_text = 'youtube';
?>
                    <iframe width="100%" height="100%"  src="<?=$youtube?>" frameborder="0" allowfullscreen></iframe>
<?php
    }
?> 
               
                </div>
                <div style="clear:left"></div>
                <div class="pqv-moreinfo">  
                    <a href="<?=getTemplateLink($product, 'catalog');?>" class="header-button-top">������ ���������� � ������</a>
                </div> 
            </div>
        </div>
    </div>
</div> 
<!-- /product -->