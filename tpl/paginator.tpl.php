<?php

$page_count = $data['page']['pagesCount'];
$current_page = $data['page']['currentPage'];
$products_count = $data['page']['p_count'] ? $data['page']['p_count'] : count($data['products']);
$onPage = $page['onPage'] ? $page['onPage'] : $data['page']['onPage'];
if ($qString == false) $qString = new queryString();
$onclick = isset($onclick) ? $onclick : false;
if ($page_count > 1) {

    
?>
<div class="f-pages page">
    <ul>
<?php
//if ($current_page != 1) {
?>
        <li class="page-prev" data-plus="-1">
            <a href="<?=isset($onclick) && $current_page != 1 ? $qString->setParam('page', $current_page - 1) : 'javascript:void(0)'?>" <?=(isset($onclick) && $current_page != 1 ? 'onclick="return '.str_replace('[page]', $current_page - 1, $onclick).'"' : '')?>>
                <img src="/theme/img/content/page_icon_prev.png" alt="">
            </a>
        </li>
<?php
//}
for ($i = 1; $i <= $page_count; $i++) {
    if (!isset($onclick) || !$onclick){
?>
        <li class="page-num page_<?=$i?> <?=$i == $current_page ? 'active' : '';?>" data-href="<?=getTemplateLink($data['catInfo'],'cat')?>" data-pages="<?=$data['page']['pagesCount']?>"><a data-uri="<?=$qString->setParam('page', $i)?>" href="javascript:void(0)"><?=$i?></a>
        </li>
<?php
    } else {
?>
        <li class="page-num page_<?=$i?> <?=$i == $current_page ? 'active' : '';?>"><a <?=($onclick ? 'onclick="return '.str_replace('[page]', $i, $onclick).'"' : '')?> href="<?=$qString->setParam('page', $i)?>"><?=$i?></a>
        </li>
<?php
    }
}
//if ($current_page != $page_count) {
?>
        <li class="page-next" data-plus="1">
            <a href="<?=$onclick && $current_page != $page_count ? $qString->setParam('page', $current_page + 1) : 'javascript:void(0)'?>" <?=($onclick && $current_page != $page_count ? 'onclick="return '.str_replace('[page]', $current_page + 1, $onclick).'"' : '')?>>
                <img src="/theme/img/content/page_icon_next.png" alt="">
            </a>
        </li>
<?php
//}
?>
    </ul>
</div>
<?php

} //d($page_count,0); d($onPage,0); d($products_count,0);
if ($products_count > 20) {
?>
<div class="f-pages count <?=$tab == 'search' ? 'wide' : ''?>" <?=$page_count <= 1 ? 'style="margin-left:534px;"' : ''?>><span>��������:</span>
    <ul>
<?php
    if($products_count > 20) {
?>
        <li  class="<?=$onPage == 20 ? 'active': ''?>"><a data-uri="<?=$qString->removeParam('page')->setParam('onPage', 20)?>" href="<?=$onclick  ? $qString->setParam('onPage', 20) : 'javascript:void(0)'?>" <?=($onclick ? 'onclick="return '.str_replace('[page]', '1, null , 20', $onclick).'"' : '')?>>20</a>
        </li>
<?php
    }
    if($products_count > 40) {
        
?>
        <li class="<?=$onPage == 40 ? 'active': ''?>"><a data-uri="<?=$qString->removeParam('page')->setParam('onPage', 40)?>" href="<?=$onclick  ? $qString->setParam('onPage', 40) : 'javascript:void(0)'?>" <?=($onclick ? 'onclick="return '.str_replace('[page]', '1, null , 40', $onclick).'"' : '')?>>40</a>
        </li>
<?php
    }
    if($products_count > 60) {
        
?>
        <li class="<?=$onPage == 60  ? 'active': ''?>"><a data-uri="<?=$qString->removeParam('page')->setParam('onPage', 60)?>" href="<?=$onclick  ? $qString->setParam('onPage', 60) : 'javascript:void(0)'?>" <?=($onclick ? 'onclick="return '.str_replace('[page]', '1, null , 60', $onclick).'"' : '')?>>60</a>
        </li>
<?php
    }
?>
        <li class="<?=$page_count == 0 ? 'active': ''?>"><a data-uri="<?=$qString->removeParam('page')->setParam('onPage', 'all')?>" href="<?=$onclick ? $qString->setParam('onPage', 'all') : 'javascript:void(0)'?>" <?=($onclick ? 'onclick="return '.str_replace('[page]', '1, null , all', $onclick).'"' : '')?>>���</a>
        </li>
    </ul>
</div> 
<?php
}
?>